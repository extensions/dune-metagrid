#ifndef DUNE_GRID_FILTEREDGRID_INDEXSET_HH
#define DUNE_GRID_FILTEREDGRID_INDEXSET_HH

//- C++ includes
#include <algorithm>
#include <cassert>
#include <limits>
#include <set>
#include <vector>

//- dune-common includes
#include <dune/common/typetraits.hh>

//- dune-geometry includes
#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/type.hh>
#include <dune/geometry/typeindex.hh>

//- dune-grid includes
#include <dune/grid/common/gridenums.hh>
#include <dune/grid/common/indexidset.hh>


namespace Dune
{

  // GlobalGeometryTypeMap
  // ---------------------

  template< int dim, class T >
  struct GlobalGeometryTypeMap
  {
    /** \brief dimension */
    static const int dimension = dim;

    /** \brief constructor */
    GlobalGeometryTypeMap ( const T &value = T() )
    {
      const size_t size = Dune::GlobalGeometryTypeIndex::size( dimension );
      data_.resize( size, value );
    }

    /** \brief return const reference to data */
    const T &operator[] ( const GeometryType &type ) const
    {
      const size_t index = Dune::GlobalGeometryTypeIndex::index( type );
      assert( index >= 0 && index < data_.size() );
      return data_[ index ];
    }

    /** \brief return const reference to data */
    T &operator[] ( const GeometryType &type )
    {
      const size_t index = Dune::GlobalGeometryTypeIndex::index( type );
      assert( index >= 0 && index < data_.size() );
      return data_[ index ];
    }

  private:
    std::vector< T > data_;
  };



  // FilteredIndexSet
  // ---------------

  template< PartitionIteratorType pitype, class Grid >
  class FilteredIndexSet
  {
    typedef FilteredIndexSet< pitype, Grid > This;

    typedef typename std::remove_const< Grid >::type::Traits Traits;

    template< class >
    friend class FilteredBoundarySegmentIndexSet;

  public:
    /** \brief host grid view type */
    typedef typename Traits::HostGridView HostGridView;
    /** \brief type of index set implementation */
    typedef typename HostGridView::IndexSet HostIndexSet;
    /** \brief index type */
    typedef unsigned int IndexType;

    typedef std::vector< GeometryType > Types;

  private:
    // grid dimension
    static const int dimension = Traits::dimension;
    // single coordinate type
    typedef typename Traits::ctype ctype;

  public:
    /** \brief constructor */
    template< class Predicate >
    FilteredIndexSet ( const HostGridView &hostGridView, const Predicate &predicate )
    : hostIndexSet_( hostGridView.indexSet() ),
      sizePerType_( IndexType( 0 ) )
    {
      // initialize indices
      for( int codim = 0; codim <= dimension; ++codim )
      {
        const auto& types = hostIndexSet().types( codim );
        typedef typename std::vector< GeometryType >::const_iterator TypeIterator;
        for( TypeIterator it = types.begin(); it != types.end(); ++it )
          indices_[ *it ].resize( hostIndexSet().size( *it ), invalidIndex() );
      }

      // temporary storage for geometry types
      std::fill( sizePerCodim_.begin(), sizePerCodim_.end(), IndexType( 0 ) );
      std::array< std::set< GeometryType >, dimension+1 > geomTypes;

      typedef typename HostGridView::template Codim< 0 >::template Partition< pitype >::Iterator Iterator;
      const Iterator end = hostGridView.template end< 0, pitype >();
      for( Iterator it = hostGridView.template begin< 0, pitype >(); it != end; ++it )
      {
        const typename Iterator::Entity &entity = *it;

        if( !predicate( entity ) )
          continue;

        // get reference element
        const auto& referenceElement
          = Dune::referenceElement< ctype, dimension >( entity.type() );

        // iterate over all sub entities
        for( int codim = 0; codim <= dimension; ++codim )
        {
          const int nSubEntities = referenceElement.size( codim );
          for( int i = 0; i < nSubEntities; ++i )
          {
            if( contains( entity, i, codim ) )
              continue;

            // add sub entity
            const GeometryType type = referenceElement.type( i, codim );
            const std::size_t hostIndex = hostIndexSet().subIndex( entity, i, codim );
            indices_[ type ][ hostIndex ] = sizePerType_[ type ]++;
            ++sizePerCodim_[ codim ];

            // make sure we know about the geometry type
            geomTypes[ codim ].insert( type );
          }
        }
      }

      // copy geometry types found in grid to vector
      for( int codim = 0; codim <= dimension; ++codim )
      {
        geomTypes_[ codim ].resize( geomTypes[ codim ].size() );
        std::copy( geomTypes[ codim ].begin(), geomTypes[ codim ].end(), geomTypes_[ codim ].begin() );
      }
    }

    /** \brief return size */
    IndexType size ( const GeometryType &type ) const
    {
      return sizePerType_[ type ];
    }

    /** \brief Return size. */
    IndexType size ( const int codim ) const
    {
      assert( (codim >= 0) && (codim <= dimension) );
      return sizePerCodim_[ codim ];
    }

    /** \brief return index */
    template< class Entity >
    IndexType index ( const Entity &entity ) const
    {
      return index< Entity::codimension >( entity );
    }

    /** \brief return index */
    template< int cd >
    IndexType index ( const typename Traits::template Codim< cd >::Entity &entity ) const
    {
      return index( entity.impl().hostEntity() );
    }

    template< int cd >
    IndexType index ( const typename Traits::HostGridView::template Codim< cd >::Entity &entity ) const
    {
      assert( contains( entity ) );
      const std::size_t hostIndex = hostIndexSet().index( entity );
      return indices_[ entity.type() ][ hostIndex ];
    }

    /** \brief return subindex */
    template< class Entity >
    IndexType subIndex ( const Entity &entity, int i, unsigned int codim ) const
    {
      return subIndex< Entity::codimension >( entity, i, codim );
    }

    /** \brief return subindex */
    template< int cd >
    IndexType subIndex ( const typename Traits::template Codim< cd >::Entity &entity, int i, unsigned int codim ) const
    {
      return subIndex( entity.impl().hostEntity(), i, codim );
    }

    template< int cd >
    IndexType subIndex ( const typename Traits::HostGridView::template Codim< cd >::Entity &entity, int i, unsigned int codim ) const
    {
      assert( contains( entity, i, codim ) );
      const std::size_t hostIndex = hostIndexSet().subIndex( entity, i, codim );
      // get reference element
      const auto& referenceElement = Dune::referenceElement< ctype, dimension >( entity.type() );
      return indices_[ referenceElement.type( i, codim ) ][ hostIndex ];
    }

    /** \brief return vector of all geometry types used in this grid */
    const std::vector< GeometryType > &geomTypes ( int codim ) const
    {
      assert( (codim >= 0) && (codim <= dimension) );
      return geomTypes_[ codim ];
    }

    /** \brief return vector of all geometry types used in this grid */
    const std::vector< GeometryType > &types ( int codim ) const
    {
      assert( (codim >= 0) && (codim <= dimension) );
      return geomTypes_[ codim ];
    }

    /** \brief return true, if entity is contained in this index set */
    template< class Entity >
    bool contains ( const Entity &entity ) const
    {
      return contains< Entity::codimension >( entity );
    }

    /** \brief return true, if entity is contained in this index set */
    template< class Entity >
    bool contains ( const Entity &entity, int i, unsigned int codim ) const
    {
      return contains< Entity::codimension >( entity, i, codim );
    }

    /** \brief return true, if entity is contained in this index set */
    template< int codim >
    bool contains ( const typename Traits::template Codim< codim >::Entity &entity ) const
    {
      return contains( entity.impl().hostEntity() );
    }

    /** \brief return true, if entity is contained in this index set */
    template< int cd >
    bool contains ( const typename Traits::template Codim< cd >::Entity &entity, int i, unsigned int codim ) const
    {
      return contains( entity.impl().hostEntity(), i, codim );
    }

    template< int codim >
    bool contains ( const typename Traits::HostGridView::template Codim< codim >::Entity &entity ) const
    {
      const std::size_t hostIndex = hostIndexSet().index( entity );
      return (indices_[ entity.type() ][ hostIndex ] != invalidIndex());
    }

    /** \brief return true, if entity is contained in this index set */
    template< int cd >
    bool contains ( const typename Traits::HostGridView::template Codim< cd >::Entity &entity, int i, unsigned int codim ) const
    {
      const std::size_t hostIndex = hostIndexSet().subIndex( entity, i, codim );
      // get reference element
      const auto& referenceElement = Dune::referenceElement< ctype, dimension >( entity.type() );
      return (indices_[ referenceElement.type( i, codim ) ][ hostIndex ] != invalidIndex());
    }

  private:
    // forbid copy constructor
    FilteredIndexSet ( const This & );
    // forbid assignmnet operator
    This &operator= ( const This & );

    // return  host index set
    const HostIndexSet &hostIndexSet () const
    {
      return hostIndexSet_;
    }

    // invalid value for an index
    static IndexType invalidIndex ()
    {
      return std::numeric_limits< IndexType >::max();
    }

    const HostIndexSet &hostIndexSet_;
    GlobalGeometryTypeMap< dimension, std::vector< IndexType > > indices_;
    std::array< IndexType, dimension+1 > sizePerCodim_;
    GlobalGeometryTypeMap< dimension, IndexType > sizePerType_;
    std::array< std::vector< GeometryType >, dimension+1 > geomTypes_;
  };

} // namespace Dune

#endif // #ifndef DUNE_GRID_FILTEREDGRID_INDEXSET_HH
