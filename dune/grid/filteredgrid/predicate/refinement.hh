#ifndef DUNE_GRID_FILTEREDGRID_PREDICATE_REFINEMENT_HH
#define DUNE_GRID_FILTEREDGRID_PREDICATE_REFINEMENT_HH

namespace Dune
{

  // FilteredGridRefinement
  // ----------------------

  struct FilteredGridRefinement
  {
    /** \brief return whether application of predicate to given grid view results in empty set */
    template< class GridView, class Predicate >
    static bool emptySet ( const GridView &gridView, const Predicate &predicate );

    /** \brief optimized global refine for filtered grid */
    template< class Grid, class Predicate >
    static void globalRefine ( Grid &grid, const Predicate &predicate, int refCount = 1 );

    /** \brief adapt grid to predicate */
    template< class Grid, class Predicate >
    static void adapt ( Grid &grid, const Predicate &predicate, int refCount = 1 );
  };



  // Implementation of FilteredGridRefinement::emptySet
  // --------------------------------------------------

  template< class GridView, class Predicate >
  inline bool FilteredGridRefinement::emptySet ( const GridView &gridView, const Predicate &predicate )
  {
    bool empty = true;

    typedef typename GridView::template Codim< 0 >::Iterator Iterator;
    const Iterator end = gridView.template end< 0 >();
    for( Iterator it = gridView.template begin< 0 >(); it != end; ++it )
    {
      if( predicate( *it ) )
      {
        empty = false;
        break;
      }
    }
    return empty;
  }


  // Implementation of FilteredGridRefinement::globalRefine
  // ------------------------------------------------------

  template< class Grid, class Predicate >
  inline void FilteredGridRefinement::globalRefine ( Grid &grid, const Predicate &predicate, int refCount )
  {
    typedef typename Grid::LeafGridView GridView;
    typedef typename GridView::template Codim< 0 >::Iterator Iterator;
    typedef typename Iterator::Entity Entity;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename IntersectionIterator::Intersection Intersection;

    // get leaf grid view
    GridView gridView = grid.leafGridView();

    int step = 0;

    // initial global refinement if necessary
    for( ; step < refCount && emptySet( gridView, predicate ); ++step )
      grid.globalRefine( 1 );

    for( ; step < refCount; ++step )
    {
      // first, refine grid
      grid.globalRefine( 1 );

      // now try to coarsen outside the filtered domain
      const Iterator end = gridView.template end< 0 >();
      for( Iterator it = gridView.template begin< 0 >(); it != end; ++it )
      {
        // get entity outside filtered domain
        const Entity &entity = *it;
        if( predicate( entity ) )
          continue;

        // check if none of the neighbors are inside filtered domain
        bool coarsen = true;
        const IntersectionIterator iend = gridView.iend( entity );
        for( IntersectionIterator iit = gridView.ibegin( entity ); iit != iend; ++iit )
        {
          const Intersection &intersection = *iit;
          if( intersection.neighbor() )
          {
            const Entity& neighbor = intersection.outside();
            if( predicate( neighbor ) )
            {
              coarsen = false;
              break;
            }
          }
        }

        // mark entity if possible
        if( coarsen )
          grid.mark( -1, entity );
      }

      // adapt grid
      grid.preAdapt();
      grid.adapt();
      grid.postAdapt();
    }
  }


  // Implementation of FilteredGridRefinement::adapt
  // -----------------------------------------------

  template< class Grid, class Predicate >
  inline void FilteredGridRefinement::adapt ( Grid &grid, const Predicate &predicate, int refCount )
  {
    typedef typename Grid::LeafGridView GridView;
    typedef typename GridView::template Codim< 0 >::Iterator Iterator;
    typedef typename Iterator::Entity Entity;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename IntersectionIterator::Intersection Intersection;

    bool globalRefine = true;

    GridView gridView = grid.leafGridView();
    const Iterator end = gridView.template end< 0 >();
    for( Iterator it = gridView.template begin< 0 >(); it != end; ++it )
    {
      const Entity &entity = *it;

      const IntersectionIterator iend = gridView.iend( entity );
      for( IntersectionIterator iit = gridView.ibegin( entity ); iit != iend; ++iit )
      {
        const Intersection &intersection = *iit;
        if( intersection.neighbor() )
        {
          const Entity& neighbor = intersection.outside();

          // mark entities at interface
          bool atInterface = ( predicate( entity ) && !predicate( neighbor ) );
          if( atInterface )
          {
            grid.mark( 1, entity );
            grid.mark( 1, neighbor );
            globalRefine = false;
          }
        }
      }
    }

    // refine grid
    if( globalRefine )
      grid.globalRefine( 1 );
    else
    {
      grid.preAdapt();
      grid.adapt();
      grid.postAdapt();
    }
  }

} // end namespace Dune

#endif // #ifndef DUNE_GRID_FILTEREDGRID_PREDICATE_REFINEMENT_HH
