#ifndef DUNE_GRID_FILTEREDGRID_HIERARCHICITERATOR_HH
#define DUNE_GRID_FILTEREDGRID_HIERARCHICITERATOR_HH

#include <dune/grid/common/entityiterator.hh>


namespace Dune
{

  // FilteredHierarchicIterator
  // --------------------------

  template< class Grid >
  class FilteredHierarchicIterator
  : public FilteredEntityIterator< Grid, typename std::remove_const< Grid >::type::Traits::HostGrid::HierarchicIterator >
  {
    typedef FilteredHierarchicIterator< Grid > This;
    typedef FilteredEntityIterator< Grid, typename std::remove_const< Grid >::type::Traits::HostGrid::HierarchicIterator > Base;

    typedef typename std::remove_const< Grid >::type::Traits::HostGrid::HierarchicIterator HostIterator;
    typedef typename std::remove_const< Grid >::type::Traits::ExtraData ExtraData;

  public:
    FilteredHierarchicIterator ( ExtraData data, const HostIterator &hostIterator )
    : Base( data, hostIterator, hostIterator )
    {}

    /** \brief increment */
    void increment ()
    {
      DUNE_THROW( InvalidStateException, "Trying to increment hierarchic iterator." );
    }
  };

} // namespace Dune

#endif // #ifndef DUNE_GRID_FILTEREDGRID_HIERARCHICITERATOR_HH
