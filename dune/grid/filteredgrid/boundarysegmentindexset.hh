#ifndef DUNE_GRID_FILTEREDGRID_BOUNDARYSEGMENTINDEX_SET_HH
#define DUNE_GRID_FILTEREDGRID_BOUNDARYSEGMENTINDEX_SET_HH

//- C++ includes
#include <cassert>
#include <limits>
#include <vector>

//- dune-common includes
#include <dune/common/typetraits.hh>

//- dune-geometry includes
#include <dune/geometry/referenceelements.hh>

//- local includes
#include <dune/grid/filteredgrid/capabilities.hh>

namespace Dune
{

  // FilteredBoundarySegmentIndexSet
  // -------------------------------

  template< class Grid >
  class FilteredBoundarySegmentIndexSet
  {
    typedef FilteredBoundarySegmentIndexSet< Grid > This;

    typedef typename std::remove_const< Grid >::type::Traits Traits;

    template< class > friend class FilteredIntersection;
    template< class > friend class FilteredIntersectionIterator;

  public:
    /** \brief type of filtered index set */
    typedef typename Traits::LeafIndexSet IndexSet;

    /** \brief index type */
    typedef unsigned int IndexType;

    /** \brief constructor */
    template< class HostGridView >
    FilteredBoundarySegmentIndexSet ( const HostGridView &hostGridView, const IndexSet &indexSet )
    : indexSet_( indexSet )
    {
      assert( Capabilities::hasSingleGeometryType< typename HostGridView::Grid >::v );
      const GeometryType gt( Capabilities::hasSingleGeometryType< typename HostGridView::Grid >::topologyId, HostGridView::dimension );

      interior_ = newInterior( gt );

      update( hostGridView );
    }

    ~FilteredBoundarySegmentIndexSet ()
    {
      freeIndices();
      delete[] interior_;
    }

    /** \brief index of intersection */
    template< class Intersection >
    IndexType index ( const Intersection &intersection ) const
    {
      assert( contains( intersection ) );
      return indices( *intersection.inside() )[ intersection.indexInInside() ];
    }

    /** \brief return size of index set */
    IndexType size () const
    {
      return size_;
    }

    /** \brief return true, if intersection is contained in index set */
    template< class Intersection >
    bool contains ( const Intersection &intersection ) const
    {
      return indices( *intersection.inside() )[ intersection.indexInInside() ] != invalidIndex();
    }

    template< class Entity >
    bool hasBoundaryIntersections ( const Entity &entity ) const
    {
      return (indices( entity ) != interior_);
    }

    template< class HostGridView >
    void update ( const HostGridView &hostGridView )
    {
      typedef typename HostGridView::template Codim< 0 >::Iterator HostIterator;
      typedef typename HostGridView::template Codim< 0 >::Entity HostEntity;
      typedef typename HostGridView::IntersectionIterator HostIntersectionIterator;
      typedef typename HostGridView::Intersection HostIntersection;

      freeIndices();

      indices_.resize( indexSet().size( 0 ), interior_ );
      size_ = 0;

      const HostIterator end = hostGridView.template end< 0 >();
      for( HostIterator it = hostGridView.template begin< 0 >(); it != end; ++it )
      {
        const HostEntity &hostEntity = *it;

        if( !indexSet().contains( hostEntity ) )
          continue;

        const HostIntersectionIterator iend = hostGridView.iend( hostEntity );
        for( HostIntersectionIterator iit = hostGridView.ibegin( hostEntity ); iit != iend; ++iit )
        {
          const HostIntersection &hostIntersection = *iit;

          bool boundary = hostIntersection.boundary();
          if( hostIntersection.neighbor() && !indexSet().contains( hostIntersection.outside() ) )
            boundary = true;
          if( !boundary )
            continue;

          const std::size_t index = indexSet().index( hostEntity );
          if( indices_[ index ] == interior_ )
            indices_[ index ] = newInterior( hostEntity.type() );

          const int face = hostIntersection.indexInInside();
          if( indices_[ index ][ face ] == invalidIndex() )
            indices_[ index ][ face ] = size_++;
        }
      }
    }

  private:
    static IndexType invalidIndex () { return std::numeric_limits< IndexType >::max(); }

    const IndexSet &indexSet () const { return indexSet_; }

    template< class Entity >
    const IndexType *indices ( const Entity &entity ) const
    {
      static_assert( (Entity::codimension == 0), "hasBoundaryIntersections is only available for elements." );
      const std::size_t index = indexSet().index( entity );
      return indices_[ index ];
    }

    static IndexType *newInterior ( GeometryType gt )
    {
      const std::size_t size
        = ReferenceElements< typename Traits::ctype, Traits::dimension >::general( gt ).size( 1 );
      IndexType *interior = new IndexType[ size ];
      std::fill( interior, interior + size, invalidIndex() );
      return interior;
    }

    void freeIndices ()
    {
      typedef typename std::vector< IndexType * >::iterator Iterator;
      for( Iterator it = indices_.begin(); it != indices_.end(); ++it )
      {
        if( *it != interior_ )
        {
          delete[] *it;
          *it = interior_;
        }
      }
    }

    const IndexSet &indexSet_;
    IndexType *interior_;
    std::vector< IndexType * > indices_;
    IndexType size_;
  };

} // namespace Dune

#endif // #ifndef DUNE_GRID_FILTEREDGRID_BOUNDARYSEGMENTINDEX_SET_HH
