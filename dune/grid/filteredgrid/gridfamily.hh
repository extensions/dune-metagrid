#ifndef DUNE_GRID_FILTEREDGRID_GRIDFAMILY_HH
#define DUNE_GRID_FILTEREDGRID_GRIDFAMILY_HH

//- dune-grid includes
#include <dune/grid/common/grid.hh>

//- dune-metagrid includes
#include <dune/grid/filteredgrid/boundarysegmentindexset.hh>
#include <dune/grid/filteredgrid/declaration.hh>
#include <dune/grid/idgrid/entity.hh>
#include <dune/grid/idgrid/entityseed.hh>
#include <dune/grid/filteredgrid/entityiterator.hh>
#include <dune/grid/filteredgrid/gridview.hh>
#include <dune/grid/filteredgrid/hierarchiciterator.hh>
#include <dune/grid/filteredgrid/indexset.hh>
#include <dune/grid/filteredgrid/intersection.hh>
#include <dune/grid/filteredgrid/intersectioniterator.hh>
#include <dune/grid/idgrid/idset.hh>


namespace Dune
{

  // FilteredGridFamily
  // ------------------

  template< class HG >
  struct FilteredGridFamily
  {
    struct Traits
    {
      typedef FilteredGrid< HG > Grid;

      typedef HG HostGrid;
      typedef typename HostGrid::LeafGridView HostGridView;

      typedef const Grid*  ExtraData;

      typedef typename HostGridView::ctype ctype;

      static const int dimension = HostGridView::dimension;
      static const int dimensionworld = HostGridView::dimensionworld;

      typedef typename HostGrid::Communication Communication;

      typedef Dune::Intersection< const Grid, FilteredIntersection< const Grid > > LeafIntersection;
      typedef Dune::Intersection< const Grid, FilteredIntersection< const Grid > > LevelIntersection;

      typedef FilteredIntersection< const Grid >  IntersectionImpl;
      typedef IntersectionImpl  LeafIntersectionImpl;
      typedef IntersectionImpl  LevelIntersectionImpl;

      typedef FilteredIntersectionIterator< const Grid >  IntersectionIteratorImpl;
      typedef IntersectionIteratorImpl  LeafIntersectionIteratorImpl;
      typedef IntersectionIteratorImpl  LevelIntersectionIteratorImpl;

      typedef Dune::IntersectionIterator
        < const Grid, FilteredIntersectionIterator< const Grid >, FilteredIntersection< const Grid > >
        LeafIntersectionIterator;
      typedef Dune::IntersectionIterator
        < const Grid, FilteredIntersectionIterator< const Grid >, FilteredIntersection< const Grid > >
        LevelIntersectionIterator;

      typedef FilteredHierarchicIterator< const Grid > HierarchicIteratorImpl;
      typedef Dune::EntityIterator< 0, const Grid, HierarchicIteratorImpl > HierarchicIterator;

      template< int codim >
      struct Codim
      {
        typedef typename HostGridView::template Codim< codim >::Geometry Geometry;
        typedef typename HostGridView::template Codim< codim >::LocalGeometry LocalGeometry;

        typedef IdGridEntity< codim, dimension, const Grid > EntityImpl;
        typedef Dune::Entity< codim, dimension, const Grid, IdGridEntity > Entity;

        typedef Entity     EntityPointer;

        typedef Dune::EntitySeed< const Grid, IdGridEntitySeed< codim, const Grid > > EntitySeed;

        template< PartitionIteratorType pitype >
        struct Partition
        {
          typedef FilteredEntityIterator< const Grid, typename HostGridView::template Codim< codim >::template Partition< pitype >::Iterator > IteratorImpl;
          typedef Dune::EntityIterator< codim, const Grid, IteratorImpl > LevelIterator;
          typedef Dune::EntityIterator< codim, const Grid, IteratorImpl > LeafIterator;
        };

        typedef typename Partition< All_Partition >::LevelIterator LevelIterator;
        typedef typename Partition< All_Partition >::LeafIterator LeafIterator;
      };

      template< PartitionIteratorType pitype >
      struct Partition
      {
        typedef Dune::GridView< FilteredGridViewTraits< HostGridView, pitype > > LevelGridView;
        typedef Dune::GridView< FilteredGridViewTraits< HostGridView, pitype > > LeafGridView;
        typedef FilteredIndexSet< pitype, const Grid > LeafIndexSet;
        typedef FilteredIndexSet< pitype, const Grid > LevelIndexSet;
      };

      typedef typename Partition< All_Partition >::LevelGridView LevelGridView;
      typedef typename Partition< All_Partition >::LeafGridView LeafGridView;

      typedef typename Partition< All_Partition >::LeafIndexSet LeafIndexSet;
      typedef typename Partition< All_Partition >::LevelIndexSet LevelIndexSet;
      typedef FilteredBoundarySegmentIndexSet< const Grid > BoundarySegmentIndexSet;

      typedef IdGridIdSet< const Grid, typename HostGrid::GlobalIdSet > GlobalIdSet;
      typedef IdGridIdSet< const Grid, typename HostGrid::LocalIdSet > LocalIdSet;
    };
  };

} // namespace Dune

#endif // #ifndef DUNE_GRID_FILTEREDGRID_GRIDFAMILY_HH
