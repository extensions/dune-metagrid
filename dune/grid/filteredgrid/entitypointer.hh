#ifndef DUNE_GRID_FILTEREDGRID_ENTITYPOINTER_HH
#define DUNE_GRID_FILTEREDGRID_ENTITYPOINTER_HH

//- dune-common includes
#include <dune/common/typetraits.hh>

//- dune-grid includes
#include <dune/grid/common/entity.hh>
#include <dune/grid/common/entity.hh>


namespace Dune
{

  // External Forward Declarations
  // -----------------------------

  template< int, int, class >
  class FilteredEntity;



  // FilteredEntityPointer
  // ---------------------

  template< class Grid, class HostIterator >
  class FilteredEntityPointer
  {
    typedef FilteredEntityPointer< Grid, HostIterator > This;

    typedef typename std::remove_const< Grid >::type::Traits Traits;

  public:
    /** \brief grid dimension */
    static const int dimension = std::remove_const< Grid >::type::dimension;
    /** \brief world dimension */
    static const int codimension = HostIterator::Entity::codimension;

    /** \brief type of entity */
    typedef typename Traits::template Codim< codimension >::Entity Entity;

  protected:
    typedef FilteredEntity< codimension, dimension, Grid > EntityImpl;

  private:
    friend class FilteredEntityPointer< Grid, HostEntityPointer >;

  public:
    typedef FilteredEntityPointer< Grid, HostEntityPointer > EntityPointerImp;

    FilteredEntityPointer ( const HostIterator &hostIterator, const Grid &grid )
    : entity_( EntityImpl( grid ) ),
      hostIterator_( hostIterator )
    {}

    explicit FilteredEntityPointer ( const EntityImpl &entity )
    : entity_( EntityImpl( entity.grid() ) ),
      hostIterator_( entity.hostEntity() )
    {}

    FilteredEntityPointer ( const This &other )
    : entity_( EntityImpl( other.grid() ) ),
      hostIterator_( other.hostIterator_ )
    {}

    template< class HI >
    FilteredEntityPointer ( const FilteredEntityPointer< Grid, HI > &other )
    : entity_( EntityImpl( other.grid() ) ),
      hostIterator_( other.hostIterator_ )
    {}

    const This &operator= ( const This &other )
    {
      releaseEntity();
      hostIterator_ = other.hostIterator_;
      return *this;
    }

    template< class HI >
    const This &operator= ( const FilteredEntityPointer< Grid, HI > &other )
    {
      releaseEntity();
      hostIterator_ = other.hostIterator_;
      return *this;
    }

    /** \brief check for equality */
    template< class HI >
    bool equals ( const FilteredEntityPointer< Grid, HI > &other ) const
    {
      return (hostIterator() == other.hostIterator());
    }

    /** \brief dereference entity */
    Entity dereference () const
    {
      if( !entityImpl() )
        entityImpl().reset( *hostIterator() );
      return entity_;
    }

    /** \brief obtain level */
    int level () const { return 0; }

    /** \brief obtain host iterator */
    const HostIterator &hostIterator() const { return hostIterator_; }

  protected:
    const Grid &grid () const { return entityImpl().grid(); }

    void releaseEntity () { entityImpl().reset(); }

  private:
    EntityImpl &entityImpl () const
    {
      return entity_.impl();
    }

    mutable Entity entity_;

  protected:
    HostIterator hostIterator_;
  };

} // namespace Dune

#endif // #ifndef DUNE_GRID_FILTEREDGRID_ENTITYPOINTER_HH
