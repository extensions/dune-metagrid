#ifndef DUNE_FILTEREDGRID_DECLARATION_HH
#define DUNE_FILTEREDGRID_DECLARATION_HH

namespace Dune
{

  template< class HostGrid >
  class FilteredGrid;

} // namespace Dune

#endif // #ifndef DUNE_FILTEREDGRID_DECLARATION_HH
