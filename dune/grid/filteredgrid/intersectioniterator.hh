#ifndef DUNE_GRID_FILTEREDGRID_INTERSECTIONITERATOR_HH
#define DUNE_GRID_FILTEREDGRID_INTERSECTIONITERATOR_HH

//- C++ includes
#include <cassert>

//- dune-common includes
#include <dune/common/typetraits.hh>

//- dune-grid includes
#include <dune/grid/common/intersection.hh>

//- local includes
#include <dune/grid/filteredgrid/intersection.hh>


namespace Dune
{

  // Forward declaration
  // -------------------

  template< class > class FilteredIntersection;



  // FilteredIntersectionIterator
  // ----------------------------

  template< class Grid >
  class FilteredIntersectionIterator
  {
    // type of this
    typedef FilteredIntersectionIterator< Grid > This;
    // type of traits
    typedef typename std::remove_const< Grid >::type::Traits Traits;

  public:
    /** \brief host grid view type */
    typedef typename Traits::HostGridView HostGridView;

    /** \brief entity type */
    typedef typename Traits::template Codim< 0 >::Entity Entity;
    /** \brief entity implementation type */
    typedef typename Entity::Implementation EntityImp;

    /** \brief type of intersection */
    typedef Dune::Intersection< Grid, FilteredIntersection< Grid > > Intersection;

    /** \brief type of intersection implementation */
    typedef FilteredIntersection< Grid > IntersectionImp;

    /** \brief host iterator type */
    typedef typename HostGridView::IntersectionIterator HostIterator;

  protected:
    typedef typename IntersectionImp::BoundarySegmentIndex BoundarySegmentIndex;

    /** \brief constructor */
    FilteredIntersectionIterator( const HostIterator &hostIterator,
                                  const Grid &grid,
                                  const BoundarySegmentIndex *boundarySegmentIndices = 0 )
    : intersection_( IntersectionImp( grid, boundarySegmentIndices ) ),
      hostIterator_( hostIterator )
    {}

  public:
    /** \brief constructor */
    FilteredIntersectionIterator()
    : intersection_(),
      hostIterator_()
    {}

    /** \brief return begin iterator */
    static FilteredIntersectionIterator
    begin ( const Entity &entity, const Grid &grid )
    {
      return begin( entity.impl(), grid );
    }

    /** \brief return begin iterator */
    static FilteredIntersectionIterator
    begin ( const EntityImp &entity, const Grid &grid )
    {
      const HostIterator hostIterator = grid.hostGridView().ibegin( entity.hostEntity() );
      return This( hostIterator, grid, grid.boundarySegmentIndexSet().indices( entity.hostEntity() ) );
    }

    /** \brief return end iterator */
    static FilteredIntersectionIterator
    end ( const Entity &entity, const Grid &grid )
    {
      return end( entity.impl(), grid );
    }

    /** \brief return end iterator */
    static FilteredIntersectionIterator
    end ( const EntityImp &entity, const Grid &grid )
    {
      const HostIterator hostIterator = grid.hostGridView().iend( entity.hostEntity() );
      return This( hostIterator, grid );
    }

    /** \brief copy constructor */
    FilteredIntersectionIterator ( const This &other )
    : intersection_( other.intersection_.impl() ),
      hostIterator_( other.hostIterator_ )
    {}

    /** \brief assignment operator */
    FilteredIntersectionIterator &operator= ( const This &other )
    {
      intersection_.impl() = other.intersection_.impl();
      hostIterator_ = other.hostIterator_;
      return *this;
    }

    /** \brief check for equality */
    bool equals ( const This &other ) const
    {
      return ( hostIterator_ == other.hostIterator_ );
    }

    /** \brief iterator increment */
    void increment ()
    {
      intersection_.impl().invalidate();
      ++hostIterator_;
    }

    /** \brief dereference intersection */
    const Intersection &dereference () const
    {
      if( !( intersection_.impl() ) )
        intersection_.impl().reset( *hostIterator() );
      return intersection_;
    }

  private:
    // return reference to host iterator
    HostIterator &hostIterator ()
    {
      return hostIterator_;
    }

    // return const reference to host iterator
    const HostIterator &hostIterator () const
    {
      return hostIterator_;
    }

    // return grid
    const Grid &grid () const
    {
      return intersection_.impl().grid();
    }

    mutable Intersection intersection_;
    HostIterator hostIterator_;
  };

}  // end namespace Dune

#endif // #ifndef DUNE_GRID_FILTEREDGRID_INTERSECTIONITERATOR_HH
