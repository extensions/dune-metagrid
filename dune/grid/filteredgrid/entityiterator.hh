#ifndef DUNE_GRID_FILTEREDGRID_ITERATOR_HH
#define DUNE_GRID_FILTEREDGRID_ITERATOR_HH

//- system includes
#include <cassert>

//- dune-common includes
#include <dune/common/typetraits.hh>

//- dune-grid includes
#include <dune/grid/common/gridenums.hh>

namespace Dune
{

  // FilteredEntityIterator
  // ----------------------

  template< class Grid, class HostIterator >
  class FilteredEntityIterator
  {
    typedef FilteredEntityIterator< Grid, HostIterator > This;

    typedef typename std::remove_const< Grid >::type::Traits Traits;

    typedef typename Traits :: ExtraData  ExtraData;

  public:
    /** \brief grid dimension */
    static const int dimension = std::remove_const< Grid >::type::dimension;
    /** \brief world dimension */
    static const int codimension = HostIterator::Entity::codimension;

    /** \brief type of entity */
    typedef typename Traits::template Codim< codimension >::Entity Entity;

  protected:
    typedef typename Traits::template Codim< codimension >::EntityImpl  EntityImpl;

  public:
    FilteredEntityIterator () {}

    FilteredEntityIterator ( ExtraData data, const HostIterator &hostIterator, const HostIterator &hostEnd )
    : data_( data ),
      hostEnd_( hostEnd )
    {
      if( proceed() )
        increment();
    }

    /** \brief increment */
    void increment ()
    {
      assert( !done() );
      do
      {
        ++hostIterator_;
      }
      while( proceed() );
    }

    FilteredEntityIterator ( const This &other )
    : data_( other.data() ),
      hostIterator_( other.hostIterator_ )
    {}

    template< class HI >
    FilteredEntityIterator ( const FilteredEntityIterator< Grid, HI > &other )
    : data_( other.data() ),
      hostIterator_( other.hostIterator_ )
    {}

    const This &operator= ( const This &other )
    {
      hostIterator_ = other.hostIterator_;
      return *this;
    }

    template< class HI >
    const This &operator= ( const FilteredEntityIterator< Grid, HI > &other )
    {
      hostIterator_ = other.hostIterator_;
      return *this;
    }

    /** \brief check for equality */
    template< class HI >
    bool equals ( const FilteredEntityIterator< Grid, HI > &other ) const
    {
      return (hostIterator() == other.hostIterator());
    }

    /** \brief dereference entity */
    Entity dereference () const
    {
      return Entity( EntityImpl( data(), *hostIterator() ) );
    }

    /** \brief obtain level */
    int level () const { return 0; }

    /** \brief obtain host iterator */
    const HostIterator &hostIterator() const { return hostIterator_; }

  protected:
    const Grid &grid () const { return *data(); }

  protected:
    ExtraData data () const { return data_; }

    ExtraData data_;
    HostIterator hostIterator_;
    HostIterator hostEnd_;

  private:
    bool proceed () const { return (!done() && !(grid().contains( *hostIterator_ ))); }
    bool done () const { return (hostIterator_ == hostEnd_); }

  };

} // namespace Dune

#endif // #ifndef DUNE_GRID_FILTEREDGRID_ITERATOR_HH
