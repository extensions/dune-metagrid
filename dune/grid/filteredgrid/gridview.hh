#ifndef DUNE_FILTEREDGRID_GRIDVIEW_HH
#define DUNE_FILTEREDGRID_GRIDVIEW_HH

//- dune-common includes
#include <dune/common/typetraits.hh>

//- dune-grid includes
#include <dune/grid/common/capabilities.hh>
#include <dune/grid/common/gridview.hh>

//- dune-metagrid includes
#include <dune/grid/filteredgrid/datahandle.hh>
#include <dune/grid/filteredgrid/declaration.hh>
#include <dune/grid/filteredgrid/indexset.hh>
#include <dune/grid/filteredgrid/intersection.hh>
#include <dune/grid/filteredgrid/intersectioniterator.hh>
#include <dune/grid/filteredgrid/entityiterator.hh>

namespace Dune
{

  // Internal Forward Declarations
  // -----------------------------

  template< class HostGridView, PartitionIteratorType pitype >
  class FilteredGridView;



  // FilteredGridViewTraits
  // ----------------------

  template< class HGV, PartitionIteratorType pitype >
  class FilteredGridViewTraits
  {
    friend class FilteredGridView< HGV, pitype >;

    typedef HGV HostGridView;

    typedef typename HostGridView::Grid HostGrid;

  public:
    typedef FilteredGridView< HostGridView, pitype > GridViewImp;

    typedef FilteredGrid< HostGrid > Grid;

    typedef FilteredIndexSet< All_Partition, const Grid > IndexSet;

    typedef FilteredIntersection< const Grid > IntersectionImpl;
    typedef Dune::Intersection< const Grid, IntersectionImpl > Intersection;

    typedef FilteredIntersectionIterator< const Grid > IntersectionIteratorImpl;
    typedef Dune::IntersectionIterator< const Grid, IntersectionIteratorImpl, IntersectionImpl > IntersectionIterator;

    typedef typename HostGridView::Communication Communication;

    template< int codim >
    struct Codim
    {
      typedef typename Grid::Traits::template Codim< codim >::Entity Entity;
      typedef typename Grid::Traits::template Codim< codim >::EntityPointer EntityPointer;

      typedef typename Grid::template Codim< codim >::Geometry Geometry;
      typedef typename Grid::template Codim< codim >::LocalGeometry LocalGeometry;

      template< PartitionIteratorType pit >
      struct Partition
      {
        typedef FilteredEntityIterator< const Grid, typename HostGridView::template Codim< codim >::template Partition< pit >::Iterator > IteratorImpl;
        typedef Dune::EntityIterator< codim, const Grid, IteratorImpl > Iterator;
      };

      typedef typename Partition< pitype >::Iterator Iterator;
    };

    static const bool conforming = HostGridView::conforming;
  };



  // FilteredGridView
  // ----------------

  template< class HGV, PartitionIteratorType pitype >
  class FilteredGridView
  {
    typedef FilteredGridView< HGV, pitype > This;

  public:
    typedef FilteredGridViewTraits< HGV, pitype > Traits;

    typedef typename Traits::HostGridView HostGridView;

    typedef typename Traits::Grid Grid;

    typedef typename Traits::IndexSet IndexSet;

    typedef typename Traits::Intersection Intersection;

    typedef typename Traits::IntersectionIterator IntersectionIterator;

    typedef typename Traits::Communication Communication;

    template< int codim >
    struct Codim
    : public Traits::template Codim< codim >
    {};

    static const bool conforming = Traits::conforming;

    FilteredGridView ( const Grid &grid, const HostGridView &hostGridView )
    : grid_( &grid ),
      hostGridView_( hostGridView )
    {}

    const Grid &grid () const
    {
      assert( grid_ );
      return *grid_;
    }

    const IndexSet &indexSet () const
    {
      return grid().leafIndexSet();
    }

    int size ( int codim ) const
    {
      return hostGridView().size( codim );
    }

    int size ( const GeometryType &type ) const
    {
      return hostGridView().size( type );
    }

    template< int codim >
    typename Codim< codim >::Iterator begin () const
    {
      return begin< codim, pitype >();
    }

    template< int codim, PartitionIteratorType pit >
    typename Codim< codim >::template Partition< pit >::Iterator begin () const
    {
      typedef typename Traits::template Codim< codim >::template Partition< pit >::IteratorImpl IteratorImpl;
      return IteratorImpl( grid().extraData(), hostGridView().template begin< codim, pit >(), hostGridView().template end< codim, pit >() );
    }

    template< int codim >
    typename Codim< codim >::Iterator end () const
    {
      return end< codim, pitype >();
    }

    template< int codim, PartitionIteratorType pit >
    typename Codim< codim >::template Partition< pit >::Iterator end () const
    {
      typedef typename Traits::template Codim< codim >::template Partition< pit >::IteratorImpl IteratorImpl;
      return IteratorImpl( grid().extraData(), hostGridView().template end< codim, pit >(), hostGridView().template end< codim, pit >() );
    }

    IntersectionIterator ibegin ( const typename Codim< 0 >::Entity &entity ) const
    {
      typedef typename Traits::IntersectionIteratorImpl IteratorImpl;
      return IteratorImpl::begin( entity, grid() );
    }

    IntersectionIterator iend ( const typename Codim< 0 >::Entity &entity ) const
    {
      typedef typename Traits::IntersectionIteratorImpl IteratorImpl;
      return IteratorImpl::end( entity, grid() );
    }

    bool isConforming () const
    {
      return hostGridView().isConforming();
    }

    const Communication &comm () const
    {
      return hostGridView().comm();
    }

    int overlapSize ( int codim ) const
    {
      return hostGridView().overlapSize( codim );
    }

    int ghostSize ( int codim ) const
    {
      return hostGridView().ghostSize( codim );
    }

    template< class DataHandle, class Data >
    void communicate ( CommDataHandleIF< DataHandle, Data > &dataHandle,
                       InterfaceType interface,
                       CommunicationDirection direction ) const
    {
      typedef CommDataHandleIF< DataHandle, Data > DataHandleIF;
      typedef FilteredGridDataHandle< DataHandleIF, Grid > WrappedDataHandle;

      WrappedDataHandle wrappedDataHandle( dataHandle, grid() );
      hostGridView().communicate( wrappedDataHandle, interface, direction );
    }

    const HostGridView &hostGridView () const { return hostGridView_; }

  private:
    const Grid *grid_;
    HostGridView hostGridView_;
  };

} // namespace Dune

#endif // #ifndef DUNE_FILTEREDGRID_GRIDVIEW_HH
