#ifndef DUNE_CARTESIANGRID_HOSTGRIDINFO_ALUGRID_HH
#define DUNE_CARTESIANGRID_HOSTGRIDINFO_ALUGRID_HH

#include <dune/common/fvector.hh>

#if HAVE_DUNE_ALUGRID
#include <dune/alugrid/common/declaration.hh>
#include <dune/alugrid/3d/topology.hh>
#endif // #if HAVE_DUNE_ALUGRID

namespace Dune
{

  // Internal Forward Declarations
  // -----------------------------

  template< class HostGrid >
  struct CartesianGridHostGridInfo;

  // ALU3dCubeOrigin
  // ---------------

  template< class HostGrid, int dimension, int codimension >
  struct ALU3dCubeOrigin
  {
    typedef typename HostGrid::template Codim< codimension >::Geometry::GlobalCoordinate OriginReturnType;

    template< class Item >
    static OriginReturnType get ( const Item &item )
    {
      return item.geometry().corner( 0 );
    }
  };

  // for vertices
  template< class HostGrid, int dimension >
  struct ALU3dCubeOrigin< HostGrid, dimension, dimension >
  {
    typedef double coord_t[ dimension ];
    typedef const coord_t &OriginReturnType;

    template< class Item >
    static OriginReturnType get ( const Item &item )
    {
      return item.impl().getItem().Point();
    }
  };

  // for elements
  template< class HostGrid, int dimension >
  struct ALU3dCubeOrigin< HostGrid, dimension, 0 >
  {
    typedef double coord_t[ dimension ];
    typedef const coord_t &OriginReturnType;

    template< class Item >
    static OriginReturnType get ( const Item &item )
    {
      return item.impl().getItem().myvertex( 0 )->Point();
    }
  };

  // specialization for intersections
  template< class HostGrid, int dimension >
  struct ALU3dCubeOrigin< HostGrid, dimension, -1 >
  {
    typedef double coord_t[ dimension ];
    typedef const coord_t &OriginReturnType;

    template< class Intersection >
    static OriginReturnType get ( const Intersection &intersection )
    {
#ifndef NDEBUG
      typedef FaceTopologyMapping< hexa > CubeFaceMapping;
      const int duneTwist = intersection.impl().twistInInside();
      const int twistedIndex = CubeFaceMapping::twistedDuneIndex( 0, duneTwist );
      assert( twistedIndex == 0 );
#endif
      return intersection.impl().it().getItem().myvertex( 0 )->Point();
    }
  };



  // CartesianGridHostGridInfo for ALUGrid and dimension 3
  // -----------------------------------------------------

  template< class HostGrid, int dim >
  struct ALUGridHostGridInfo;

  template < class HostGrid >
  struct ALUGridHostGridInfo< HostGrid, 3 >
  {
    static const int dimension = 3;

    //////////////////////////////////////////////////
    //  direction methods
    //////////////////////////////////////////////////

    //! default direction for given dimension
    static unsigned int defaultDirection ( const int mydimension )
    {
      return (1 << mydimension)-1;
    }

    //! direction for face i
    static unsigned int direction ( const int i, const int dimension )
    {
      assert( (i >= 0) && (i < 2*dimension) );
      return (1 << (i / 2)) ^ ((1 << dimension) - 1);
    }

    //! direction for given host entity
    template< class HostEntity >
    static unsigned int direction ( const HostEntity& hostEntity )
    {
      typedef typename HostEntity::Geometry HostGeometry;
      unsigned int direction = 0;
      const HostGeometry &geo = hostEntity.geometry();
      const typename HostGeometry::GlobalCoordinate origin = geo.corner( 0 );
      for( int d = 0; d <HostGeometry::mydimension; ++d )
      {
        const typename HostGeometry::GlobalCoordinate point = geo.corner( 1 << d );
        for( int i = 0; i < HostGeometry::coorddimension; ++i )
        {
          if( std::abs( point[ i ] - origin[ i ] ) > 1e-8 )
            direction |= (1 << i);
        }
      }
      return direction;
    }

    //! default origin
    static FieldVector< double, dimension > defaultOrigin ()
    {
      return FieldVector< double, dimension >( 0 );
    }

    //! origin for given entity or intersection
    template< class HostItem >
    static typename ALU3dCubeOrigin< HostGrid, HostItem::dimension, HostItem::codimension >::OriginReturnType
    origin ( const HostItem &hostItem )
    {
      return ALU3dCubeOrigin< HostGrid, HostItem::dimension, HostItem::codimension >::get( hostItem );
    }

    //! origin for given entity or intersection
    template< class HostItem >
    static typename ALU3dCubeOrigin< HostGrid, HostGrid::dimension, -1 >::OriginReturnType
    originIntersection ( const HostItem &hostItem )
    {
      return ALU3dCubeOrigin< HostGrid, HostGrid::dimension, -1 >::get( hostItem );
    }

    //////////////////////////////////////////////////////////
    //  child index methods
    //////////////////////////////////////////////////////////

    //! return child index for entity
    template< class HostEntity >
    static int childIndex ( const HostEntity &hostEntity )
    {
      // apply the same change as for the vertices of the hexa
      typedef ElementTopologyMapping< hexa > ElemTopo;
      return ElemTopo::alu2duneVertex( hostEntity.impl().getItem().nChild() );
    }

    //////////////////////////////////////////////////////////
    //  level methods
    //////////////////////////////////////////////////////////

    //! return inside level for intersection
    template <class HostIntersection>
    static int insideLevel ( const HostIntersection &hostIntersection )
    {
#ifndef NDEBUG
      const int level = hostIntersection.impl().level();
      const int insideLevel = hostIntersection.inside().level() ;
      //assert( level == insideLevel );
#endif
      return hostIntersection.impl().level();
    }

    //! return outside level for intersection
    template< class HostIntersection >
    static int outsideLevel ( const HostIntersection &hostIntersection,
                              const int insideLevel )
    {
      // outsideLevel might be less than insideLevel if there is no level neighbor
      const int outsideLevel = hostIntersection.impl().it().outsideLevel();
      assert( !hostIntersection.neighbor() || (outsideLevel == hostIntersection.outside().level()) );
      return outsideLevel;
    }

    template< class HostIntersection >
    static int childIndexInInside ( const HostIntersection &hostIntersection,
                                    const int insideLevel,
                                    const int outsideLevel )
    {
      return getIntersectionChildLevel( hostIntersection.impl().twistInInside(),
                                        hostIntersection.impl().it().getItem().nChild(),
                                        insideLevel, outsideLevel );
    }

    template< class HostIntersection >
    static int childIndexInOutside ( const HostIntersection &hostIntersection,
                                     const int insideLevel,
                                     const int outsideLevel )
    {
      return getIntersectionChildLevel( hostIntersection.impl().twistInOutside(),
                                        hostIntersection.impl().it().getItem().nChild(),
                                        outsideLevel, insideLevel );
    }

  protected:
    static int
    getIntersectionChildLevel( const int duneTwist, const int child,
                               const int myLevel, const int otherLevel )
    {
      // make sure non-confoming level difference is at most 1
      assert( std::abs( myLevel - otherLevel ) <= 1 );
      if( myLevel < otherLevel )
      {
        // swap children 2 and 3
        static const int map[4] = { 0, 1, 3, 2 };
        return map[ child ];
      }
      else
        return -1;
    }
  };

  // CartesianGridHostGridInfo for ALUGrid< 3, 3, cube, nonconforming, Comm >
  // -------------------------------------------------------------------------
#if HAVE_DUNE_ALUGRID
  template< class Comm >
  struct CartesianGridHostGridInfo< ALUGrid< 3, 3, cube, nonconforming, Comm > >
  : public ALUGridHostGridInfo< ALUGrid< 3, 3, cube, nonconforming, Comm >, 3 >
  {};
#endif // #if HAVE_DUNE_ALUGRID

} // namespace Dune


#endif // #ifndef DUNE_CARTESIANGRID_HOSTGRIDINFO_HH
