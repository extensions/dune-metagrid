#ifndef DUNE_CARTESIANGRID_PERSISTENTCONTAINER_HH
#define DUNE_CARTESIANGRID_PERSISTENTCONTAINER_HH

#include <dune/grid/utility/persistentcontainer.hh>
#include <dune/grid/utility/persistentcontainerwrapper.hh>

#include <dune/grid/cartesiangrid/declaration.hh>

#if HAVE_DUNE_SPGRID

namespace Dune
{

  // PersistentContainer for CartesianGrid
  // -------------------------------------

  template< class HostGrid, class T >
  class PersistentContainer< CartesianGrid< HostGrid >, T >
  : public PersistentContainerWrapper< CartesianGrid< HostGrid >, T >
  {
    typedef PersistentContainerWrapper< CartesianGrid< HostGrid >, T > Base;

  public:
    typedef typename Base::Grid Grid;
    typedef typename Base::Value Value;

    PersistentContainer ( const Grid &grid, int codim, const Value &value = Value() )
    : Base( grid, codim, value )
    {}
  };

} // namespace Dune

#endif // #if HAVE_DUNE_SPGRID

#endif // #ifndef DUNE_CARTESIANGRID_PERSISTENTCONTAINER_HH
