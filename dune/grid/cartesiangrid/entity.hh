#ifndef DUNE_CARTESIANGRID_ENTITY_HH
#define DUNE_CARTESIANGRID_ENTITY_HH

//- dune-geometry includes
#include <dune/geometry/referenceelements.hh>

//- dune-grid includes
#include <dune/grid/common/grid.hh>

//- local includes
#include <dune/grid/cartesiangrid/geometry.hh>
#include <dune/grid/cartesiangrid/hostgridinfo.hh>
#include <dune/grid/idgrid/entity.hh>

#if HAVE_DUNE_SPGRID

namespace Dune
{

  // External Forward Declarations
  // -----------------------------

  template< class, class > class CartesianGridIntersection;
  template< class, class > class CartesianGridIntersectionIterator;


  // CartesianGridEntity
  // -------------------

  /** \copydoc CartesianGridEntity
   *
   *  \nosubgrouping
   */
  template< int codim, int dim, class Grid >
  class CartesianGridEntity : public IdGridEntity< codim, dim, Grid >
  {
    typedef IdGridEntity< codim, dim, Grid > Base ;
    typedef typename Base::Traits Traits;

  public:
    using Base :: data ;
    using Base :: hostEntity ;
    using Base :: level ;
    using Base :: codimension ;

    /** \name Types Required by DUNE
     *  \{ */

    //! type of corresponding geometry
    typedef typename Traits::template Codim< codimension >::Geometry Geometry;

    /** \} */

  protected:
    typedef typename Traits::HostGrid HostGrid;

    // type of host grid info
    typedef CartesianGridHostGridInfo< HostGrid > HostGridInfo;

    typedef typename Traits :: template Codim< codimension > :: GeometryImpl  GeometryImpl;
    typedef typename Base :: ExtraData     ExtraData;

  public:
    /** \name Host Types
     *  \{ */

    //! type of corresponding host entity
    typedef typename Base :: HostEntity  HostEntity;
    /** \} */

    /** \name Construction, Initialization and Destruction
     *  \{ */

    /** \brief construct a null entity */
    explicit CartesianGridEntity ( ExtraData data )
    : Base( data )
    {}

    /** \brief construct an initialized entity
     *
     *  \param[in]  hostEntity  corresponding entity in the host grid
     *
     *  \note The reference to the host entity must remain valid  as long as
     *        this entity is in use.
     */
    CartesianGridEntity ( ExtraData data, const HostEntity &hostEntity )
    : Base( data, hostEntity )
    {}

    /** \} */

    /** \name Methods Shared by Entities of All Codimensions
     *  \{ */

    /** obtain the geometry of this entity */
    Geometry geometry () const
    {
      return Geometry( GeometryImpl( data()->geometricLevel( level() ),
                                     HostGridInfo::direction( hostEntity() ),
                                     HostGridInfo::origin( hostEntity() ) ) );
    }
  };



  // CartesianGridEntity for codimension 0
  // -------------------------------------

  /** \copydoc CartesianGridEntity
   *
   *  \nosubgrouping
   */
  template< int dim, class Grid >
  class CartesianGridEntity< 0, dim, Grid > : public IdGridEntity< 0, dim, Grid >
  {
    typedef IdGridEntity< 0, dim, Grid > Base ;
    typedef typename Base::Traits Traits;

    typedef typename Traits::HostGrid HostGrid;

  public:
    using Base :: mydimension ;
    using Base :: codimension ;
    using Base :: data ;
    using Base :: hostEntity ;
    using Base :: level ;

    /** \name Host Types
     *  \{ */

    //! type of corresponding host entity
    typedef typename Base :: HostEntity  HostEntity;
    /** \} */

    typedef typename Base :: LocalGeometry  LocalGeometry;

  protected:
    // type of host grid info
    typedef CartesianGridHostGridInfo< HostGrid > HostGridInfo;

    typedef typename Traits :: template Codim< codimension > :: GeometryImpl  GeometryImpl;
    typedef typename Traits :: ExtraData     ExtraData;
  public:
    /** \name Types Required by DUNE
     *  \{ */

    //! type of corresponding geometry
    typedef typename Traits::template Codim< codimension >::Geometry Geometry;

    /** \} */

    /** \name Construction, Initialization and Destruction
     *  \{ */

    /** \brief construct a null entity */
    explicit CartesianGridEntity ( ExtraData data )
    : Base( data )
    {}

    /** \brief construct an initialized entity
     *
     *  \param[in]  hostEntity  corresponding entity in the host grid
     *
     *  \note The reference to the host entity must remain valid as long as
     *        this entity is in use.
     */
    CartesianGridEntity ( ExtraData data, const HostEntity &hostEntity )
    : Base( data, hostEntity )
    {}

    /** \} */

    /** \name Methods Shared by Entities of All Codimensions
     *  \{ */

    /** obtain the geometry of this entity */
    Geometry geometry () const
    {
      return Geometry( GeometryImpl( data()->geometricLevel( level() ),
                                     HostGridInfo::defaultDirection( mydimension ),
                                     HostGridInfo::origin( hostEntity() ) ) );
    }

    /** \} */

    LocalGeometry geometryInFather () const
    {
      return LocalGeometry( *(data()->geometryInFather_[ HostGridInfo::childIndex( hostEntity() ) ]) );
    }

  };

} // namespace Dune

#endif // #if HAVE_DUNE_SPGRID

#endif // #ifndef DUNE_CARTESIANGRID_ENTITY_HH
