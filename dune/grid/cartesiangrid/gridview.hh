#ifndef DUNE_CARTESIANGRID_GRIDVIEW_HH
#define DUNE_CARTESIANGRID_GRIDVIEW_HH

#include <dune/common/typetraits.hh>

#include <dune/grid/common/capabilities.hh>
#include <dune/grid/common/gridview.hh>

#include <dune/grid/idgrid/datahandle.hh>
#include <dune/grid/idgrid/indexset.hh>
#include <dune/grid/idgrid/gridview.hh>
#include <dune/grid/cartesiangrid/intersection.hh>

namespace Dune
{

  // Internal Forward Declarations
  // -----------------------------

  template< class HostGridView, PartitionIteratorType pitype >
  class CartesianGridView;



  // CartesianGridViewTraits
  // -----------------------

  template< class HGV, PartitionIteratorType ptype >
  struct CartesianGridViewTraits
  {
    static const PartitionIteratorType pitype = ptype;

    friend class CartesianGridView< HGV, pitype >;

    typedef HGV HostGridView;

    typedef typename HostGridView::Grid HostGrid;

    typedef CartesianGridView< HostGridView, pitype > GridViewImp;

    typedef CartesianGrid< HostGrid > Grid;

    typedef IdGridIndexSet< const Grid, typename HostGridView::IndexSet > IndexSet;

    typedef CartesianGridIntersection< const Grid, typename HostGridView::Intersection > IntersectionImpl;
    typedef Dune::Intersection< const Grid, IntersectionImpl > Intersection;

    typedef IdGridIntersectionIterator< const Grid, typename HostGridView::IntersectionIterator > IntersectionIteratorImpl;
    typedef Dune::IntersectionIterator< const Grid, IntersectionIteratorImpl, IntersectionImpl >  IntersectionIterator;

    typedef typename HostGridView::Communication Communication;

    template< int codim >
    struct Codim
    {
      typedef typename Grid::Traits::template Codim< codim >::Entity Entity;

#if ! DUNE_VERSION_NEWER(DUNE_GRID, 2, 5)
      typedef typename Grid::Traits::template Codim< codim >::EntityPointer EntityPointer;
#endif

      typedef typename Grid::template Codim< codim >::Geometry Geometry;
      typedef typename Grid::template Codim< codim >::LocalGeometry LocalGeometry;

      template< PartitionIteratorType pit >
      struct Partition
      {
        typedef typename HostGridView::template Codim< codim >::template Partition< pit > HostPartition;

        typedef IdGridIterator< const Grid, typename HostPartition::Iterator > IteratorImpl;
        typedef Dune::EntityIterator< codim, const Grid, IteratorImpl > Iterator;
      };

      typedef typename Partition< pitype >::Iterator Iterator;
    };

    static const bool conforming = HostGridView::conforming;
  };



  // CartesianGridView
  // -----------------

  template< class HGV, PartitionIteratorType pitype >
  class CartesianGridView : public IdGridViewBasic< CartesianGridViewTraits< HGV, pitype > >
  {
    typedef CartesianGridView< HGV, pitype > This;
    typedef IdGridViewBasic< CartesianGridViewTraits< HGV, pitype > > Base;

  public:
    typedef typename Base::HostGridView HostGridView;
    typedef typename Base::Grid Grid;

    CartesianGridView ( const Grid &grid, const HostGridView &hostGridView )
    : Base( grid, hostGridView )
    {}
  };

} // namespace Dune

#endif // #ifndef DUNE_CARTESIANGRID_GRIDVIEW_HH
