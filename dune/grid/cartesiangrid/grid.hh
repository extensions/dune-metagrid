#ifndef DUNE_CARTESIANGRID_GRID_HH
#define DUNE_CARTESIANGRID_GRID_HH

#include <string>

#include <dune/common/version.hh>

#include <dune/grid/common/grid.hh>

#include <dune/grid/idgrid/adaptcallback.hh>
#include <dune/grid/idgrid/entityseed.hh>
#include <dune/grid/idgrid/idset.hh>
#include <dune/grid/idgrid/indexset.hh>
#include <dune/grid/idgrid/iterator.hh>
#include <dune/grid/idgrid/intersectioniterator.hh>
#include <dune/grid/cartesiangrid/backuprestore.hh>
#include <dune/grid/cartesiangrid/capabilities.hh>
#include <dune/grid/cartesiangrid/declaration.hh>
#include <dune/grid/cartesiangrid/entity.hh>
#include <dune/grid/cartesiangrid/geometry.hh>
#include <dune/grid/cartesiangrid/gridview.hh>
#include <dune/grid/cartesiangrid/intersection.hh>
#include <dune/grid/cartesiangrid/persistentcontainer.hh>

#if HAVE_DUNE_SPGRID
#include <dune/grid/spgrid/geometricgridlevel.hh>
#include <dune/grid/spgrid/referencecube.hh>

namespace Dune
{

  // CartesianGridExportParams
  // -------------------------

  template< class HG >
  struct CartesianGridExportParams
  {
    typedef HG HostGrid;
  };



  // CartesianGridFamily
  // -------------------

  template< class HostGrid >
  struct CartesianGridFamily
  {
    struct Traits
    : public CartesianGridExportParams< HostGrid >
    {
      typedef CartesianGrid< HostGrid > Grid;

      // type of extra data stored in entity class
      typedef const Grid*  ExtraData ;

      // export host grid type
      typedef HostGrid HostGridType;

      typedef typename HostGrid::ctype ctype;

      static const int dimension = HostGrid::dimension;
      static const int dimensionworld = HostGrid::dimensionworld;

      typedef SPReferenceCubeContainer< ctype, dimension > ReferenceCubeContainer;
      typedef typename ReferenceCubeContainer::ReferenceCube ReferenceCube;

      typedef CartesianGridIntersection< const Grid, typename HostGrid::LeafIntersection > LeafIntersectionImpl;
      typedef Dune::Intersection< const Grid, LeafIntersectionImpl > LeafIntersection;

      typedef CartesianGridIntersection< const Grid, typename HostGrid::LevelIntersection > LevelIntersectionImpl;
      typedef Dune::Intersection< const Grid, LevelIntersectionImpl > LevelIntersection;

      typedef IdGridIntersectionIterator< const Grid, typename HostGrid::LeafIntersectionIterator > LeafIntersectionIteratorImpl;
      typedef Dune::IntersectionIterator< const Grid, LeafIntersectionIteratorImpl, LeafIntersectionImpl > LeafIntersectionIterator;

      typedef IdGridIntersectionIterator< const Grid, typename HostGrid::LevelIntersectionIterator > LevelIntersectionIteratorImpl;
      typedef Dune::IntersectionIterator< const Grid, LevelIntersectionIteratorImpl, LevelIntersectionImpl > LevelIntersectionIterator;

      typedef IdGridIterator< const Grid, typename HostGrid::HierarchicIterator > HierarchicIteratorImpl;
      typedef Dune::EntityIterator< 0, const Grid, HierarchicIteratorImpl > HierarchicIterator;

      template< int codim >
      struct Codim
      {
        typedef typename ReferenceCubeContainer::template Codim< codim >::ReferenceCube ReferenceCube;

        typedef SPLocalGeometry< dimension-codim, dimension, const Grid > LocalGeometryImpl;

        typedef CartesianGridGeometry< dimension-codim, dimensionworld, const Grid >    GeometryImpl;
        typedef Dune::Geometry< dimension-codim, dimensionworld, const Grid, CartesianGridGeometry >  Geometry;
        typedef Dune::Geometry< dimension-codim, dimension,      const Grid, LocalGeometryReference > LocalGeometry;

        typedef CartesianGridEntity< codim, dimension, const Grid > EntityImpl;
        typedef Dune::Entity< codim, dimension, const Grid, CartesianGridEntity > Entity;

#if ! DUNE_VERSION_NEWER(DUNE_GRID, 2, 5)
        typedef Entity      EntityPointer;
#endif

        typedef IdGridEntitySeed< codim, const Grid > EntitySeedImpl;
        typedef Dune::EntitySeed< const Grid, EntitySeedImpl > EntitySeed;

        template< PartitionIteratorType pitype >
        struct Partition
        {
          typedef typename HostGrid::template Codim< codim >::template Partition< pitype > HostPartition;

          typedef IdGridIterator< const Grid, typename HostPartition::LeafIterator > LeafIteratorImpl;
          typedef Dune::EntityIterator< codim, const Grid, LeafIteratorImpl > LeafIterator;

          typedef IdGridIterator< const Grid, typename HostPartition::LevelIterator > LevelIteratorImpl;
          typedef Dune::EntityIterator< codim, const Grid, LevelIteratorImpl > LevelIterator;
        };

        typedef typename Partition< All_Partition >::LeafIterator LeafIterator;
        typedef typename Partition< All_Partition >::LevelIterator LevelIterator;
      };

      typedef IdGridIndexSet< const Grid, typename HostGrid::Traits::LeafIndexSet >  LeafIndexSet;
      typedef IdGridIndexSet< const Grid, typename HostGrid::Traits::LevelIndexSet > LevelIndexSet;

      typedef IdGridIdSet< const Grid, typename HostGrid::Traits::GlobalIdSet > GlobalIdSet;
      typedef IdGridIdSet< const Grid, typename HostGrid::Traits::LocalIdSet >  LocalIdSet;

      typedef typename HostGrid::Traits::Communication Communication;

      template< PartitionIteratorType pitype >
      struct Partition
      {
        typedef Dune::GridView< CartesianGridViewTraits< typename HostGrid::LeafGridView,  pitype > > LeafGridView;
        typedef Dune::GridView< CartesianGridViewTraits< typename HostGrid::LevelGridView, pitype > > LevelGridView;
      };

      typedef typename Partition< All_Partition > :: LeafGridView  LeafGridView;
      typedef typename Partition< All_Partition > :: LevelGridView LevelGridView;
    };
  };



  // CartesianGridObjectStream
  // -------------------------

  template< class HostGrid, bool hasObjectStream >
  struct CartesianGridObjectStream;

  template< class HostGrid >
  struct CartesianGridObjectStream< HostGrid, false >
  {
    // dummy typedef
    typedef int ObjectStreamType;
  };

  template< class HostGrid >
  struct CartesianGridObjectStream< HostGrid, true >
  : public HasObjectStream
  {
    typedef typename HostGrid::InStreamType InStreamType;
    typedef typename HostGrid::OutStreamType OutStreamType;
  };



  // CartesianGrid
  // -------------

  /** \class CartesianGrid
   *  \brief identical grid wrapper
   *  \ingroup CartesianGrid
   *
   *  \tparam  HostGrid   DUNE grid to be wrapped (called host grid)
   *
   *  \nosubgrouping
   */
  template< class HostGrid >
  class CartesianGrid
  /** \cond */
  : public GridDefaultImplementation
      < HostGrid::dimension, HostGrid::dimensionworld, typename HostGrid::ctype, CartesianGridFamily< HostGrid > >,
    public CartesianGridExportParams< HostGrid >,
    public CartesianGridObjectStream< HostGrid, std::is_base_of< HasObjectStream, HostGrid >::value >
  /** \endcond */
  {
    typedef CartesianGrid< HostGrid > Grid;

    typedef GridDefaultImplementation
      < HostGrid::dimension, HostGrid::dimensionworld, typename HostGrid::ctype, CartesianGridFamily< HostGrid > >
      Base;

    template< int, int, class > friend class CartesianGridEntity;
    template< class, class > friend class CartesianGridIntersection;
    template< class, class > friend class CartesianGridIntersectionIterator;
    template< class, class > friend class CartesianGridIdSet;
    template< class, class > friend class CartesianGridIndexSet;
    template< class, PartitionIteratorType > friend class CartesianGridView;
    template< class > friend class HostGridAccess;
    template< class > friend class DGFGridFactory;

    template< class, class > friend class CartesianGridDataHandle;

  public:
    /** \cond */
    typedef CartesianGridFamily< HostGrid > GridFamily;
    /** \endcond */

    /** \name Traits
     *  \{ */

    //! type of the grid traits
    typedef typename GridFamily::Traits Traits;

    /** \brief traits structure containing types for a codimension
     *
     *  \tparam codim  codimension
     *
     *  \nosubgrouping
     */
    template< int codim >
    struct Codim
    : public Base::template Codim< codim >
    {
      typedef typename Traits::template Codim< codim >::LocalGeometryImpl LocalGeometryImpl;
    };

    /** \} */

    /** \name Iterator Types
     *  \{ */

    //! iterator over the grid hierarchy
    typedef typename Traits::HierarchicIterator HierarchicIterator;
    //! iterator over intersections with other entities on the leaf level
    typedef typename Traits::LeafIntersectionIterator LeafIntersectionIterator;
    //! iterator over intersections with other entities on the same level
    typedef typename Traits::LevelIntersectionIterator LevelIntersectionIterator;

    /** \} */

    /** \name Grid View Types
     *  \{ */

    /** \brief Types for GridView */
    template< PartitionIteratorType pitype >
    struct Partition
    {
      typedef typename GridFamily::Traits::template Partition< pitype >::LevelGridView LevelGridView;
      typedef typename GridFamily::Traits::template Partition< pitype >::LeafGridView LeafGridView;
    };

    /** \brief View types for All_Partition */
    typedef typename Partition< All_Partition >::LevelGridView LevelGridView;
    typedef typename Partition< All_Partition >::LeafGridView LeafGridView;

    /** \} */

    /** \name Index and Id Set Types
     *  \{ */

    /** \brief type of leaf index set
     *
     *  The index set assigns consecutive indices to the entities of the
     *  leaf grid. The indices are of integral type and can be used to access
     *  arrays.
     *
     *  The leaf index set is a model of Dune::IndexSet.
     */
    typedef typename Traits::LeafIndexSet LeafIndexSet;

    /** \brief type of level index set
     *
     *  The index set assigns consecutive indices to the entities of a grid
     *  level. The indices are of integral type and can be used to access
     *  arrays.
     *
     *  The level index set is a model of Dune::IndexSet.
     */
    typedef typename Traits::LevelIndexSet LevelIndexSet;

    /** \brief type of global id set
     *
     *  The id set assigns a unique identifier to each entity within the
     *  grid. This identifier is unique over all processes sharing this grid.
     *
     *  \note Id's are neither consecutive nor necessarily of an integral
     *        type.
     *
     *  The global id set is a model of Dune::IdSet.
     */
    typedef typename Traits::GlobalIdSet GlobalIdSet;

    /** \brief type of local id set
     *
     *  The id set assigns a unique identifier to each entity within the
     *  grid. This identifier needs only to be unique over this process.
     *
     *  Though the local id set may be identical to the global id set, it is
     *  often implemented more efficiently.
     *
     *  \note Ids are neither consecutive nor necessarily of an integral
     *        type.
     *  \note Local ids need not be compatible with global ids. Also, no
     *        mapping from local ids to global ones needs to exist.
     *
     *  The global id set is a model of Dune::IdSet.
     */
    typedef typename Traits::LocalIdSet LocalIdSet;

    /** \} */

    /** \name Miscellaneous Types
     * \{ */

    //! type of vector coordinates (e.g., double)
    typedef typename Traits::ctype ctype;

    static const int dimension = Traits::dimension;
    static const int dimensionworld = Traits::dimensionworld;

    //! communicator with all other processes having some part of the grid
    typedef typename Traits::Communication Communication;

    /** \} */

  private:
    typedef typename Traits::ReferenceCubeContainer ReferenceCubeContainer;
    typedef typename Traits::ReferenceCube ReferenceCube;
    typedef SPGeometricGridLevel< ctype, dimension > GridLevel;

    static const int numDirections = GridLevel::numDirections;

    enum { MAXL = 32 };

  public:
    /** \name Construction and Destruction
     *  \{ */

    /** \brief constructor
     *
     *  The references to host grid and coordinate function are stored in the
     *  grid. Therefore, they must remain valid until the grid is destroyed.
     *
     *  \param[in]  hostGrid       reference to the grid to wrap
     */
    explicit CartesianGrid ( HostGrid &hg )
    : hostGrid_( &hg ),
      removeHostGrid_( false ),
      gridLevels_(),
      levelIndexSets_( MAXL, nullptr ),
      leafIndexSet_( 0 ),
      globalIdSet_( 0 ),
      localIdSet_( 0 )
    {
      initGridLevels();
      buildLocalIntersectionGeometries();
      buildGeometryInFather();
    }

    /** \brief constructor
     *
     *  The references to host grid and coordinate function are stored in the
     *  grid. Therefore, they must remain valid until the grid is destroyed.
     *
     *  \param[in]  hostGrid       pointer to the grid to wrap
     *
     *  \note The host grid pointer will be deleted upon destruction of the
     *        CartesianGrid.
     */
    explicit CartesianGrid ( HostGrid *hg )
    : hostGrid_( hg ),
      removeHostGrid_( true ),
      gridLevels_(),
      levelIndexSets_( MAXL, nullptr ),
      leafIndexSet_( 0 ),
      globalIdSet_( 0 ),
      localIdSet_( 0 )
    {
      initGridLevels();
      buildLocalIntersectionGeometries();
      buildGeometryInFather();
    }

    /** \brief default constructor
     *
     *  The references to host grid and coordinate function are stored in the
     *  grid. Therefore, they must remain valid until the grid is destroyed.
     *
     *  \param[in]  hostGrid       pointer to the grid to wrap
     *
     *  \note The host grid pointer will be deleted upon destruction of the
     *        CartesianGrid.
     */
    CartesianGrid ()
    : hostGrid_( new HostGrid() ), // create empty host grid, needed e.g. for comm obj
      gridLevels_(),
      removeHostGrid_( true ),
      levelIndexSets_( MAXL, nullptr ),
      leafIndexSet_( 0 ),
      globalIdSet_( 0 ),
      localIdSet_( 0 )
    {
      buildLocalIntersectionGeometries();
      buildGeometryInFather();
    }

    //! init grid levels
    void initGridLevels ( )
    {
      // get grid width in each direction
      FieldVector< ctype, dimensionworld > h = gridWidth();

      gridLevels_.resize( MAXL, (GridLevel* ) 0 );
      // create grid levels
      for( int level = 0; level < MAXL; ++level)
      {
        gridLevels_[ level ] = new GridLevel( refCubes_, h );
        h *= 0.5 ;
      }
    }

    /** \brief destructor
     */
    ~CartesianGrid ()
    {
      // TODO: Delete local geometries

      if( localIdSet_ )
        delete localIdSet_;
      if( globalIdSet_ )
        delete globalIdSet_;

      if( leafIndexSet_ )
        delete leafIndexSet_;

      for( unsigned int i = 0; i < levelIndexSets_.size(); ++i )
      {
        if( levelIndexSets_[ i ] )
          delete( levelIndexSets_[ i ] );
      }

      for( unsigned int i = 0; i < gridLevels_.size(); ++i )
      {
        if( gridLevels_[ i ] )
          delete( gridLevels_[ i ] );
      }

      if( removeHostGrid_ )
        delete hostGrid_;
    }

    /** \} */

    /** \name Size Methods
     *  \{ */

    /** \brief obtain maximal grid level
     *
     *  Grid levels are numbered 0, ..., L, where L is the value returned by
     *  this method.
     *
     *  \returns maximal grid level
     */
    int maxLevel () const
    {
      return hostGrid().maxLevel();
    }

    /** \brief obtain number of entites on a level
     *
     *  \param[in]  level  level to consider
     *  \param[in]  codim  codimension to consider
     *
     *  \returns number of entities of codimension \em codim on grid level
     *           \em level.
     */
    int size ( int level, int codim ) const
    {
      return hostGrid().size( level, codim );
    }

    /** \brief obtain number of leaf entities
     *
     *  \param[in]  codim  codimension to consider
     *
     *  \returns number of leaf entities of codimension \em codim
     */
    int size ( int codim ) const
    {
      return hostGrid().size( codim );
    }

    /** \brief obtain number of entites on a level
     *
     *  \param[in]  level  level to consider
     *  \param[in]  type   geometry type to consider
     *
     *  \returns number of entities with a geometry of type \em type on grid
     *           level \em level.
     */
    int size ( int level, GeometryType type ) const
    {
      return hostGrid().size( level, type );
    }

    /** \brief returns the number of boundary segments within the macro grid
     *
     *  \returns number of boundary segments within the macro grid
     */
    int size ( GeometryType type ) const
    {
      return hostGrid().size( type );
    }

    /** \brief obtain number of leaf entities
     *
     *  \param[in]  type   geometry type to consider
     *
     *  \returns number of leaf entities with a geometry of type \em type
     */
    size_t numBoundarySegments () const
    {
      return hostGrid().numBoundarySegments( );
    }
    /** \} */

    template< int codim >
    typename Codim< codim >::LevelIterator lbegin ( int level ) const
    {
      return lbegin< codim, All_Partition >( level );
    }

    template< int codim >
    typename Codim< codim >::LevelIterator lend ( int level ) const
    {
      return lend< codim, All_Partition >( level );
    }

    template< int codim, PartitionIteratorType pitype >
    typename Codim< codim >::template Partition< pitype >::LevelIterator
    lbegin ( int level ) const
    {
      typedef typename Traits::template Codim< codim >::template Partition< pitype >::LevelIteratorImpl Impl;
      return Impl( extraData(), hostGrid().template lbegin< codim, pitype >( level ) );
    }

    template< int codim, PartitionIteratorType pitype >
    typename Codim< codim >::template Partition< pitype >::LevelIterator
    lend ( int level ) const
    {
      typedef typename Traits::template Codim< codim >::template Partition< pitype >::LevelIteratorImpl Impl;
      return Impl( extraData(), hostGrid().template lend< codim, pitype >( level ) );
    }

    template< int codim >
    typename Codim< codim >::LeafIterator leafbegin () const
    {
      return leafbegin< codim, All_Partition >();
    }

    template< int codim >
    typename Codim< codim >::LeafIterator leafend () const
    {
      return leafend< codim, All_Partition >();
    }

    template< int codim, PartitionIteratorType pitype >
    typename Codim< codim >::template Partition< pitype >::LeafIterator
    leafbegin () const
    {
      typedef typename Traits::template Codim< codim >::template Partition< pitype >::LeafIteratorImpl Impl;
      return Impl( extraData(), hostGrid().template leafbegin< codim, pitype >() );
    }

    template< int codim, PartitionIteratorType pitype >
    typename Codim< codim >::template Partition< pitype >::LeafIterator
    leafend () const
    {
      typedef typename Traits::template Codim< codim >::template Partition< pitype >::LeafIteratorImpl Impl;
      return Impl( extraData(), hostGrid().template leafend< codim, pitype >() );
    }

    const GlobalIdSet &globalIdSet () const
    {
      if( !globalIdSet_ )
        globalIdSet_ = new GlobalIdSet( hostGrid().globalIdSet() );
      assert( globalIdSet_ );
      return *globalIdSet_;
    }

    const LocalIdSet &localIdSet () const
    {
      if( !localIdSet_ )
        localIdSet_ = new LocalIdSet( hostGrid().localIdSet() );
      assert( localIdSet_ );
      return *localIdSet_;
    }

    const LevelIndexSet &levelIndexSet ( int level ) const
    {
      if( (level < 0) || (level > maxLevel()) )
      {
        DUNE_THROW( GridError, "LevelIndexSet for nonexisting level " << level
                               << " requested." );
      }

      LevelIndexSet *&levelIndexSet = levelIndexSets_[ level ];
      if( !levelIndexSet )
        levelIndexSet = new LevelIndexSet( hostGrid().levelIndexSet( level ) );
      assert( levelIndexSet );
      return *levelIndexSet;
    }

    const LeafIndexSet &leafIndexSet () const
    {
      if( !leafIndexSet_ )
        leafIndexSet_ = new LeafIndexSet( hostGrid().leafIndexSet() );
      assert( leafIndexSet_ );
      return *leafIndexSet_;
    }

    void globalRefine ( int refCount )
    {
      hostGrid().globalRefine( refCount );
      // update overall status
      update();
    }

    bool mark ( int refCount, const typename Codim< 0 >::Entity &entity )
    {
      return hostGrid().mark( refCount, getHostEntity< 0 >( entity ) );
    }

    int getMark ( const typename Codim< 0 >::Entity &entity ) const
    {
      return hostGrid().getMark( getHostEntity< 0 >( entity ) );
    }

    bool preAdapt ()
    {
      return hostGrid().preAdapt();
    }

    bool adapt ()
    {
      bool ret = hostGrid().adapt();
      update();
      return ret;
    }

    /** \brief  @copydoc Dune::Grid::adapt()
        \param handle handler for restriction and prolongation operations
        which is a Model of the AdaptDataHandleInterface class.
    */
    template< class GridImp, class DataHandle >
    bool adapt ( AdaptDataHandleInterface< GridImp, DataHandle > &datahandle )
    {
      typedef IdGridAdaptDataHandle< Grid,
              AdaptDataHandleInterface< GridImp, DataHandle > > WrappedDataHandle;

      WrappedDataHandle wrappedDataHandle( extraData(), datahandle );
      const bool ret = hostGrid().adapt( wrappedDataHandle );
      update();
      return ret;
    }

    void postAdapt ()
    {
      hostGrid().postAdapt();
    }

    /** \name Parallel Data Distribution and Communication Methods
     *  \{ */

    /** \brief obtain size of overlap region for the leaf grid
     *
     *  \param[in]  codim  codimension for with the information is desired
     */
    int overlapSize ( int codim ) const
    {
      return hostGrid().overlapSize( codim );
    }

    /** \brief obtain size of ghost region for the leaf grid
     *
     *  \param[in]  codim  codimension for with the information is desired
     */
    int ghostSize( int codim ) const
    {
      return hostGrid().ghostSize( codim );
    }

    /** \brief obtain size of overlap region for a grid level
     *
     *  \param[in]  level  grid level (0, ..., maxLevel())
     *  \param[in]  codim  codimension (0, ..., dimension)
     */
    int overlapSize ( int level, int codim ) const
    {
      return hostGrid().overlapSize( level, codim );
    }

    /** \brief obtain size of ghost region for a grid level
     *
     *  \param[in]  level  grid level (0, ..., maxLevel())
     *  \param[in]  codim  codimension (0, ..., dimension)
     */
    int ghostSize ( int level, int codim ) const
    {
      return hostGrid().ghostSize( level, codim );
    }

    /** \brief communicate information on a grid level
     *
     *  \param      datahandle  communication data handle (user defined)
     *  \param[in]  interface   communication interface (one of
     *                          InteriorBorder_InteriorBorder_Interface,
     *                          InteriorBorder_All_Interface,
     *                          Overlap_OverlapFront_Interface,
     *                          Overlap_All_Interface,
     *                          All_All_Interface)
     *  \param[in]  direction   communication direction (one of
     *                          ForwardCommunication or BackwardCommunication)
     *  \param[in]  level       grid level to communicate
     */
    template< class DataHandle, class Data >
    void communicate ( CommDataHandleIF< DataHandle, Data > &dataHandle,
                       InterfaceType interface,
                       CommunicationDirection direction,
                       int level ) const
    {
      levelGridView( level ).communicate( dataHandle, interface, direction );
    }

    /** \brief communicate information on leaf entities
     *
     *  \param      datahandle  communication data handle (user defined)
     *  \param[in]  interface   communication interface (one of
     *                          InteriorBorder_InteriorBorder_Interface,
     *                          InteriorBorder_All_Interface,
     *                          Overlap_OverlapFront_Interface,
     *                          Overlap_All_Interface,
     *                          All_All_Interface)
     *  \param[in]  direction   communication direction (one of
     *                          ForwardCommunication, BackwardCommunication)
     */
    template< class DataHandle, class Data >
    void communicate ( CommDataHandleIF< DataHandle, Data > &dataHandle,
                       InterfaceType interface,
                       CommunicationDirection direction ) const
    {
      leafGridView().communicate( dataHandle, interface, direction );
    }

    /** \brief obtain Communication object
     *
     *  The Communication object should be used to globally
     *  communicate information between all processes sharing this grid.
     *
     *  \note The Communication object returned is identical to the
     *        one returned by the host grid.
     */
    const Communication &comm () const
    {
      return hostGrid().comm();
    }

    // data handle interface different between geo and interface

    /** \brief rebalance the load each process has to handle
     *
     *  A parallel grid is redistributed such that each process has about
     *  the same load (e.g., the same number of leaf entites).
     *
     *  \note DUNE does not specify, how the load is measured.
     *
     *  \returns \b true, if the grid has changed.
     */
    bool loadBalance ()
    {
      const bool gridChanged = hostGrid().loadBalance();
      if( gridChanged )
        update();
      return gridChanged;
    }

    /** \brief rebalance the load each process has to handle
     *
     *  A parallel grid is redistributed such that each process has about
     *  the same load (e.g., the same number of leaf entites).
     *
     *  The data handle is used to communicate the data associated with
     *  entities that move from one process to another.
     *
     *  \note DUNE does not specify, how the load is measured.
     *
     *  \param  datahandle  communication data handle (user defined)
     *
     *  \returns \b true, if the grid has changed.
     */
    template< class DataHandle, class Data >
    bool loadBalance ( CommDataHandleIF< DataHandle, Data > &datahandle )
    {
      typedef CommDataHandleIF< DataHandle, Data > DataHandleIF;
      typedef IdGridDataHandle< Grid, DataHandleIF > WrappedDataHandle;

      WrappedDataHandle wrappedDataHandle( extraData(), datahandle );
      const bool gridChanged = hostGrid().loadBalance( wrappedDataHandle );
      if ( gridChanged )
        update();
      return gridChanged;
    }

    /** \brief View for a grid level */
    template< PartitionIteratorType pitype >
    typename Partition< pitype >::LevelGridView levelGridView ( int level ) const
    {
      typedef typename Partition< pitype >::LevelGridView View;
      typedef typename View::GridViewImp ViewImp;
      return View( ViewImp( *this, hostGrid().template levelGridView< pitype >( level ) ) );
    }

    /** \brief View for the leaf grid */
    template< PartitionIteratorType pitype >
    typename Partition< pitype >::LeafGridView leafGridView () const
    {
      typedef typename Traits::template Partition< pitype >::LeafGridView View;
      typedef typename View::GridViewImp ViewImp;
      return View( ViewImp( *this, hostGrid().template leafGridView< pitype >() ) );
    }

    /** \brief View for a grid level for All_Partition */
    LevelGridView levelGridView ( int level ) const
    {
      typedef typename LevelGridView::GridViewImp ViewImp;
      return LevelGridView( ViewImp( *this, hostGrid().levelGridView( level ) ) );
    }

    /** \brief View for the leaf grid for All_Partition */
    LeafGridView leafGridView() const
    {
      typedef typename LeafGridView::GridViewImp ViewImp;
      return LeafGridView( ViewImp( *this, hostGrid().leafGridView() ) );
    }

    /** \brief View for a grid level */
    template< PartitionIteratorType pitype >
    typename Partition< pitype >::LevelGridView levelView ( int level ) const
    {
      typedef typename Partition< pitype >::LevelGridView View;
      typedef typename View::GridViewImp ViewImp;
      return View( ViewImp( *this, hostGrid().template levelGridView< pitype >( level ) ) );
    }

    /** \brief View for the leaf grid */
    template< PartitionIteratorType pitype >
    typename Partition< pitype >::LeafGridView leafView () const
    {
      typedef typename Traits::template Partition< pitype >::LeafGridView View;
      typedef typename View::GridViewImp ViewImp;
      return View( ViewImp( *this, hostGrid().template leafGridView< pitype >() ) );
    }

    /** \brief View for a grid level for All_Partition */
    LevelGridView levelView ( int level ) const
    {
      typedef typename LevelGridView::GridViewImp ViewImp;
      return LevelGridView( ViewImp( *this, hostGrid().levelGridView( level ) ) );
    }

    /** \brief View for the leaf grid for All_Partition */
    LeafGridView leafView() const
    {
      typedef typename LeafGridView::GridViewImp ViewImp;
      return LeafGridView( ViewImp( *this, hostGrid().leafGridView() ) );
    }

    /** \brief obtain Entity from EntitySeed. */
    template< class EntitySeed >
    typename Traits::template Codim< EntitySeed::codimension >::Entity
    entity ( const EntitySeed &seed ) const
    {
      typedef typename Traits::template Codim< EntitySeed::codimension >::EntityImpl EntityImpl;
      return EntityImpl( extraData(), hostGrid().entity( seed.impl().hostEntitySeed() ) );
    }

#if ! DUNE_VERSION_NEWER(DUNE_GRID, 2, 5)
    /** \brief obtain Entity from EntitySeed. */
    template< class EntitySeed >
    typename Traits::template Codim< EntitySeed::codimension >::Entity
    entityPointer ( const EntitySeed &seed ) const
    {
      return entity( seed );
    }
#endif

    /** \} */

    typedef typename Traits :: ExtraData  ExtraData ;
    //! return pointer to grid for access of interface classes to some grid information
    ExtraData extraData () const { return this; }

    /** \name Miscellaneous Methods
     *  \{ */

    const HostGrid &hostGrid () const
    {
      return *hostGrid_;
    }

    HostGrid &hostGrid ()
    {
      return *hostGrid_;
    }

    const ReferenceCube &referenceCube () const
    {
      return refCubes_.get();
    }

    template< int codim >
    const typename Traits::template Codim< codim >::ReferenceCube &
    referenceCube () const
    {
      return refCubes_.template get< codim >();
    }

    //! return geometric information of given level
    const GridLevel &geometricLevel ( const size_t level ) const
    {
      // if this asserstion fails the gridLevels have not been initialized
      assert( gridLevels_.size() == MAXL );
      assert( level < gridLevels_.size() );
      assert( gridLevels_[ level ] );
      return *(gridLevels_[ level ]);
    }

    /** \brief update grid caches
     *
     *  This method has to be called whenever the underlying host grid changes.
     *
     *  \note If you adapt the host grid through this geometry grid's
     *        adaptation or load balancing methods, update is automatically
     *        called.
     */
    //- --update
    void update ()
    {
      const int newNumLevels = maxLevel()+1;

      for( int i = newNumLevels; i < MAXL; ++i )
      {
        if( levelIndexSets_[ i ] != 0 )
        {
          delete levelIndexSets_[ i ];
          levelIndexSets_[ i ] = 0;
        }
      }
    }

    /** \} */

  protected:
    FieldVector< ctype, dimensionworld > gridWidth () const
    {
      typedef typename HostGrid::template Codim< 0 >::Geometry HostGeometry;
      typedef typename HostGeometry::GlobalCoordinate GlobalCoordinate;

      GlobalCoordinate h( 0 );

      typedef typename HostGrid::template Codim< 0 >::LevelIterator MacroIterator;
      MacroIterator it = hostGrid().template lbegin< 0 >( 0 );
      if( it != hostGrid().template lend< 0 >( 0 ) )
      {
        if( it->type().id() != Capabilities::hasSingleGeometryType< Grid >::topologyId )
          DUNE_THROW( InvalidStateException, "HostGrid has wrong geometry type " << it->type() );

        const HostGeometry &geo = it->geometry();
        const GlobalCoordinate zero = geo.corner( 0 );
        for(int d = 0; d < dimensionworld; ++d )
          h[ d ] = std::abs( geo.corner( 1 << d )[ d ] - zero[ d ] );
      }

      // get max grid width for all processes (for macro level)
      hostGrid().comm().max( &h[0], dimensionworld );

      return h;
    }

    void buildLocalIntersectionGeometries ()
    {
      typedef typename Codim< 1 >::LocalGeometryImpl LocalGeometryImpl;
      typedef typename LocalGeometryImpl::GlobalCoordinate Coordinate;
      typedef SPDirection< dimension > Direction ;

      for( int face = 0; face < ReferenceCube::numFaces; ++face )
      {
        const unsigned int direction = ((1 << dimension) - 1) ^ (1 << (face/2));
        Coordinate origin( ctype( 0 ) );
        origin[ face/2 ] = ctype( face & 1 );
        const SPGeometryCache< ctype, dimension, 1 > cache( Coordinate( 1.0 ), Direction( direction ) );
        localFaceGeometry_[ face ] = new LocalGeometryImpl( cache, origin );

        for( int index = 0; index < (1 << (dimension-1)); ++index )
        {
          // origin[ face/2 ] = ctype( face & 1 );
          for( int i = 0; i < dimension-1; ++i )
          {
            const int j = (i < face/2 ? i : i+1 );
            origin[ j ] = 0.5 * ((index >> i) & 1);
          }
          const SPGeometryCache< ctype, dimension,1 > cache( Coordinate( 0.5 ), Direction( direction ) );
          localRefinedFaceGeometry_[ (1 << (dimension-1))*face + index ] = new LocalGeometryImpl( cache, origin );
        }
      }
    }

    void buildGeometryInFather ()
    {
      typedef typename Codim< 0 >::LocalGeometryImpl LocalGeometryImpl;
      typedef typename LocalGeometryImpl::GlobalCoordinate Coordinate;
      typedef SPDirection< dimension > Direction ;

      const unsigned int numChildren = (1 << dimension);
      const SPGeometryCache< ctype, dimension, 0 > cache( Coordinate( 0.5 ), Direction( numDirections-1 ) );
      for( unsigned int index = 0; index < numChildren; ++index )
      {
        Coordinate origin;
        for( int i = 0; i < dimension; ++i )
          origin[ i ] = 0.5 * ((index >> i) & 1);
        geometryInFather_[ index ] = new LocalGeometryImpl( cache, origin );
      }
    }

  public:
    template< int codim >
    static const typename HostGrid::template Codim< codim >::Entity &
    getHostEntity( const typename Codim< codim >::Entity &entity )
    {
      return entity.impl().hostEntity();
    }

  protected:
    HostGrid *const hostGrid_;
    bool removeHostGrid_;
    ReferenceCubeContainer refCubes_;
    std::vector< GridLevel * > gridLevels_;
    mutable std::vector< LevelIndexSet * > levelIndexSets_;
    mutable LeafIndexSet *leafIndexSet_;
    mutable GlobalIdSet *globalIdSet_;
    mutable LocalIdSet *localIdSet_;

    const typename Codim< 0 >::LocalGeometryImpl *geometryInFather_[ 1 << dimension ];
    const typename Codim< 1 >::LocalGeometryImpl *localFaceGeometry_[ ReferenceCube::numFaces ];
    const typename Codim< 1 >::LocalGeometryImpl *localRefinedFaceGeometry_[ (1 << (dimension-1))*ReferenceCube::numFaces ];
  };

} // namespace Dune

#endif // #if HAVE_DUNE_SPGRID

#include <dune/grid/cartesiangrid/twistutility.hh>

#endif // #ifndef DUNE_CARTESIANGRID_GRID_HH
