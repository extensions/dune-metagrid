#ifndef DUNE_CARTESIANGRID_HOSTGRIDINFO_HH
#define DUNE_CARTESIANGRID_HOSTGRIDINFO_HH

#include <dune/grid/cartesiangrid/hostgridinfo/generic.hh>
#include <dune/grid/cartesiangrid/hostgridinfo/alugrid.hh>

#endif // #ifndef DUNE_CARTESIANGRID_HOSTGRIDINFO_HH
