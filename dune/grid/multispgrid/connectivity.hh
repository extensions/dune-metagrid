#ifndef DUNE_MULTISPGRID_CONNECTIVITY_HH
#define DUNE_MULTISPGRID_CONNECTIVITY_HH

#include <algorithm>
#include <cassert>
#include <vector>

#include <dune/common/array.hh>
#include <dune/common/typetraits.hh>

#include <dune/geometry/referenceelements.hh>

#include <dune/grid/common/gridenums.hh>

#if HAVE_DUNE_SPGRID
#include "geometry.hh"
#include "utility.hh"

namespace Dune
{

  // Forward declaration
  // -------------------

  template< int, int, class > class MultiSPGeometry;



  // MultiSPMacroConnectivity
  // ------------------------

  template< class Grid >
  class MultiSPMacroConnectivity
  {
    // this type
    typedef MultiSPMacroConnectivity< Grid > This;
    // traits class
    typedef typename std::remove_const< Grid >::type::Traits Traits;

  public:
    //! \brief single coordinate type
    typedef typename Traits::ctype ctype;
    // \brief dimension
    static const int dimension = Traits::dimension;
    //! \brief number of faces
    static const int numFaces = 2*dimension;
    // host grid type
    typedef typename Traits::HostGrid HostGrid;

  private:
    struct CoordinateIndexSet;
    struct GlobalGeometry;

  public:
    //! \brief constructor
    explicit MultiSPMacroConnectivity ( const std::vector< HostGrid * > &hostGrids )
    : nodes_( hostGrids.size() )
    {
      // get number of nodes
      const size_t nodes = This::nodes();

      // initialize data
      for( size_t node = 0; node < nodes; ++node )
        nodes_[ node ].fill( -1 );

      // get faces
      typedef std::vector< int > Face;
      std::vector< array< Face, numFaces > > faces( nodes );
      for( size_t node = 0; node < nodes; ++node )
      {
        const HostGrid &hostGrid = *( hostGrids[ node ] );
        GlobalGeometry geometry( hostGrid );
        for( int face = 0; face < numFaces; ++face )
          faces[ node ][ face ] = getFace( face, geometry );
      }

      // check for common faces
      for( int face = 0; face < numFaces; ++face )
      {
        for( size_t i = 0; i < nodes; ++i )
        {
          for( size_t j= i+1; j < nodes; ++j )
          {
            if( faces[ i ][ face ] == faces[ j ][ opposite( face ) ] )
            {
              if( nodes_[ i ][ face ] != -1 || nodes_[ j ][ opposite( face ) ] != -1 )
                DUNE_THROW( InvalidStateException, "More than two nodes share common face" );
              nodes_[ i ][ face ] = j;
              nodes_[ j ][ opposite( face ) ] = i;
            }
          }
        }
      }
    }

    //! \brief return number of nodes
    size_t nodes () const
    {
      return nodes_.size();
    }

    //! \brief return number of faces
    int faces () const
    {
      return numFaces;
    }

    //! \brief return true if node is at boundary
    bool boundary ( const int face, const int node ) const
    {
      assert( face >= 0 && face < faces() );
      assert( node >= 0 && node < int( nodes() ) );
      return ( nodes_[ node ][ face ] < 0 );
    }

    //! \brief return true if node has neighbor
    bool hasNeighbor ( const int face, const int node ) const
    {
      return !boundary( face, node );
    }

    //! \brief return neighbor node at given face
    int neighbor ( const int face, const int node ) const
    {
      assert( hasNeighbor( face, node ) );
      return nodes_[ node ][ face ];
    }

  private:
    // hide copy constructor
    MultiSPMacroConnectivity ( const This &other );
    // hide assignment operator
    This &operator= ( const This &other );

    // get opposite face
    int opposite ( int face ) const
    {
      return (face ^ 1);
    }

    // get i-th face of geometry
    std::vector< int > getFace ( int face, const GlobalGeometry &geometry ) const
    {
      std::vector< int > indices;
      int corners = ( 1 << (dimension-1) );
      for( int i = 0; i < corners; ++i )
        indices.push_back( coordIndexSet_[ geometry.corner( i, face ) ] );
      std::sort( indices.begin(), indices.end() );
      return indices;
    }

    std::vector< array< int, numFaces > > nodes_;
    mutable CoordinateIndexSet coordIndexSet_;
  };



  // Implementation of MultiSPMacroConnectivity::CoordinateIndexSet
  // ------------------------------------------------------------

  template< class Grid >
  class MultiSPMacroConnectivity< Grid >::CoordinateIndexSet
  {
    // this type
    typedef CoordinateIndexSet This;

    // map
    typedef MultiSPCoordinateMap< int, ctype, dimension > Map;

  public:
    //! \brief coordinate type
    typedef typename Map::Coordinate GlobalVector;

    //! \brief constructor
    explicit CoordinateIndexSet ( double tolerance = 1e-8 )
    : map_( tolerance )
    {}

    //! \brief return index of x
    int &operator[] ( const GlobalVector &x )
    {
      if( map_.find( x ) != map_.end() )
        return map_[ x ];
      else
        return ( map_[ x ] = map_.size() );
    }

  private:
    Map map_;
  };



  // Implementation of MultiSPMacroConnectivity::GlobalGeometry
  // --------------------------------------------------------

  template< class Grid >
  struct MultiSPMacroConnectivity< Grid >::GlobalGeometry
  {
    //! \brief single coordinate type
    typedef typename HostGrid::ctype ctype;
    //! \brief dimension
    static const int dimension = HostGrid::dimension;
    //! \brief vector type
    typedef FieldVector< ctype, dimension > GlobalVector;

  private:
    typedef MultiSPGeometry< dimension, dimension, Grid > GeometryImp;

  public:
    //! \brief constructor
    explicit GlobalGeometry ( const HostGrid &hostGrid )
    : geometryImp_( init( hostGrid ) )
    {}

    //! \brief return i-th corner of given face
    GlobalVector corner ( int i, int face ) const
    {
      const ReferenceElement< ctype, dimension > &referenceCube
        = ReferenceElements< ctype, dimension >::cube();
      int subEntity = referenceCube.subEntity( face, 1, i, dimension );
      return geometryImp_.corner( subEntity );
    }

  private:
    // initialize geometry for given host grid
    static GeometryImp init ( const HostGrid &hostGrid )
    {
      // get macro grid level
      const typename HostGrid::GridLevel &gridLevel = hostGrid.gridLevel( 0 );

      // get cartesian mesh
      typedef typename HostGrid::Domain Domain;
      const Domain &domain = gridLevel.domain();

      // get macro cube
      typedef typename Domain::Cube Cube;
      const Cube &cube = domain.cube();

      // create geometry
      GlobalVector origin = cube.origin();
      GlobalVector width = cube.width();
      unsigned int direction( 0 );
      typename GeometryImp::GeometryCache geometryCache( width, direction );
      return GeometryImp( origin, geometryCache );
    }

    GeometryImp geometryImp_;
  };



  // MultiSPLeafConnectivity
  // -----------------------

  template< PartitionIteratorType pitype, class Grid >
  class MultiSPLeafConnectivity
  {
    // this type
    typedef MultiSPLeafConnectivity< pitype, Grid > This;
    // grid traits
    typedef typename std::remove_const< Grid >::type::Traits Traits;

  public:
    template< PartitionIteratorType pit >
    struct Partition
    {
      //! \brief host grid view
      typedef typename Traits::HostGrid::template Partition< pit >::LeafGridView HostGridView;
    };

    //! \brief leaf grid view
    typedef typename Partition< pitype >::HostGridView HostGridView;

    //! \brief constructor
    explicit MultiSPLeafConnectivity ( const Grid &grid )
    : grid_( &grid )
    {}

    //! \brief return number of nodes
    int nodes () const
    {
      return macroConnectivity().nodes();
    }

    //! \brief return number of faces
    int faces () const
    {
      return macroConnectivity().faces();
    }

    //! \brief
    bool contains ( const int node ) const
    {
      assert( node >= 0 && node < nodes() );
      return true;
    }

    //! \brief return true if node is at boundary
    bool boundary ( const int face, const int node ) const
    {
      return macroConnectivity().boundary( face, node );
    }

    //! \brief return true if node has neighbor
    bool hasNeighbor ( const int face, const int node ) const
    {
      return macroConnectivity().hasNeighbor( face, node );
    }

    //! \brief return neighbor node at given face
    int neighbor ( const int face, const int node ) const
    {
      return macroConnectivity().neighbor( face, node );
    }

    //! \brief return host grid view
    template< PartitionType pit >
    const typename Partition< pit >::HostGridView hostGridView ( const int node ) const
    {
      assert( contains( node ) );
      return grid().hostGrid( node ).template leafGridView< pit >();
    }

    //! \brief return host grid view
    const HostGridView hostGridView ( const int node ) const
    {
      assert( contains( node ) );
      return grid().hostGrid( node ).template leafGridView< pitype >();
    }

    //! \brief return first node
    int first () const
    {
      return 0;
    }

    //! \brief return last node
    int last () const
    {
      return nodes()-1;
    }

    //! \brief get next node
    int next ( const int node ) const
    {
      assert( node >= first () && node < last() );
      return node+1;
    }

    //! \brief return grid
    const Grid &grid () const
    {
      assert( grid_ );
      return *grid_;
    }

  private:
    // return macro connectivity
    const typename Grid::MacroConnectivity &macroConnectivity () const
    {
      return grid().macroConnectivity();
    }

    const Grid *grid_;
  };



  // MultiSPLevelConnectivity
  // ------------------------

  template< PartitionIteratorType pitype, class Grid >
  class MultiSPLevelConnectivity
  {
    // this type
    typedef MultiSPLevelConnectivity< pitype, Grid > This;
    // grid traits
    typedef typename std::remove_const< Grid >::type::Traits Traits;

  public:
    template< PartitionIteratorType pit >
    struct Partition
    {
      //! \brief host grid view
      typedef typename Traits::HostGrid::template Partition< pit >::LevelGridView HostGridView;
    };

    //! \brief level grid view
    typedef typename Partition< pitype >::HostGridView HostGridView;

    //! \brief constructor
    MultiSPLevelConnectivity ( const int level, const Grid &grid )
    : grid_( &grid ),
      level_( level )
    {}

    //! \brief return number of nodes
    int nodes () const
    {
      return macroConnectivity().nodes();
    }

    //! \brief return number of faces
    int faces () const
    {
      return macroConnectivity().faces();
    }

    //! \brief
    bool contains ( const int node ) const
    {
      assert( node >= 0 && node < nodes() );
      return ( level() == 0 || grid().hostGrid( node ).maxLevel() >= level() );
    }

    //! \brief return true if node is at boundary
    bool boundary ( const int face, const int node ) const
    {
      assert( contains( node ) );
      return macroConnectivity().boundary( face, node );
    }

    //! \brief return true if node has neighbor
    bool hasNeighbor ( const int face, const int node ) const
    {
      assert( contains( node ) );
      return contains( macroConnectivity().neighbor( face, node ) );
    }

    //! \brief return neighbor node at given face
    int neighbor ( const int face, const int node ) const
    {
      assert( hasNeighbor( face, node ) );
      return macroConnectivity().neighbor( face, node );
    }

    //! \brief return host grid view
    template< PartitionType pit >
    const typename Partition< pit >::HostGridView hostGridView ( const int node ) const
    {
      assert( contains( node ) );
      return grid().hostGrid( node ).template levelGridView< pit >( level() );
    }

    //! \brief return host grid view
    HostGridView hostGridView ( const int node ) const
    {
      assert( contains( node ) );
      return grid().hostGrid( node ).template levelGridView< pitype >( level() );
    }

    //! \brief return first node
    int first () const
    {
      if( level() == 0 )
        return 0;

      const int nodes = This::nodes();
      for( int node = 0; node < nodes; ++node )
      {
        if( contains( node ) )
          return node;
      }

      assert( false );
      return nodes;
    }

    //! \brief return last node
    int last () const
    {
      const int nodes = This::nodes();
      if( level() == 0 )
        return nodes-1;

      for( int node = nodes-1; node >= 0; --node )
      {
        if( contains( node ) )
          return node;
      }
      assert( false );
      return -1;
    }

    //! \brief get next node
    int next ( const int node ) const
    {
      assert( node >= first () && node < last() );

      if( level() == 0 )
        return node+1;
      for( int ret = node+1 ; ret < nodes(); ++ret )
      {
        if( contains( ret ) )
          return ret;
      }
      assert( false );
      return nodes();
    }

    //! \brief return grid
    const Grid &grid () const
    {
      assert( grid_ );
      return *grid_;
    }

  private:
    // return macro connectivity
    const typename Grid::MacroConnectivity &macroConnectivity () const
    {
      return grid().macroConnectivity();
    }

    // return level
    int level () const
    {
      return level_;
    }

    const Grid *grid_;
    int level_;
  };

} // namespace Dune

#endif // #if HAVE_DUNE_SPGRID

#endif // #ifndef DUNE_MULTISPGRID_CONNECTIVITY_HH
