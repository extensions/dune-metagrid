#ifndef DUNE_MULTISPGRID_DECLARATION_HH
#define DUNE_MULTISPGRID_DECLARATION_HH

namespace Dune
{

  template< class ct, int dim >
  class MultiSPGrid;

} // namespace Dune

#endif // #ifndef DUNE_MULTISPGRID_DECLARATION_HH
