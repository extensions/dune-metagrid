#ifndef DUNE_MULTISPGRID_TWISTUTILITY_HH
#define DUNE_MULTISPGRID_TWISTUTILITY_HH

#if HAVE_DUNE_FEM

#include <dune/fem/quadrature/caching/twistutility.hh>

#include "declaration.hh"

namespace Dune
{

  namespace Fem
  {

    // Template specializtion of TwistUtility
    // --------------------------------------

    template< class ct, int dim >
    struct TwistUtility< Dune::MultiSPGrid< ct, dim > >
    : public TwistFreeTwistUtility< Dune::MultiSPGrid< ct, dim > >
    {};

  } // namespace Fem

} // namespace Dune

#endif // #if HAVE_DUNE_FEM

#endif // #ifndef DUNE_MULTISPGRID_TWISTUTILITY_HH
