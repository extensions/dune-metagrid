#ifndef DUNE_MULTISPGRID_ENTITYITERATOR_HH
#define DUNE_MULTISPGRID_ENTITYITERATOR_HH

#include <algorithm>
#include <cassert>

#include <dune/common/exceptions.hh>
#include <dune/common/typetraits.hh>

#include <dune/grid/common/entityiterator.hh>

#include "entitypointer.hh"
#include "gridview.hh"

namespace Dune
{

  // Forward declarations
  // --------------------

  template< int, class > class MultiSPEntityPointerTraits;
  template< class > class MultiSPEntityPointer;



  // MultiSPEntityIterator
  // ---------------------

  /** \brief Entity iterator
   *
   *  \ingroup EntityPointer
   */
  template< class Traits >
  class MultiSPEntityIterator
  : public MultiSPEntityPointer< Traits >
  {
    // this type
    typedef MultiSPEntityIterator< Traits > This;
    // base type
    typedef MultiSPEntityPointer< Traits > Base;

  public:
    //! \brief host iterator type
    typedef typename Base::HostIterator HostIterator;

    //! \brief grid view type
    typedef typename Traits::Connectivity Connectivity;
    //! \brief host grid view type
    typedef typename Connectivity::HostGridView HostGridView;

  protected:
    using Base::releaseEntity;
    using Base::node_;
    using Base::hostIterator_;

    //! \brief constructor
    MultiSPEntityIterator ( const HostIterator &hostIterator,
                          const HostIterator &hostEnd,
                          const int node,
                          const Connectivity &connectivity )
    : Base( hostIterator, node, connectivity.grid() ),
      hostEnd_( hostEnd ),
      connectivity_( connectivity )
    {}

    //! \brief return begin iterator
    static MultiSPEntityIterator begin ( const Connectivity &connectivity )
    {
      const int node = connectivity.first();
      const HostGridView hostGridView = connectivity.hostGridView( node );
      const HostIterator hostIterator = hostGridView.template begin< Base::codimension >();
      const HostIterator hostEnd = hostGridView.template end< Base::codimension >();
      return This( hostIterator, hostEnd, node, connectivity );
    }

    //! \brief return begin iterator
    static MultiSPEntityIterator end ( const Connectivity &connectivity )
    {
      const int node = connectivity.last();
      const HostGridView hostGridView = connectivity.hostGridView( node );
      const HostIterator hostEnd = hostGridView.template end< Base::codimension >();
      return This( hostEnd, hostEnd, node, connectivity );
    }

  public:
    using Base::grid;

    //! \brief iterator increment
    void increment ()
    {
      // release entity
      releaseEntity();

      // try to increment host iterator
      ++hostIterator_;
      if( hostIterator_ != hostEnd_ )
        return;

      // if we are on the last node, we have reached the end iterator
      if( node_ == connectivity().last() )
      {
        assert( (*this).equals( end( connectivity() ) ) );
        return;
      }

      // try to go to next node and re-initialize host iterators
      node_ = connectivity().next( node_ );
      const HostGridView hostGridView = connectivity().hostGridView( node_ );
      hostIterator_ = hostGridView.template begin< Base::codimension >();
      hostEnd_ = hostGridView.template end< Base::codimension >();
      assert( hostIterator_ != hostEnd_ );
    }

  private:
    // return connectivity
    const Connectivity &connectivity () const
    {
      return connectivity_;
    }

    HostIterator hostEnd_;
    Connectivity connectivity_;
  };



  // MultiSPLeafIteratorTraits
  // -------------------------

  template< int codim, PartitionIteratorType pitype, class Grid >
  class MultiSPLeafIteratorTraits
  : public MultiSPEntityPointerTraits< codim, Grid >
  {
    // base type
    typedef MultiSPEntityPointerTraits< codim, Grid > Base;
    // grid traits
    typedef typename std::remove_const< Grid >::type::Traits Traits;

  public:
    //! \brief connectivity
    typedef typename Traits::template Partition< pitype >::LeafConnectivity Connectivity;
    //\brief host grid view type
    typedef typename Connectivity::HostGridView HostGridView;
    //! \brief host iterator type
    typedef typename HostGridView::template Codim< codim >::Iterator HostIterator;
  };



  // MultiSPLeafIterator
  // -------------------

  /** \brief Leaf iterator
   *
   *  \ingroup EntityPointer
   */

  template< int codim, PartitionIteratorType pitype, class Grid >
  class MultiSPLeafIterator
  : public MultiSPEntityIterator< MultiSPLeafIteratorTraits< codim, pitype, Grid > >
  {
    // this type
    typedef MultiSPLeafIterator< codim, pitype, Grid > This;
    // traits class
    typedef MultiSPLeafIteratorTraits< codim, pitype, Grid > Traits;
    // base type
    typedef MultiSPEntityIterator< Traits > Base;

  protected:
    //! \brief grid type
    typedef typename Base::Connectivity Connectivity;

    struct Begin {};
    struct End {};

    //! \brief constructor for begin iterator
    MultiSPLeafIterator ( const Begin &,
                        const Connectivity &connectivity )
    : Base( Base::begin( connectivity ) )
    {}

    //! \brief constructor for begin iterator
    MultiSPLeafIterator ( const End &,
                        const Connectivity &connectivity )
    : Base( Base::end( connectivity ) )
    {}

  public:
    //! \brief return begin iterator
    static MultiSPLeafIterator begin ( const Grid &grid )
    {
      return This( Begin(), grid.template leafConnectivity< pitype >() );
    }

    //! \brief return end iterator
    static MultiSPLeafIterator end ( const Grid &grid )
    {
      return This( End(), grid.template leafConnectivity< pitype >() );
    }
  };



  // MultiSPLevelIteratorTraits
  // --------------------------

  template< int codim, PartitionIteratorType pitype, class Grid >
  class MultiSPLevelIteratorTraits
  : public MultiSPEntityPointerTraits< codim, Grid >
  {
    // base type
    typedef MultiSPEntityPointerTraits< codim, Grid > Base;
    // grid traits
    typedef typename std::remove_const< Grid >::type::Traits Traits;

  public:
    //! \brief connectivity
    typedef typename Traits::template Partition< pitype >::LevelConnectivity Connectivity;
    //\brief host grid view type
    typedef typename Connectivity::HostGridView HostGridView;
    //! \brief host iterator type
    typedef typename HostGridView::template Codim< codim >::Iterator HostIterator;
  };



  // MultiSPLevelIterator
  // --------------------

  /** \brief Level iterator
   *
   *  \ingroup EntityPointer
   */

  template< int codim, PartitionIteratorType pitype, class Grid >
  class MultiSPLevelIterator
  : public MultiSPEntityIterator< MultiSPLevelIteratorTraits< codim, pitype, Grid > >
  {
    // this type
    typedef MultiSPLevelIterator< codim, pitype, Grid > This;
    // traits class
    typedef MultiSPLevelIteratorTraits< codim, pitype, Grid > Traits;
    // base type
    typedef MultiSPEntityIterator< Traits > Base;

  protected:
    //! \brief grid type
    typedef typename Base::Connectivity Connectivity;

    struct Begin {};
    struct End {};

    //! \brief constructor for begin iterator
    MultiSPLevelIterator ( const Begin &,
                         const Connectivity &connectivity )
    : Base( Base::begin( connectivity ) )
    {}

    //! \brief constructor for begin iterator
    MultiSPLevelIterator ( const End &,
                         const Connectivity &connectivity )
    : Base( Base::end( connectivity ) )
    {}

  public:
    //! \brief return begin iterator
    static MultiSPLevelIterator begin ( const int level, const Grid &grid )
    {
      return This( Begin(), grid.template levelConnectivity< pitype >( level ) );
    }

    //! \brief return end iterator
    static MultiSPLevelIterator end ( const int level, const Grid &grid )
    {
      return This( End(), grid.template levelConnectivity< pitype >( level ) );
    }
  };

} // namespace Dune

#endif // #ifndef DUNE_MULTISPGRID_ENTITYITERATOR_HH
