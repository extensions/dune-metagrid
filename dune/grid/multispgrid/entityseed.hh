#ifndef DUNE_MULTISPGRID_ENTITYSEED_HH
#define DUNE_MULTISPGRID_ENTITYSEED_HH

#include <dune/common/typetraits.hh>

namespace Dune
{

  // MultiSPEntitySeed
  // -----------------

  /** \brief Entity seed
   *
   *  \ingroup Seed
   */
  template< int codim, class Grid >
  class MultiSPEntitySeed
  {
    // this type
    typedef MultiSPEntitySeed< codim, Grid > This;
    // grid traits
    typedef typename std::remove_const< Grid >::type::Traits Traits;

  public:
    //! \brief grid dimension
    static const int dimension = Traits::dimension;
    //! \brief codimension
    static const int codimension = codim;

    //! \brief entity type
    typedef typename Traits::template Codim< codimension >::Entity Entity;
    //! \brief entity implementation type
    typedef typename Entity::Implementation EntityImp;

    //! \brief entity pointer
    typedef typename Traits::template Codim< codimension >::EntityPointer EntityPointer;
    //! \brief entity pointer implementation
    typedef typename EntityPointer::Implementation EntityPointerImp;

  protected:
    //! \brief host grid
    typedef typename Traits::HostGrid HostGrid;
    //! \brief host entity seed
    typedef typename HostGrid::template Codim< codim >::EntitySeed HostSeed;
    //! \brief host entity pointer
    typedef typename HostGrid::template Codim< codim >::EntityPointer HostEntityPointer;

  public:
    //! \brief default constructor
    MultiSPEntitySeed ()
    {}

    //! \brief constructor
    explicit MultiSPEntitySeed ( const Entity &entity )
    : hostSeed_( entity.impl().hostEntity().seed() ),
      node_( entity.impl().node() )
    {}

    //! \brief constructor
    explicit MultiSPEntitySeed ( const EntityImp &entity )
    : hostSeed_( entity.hostEntity().seed() ),
      node_( entity.node() )
    {}

    //! \brief determine whether this seed generates a valid entity
    bool isValid () const { return hostSeed_.isValid(); }

    //! \brief return entity pointer
    EntityPointer entityPointer ( const Grid &grid ) const
    {
      const HostGrid &hostGrid = grid.hostGrid( node_ );
      const HostEntityPointer hostEntityPointer = hostGrid.entityPointer( hostSeed_ );
      return EntityPointer( EntityPointerImp( hostEntityPointer, node_, grid ) );
    }

  private:
    HostSeed hostSeed_;
    int node_;
  };

} // namespace Dune

#endif // #ifndef DUNE_MULTISPGRID_ENTITYSEED_HH
