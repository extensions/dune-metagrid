#ifndef DUNE_MULTISPGRID_HIERARCHICITERATOR_HH
#define DUNE_MULTISPGRID_HIERARCHICITERATOR_HH

#include <dune/common/exceptions.hh>

#include <dune/grid/common/entityiterator.hh>

#include "entitypointer.hh"

namespace Dune
{

  // Forward declaration
  // -------------------

  template< class > class MultiSPEntityPointer;
  template< int, class > class MultiSPEntityPointerTraits;



  // MultiSPHierarchicIteratorTraits
  // -------------------------------

  template< class Grid >
  class MultiSPHierarchicIteratorTraits
  : public MultiSPEntityPointerTraits< 0, Grid >
  {
    // grid traits
    typedef typename std::remove_const< Grid >::type::Traits Traits;

  public:
    //! \brief host iterator type
    typedef typename Traits::HostGrid::HierarchicIterator HostIterator;
  };



  // MultiSPHierarchicIterator
  // -------------------------

  /** \brief Hierarchic iterator
   *
   *  \ingroup EntityPointer
   */
  template< class Grid >
  class MultiSPHierarchicIterator
  : public MultiSPEntityPointer< MultiSPHierarchicIteratorTraits< Grid > >
  {
    // this type
    typedef MultiSPHierarchicIterator< Grid > This;
    // traits class
    typedef MultiSPHierarchicIteratorTraits< Grid > Traits;
    // base type
    typedef MultiSPEntityPointer< Traits > Base;

  public:
    //! \brief host iterator type
    typedef typename Base::HostIterator HostIterator;

  protected:
    using Base::releaseEntity;
    using Base::hostIterator_;

    //! \brief constructor
    MultiSPHierarchicIterator ( const HostIterator &hostIterator, const int node, const Grid &grid )
    : Base( hostIterator, node, grid )
    {}

  public:
    //! \brief entity implementation
    typedef typename Base::EntityImp EntityImp;

    //! \brief return begin iterator
    static MultiSPHierarchicIterator begin ( int maxLevel, const EntityImp &entity )
    {
      const HostIterator &hostIterator = entity.hostEntity().hbegin( maxLevel );
      return This( hostIterator, entity.node(), entity.grid() );
    }

    //! \brief return end iterator
    static MultiSPHierarchicIterator end ( int maxLevel, const EntityImp &entity )
    {
      const HostIterator &hostIterator = entity.hostEntity().hend( maxLevel );
      return This( hostIterator, entity.node(), entity.grid() );
    }

    //! \brief iterator increment
    void increment ()
    {
      releaseEntity();
      ++hostIterator_;
    }
  };

} // namespace Dune

#endif // #ifndef DUNE_MULTISPGRID_HIERARCHICITERATOR_HH
