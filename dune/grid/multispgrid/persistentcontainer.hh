#ifndef DUNE_MULTISPGRID_PERSISTENTCONTAINER_HH
#define DUNE_MULTISPGRID_PERSISTENTCONTAINER_HH

#include <algorithm>
#include <cassert>
#include <cstddef>
#include <iterator>
#include <numeric>
#include <vector>

#include <dune/common/typetraits.hh>

#if HAVE_DUNE_SPGRID
#include <dune/grid/spgrid/persistentcontainer.hh>

#include "declaration.hh"

namespace Dune
{

  // PersistentContainer
  // -------------------

  /** \brief Template specialization of PersistentContainer for
   *         MultiSPGrid.
   *
   *  \ingroup Features
   */
  template< class ct, int dim, class DataImp >
  class PersistentContainer< MultiSPGrid< ct, dim >, DataImp >
  {
    // this type
    typedef PersistentContainer< MultiSPGrid< ct, dim >, DataImp > This;

  public:
    //! \brief grid type
    typedef MultiSPGrid< ct, dim > Grid;
    //! \brief data type
    typedef DataImp Data;

  private:
    // grid traits
    typedef typename std::remove_const< Grid >::type::Traits Traits;

    // host grid type
    typedef typename Traits::HostGrid HostGrid;
    // host grid persistent container
    typedef PersistentContainer< HostGrid, DataImp > HostContainer;

    struct Size;
    struct Clear;
    struct Reserve;
    struct Update;

    struct IteratorTraits
    {
      typedef Data value_type;
      typedef value_type * pointer;
      typedef value_type & reference;
      typedef std::ptrdiff_t difference_type;

      typedef This Container;
      typedef typename std::vector< HostContainer >::iterator VectorIterator;
      typedef typename HostContainer::Iterator HostIterator;
    };

    struct ConstIteratorTraits
    {
      typedef Data value_type;
      typedef const value_type * const_pointer;
      typedef const value_type & const_reference;
      typedef std::ptrdiff_t difference_type;

      typedef const This Container;
      typedef typename std::vector< HostContainer >::const_iterator VectorIterator;
      typedef typename HostContainer::ConstIterator HostIterator;
    };

    template< class T >
    struct DofIterator;

  public:
    //! \brief iterator
    typedef DofIterator< IteratorTraits > Iterator;
    //! \brief const iterator
    typedef DofIterator< ConstIteratorTraits > ConstIterator;

    //! \brief constructor
    PersistentContainer ( const Grid &grid, const int codim )
    : hostContainers_( 0 )
    {
      const int nodes = grid.nodes();
      for( int node = 0; node < nodes; ++node )
      {
        assert( hostContainers_.size() == node );
        const HostGrid &hostGrid = grid.hostGrid( node );
        hostContainers_.push_back( HostContainer( hostGrid, codim ) );
      }
    }

    //! \brief access data
    template< class Entity >
    Data &operator[] ( const Entity &entity )
    {
      return hostContainers_[ node( entity ) ][ hostEntity( entity ) ];
    }

    //! \brief access data
    template< class Entity >
    const Data &operator[] ( const Entity &entity ) const
    {
      return hostContainers_[ node( entity ) ][ hostEntity( entity ) ];
    }

    //! \brief access sub entity data
    template< class Entity >
    Data &operator() ( const Entity &entity, const int subEntity )
    {
      return hostContainers_[ node( entity ) ][ hostEntity( entity, subEntity ) ];
    }

    //! \brief access sub entity data
    template< class Entity >
    const Data &operator() ( const Entity &entity, const int subEntity ) const
    {
      return hostContainers_[ node( entity ) ][ hostEntity( entity, subEntity ) ];
    }

    //! \brief iterator begin
    Iterator begin ()
    {
      return Iterator::begin( *this );
    }

    //! \brief const iterator begin
    ConstIterator begin () const
    {
      return ConstIterator::begin( *this );
    }

    //! \brief iterator end
    Iterator end ()
    {
      return Iterator::end( *this );
    }

    //! \brief const iterator end
    ConstIterator end () const
    {
      return ConstIterator::end( *this );
    }

    //! \brief return size
    size_t size () const
    {
      std::vector< size_t > sizes( hostContainers_.size() );
      std::transform( hostContainers_.begin(), hostContainers_.end(), sizes.begin(), Size() );
      return std::accumulate( sizes.begin(), sizes.end(), 0 );
    }

    //! \brief adjust container to correct size and set all values to default
    void clear ()
    {
      std::for_each( hostContainers_.begin(), hostContainers_.end(), Clear() );
    }

    //! \brief enlarge container, compress is not necessary but could be done
    void reserve ()
    {
      std::for_each( hostContainers_.begin(), hostContainers_.end(), Reserve() );
    }

    //! \brief adjust container to correct size including compress
    void update ()
    {
      std::for_each( hostContainers_.begin(), hostContainers_.end(), Update() );
    }

  private:
    // return node of entity
    template< class Entity >
    int node ( const Entity &entity ) const
    {
      return entity.impl().node();
    }

    // return host entity
    template< class Entity >
    const typename Entity::HostEntity &hostEntity ( const Entity &entity ) const
    {
      return entity.impl().hostEntity();
    }

    std::vector< HostContainer > hostContainers_;
  };



  // Implementations
  // ---------------

  template< class ct, int dim, class DataImp >
  struct PersistentContainer< MultiSPGrid< ct, dim >, DataImp >::Size
  {
    size_t operator() ( HostContainer &container )
    {
      return container.size();
    }
  };


  template< class ct, int dim, class DataImp >
  struct PersistentContainer< MultiSPGrid< ct, dim >, DataImp >::Clear
  {
    void operator() ( HostContainer &container )
    {
      container.clear();
    }
  };


  template< class ct, int dim, class DataImp >
  struct PersistentContainer< MultiSPGrid< ct, dim >, DataImp >::Reserve
  {
    void operator() ( HostContainer &container )
    {
      container.reserve();
    }
  };


  template< class ct, int dim, class DataImp >
  struct PersistentContainer< MultiSPGrid< ct, dim >, DataImp >::Update
  {
    void operator() ( HostContainer &container )
    {
      container.update();
    }
  };


  template< class ct, int dim, class DataImp >
  template< class Traits >
  struct PersistentContainer< MultiSPGrid< ct, dim >, DataImp >::DofIterator
  : public std::iterator< std::forward_iterator_tag,
                          typename Traits::value_type,
                          typename Traits::difference_type,
                          typename Traits::pointer,
                          typename Traits::reference >

  {
    // this type
    typedef DofIterator< Traits > This;
    // container
    typedef typename Traits::Container Container;
    // vector iterator
    typedef typename Traits::VectorIterator VectorIterator;
    // host iterator
    typedef typename Traits::HostIterator HostIterator;

  public:
    static DofIterator begin ( Container &container )
    {
      VectorIterator vbegin = container.hostContainers().begin();
      VectorIterator vend = container.hostContainers().end();
      HostIterator hbegin = ( vbegin == vend ? HostIterator() : vbegin->begin() );
      return This( vbegin, vend, hbegin );
    }

    static DofIterator end ( Container &container )
    {
      VectorIterator vend = container.hostContainers().end();
      HostIterator hend = HostIterator();
      return This( vend, vend, hend );
    }

    //! \brief default constructor
    DofIterator () {}

    //! \brief constructor
    DofIterator ( const VectorIterator &vectorIterator,
                  const VectorIterator &vectorEnd,
                  const HostIterator &hostIterator )
    : vectorIterator_( vectorIterator ),
      vectorEnd_( vectorEnd ),
      hostIterator_( hostIterator )
    {}

    //! \brief copy constructor
    DofIterator ( const DofIterator< IteratorTraits > &other )
    : vectorIterator_( other.vectorIterator_ ),
      vectorEnd_( other.vectorEnd_ ),
      hostIterator_( other.hostIterator_ )
    {}

    //! \brief assignment operator
    This &operator= ( const This &other )
    {
      vectorIterator_ = other.vectorIterator_;
      vectorEnd_ = other.vectorEnd_;
      hostIterator_ = other.hostIterator_;
    }

    //! \brief check for equality
    bool operator== ( const This &other ) const
    {
      return ( vectorIterator_ == other.vectorIterator_
               && hostIterator_ == other.hostIterator_ );
    }

    //! \brief check for inequality
    bool operator!= ( const This &other ) const
    {
      return !( *this == other );
    }

    //! \brief dereferencing
    typename Traits::reference operator* () const
    {
      return *hostIterator_;
    }

    //! \brief dereferencing
    typename Traits::pointer operator-> () const
    {
      return &(*hostIterator_);
    }

    //! \brief iterator increment
    This &operator++ ()
    {
      increment();
      return *this;
    }

  private:
    // iterator increment
    void increment ()
    {
      ++hostIterator_;
      if( hostIterator_ != vectorIterator_->end() )
        return;

      ++vectorIterator_;
      if( vectorIterator_ == vectorEnd_ )
        return;

      hostIterator_ = vectorIterator_->begin();
    }

    VectorIterator vectorIterator_, vectorEnd_;
    HostIterator hostIterator_;
  };

} // namespace Dune

#endif // #if HAVE_DUNE_SPGRID

#endif // #ifndef DUNE_MULTISPGRID_PERSISTENTCONTAINER_HH
