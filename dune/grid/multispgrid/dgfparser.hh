#ifndef DUNE_MULTISPGRID_DGFPARSER_HH
#define DUNE_MULTISPGRID_DGFPARSER_HH

#include <cassert>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/io/file/dgfparser/blocks/boundarydom.hh>
#include <dune/grid/io/file/dgfparser/dgfparser.hh>

#if HAVE_DUNE_SPGRID
#include <dune/grid/spgrid/dgfparser.hh>
#include <dune/grid/spgrid.hh>

#include "grid.hh"

namespace Dune
{

  // DGFGridInfo< MultiSPGrid >
  // --------------------------

  template< class ct, int dim >
  struct DGFGridInfo< MultiSPGrid< ct, dim > >
  {
    typedef MultiSPGrid< ct, dim > Grid;

    static int refineStepsForHalf ( const typename Grid::RefinementPolicy &policy = typename Grid::RefinementPolicy() )
    {
      return DGFGridInfo< typename Grid::HostGrid >::refineStepsForHalf();
    }

    static double refineWeight ( const typename Grid::RefinementPolicy &policy = typename Grid::RefinementPolicy() )
    {
      return DGFGridInfo< typename Grid::HostGrid >::refineWeight();
    }
  };



  // DGFGridFactory
  // --------------

  template< class ct, int dim >
  class DGFGridFactory< MultiSPGrid< ct, dim > >
  {
    // this type
    typedef DGFGridFactory< MultiSPGrid< ct, dim > > This;

  public:
    //! \brief single coordinate type
    typedef ct ctype;
    //! \brief grid dimension
    static const int dimension = dim;

    //! \brief grid type
    typedef MultiSPGrid< ct, dim > Grid;

    //! \brief mpi communicator type
    typedef MPIHelper::MPICommunicator MPICommunicatorType;

  private:
    // host grid type
    typedef typename Grid::HostGrid HostGrid;
    // host grid refinement policy
    typedef typename HostGrid::RefinementPolicy RefinementPolicy;

    // geometry types
    typedef typename Grid::template Codim< 0 >::Geometry::Implementation GeometryImp;

    // vertex coordinate type
    typedef FieldVector< ctype, dimension > GlobalVector;

    // boundary domain block
    typedef dgf::BoundaryDomBlock BoundaryDomainBlock;

  public:
    //! \brief constructor
    explicit DGFGridFactory ( std::istream &input,
                              MPICommunicatorType comm = MPIHelper::getCommunicator() )
    : grid_( nullptr ),
      boundaryDomainBlock_( nullptr )
    {
      generate( input, comm );
    }

    //! \brief constructor
    explicit DGFGridFactory ( const std::string &filename,
                              MPICommunicatorType comm = MPIHelper::getCommunicator() )
    : grid_( nullptr ),
      boundaryDomainBlock_( nullptr )
    {
      std::ifstream input( filename.c_str() );
      if( !input )
        DUNE_THROW( DGFException, "Unable to open file " << filename );
      generate( input, comm );
      input.close();
    }

    //! \brief destructor yielding a memory leak...
    ~DGFGridFactory ()
    {
      // delete boundary domin block
      if( boundaryDomainBlock_ )
        delete boundaryDomainBlock_;

      // delete host grid factories
      typedef typename std::vector< DGFGridFactory< HostGrid > * >::iterator Iterator;
      for( Iterator it = hostGridFactories_.begin(); it != hostGridFactories_.end(); ++it )
      {
        DGFGridFactory< HostGrid > *hostGridFactory = *it;
        if( hostGridFactory )
        {
          delete hostGridFactory;
          hostGridFactory = nullptr;
        }
      }
    }

    //! \brief get grid
    Grid *grid () const
    {
      assert( grid_ );
      return grid_;
    }

    //! \brief please doc me
    template< class Intersection >
    bool wasInserted ( const Intersection &intersection ) const
    {
      return false;
    }

    //! \brief return boundary id
    template< class Intersection >
    int boundaryId ( const Intersection &intersection ) const
    {
      // return immediately, if boundary domain block is not active
      if( !boundaryDomainBlock_ )
        return intersection.indexInInside() + 1;

      // look for intersection in boundary domain block
      std::vector< GlobalVector > corners;
      getCorners( intersection.geometry(), corners );
      const dgf::DomainData *data = boundaryDomainBlock_->contains( corners );
      if( data )
        return data->id();
      else
      {
        // we require valid boundary ids for all boundary segments in macro grid
        DUNE_THROW( DGFException, "No default value for boundary id defined" );
      }
    }

    //! \brief return true if we have boundary parameters
    bool haveBoundaryParameters () const
    {
      return ( boundaryDomainBlock_ ? boundaryDomainBlock_->hasParameter() : 0 );
    }

    //! \brief return boundary parameters
    template< class Intersection >
    const typename DGFBoundaryParameter::type &
    boundaryParameter ( const Intersection &intersection ) const
    {
      assert( haveBoundaryParameters() );
      std::vector< GlobalVector > corners;
      getCorners( intersection.geometry(), corners );
      const dgf::DomainData *data = boundaryDomainBlock_->contains( corners );
      if( data )
        return data->parameter();
      else
        return DGFBoundaryParameter::defaultValue();
    }

    //! \brief grid can't handle parameters
    template< int codim >
    int numParameters () const
    {
      return 0;
    }

    //! \brief grid can't handle parameters
    template< class Entity >
    std::vector< double > &parameter ( const Entity &entity )
    {
      DUNE_THROW( InvalidStateException, "Calling DGFGridFactory::parameter is only allowed if there are parameters" );
    }

  private:
    // generate grid
    void generate ( std::istream &input, MPICommunicatorType comm );

    // pass boundary ids to grid
    void provideBoundaryIds ();

    // compute refinement policy
    RefinementPolicy getPolicy ( const FieldVector< int, dim > cells ) const
    {
      unsigned int policy = 0;
      for( int i = 0; i < dim; ++i )
      {
        if( cells[ i ] > 1 )
          policy += (1 << i);
      }
      if( policy == 0 )
        policy = ( 1 << dim ) - 1;
      return RefinementPolicy( policy );
    }

    // get geometry's corners
    template< class Geometry >
    static void getCorners ( const Geometry &geometry, std::vector< GlobalVector > &corners )
    {
      corners.resize( geometry.corners() );
      for( int i = 0; i < geometry.corners(); ++i )
      {
        const typename Geometry::GlobalCoordinate corner = geometry.corner( i );
        for( int j = 0; j < dimension; ++j )
          corners[ i ][ j ] = corner[ j ];
      }
    }

    Grid *grid_;
    std::vector< DGFGridFactory< HostGrid > * > hostGridFactories_;
    std::vector< RefinementPolicy > refinementPolicies_;
    BoundaryDomainBlock *boundaryDomainBlock_;
  };



  // Implementation of DGFGridFactory< MultiSPGrid< ct, dim > >
  // ----------------------------------------------------------

  template< class ct, int dim >
  inline void DGFGridFactory< MultiSPGrid< ct, dim > >::generate ( std::istream &input, MPICommunicatorType comm )
  {
    assert( hostGridFactories_.size() == 0 );
    assert( refinementPolicies_.size() == 0 );

    // try to read interval block
    dgf::IntervalBlock intervalBlock( input );
    if( !intervalBlock.isactive() )
      DUNE_THROW( DGFException, "No interval block found" );

    // get number of nodes
    const int nodes = intervalBlock.numIntervals();
    if( intervalBlock.dimw() != dim )
      DUNE_THROW( DGFException, "Interval block has wrong dimension" );

    std::vector< HostGrid * > hostGrids;
    for( int node = 0; node < nodes; ++node )
    {
      // get single interval
      const dgf::IntervalBlock::Interval &interval = intervalBlock.get( node );

      // copy interval bounds and cells
      GlobalVector left, right;
      FieldVector< int, dim > cells;
      for( int i = 0; i < dim; ++i )
      {
        left[ i ] = interval.p[ 0 ][ i ];
        right[ i ] = interval.p[ 1 ][ i ];
        cells[ i ] = interval.n[ i ];
      }

      // create host grid factory
      std::stringstream singleInterval;
      singleInterval << "DGF" << std::endl;
      singleInterval << intervalBlock.id() << std::endl;
      singleInterval << left << std::endl;
      singleInterval << right << std::endl;
      singleInterval << cells << std::endl;
      singleInterval << "#" << std::endl;
      hostGridFactories_.push_back( new DGFGridFactory< HostGrid >( singleInterval, comm ) );

      // get host grid and refinement policy
      hostGrids.push_back( hostGridFactories_[ node ]->grid() );
      refinementPolicies_.push_back( getPolicy( cells ) );
    }

    // finally, create grid
    grid_ = new Grid( hostGrids, refinementPolicies_ );

    // check for boundary domain block
    boundaryDomainBlock_ = new BoundaryDomainBlock( input, dimension );
    if( boundaryDomainBlock_->isactive() )
    {
      // get boundary ids from boundary domain block and pass them to the grid
      provideBoundaryIds();
    }
    else
    {
      delete boundaryDomainBlock_;
      boundaryDomainBlock_ = nullptr;
    }
  }



  template< class ct, int dim >
  inline void DGFGridFactory< MultiSPGrid< ct, dim > >::provideBoundaryIds ()
  {
    // get grid
    assert( grid_ );
    Grid &grid = *grid_;

    size_t numBoundarySegments = grid.numBoundarySegments();
    std::vector< int > boundaryIds( numBoundarySegments, 0 );

    // get level 0 grid view
    typedef typename Grid::LevelGridView GridView;
    GridView gridView = grid.levelGridView( 0 );

    // traverse grid
    typedef typename GridView::template Codim< 0 >::Iterator Iterator;
    const Iterator end = gridView.template end< 0 >();
    for( Iterator it = gridView.template begin< 0 >(); it != end; ++it )
    {
      typedef typename Iterator::Entity Entity;
      const Entity &entity = *it;
      if( !entity.hasBoundaryIntersections() )
        continue;

      // find boundary intersections
      typedef typename GridView::IntersectionIterator IntersectionIterator;
      const IntersectionIterator iend = gridView.iend( entity );
      for( IntersectionIterator iit = gridView.ibegin( entity ); iit != iend; ++iit )
      {
        const typename IntersectionIterator::Intersection &intersection = *iit;
        if( !intersection.boundary() )
          continue;

        // get boundary id from boundary domain block
        const size_t boundarySegmentIndex = intersection.boundarySegmentIndex();
        int boundaryId = This::boundaryId( intersection );
        boundaryIds[ boundarySegmentIndex ] = boundaryId;
      }
    }

    // assert we have valide ids for all boundary segments in macro grid
    for( typename std::vector< int >::const_iterator it = boundaryIds.begin(); it != boundaryIds.end(); ++it )
      if( *it <= 0 ) DUNE_THROW( DGFException, "No default value for boundary id defined" );

    // dirty hack - pass boundary id to grid
    grid.provideBoundaryIds( boundaryIds );
  }

} // namespace Dune

#endif // #if HAVE_DUNE_SPGRID

#endif // #ifndef DUNE_MULTISPGRID_DGFPARSER_HH
