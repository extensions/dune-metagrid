#ifndef DUNE_MULTISPGRID_ENTITYPOINTER_HH
#define DUNE_MULTISPGRID_ENTITYPOINTER_HH

#include <dune/common/typetraits.hh>

#include <dune/grid/common/entity.hh>
#include <dune/grid/common/entitypointer.hh>

namespace Dune
{

  // Forward declarations
  // --------------------

  template< int, int, class >
  class MultiSPEntity;



  // MultiSPEntityPointerTraits
  // --------------------------

  template< int codim, class GridImp >
  class MultiSPEntityPointerTraits
  {
    // this type
    typedef MultiSPEntityPointerTraits< codim, GridImp > This;
    // grid traits
    typedef typename std::remove_const< GridImp >::type::Traits Traits;

  public:
    //! \brief base traits
    typedef This BaseTraits;

    //! \brief codimension
    static const int codimension = codim;
    //! \brief grid dimension
    static const int dimension = Traits::dimension;

    //! \brief grid type
    typedef typename std::remove_const< GridImp >::type Grid;

    //! \brief type of entity implementation
    typedef MultiSPEntity< codimension, dimension, const Grid > EntityImp;
    //! \brief type of entity
    typedef Dune::Entity< codimension, dimension, const Grid, MultiSPEntity > Entity;

    //! \brief host itereator type
    typedef typename Traits::HostGrid::template Codim< codim >::EntityPointer HostIterator;
  };



  // MultiSPEntityPointer
  // --------------------

  /** \brief Entity pointer
   *
   *  \ingroup EntityPointer
   */

  template< class Traits >
  class MultiSPEntityPointer
  {
    // this type
    typedef MultiSPEntityPointer< Traits > This;

  public:
    //! \brief codimension
    static const int codimension = Traits::codimension;
    //! \brief grid dimension
    static const int dimension = Traits::dimension;

    //! \brief grid type
    typedef typename Traits::Grid Grid;

    //! \brief entity type
    typedef typename Traits::Entity Entity;
    //! \brief type of entity implementation
    typedef typename Traits::EntityImp EntityImp;

    //! \brief type of host iterator
    typedef typename Traits::HostIterator HostIterator;

    //! \brief type of entity pointer implementation
    typedef MultiSPEntityPointer< typename Traits::BaseTraits > EntityPointerImp;

  private:
    friend class MultiSPEntityPointer< typename Traits::BaseTraits >;

  public:
    //! \brief constructor
    explicit MultiSPEntityPointer ( const HostIterator &hostIterator, const int node, const Grid &grid )
    : entity_( EntityImp( grid ) ),
      node_( node ),
      hostIterator_( hostIterator )
    {}

    //! \brief constructor
    explicit MultiSPEntityPointer ( const EntityImp &entity )
    : entity_( EntityImp( entity.grid() ) ),
      node_( entity.node() ),
      hostIterator_( entity.hostEntity() )
    {}

    // warning: copying the entity copies the wrong pointer to the host entity!
    //! \brief copy constructor
    MultiSPEntityPointer ( const This &other )
    : entity_( EntityImp( other.grid() ) ),
      node_( other.node_ ),
      hostIterator_( other.hostIterator() )
    {}

    // warning: copying the entity copies the wrong pointer to the host entity!
    //! \brief copy constructor
    template< class T >
    MultiSPEntityPointer ( const MultiSPEntityPointer< T > &other )
    : entity_( EntityImp( other.grid() ) ),
      node_( other.node_ ),
      hostIterator_( other.hostIterator() )
    {}

    // warning: copying the entity copies the wrong pointer to the host entity!
    //! \brief assignment operator
    This &operator= ( const This &other )
    {
      entityImp() = EntityImp( other.grid() );
      node_ = other.node_;
      hostIterator_ = other.hostIterator_;
      return *this;
    }

    // warning: copying the entity copies the wrong pointer to the host entity!
    //! \brief assignment operator
    template< class T >
    This &operator= ( const MultiSPEntityPointer< T > &other )
    {
      entityImp() = EntityImp( other.grid() );
      node_ = other.node_;
      hostIterator_ = other.hostIterator_;
      return *this;
    }

    //! \brief dereference entity
    Entity &dereference () const
    {
      if( !entityImp() )
        entityImp() = EntityImp( *hostIterator(), node(), grid() );
      return entity_;
    }

    //! \brief check for equality
    template< class T >
    bool equals ( const MultiSPEntityPointer< T > &other ) const
    {
      return ( node() == other.node() && hostIterator() == other.hostIterator() );
    }

    //! \brief return level
    int level () const
    {
      return hostIterator().level();
    }

    //! \brief return host iterator
    const HostIterator &hostIterator () const
    {
      return hostIterator_;
    }

    //! \brief return node
    int node () const
    {
      return node_;
    }

    //! \brief return grid
    const Grid &grid () const
    {
      return entityImp().grid();
    }

  protected:
    //! \brief release entity
    void releaseEntity ()
    {
      entityImp() = EntityImp( grid() );
      assert( !entityImp() );
    }

  private:
    // return as imp
    EntityImp &entityImp () const
    {
      return entity_.impl();
    }

    mutable Entity entity_;

  protected:
    int node_;
    HostIterator hostIterator_;
  };

} // namespace Dune

#endif // #ifndef DUNE_MULTISPGRID_ENTITYPOINTER_HH
