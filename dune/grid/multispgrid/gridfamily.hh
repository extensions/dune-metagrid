#ifndef DUNE_MULTISPGRID_GRIDFAMILY_HH
#define DUNE_MULTISPGRID_GRIDFAMILY_HH

#include <dune/common/parallel/communication.hh>

#include <dune/grid/common/grid.hh>

#if HAVE_DUNE_SPGRID
#include <dune/grid/spgrid.hh>

#include "boundarysegmentindexset.hh"
#include "capabilities.hh"
#include "connectivity.hh"
#include "declaration.hh"
#include "entity.hh"
#include "entityiterator.hh"
#include "entitypointer.hh"
#include "entityseed.hh"
#include "geometry.hh"
#include "gridview.hh"
#include "hierarchiciterator.hh"
#include "idset.hh"
#include "indexset.hh"
#include "intersection.hh"
#include "intersectioniterator.hh"

namespace Dune
{

  // MultiSPGrid
  // -----------

  template< class ct, int dim >
  struct MultiSPGridFamily
  {
    //! \brief single coordinate type
    typedef ct ctype;
    //! \brief grid dimension
    static const int dimension = dim;
    //! \brief world dimension
    static const int dimensionworld = dimension;

    //! \brief host grid type
    typedef SPGrid< ctype, dimension, SPAnisotropicRefinement > HostGrid;

    struct Traits
    {
      //! \brief single coordinate type
      typedef ct ctype;
      //! \brief grid dimension
      static const int dimension = dim;
      //! \brief world dimension
      static const int dimensionworld = dimension;

      //! \brief grid type
      typedef MultiSPGrid< ct, dimension > Grid;

      //! \brief host grid type
      typedef SPGrid< ctype, dimension, SPAnisotropicRefinement > HostGrid;
      //! \brief reference cube
      typedef typename HostGrid::ReferenceCube ReferenceCube;

      //! \brief macro connectivity
      typedef MultiSPMacroConnectivity< const Grid > MacroConnectivity;

      //! \brief collective communication
      typedef typename Dune::Communication< No_Comm > Communication;

      //! \brief leaf intersection
      typedef Dune::Intersection< const Grid, MultiSPLeafIntersection< const Grid > > LeafIntersection;
      //! \brief level intersection
      typedef Dune::Intersection< const Grid, MultiSPLevelIntersection< const Grid > > LevelIntersection;

      //! \brief leaf intersection iterator
      typedef Dune::IntersectionIterator
        < const Grid, MultiSPLeafIntersectionIterator< const Grid >, MultiSPLeafIntersection< const Grid > >
        LeafIntersectionIterator;
      //! \brief level intersection iterator
      typedef Dune::IntersectionIterator
        < const Grid, MultiSPLevelIntersectionIterator< const Grid >, MultiSPLevelIntersection< const Grid > >
        LevelIntersectionIterator;

      //! \brief hierarchic iterator
      typedef Dune::EntityIterator< 0, const Grid, MultiSPHierarchicIterator< const Grid > >
        HierarchicIterator;

      //! \brief leaf index set
      typedef MultiSPLeafIndexSet< const Grid > LeafIndexSet;
      //! \brief level index set
      typedef MultiSPLevelIndexSet< const Grid > LevelIndexSet;
      //! \brief boundary segment index set
      typedef MultiSPBoundarySegmentIndexSet< const Grid > BoundarySegmentIndexSet;

      //! \brief global id set
      typedef MultiSPGlobalIdSet< const Grid > GlobalIdSet;
      //! \brief local id set
      typedef MultiSPLocalIdSet< const Grid > LocalIdSet;

      template< int codim >
      class Codim
      {
        typedef MultiSPEntityPointerTraits< codim, const Grid > EntityPointerTraits;

      public:
        //! \brief geometry
        typedef Dune::Geometry< dimension - codim, dimensionworld, const Grid, MultiSPGeometry > Geometry;
        //! \brief local geometry
        typedef Dune::Geometry< dimension - codim, dimension, const Grid, MultiSPLocalGeometry > LocalGeometry;

        //! \brief entity pointer implementation
        typedef MultiSPEntityPointer< EntityPointerTraits > EntityPointerImp;
        //! \brief entity pointer
        typedef Dune::EntityPointer< const Grid, EntityPointerImp > EntityPointer;

        //! \brief entity
        typedef Dune::Entity< codim, dimension, const Grid, MultiSPEntity > Entity;
        //! \brief entity seed
        typedef MultiSPEntitySeed< codim, const Grid > EntitySeed;

        template< PartitionIteratorType pitype >
        struct Partition
        {
          //! \brief leaf iterator
          typedef Dune::EntityIterator
            < codim, const Grid, MultiSPLeafIterator< codim, pitype, const Grid > > LeafIterator;
          //! \brief level iterator
          typedef Dune::EntityIterator
            < codim, const Grid, MultiSPLevelIterator< codim, pitype, const Grid > > LevelIterator;
        };

        //! \brief leaf iterator
        typedef typename Partition< All_Partition >::LeafIterator LeafIterator;
        //! \brief level iterator
        typedef typename Partition< All_Partition >::LevelIterator LevelIterator;

      };

      template< PartitionIteratorType pitype >
      struct Partition
      {
        //! \brief leaf grid view
        typedef Dune::GridView< MultiSPLeafGridViewTraits< pitype, const Grid > > LeafGridView;
        //! \brief level grid view
        typedef Dune::GridView< MultiSPLevelGridViewTraits< pitype, const Grid > > LevelGridView;

        //! \brief leaf connectivity
        typedef MultiSPLeafConnectivity< pitype, const Grid > LeafConnectivity;
        //! \brief level connectivity
        typedef MultiSPLevelConnectivity< pitype, const Grid > LevelConnectivity;
      };

      //! \brief leaf grid view
      typedef typename Partition< All_Partition >::LeafGridView LeafGridView;
      //! \brief level grid view
      typedef typename Partition< All_Partition >::LevelGridView LevelGridView;

      //! \brief leaf connectivity
      typedef typename Partition< All_Partition >::LeafConnectivity LeafConnectivity;
      //! \brief level connectivity
      typedef typename Partition< All_Partition >::LevelConnectivity LevelConnectivity;
    };
  };

} // namespace Dune

#endif // #if HAVE_DUNE_SPGRID

#endif // #ifndef DUNE_MULTISPGRID_GRIDFAMILY_HH
