#ifndef DUNE_MULTISPGRID_INTERSECTIONITERATOR_HH
#define DUNE_MULTISPGRID_INTERSECTIONITERATOR_HH

#include <cassert>

#include <dune/common/exceptions.hh>
#include <dune/common/typetraits.hh>

#include <dune/grid/common/intersection.hh>

#include "entity.hh"
#include "intersection.hh"

namespace Dune
{

  // Forward declaration
  // -------------------

  template< int, int, class > class MultiSPEntity;
  template< class > class MultiSPLeafIntersection;
  template< class > class MultiSPLevelIntersection;



  // MultiSPIntersectionIterator
  // ---------------------------

  /** \brief Base class for intersection iterators
   *
   *  \ingroup IntersectionIterator
   */
  template< class Traits >
  class MultiSPIntersectionIterator
  {
    // this type
    typedef MultiSPIntersectionIterator< Traits > This;

  public:
    //! \brief entity implementation type
    typedef typename Traits::EntityImp EntityImp;
    //! \brief intersection type
    typedef typename Traits::Intersection Intersection;
    //! \brief intersection implementation type
    typedef typename Traits::IntersectionImp IntersectionImp;

    //! \brief connectivity type
    typedef typename Traits::Connectivity Connectivity;
    //! \brief host leaf grid view
    typedef typename Connectivity::HostGridView HostGridView;
    //! \brief host intersection iterator
    typedef typename HostGridView::IntersectionIterator HostIterator;
    //! \brief boundary segment iterator
    typedef typename HostGridView::Implementation::BoundarySegmentIterator BoundarySegmentIterator;
    //! \brief host intersection
    typedef typename HostIterator::Intersection HostIntersection;

  protected:
    static const int inner = IntersectionImp::inner, outer = IntersectionImp::outer;

    struct Begin {};
    struct End {};

    //! \brief constructor begin iterator
    MultiSPIntersectionIterator ( const Begin &,
                                const EntityImp &entity,
                                const Connectivity &connectivity )
    : intersection_( IntersectionImp( entity.node(), connectivity ) ),
      hostIterator_( connectivity.hostGridView( node( inner ) ).ibegin( entity.hostEntity() ) ),
      hostEnd_( connectivity.hostGridView( node( inner ) ).iend( entity.hostEntity() ) ),
      boundarySegmentIterator_( boundarySegmentEnd( 0, node( inner ) ) ),
      boundarySegmentEnd_( boundarySegmentIterator_ )
    {
      // set inner intersection
      assert( hostIterator_ != hostEnd_ );
      intersectionImp().reset( inner, *hostIterator_ );

      // return immediately if the intersection is conforming
      if( intersection_.conforming() || !intersection_.neighbor() )
        return;

      // find first outer host intersection
      const int face = intersection_.indexInOutside();
      boundarySegmentIterator_ = boundarySegmentBegin( face, node( outer ) );
      boundarySegmentEnd_ = boundarySegmentEnd( face, node( outer ) );
      while( !intersectionImp().reset( outer, *boundarySegmentIterator_ ) )
      {
        ++boundarySegmentIterator_;
        if( boundarySegmentIterator_ == boundarySegmentEnd_ )
          break;
      }

      // throw exception if we could not compute a valid intersection
      if( boundarySegmentIterator_ == boundarySegmentEnd_ )
        DUNE_THROW( InvalidStateException, "Could not find intersection in neighboring host grid view" );

      return;
    }

    //! \brief constructor end iterator
    MultiSPIntersectionIterator ( const End &,
                                const EntityImp &entity,
                                const Connectivity &connectivity )
    : intersection_( IntersectionImp( entity.node(), connectivity ) ),
      hostIterator_( connectivity.hostGridView( node( inner ) ).iend( entity.hostEntity() ) ),
      hostEnd_( hostIterator_ ),
      boundarySegmentIterator_( boundarySegmentEnd( 0, node( inner ) ) ),
      boundarySegmentEnd_( boundarySegmentIterator_ )
    {}

  public:
    //! \brief copy constructor
    MultiSPIntersectionIterator( const This &other )
    : intersection_( IntersectionImp( other.node( inner ), other.connectivity() ) ),
      hostIterator_( other.hostIterator_ ),
      hostEnd_( other.hostEnd_ ),
      boundarySegmentIterator_( other.boundarySegmentIterator_ ),
      boundarySegmentEnd_( other.boundarySegmentEnd_ )
    {
      if( hostIterator_ != hostEnd_ )
      {
        intersectionImp().reset( inner, *hostIterator_ );
        if( intersection_.conforming() )
          return;
      }
      if( boundarySegmentIterator_ != boundarySegmentEnd_ )
        intersectionImp().reset( outer, *boundarySegmentIterator_ );
    }

    //! \brief assignment operator
    This &operator= ( const This &other )
    {
      intersectionImp() = IntersectionImp( other.node( inner ), other.connectivity() );
      hostIterator_ = other.hostIterator_;
      hostEnd_ = other.hostEnd_;
      boundarySegmentIterator_ = other.boundarySegmentIterator_;
      boundarySegmentEnd_ = other.boundarySegmentEnd_;

      if( hostIterator_ != hostEnd_ )
      {
        intersectionImp().reset( inner, *hostIterator_ );
        if( intersection_.conforming() )
          return *this;
      }
      if( boundarySegmentIterator_ != boundarySegmentEnd_ )
        intersectionImp().reset( outer, *boundarySegmentIterator_ );
      return *this;
    }

    //! \brief check for equality
    bool equals ( const This &other ) const
    {
      if( ( node( inner ) != other.node( inner ) ) || ( hostIterator_ != other.hostIterator_ ) )
        return false;
      if( hostIterator_ == hostEnd_ || intersection_.conforming() )
        return true;
      return ( boundarySegmentIterator_ == other.boundarySegmentIterator_ );
    }

    //! \brief iterator increment
    void increment ()
    {
      // if we are at a non-conforming intersection, we have to increment boundary segment iterator first
      if( !intersection_.conforming() && intersection_.neighbor() )
      {
        intersectionImp().release( outer );
        do
        {
          ++boundarySegmentIterator_;
          if( boundarySegmentIterator_ == boundarySegmentEnd_ )
            break;
        } while( !intersectionImp().reset( outer, *boundarySegmentIterator_ ) );

        // if we found a valid intersection we may leave increment()
        if( boundarySegmentIterator_ != boundarySegmentEnd_ )
          return;
      }

      assert( boundarySegmentIterator_ == boundarySegmentEnd_ );
      intersectionImp().release();

      // increment inner host intersection iterator
      ++hostIterator_;
      if( hostIterator_ == hostEnd_ )
        return;

      // return immediately if the intersection is conforming
      intersectionImp().reset( inner, *hostIterator_ );
      if( intersection_.conforming() || !intersection_.neighbor() )
        return;

      // initialize boundary segment iterators and eventually call inrement() again
      const int face = intersection_.indexInOutside();
      boundarySegmentIterator_ = boundarySegmentBegin( face, node( outer ) );
      boundarySegmentEnd_ = boundarySegmentEnd( face, node( outer ) );
      if( !intersectionImp().reset( outer, *boundarySegmentIterator_ ) )
        increment();
      return;
    }

    //! \brief dereference intersection
    const Intersection &dereference () const
    {
      return intersection_;
    }

  private:
    // return connectivity
    const Connectivity &connectivity () const
    {
      return intersectionImp().connectivity();
    }

    // return host grid view at given face
    HostGridView hostGridView ( const int node ) const
    {
      return connectivity().hostGridView( node );
    }

    // return as imp
    IntersectionImp &intersectionImp ()
    {
      return intersection_.impl();
    }

    // return as imp
    const IntersectionImp &intersectionImp () const
    {
      return intersection_.impl();
    }

    // return host intersection
    int node ( const int position ) const
    {
      return intersectionImp().node( position );
    }

    // initialize boundary segment iterators
    BoundarySegmentIterator boundarySegmentBegin ( const int face, const int node ) const
    {
      return hostGridView( node ).impl().boundarySegmentBegin( face );
    }

    // initialize boundary segment iterators
    BoundarySegmentIterator boundarySegmentEnd ( const int face, const int node ) const
    {
      return hostGridView( node ).impl().boundarySegmentEnd( face );
    }

    Intersection intersection_;
    HostIterator hostIterator_, hostEnd_;
    BoundarySegmentIterator boundarySegmentIterator_, boundarySegmentEnd_;
  };



  // MultiSPLeafIntersectionIteratorTraits
  // -------------------------------------

  template< class Grid >
  class MultiSPLeafIntersectionIteratorTraits
  {
    // grid traits
    typedef typename std::remove_const< Grid >::type::Traits Traits;

  public:
    //! \brief intersection type
    typedef typename Traits::LeafIntersection Intersection;
    //! \brief intersection implementation type
    typedef MultiSPLeafIntersection< Grid > IntersectionImp;
    //! \brief entity implementation type
    typedef MultiSPEntity< 0, Traits::dimension, Grid > EntityImp;
    //! \brief connectivity type
    typedef typename Traits::LeafConnectivity Connectivity;
  };



  // MultiSPLeafIntersectionIterator
  // -------------------------------

  /** \brief Leaf intersection iterator
   *
   *  \ingroup IntersectionIterator
   */
  template< class Grid >
  class MultiSPLeafIntersectionIterator
  : public MultiSPIntersectionIterator< MultiSPLeafIntersectionIteratorTraits< Grid > >
  {
    // this type
    typedef MultiSPLeafIntersectionIterator< Grid > This;
    // traits class
    typedef MultiSPLeafIntersectionIteratorTraits< Grid > Traits;
    // base type
    typedef MultiSPIntersectionIterator< Traits > Base;

  public:
    //! \brief implementation of entity
    typedef typename Traits::EntityImp EntityImp;
    //! \brief implementation of entity
    typedef typename Traits::Connectivity Connectivity;

  protected:
    typedef typename Base::Begin Begin;
    typedef typename Base::End End;

    //! \brief constructor for begin iterator
    MultiSPLeafIntersectionIterator ( const Begin &, const EntityImp &entity, const Connectivity &connectivity )
    : Base( Begin(), entity, connectivity )
    {}

    //! \brief constructor for end iterator
    MultiSPLeafIntersectionIterator ( const End &, const EntityImp &entity, const Connectivity &connectivity )
    : Base( End(), entity, connectivity )
    {}

  public:
    //! \brief return begin iterator
    static MultiSPLeafIntersectionIterator begin ( const EntityImp &entity )
    {
      return This( Begin(), entity, entity.grid().leafConnectivity() );
    }

    //! \brief return begin iterator
    static MultiSPLeafIntersectionIterator end ( const EntityImp &entity )
    {
      return This( End(), entity, entity.grid().leafConnectivity() );
    }
  };



  // MultiSPLevelIntersectionIteratorTraits
  // --------------------------------------

  template< class Grid >
  class MultiSPLevelIntersectionIteratorTraits
  {
    // grid traits
    typedef typename std::remove_const< Grid >::type::Traits Traits;

  public:
    //! \brief intersection type
    typedef typename Traits::LevelIntersection Intersection;
    //! \brief intersection implementation type
    typedef MultiSPLevelIntersection< Grid > IntersectionImp;
    //! \brief entity implementation type
    typedef MultiSPEntity< 0, Traits::dimension, Grid > EntityImp;
    //! \brief connectivity type
    typedef typename Traits::LevelConnectivity Connectivity;
  };



  // MultiSPLevelIntersectionIterator
  // --------------------------------

  /** \brief Level intersection iterator
   *
   *  \ingroup IntersectionIterator
   */
  template< class Grid >
  class MultiSPLevelIntersectionIterator
  : public MultiSPIntersectionIterator< MultiSPLevelIntersectionIteratorTraits< Grid > >
  {
    // this type
    typedef MultiSPLevelIntersectionIterator< Grid > This;
    // traits class
    typedef MultiSPLevelIntersectionIteratorTraits< Grid > Traits;
    // base type
    typedef MultiSPIntersectionIterator< Traits > Base;

  public:
    //! \brief implementation of entity
    typedef typename Traits::EntityImp EntityImp;
    //! \brief implementation of entity
    typedef typename Traits::Connectivity Connectivity;

  protected:
    typedef typename Base::Begin Begin;
    typedef typename Base::End End;

    //! \brief constructor for begin iterator
    MultiSPLevelIntersectionIterator ( const Begin &, const EntityImp &entity, const Connectivity &connectivity )
    : Base( Begin(), entity, connectivity )
    {}

    //! \brief constructor for end iterator
    MultiSPLevelIntersectionIterator ( const End &, const EntityImp &entity, const Connectivity &connectivity )
    : Base( End(), entity, connectivity )
    {}

  public:
    //! \brief return begin iterator
    static MultiSPLevelIntersectionIterator begin ( const EntityImp &entity )
    {
      return This( Begin(), entity, entity.grid().levelConnectivity( entity.level() ) );
    }

    //! \brief return begin iterator
    static MultiSPLevelIntersectionIterator end ( const EntityImp &entity )
    {
      return This( End(), entity, entity.grid().levelConnectivity( entity.level() ) );
    }
  };

} // namespace Dune

#endif // #ifndef DUNE_MULTISPGRID_INTERSECTIONITERATOR_HH
