#ifndef DUNE_MULTISPGRID_GRID_HH
#define DUNE_MULTISPGRID_GRID_HH

#include <algorithm>
#include <cassert>
#include <vector>

#include <dune/common/exceptions.hh>
#include <dune/common/nullptr.hh>

#if HAVE_DUNE_SPGRID
#include "gridfamily.hh"
#include "twistutility.hh"

namespace Dune
{

  // Forward declaration
  // -------------------

  template< class > class DGFGridFactory;
  template< class > class MultiSPIntersection;



  // MultiSPGrid
  // -----------

  /** \brief the grid base class

      \ingroup Grid
  */
  template< class ct, int dim >
  class MultiSPGrid
  : public GridDefaultImplementation< MultiSPGridFamily< ct, dim >::dimension,
                                      MultiSPGridFamily< ct, dim >::dimensionworld,
                                      typename MultiSPGridFamily< ct, dim >::ctype,
                                      MultiSPGridFamily< ct, dim >
                                    >
  {
    // this type
    typedef MultiSPGrid< ct, dim > This;

  public:
    //! \brief grid family
    typedef MultiSPGridFamily< ct, dim > GridFamily;
    //! \brief traits
    typedef typename GridFamily::Traits Traits;

    //! \brief grid dimension
    static const int dimension = Traits::dimension;
    //! \brief world dimension
    static const int dimensionworld = Traits::dimensionworld;
    //! \brief single coordinate type
    typedef typename Traits::ctype ctype;

    //! \brief level index set
    typedef typename Traits::LevelIndexSet LevelIndexSet;
    //! \brief leaf index set
    typedef typename Traits::LeafIndexSet LeafIndexSet;
    //! \brief boundary segment index set
    typedef typename Traits::BoundarySegmentIndexSet BoundarySegmentIndexSet;

    //! \brief global id set
    typedef typename Traits::GlobalIdSet GlobalIdSet;
    //! \brief local id set
    typedef typename Traits::LocalIdSet LocalIdSet;

    //! \brief collective communication type
    typedef typename Traits::Communication Communication;

    template< int codim >
    struct Codim
    : public Traits::template Codim< codim >
    {};

    template< PartitionIteratorType pitype >
    struct Partition
    : public Traits::template Partition< pitype >
    {};

    //! \brief leaf grid view
    typedef typename Partition< All_Partition >::LeafGridView LeafGridView;
    //! \brief level grid view
    typedef typename Partition< All_Partition >::LevelGridView LevelGridView;

    //! \brief host grid type
    typedef typename Traits::HostGrid HostGrid;
    //! \brief host grid refinement policy
    typedef typename HostGrid::RefinementPolicy RefinementPolicy;

    //! \brief macro connectivity
    typedef typename Traits::MacroConnectivity MacroConnectivity;
    //! \brief leaf connectivity
    typedef typename Traits::LeafConnectivity LeafConnectivity;
    //! \brief level connectivity
    typedef typename Traits::LevelConnectivity LevelConnectivity;

   public:
    //! \brief constructor
    MultiSPGrid ( std::vector< HostGrid * > &hostGrids,
                std::vector< RefinementPolicy > &refinementPolicies )
    : hostGrids_( hostGrids ),
      refinementPolicies_( refinementPolicies ),
      macroConnectivity_( hostGrids_ ),
      globalIdSet_( *this ),
      localIdSet_( *this ),
      leafIndexSet_( leafConnectivity() ),
      levelIndexSets_( 1, LevelIndexSet( levelConnectivity( 0 ) ) ),
      mark_( nodes(), 0 ),
      wasRefined_( nodes(), false )
    {
      // build boundary segment index set - needed for intersection iterators
      boundarySegmentIndexSet_ = BoundarySegmentIndexSet( *this );
      // boundary ids may be provided by DGF grid factory
      assert( boundaryId_.empty() );
    }

    //! \brief constructor
    MultiSPGrid ( std::vector< HostGrid * > &hostGrids )
    : hostGrids_( hostGrids ),
      refinementPolicies_( 0 ),
      macroConnectivity_( hostGrids_ ),
      globalIdSet_( *this ),
      localIdSet_( *this ),
      leafIndexSet_( leafConnectivity() ),
      levelIndexSets_( 1, LevelIndexSet( levelConnectivity( 0 ) ) ),
      mark_( nodes(), 0 ),
      wasRefined_( nodes(), false )
    {
      // use default values for refinement
      refinementPolicies_.resize( nodes(), RefinementPolicy() );

      // build boundary segment index set - needed for intersection iterators
      boundarySegmentIndexSet_ = BoundarySegmentIndexSet( *this );
      // boundary ids may be provided by DGF grid factory
      assert( boundaryId_.empty() );
    }

    //! \brief return maximum level
    int maxLevel () const
    {
      int maxLevel = 0;
      typedef typename std::vector< HostGrid * >::const_iterator Iterator;
      for( Iterator it = hostGrids_.begin(); it != hostGrids_.end(); ++it )
        maxLevel = std::max( (*it)->maxLevel(), maxLevel );
      return maxLevel;
    }

    //! \brief return size per level and codim
    int size ( int level, int codim ) const
    {
      int size = 0;
      typedef typename std::vector< HostGrid * >::const_iterator Iterator;
      for( Iterator it = hostGrids_.begin(); it != hostGrids_.end(); ++it )
      {
        if( level <= (*it)->maxLevel() )
          size += (*it)->size( level, codim );
      }
      assert( size == int( levelIndexSet( level ).size( codim ) ) );
      return size;
    }

    //! \brief return leaf level size per codim
    int size ( int codim ) const
    {
      int size = 0;
      typedef typename std::vector< HostGrid * >::const_iterator Iterator;
      for( Iterator it = hostGrids_.begin(); it != hostGrids_.end(); ++it )
        size += (*it)->size( codim );
      assert( size == int( leafIndexSet().size( codim ) ) );
      return size;
    }

    //! \brief return size per level and type
    int size ( int level, GeometryType type ) const
    {
      int size = 0;
      typedef typename std::vector< HostGrid * >::const_iterator Iterator;
      for( Iterator it = hostGrids_.begin(); it != hostGrids_.end(); ++it )
      {
        if( level <= (*it)->maxLevel() )
          size += (*it)->size( level, type );
      }
      assert( size == int( levelIndexSet( level ).size( type ) ) );
      return size;
    }

    //! \brief return leaf level size per type
    int size ( GeometryType type ) const
    {
      int size = 0;
      typedef typename std::vector< HostGrid * >::const_iterator Iterator;
      for( Iterator it = hostGrids_.begin(); it != hostGrids_.end(); ++it )
        size += (*it)->size( type );
      assert( size == int( leafIndexSet().size( type ) ) );
      return size;
    }

    //! \brief return number of boundary segments
    size_t numBoundarySegments () const
    {
      return boundarySegmentIndexSet().size();
    }

    //! \brief return level grid view
    template< PartitionIteratorType pitype >
    typename Partition< pitype >::LevelGridView levelGridView ( int level ) const
    {
      return MultiSPLevelGridView< pitype, const This >( level, *this );
    }

    //! \brief return leaf grid view
    template< PartitionIteratorType pitype >
    typename Partition< pitype >::LeafGridView leafGridView () const
    {
      return MultiSPLeafGridView< pitype, const This >( *this );
    }

    //! \brief return level grid view
    LevelGridView levelGridView ( int level ) const
    {
      return levelGridView< All_Partition >( level );
    }

    //! \brief return leaf grid view
    LeafGridView leafGridView () const
    {
      return leafGridView< All_Partition >();
    }

    //! \brief return leaf iterator
    template< int codim >
    typename Codim< codim >::template Partition< All_Partition >::LeafIterator
    leafbegin () const
    {
      return leafbegin< codim, All_Partition >();
    }

    //! \brief return leaf iterator
    template< int codim, PartitionIteratorType pitype >
    typename Codim< codim >::template Partition< pitype >::LeafIterator
    leafbegin () const
    {
      typedef typename Codim< codim >::template Partition< pitype >::LeafIterator::Implementation IteratorImp;
      return IteratorImp::begin( *this );
    }

    //! \brief return leaf iterator
    template< int codim >
    typename Codim< codim >::template Partition< All_Partition >::LeafIterator
    leafend () const
    {
      return leafend< codim, All_Partition >();
    }

    //! \brief return leaf iterator
    template< int codim, PartitionIteratorType pitype >
    typename Codim< codim >::template Partition< pitype >::LeafIterator
    leafend () const
    {
      typedef typename Codim< codim >::template Partition< pitype >::LeafIterator::Implementation IteratorImp;
      return IteratorImp::end( *this );
    }

    //! \brief return level iterator
    template< int codim >
    typename Codim< codim >::template Partition< All_Partition >::LevelIterator
    lbegin ( int level ) const
    {
      return lbegin< codim, All_Partition >( level );
    }

    //! \brief return level iterator
    template< int codim, PartitionIteratorType pitype >
    typename Codim< codim >::template Partition< pitype >::LevelIterator
    lbegin ( int level ) const
    {
      typedef typename Codim< codim >::template Partition< pitype >::LevelIterator::Implementation IteratorImp;
      return IteratorImp::begin( level, *this );
    }

    //! \brief return level iterator
    template< int codim >
    typename Codim< codim >::template Partition< All_Partition >::LevelIterator
    lend ( int level ) const
    {
      return lend< codim, All_Partition >( level );
    }

    //! \brief return level iterator
    template< int codim, PartitionIteratorType pitype >
    typename Codim< codim >::template Partition< pitype >::LevelIterator
    lend ( int level ) const
    {
      typedef typename Codim< codim >::template Partition< pitype >::LevelIterator::Implementation IteratorImp;
      return IteratorImp::end( level, *this );
    }

    //! \brief return global id set
    const GlobalIdSet &globalIdSet () const
    {
      return globalIdSet_;
    }

    //! \brief return local id set
    const LocalIdSet &localIdSet () const
    {
      return localIdSet_;
    }

    //! \brief return leaf index set
    const LeafIndexSet &leafIndexSet () const
    {
      if( !leafIndexSet_ )
        leafIndexSet_.update();
      return leafIndexSet_;
    }

    //! \brief return level index set
    const LevelIndexSet &levelIndexSet ( int level ) const
    {
      assert( level >= 0 && level <= maxLevel() );
      const LevelIndexSet &levelIndexSet = levelIndexSets_[ level ];
      if( !levelIndexSet )
        levelIndexSet.update();
      return levelIndexSet;
    }

    //! \brief return boundary segment index set
    const BoundarySegmentIndexSet &boundarySegmentIndexSet () const
    {
      return boundarySegmentIndexSet_;
    }

    //! \brief refine grid
    void globalRefine ( int refCount )
    {
      const int nodes = This::nodes();
      for( int node = 0; node < nodes; ++node )
      {
        RefinementPolicy refinementPolicy = This::refinementPolicy( node );
        hostGrid( node ).globalRefine( refCount, refinementPolicy );
      }
      update();
    }

    //! \brief mark entity
    bool mark ( int refCount, const typename Codim< 0 >::Entity &entity )
    {
      const int node = entity.impl().node();
      return mark( refCount, node );
    }

    //! \brief mark node for refinement
    bool mark ( int refCount, const int node )
    {
      mark_[ node ] = std::max( mark_[ node ], refCount );
      return ( mark_[ node ] != 0 );
    }

    //! \brief get mark for entity
    int getMark ( const typename Codim< 0 >::Entity &entity ) const
    {
      return getMark( entity.impl().node() );
    }

    //! \brief get mark for node
    int getMark ( const int node ) const
    {
      return mark_[ node ];
    }

    //! \brief return true if host grid was refined in last call to adapt()
    bool wasRefined ( const int node ) const
    {
      return wasRefined_[ node ];
    }

    //! \brief pre adapt
    bool preAdapt ()
    {
      // no coarsening
      return false;
    }

    //! \brief adapt
    bool adapt()
    {
      bool ret = false;
      const int nodes = This::nodes();
      for( int node = 0; node < nodes; ++node )
      {
        // get mark
        const int mark = getMark( node );
        if( mark <= 0 )
          continue;

        // refinement of host grid
        hostGrid( node ).globalRefine( mark, refinementPolicy( node ) );
        ret = true;
      }
      update();
      return ret;
    }

    //! \brief post adapt
    void postAdapt ()
    {
      // get number of nodes
      const int nodes = This::nodes();

      // assert sizes are correct
      assert( int( mark_.size() ) == nodes );
      assert( mark_.size() == wasRefined_.size() );

      // update markers
      for( int node = 0; node < nodes; ++node )
      {
        wasRefined_[ node ] = ( mark_[ node ] > 0 );
        mark_[ node ] = 0;
      }
    }

    //! \brief return const reference to a collective communication object
    const Communication &comm () const
    {
      return comm_;
    }

    //! \brief return entity pointer from entity seed
    template< class EntitySeed >
    typename Codim< EntitySeed::codimension >::EntityPointer
    entityPointer ( const EntitySeed &seed ) const
    {
      return seed.entityPointer( *this );
    }

  public:
    //! \brief return number of host grids
    size_t nodes () const
    {
      return macroConnectivity().nodes();
    }

    //! \brief return fixed refinement policy for node
    RefinementPolicy &refinementPolicy ( const int node )
    {
      return refinementPolicies_[ node ];
    }

    //! \brief return fixed refinement policy for node
    const RefinementPolicy &refinementPolicy ( const int node ) const
    {
      return refinementPolicies_[ node ];
    }

    //! \brief return host grid at given node
    const HostGrid &hostGrid ( const int node ) const
    {
      assert( 0 <= node || node < int( nodes() ) );
      return *( hostGrids_[ node ] );
    }

    //! \brief return macro connectivity
    const MacroConnectivity &macroConnectivity () const
    {
      return macroConnectivity_;
    }

    //! \brief return connectivity for leaf level
    template< PartitionIteratorType pitype >
    typename Partition< pitype >::LeafConnectivity leafConnectivity () const
    {
      return typename Partition< pitype >::LeafConnectivity( *this );
    }

    //! \brief return connectivity for leaf level
    LeafConnectivity leafConnectivity () const
    {
      return leafConnectivity< All_Partition >();
    }

    //! \brief return connectivity for level
    template< PartitionIteratorType pitype >
    typename Partition< pitype >::LevelConnectivity levelConnectivity ( const int level ) const
    {
      assert( level >= 0 && level <= maxLevel() );
      return typename Partition< pitype >::LevelConnectivity( level, *this );
    }

    //! \brief return connectivity for level
    LevelConnectivity levelConnectivity ( const int level ) const
    {
      return levelConnectivity< All_Partition >( level );
    }

  protected:
    // allow DGF grid factory to pass boundary ids to grid after construction
    template< class > friend class DGFGridFactory;
    template< class > friend class MultiSPIntersection;

    //! \brief DGF factory may provide boundary ids
    void provideBoundaryIds ( std::vector< int > &boundaryId )
    {
      assert( boundaryId_.empty() );
      assert( boundaryId.size() == numBoundarySegments() );
      std::swap( boundaryId, boundaryId_ );
    }

    //! \brief return boundary id
    template< class Traits >
    int boundaryId ( const MultiSPIntersection< Traits > &intersection ) const
    {
      if( boundaryId_.empty() )
        return ( intersection.indexInInside() + 1 );
      return boundaryId_[ intersection.boundarySegmentIndex() ];
    }

  private:
    // hide copy constructor
    MultiSPGrid ( const MultiSPGrid &other );
    // hide assignment operator
    const MultiSPGrid &operator= ( const MultiSPGrid &other );

    // return host grid at given node
    HostGrid &hostGrid ( const int node )
    {
      assert( 0 <= node || node < int( nodes() ) );
      return *( hostGrids_[ node ] );
    }

    // update index sets
    void update ()
    {
      leafIndexSet_.invalidate();

      const int oldSize = levelIndexSets_.size();
      const int newSize = maxLevel()+1;

      for( int level = newSize; level < oldSize; ++level )
        levelIndexSets_.pop_back();

      for( int level = 1; level < newSize; ++level )
      {
        if( level < oldSize )
          levelIndexSets_[ level ].invalidate();
        else
          levelIndexSets_.push_back( LevelIndexSet( levelConnectivity( level ) ) );
      }
    }

    // inner host grids
    std::vector< HostGrid * > hostGrids_;
    // refinement policies
    std::vector< RefinementPolicy > refinementPolicies_;

    // macro connectivity
    MacroConnectivity macroConnectivity_;

    // global id set
    mutable GlobalIdSet globalIdSet_;
    // local id set
    mutable LocalIdSet localIdSet_;

    // leaf index set
    mutable LeafIndexSet leafIndexSet_;
    // level index sets
    mutable std::vector< LevelIndexSet > levelIndexSets_;
    // boundary segment index set
    mutable BoundarySegmentIndexSet boundarySegmentIndexSet_;

    // boundary ids
    std::vector< int > boundaryId_;
    // marks for refinement
    std::vector< int > mark_;
    // remember marks
    std::vector< bool > wasRefined_;

    // collective communication
    Communication comm_;
  };

} // namespace Dune

#endif // #if HAVE_DUNE_SPGRID

#endif // #ifndef DUNE_MULTISPGRID_GRID_HH
