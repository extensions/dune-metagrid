#ifndef DUNE_MULTISPGRID_GEOMETRY_HH
#define DUNE_MULTISPGRID_GEOMETRY_HH

#include <dune/common/typetraits.hh>

#include <dune/grid/common/geometry.hh>

#if HAVE_DUNE_SPGRID
#include <dune/grid/spgrid/geometry.hh>

namespace Dune
{

  // Forward declarations
  // --------------------

  template< int, int, class > class MultiSPGeometry;
  template< int, int, class > class MultiSPLocalGeometry;
  template< int, int, class, class > class SPBasicGeometry;



  // MultiSPGeometry
  // ---------------

  /** \brief Geometry
   *
   *  \ingroup Geometry
   */
  template< int mydim, int cdim, class Grid >
  class MultiSPGeometry
  : public SPBasicGeometry< mydim, cdim, Grid, MultiSPGeometry< mydim, cdim, Grid > >
  {
    // this type
    typedef MultiSPGeometry< mydim, cdim, Grid > This;
    // base type
    typedef SPBasicGeometry< mydim, cdim, Grid, This > Base;

    // grid traits
    typedef typename Base::Traits Traits;

  public:
    //! \brief single coordinate type
    typedef typename Base::ctype ctype;

    //! \brief my dimension
    static const int mydimension = Base::mydimension;
    //! \brief grid dimension
    static const int dimension = Base::dimension;
    //! \brief codimension
    static const int codimension = Base::codimension;

    //! \brief geometry cache
    typedef typename Base::GeometryCache GeometryCache;

    //! \brief global vector
    typedef typename Base::GlobalVector GlobalVector;
    //! \brief local vector
    typedef typename Base::LocalVector LocalVector;

    //! \brief host grid geometry
    typedef typename Traits::HostGrid::template Codim< codimension >::Geometry HostGeometry;

    //! \brief constructor taking host geometry
    MultiSPGeometry ( const HostGeometry &hostGeometry )
    : origin_( hostGeometry.impl().origin() ),
      geometryCache_( hostGeometry.impl().geometryCache() )
    {}

    //! \brief constructor
    MultiSPGeometry ( GlobalVector origin, const GeometryCache &geometryCache )
    : origin_( origin ),
      geometryCache_( geometryCache )
    {}

    //! \brief return origin
    GlobalVector origin () const
    {
      return origin_;
    }

    //! \brief return geometry cache
    const GeometryCache &geometryCache () const
    {
      return geometryCache_;
    }

  private:
    GlobalVector origin_;
    GeometryCache geometryCache_;
  };



  // MultiSPLocalGeometry
  // --------------------

  /** \brief Local Geometry
   *
   *  \ingroup Geometry
   */
  template< int mydim, int cdim, class Grid >
  class MultiSPLocalGeometry
  : public SPBasicGeometry< mydim, cdim, Grid, MultiSPLocalGeometry< mydim, cdim, Grid > >
  {
    // this type
    typedef MultiSPLocalGeometry< mydim, cdim, Grid > This;
    // base type
    typedef SPBasicGeometry< mydim, cdim, Grid, This > Base;

    // grid traits
    typedef typename Base::Traits Traits;

  public:
    //! \brief single coordinate type
    typedef typename Base::ctype ctype;

    //! \brief my dimension
    static const int mydimension = Base::mydimension;
    //! \brief grid dimension
    static const int dimension = Base::dimension;
    //! \brief codimension
    static const int codimension = Base::codimension;

    //! \brief geometry cache
    typedef typename Base::GeometryCache GeometryCache;

    //! \brief global vector
    typedef typename Base::GlobalVector GlobalVector;
    //! \brief local vector
    typedef typename Base::LocalVector LocalVector;

    //! \brief host grid geometry
    typedef typename Traits::HostGrid::template Codim< codimension >::LocalGeometry HostGeometry;

    //! \brief constructor taking host geometry
    MultiSPLocalGeometry ( const HostGeometry &hostGeometry )
    : origin_( hostGeometry.impl().impl().origin() ),
      geometryCache_( hostGeometry.impl().impl().geometryCache() )
    {}

    //! \brief constructor
    MultiSPLocalGeometry ( GlobalVector origin, const GeometryCache &geometryCache )
    : origin_( origin ),
      geometryCache_( geometryCache )
    {}

    //! \brief return origin
    GlobalVector origin () const
    {
      return origin_;
    }

    //! \brief return geometry cache
    const GeometryCache &geometryCache () const
    {
      return geometryCache_;
    }

  private:
    GlobalVector origin_;
    GeometryCache geometryCache_;
  };

} // namespace Dune

#endif // #if HAVE_DUNE_SPGRID

#endif // #ifndef DUNE_MULTISPGRID_GEOMETRY_HH
