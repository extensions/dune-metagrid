#ifndef DUNE_MULTISPGRID_INTERSECTION_HH
#define DUNE_MULTISPGRID_INTERSECTION_HH

#include <cassert>

#include <dune/common/exceptions.hh>
#include <dune/common/typetraits.hh>

#include <dune/geometry/referenceelements.hh>

#include <dune/grid/common/intersection.hh>

#if HAVE_DUNE_SPGRID
#include "geometry.hh"

namespace Dune
{

  // Forward declaration
  // -------------------

  template< int, int, class > class MultiSPGeometry;
  template< int, int, class > class MultiSPLocalGeometry;



  // MultiSPIntersection
  // -------------------

  /** \brief Base class for intersections
   *
   *  \ingroup IntersectionIterator
   */
  template< class Traits >
  class MultiSPIntersection
  {
    // this type
    typedef MultiSPIntersection< Traits > This;

  public:
    //! \brief grid dimension
    static const int dimension = Traits::dimension;
    //! \brief mydimsion
    static const int mydimension = Traits::mydimension;

    //! \brief grid type
    typedef typename Traits::Grid Grid;

    //! \brief type of entity pointer
    typedef typename Traits::EntityPointer EntityPointer;
    //! \brief geometry type
    typedef typename Traits::Geometry Geometry;
    //! \brief local geometry type
    typedef typename Traits::LocalGeometry LocalGeometry;

    //! \brief global coordinate
    typedef typename Geometry::GlobalCoordinate GlobalCoordinate;
    //! \brief local coordinate
    typedef typename Geometry::LocalCoordinate LocalCoordinate;

    //! \brief connectivity type
    typedef typename Traits::Connectivity Connectivity;
    //! \brief host intersection type
    typedef typename Traits::HostIntersection HostIntersection;

  protected:
    template< class > friend class MultiSPIntersectionIterator;
    template< class Grid > friend class MultiSPBoundarySegmentIndexSet;

    static const int inner = 0, outer = 1;

  private:
    // implementation of entity pointer
    typedef typename EntityPointer::Implementation EntityPointerImp;
    // geometry implementation
    typedef MultiSPGeometry< mydimension, dimension, const Grid > GeometryImp;
    // local geometry implementation
    typedef MultiSPLocalGeometry< mydimension, dimension, const Grid > LocalGeometryImp;

    class GeometryFactory;

  public:
    //! \brief empyt constructor
    MultiSPIntersection ( const int node, const Connectivity &connectivity )
    : connectivity_( connectivity ),
      node_( node )
    {
      release();
    }

    //! \brief copy constructor
    MultiSPIntersection ( const This &other )
    : connectivity_( other.connectivity_ ),
      node_( other.node_ )
    {
      release();
    }

    //! \brief assignment operator
    This &operator= ( const This &other )
    {
      connectivity_ = other.connectivity_;
      node_ = other.node_;
      release();
      return *this;
    }

    //! \brief return true if intersection is valid
    operator bool () const
    {
      // this intersection is always false, if inner host intersection is empty
      if( !hostIntersection_[ inner ] )
        return false;
      // if intersection is conforming, we don't need to check outer host intersection
      if( conforming() || !neighbor() )
        return true;
      // outer host intersection is set if and only if a non-conforming intersection is valid
      return ( hostIntersection_[ outer ] );
    }

    //! \brief return true if intersection is on boundary
    bool boundary () const
    {
      // implementation of boundary() depends on inner host intersection only
      assert( hostIntersection_[ inner ] );
      return grid().boundarySegmentIndexSet().contains( *this );
    }

    //! \brief return true if intersection has neighbor
    bool neighbor () const
    {
      // implementation of neighbor() depends on inner host intersection only
      if( hostIntersection( inner ).neighbor() )
        return true;
      assert( hostIntersection( inner ).boundary() );
      return ( !boundary() && connectivity().hasNeighbor( indexInInside(), node( inner ) ) );
    }

    //! \brief return true, if intersection is conforming
    bool conforming () const
    {
      // implementation of conforming() depends on inner host intersection only
      return ( hostIntersection( inner ).neighbor() || boundary() );
    }

    //! \brief return boundary id
    int boundaryId () const
    {
      return ( boundary() ? ( grid().boundaryId( *this ) ) : 0 );
    }

    //! \brief return boundary segment index
    size_t boundarySegmentIndex () const
    {
      // implementation of boundarySegmentIndex() depends on inner host intersection only
      assert( hostIntersection_[ inner ] );
      return grid().boundarySegmentIndexSet().index( *this );
    }

    //! \brief return index in inside
    int indexInInside () const
    {
      return hostIntersection( inner ).indexInInside();
    }

    //! \brief return index in outside
    int indexInOutside () const
    {
      // this works because of SPIntersection's implementation...
      return hostIntersection( inner ).indexInOutside();
    }

    //! \brief return entity pointer to inside entity
    EntityPointer inside () const
    {
      return EntityPointer( EntityPointerImp( hostIntersection( inner ).inside(), node( inner ), grid() ) );
    }

    //! \brief return entity pointer to outside entity
    EntityPointer outside () const
    {
      assert( neighbor() );
      if( conforming() )
        return EntityPointer( EntityPointerImp( hostIntersection( inner ).outside(), node( inner ), grid() ) );
      else
      {
        return EntityPointer( EntityPointerImp( hostIntersection( outer ).inside(), node( outer ), grid() ) );
      }
    }

    //! \brief return local geometry relative to inside entity
    LocalGeometry geometryInInside () const
    {
      assert( *this );
      if( conforming() || !neighbor() )
        return LocalGeometry( LocalGeometryImp( This::hostIntersection( inner ).geometryInInside() ) );
      else
      {
        const HostIntersection &hostIntersection = This::hostIntersection( inner );
        return LocalGeometry( LocalGeometryImp( geometryFactory_.origin( hostIntersection ),
                                                geometryFactory_.geometryCache( hostIntersection ) ) );
      }
    }

    //! \brief return local geometry relative to outside entity
    LocalGeometry geometryInOutside () const
    {
      assert( *this );
      if( conforming() || !neighbor() )
        return LocalGeometry( LocalGeometryImp( This::hostIntersection( inner ).geometryInOutside() ) );
      else
      {
        const HostIntersection &hostIntersection = This::hostIntersection( outer );
        return LocalGeometry( LocalGeometryImp( geometryFactory_.origin( hostIntersection ),
                                                geometryFactory_.geometryCache( hostIntersection ) ) );
      }
    }

    //! \brief return geometry
    Geometry geometry () const
    {
      assert( *this );
      if( conforming() || !neighbor() )
        return Geometry( GeometryImp( hostIntersection( inner ).geometry() ) );
      else
        return Geometry( GeometryImp( geometryFactory_.origin(), geometryFactory_.geometryCache() ) );
    }

    //! \brief return geometry type
    GeometryType type () const
    {
      typedef typename GenericGeometry::CubeTopology< mydimension >::type Topology;
      return GeometryType( Topology() );
    }

    //! \brief return outer normal
    GlobalCoordinate outerNormal ( const LocalCoordinate &local ) const
    {
      return hostIntersection( inner ).outerNormal( local );
    }

    //! \brief return integration outer normal
    GlobalCoordinate integrationOuterNormal ( const LocalCoordinate &local ) const
    {
      if( conforming() || !neighbor() )
        return hostIntersection( inner ).integrationOuterNormal( local );
      else
      {
        GlobalCoordinate normal = unitOuterNormal( local );
        normal *= geometryFactory_.geometryCache().volume();
        return normal;
      }
    }

    //! \brief return unit outer normal
    GlobalCoordinate unitOuterNormal( const LocalCoordinate &local ) const
    {
      return hostIntersection( inner ).unitOuterNormal( local );
    }

    //! \brief return center outer normal
    GlobalCoordinate centerUnitOuterNormal( ) const
    {
      return hostIntersection( inner ).centerUnitOuterNormal();
    }

  public:
    //! \brief return node
    int node ( const int position ) const
    {
      if( position == inner )
        return node_;
      else
        return connectivity().neighbor( indexInInside(), node_ );
    }

    //! \brief return grid
    const Connectivity &connectivity () const
    {
      return connectivity_;
    }

    //! \brief return grid
    const Grid &grid () const
    {
      return connectivity().grid();
    }

  protected:
    //! \brief return pointer to host intersection
    HostIntersection &hostIntersection ( const int position )
    {
      assert( hostIntersection_[ position ] );
      return *hostIntersection_[ position ];
    }

    //! \brief return pointer to host intersection
    const HostIntersection &hostIntersection ( const int position ) const
    {
      assert( hostIntersection_[ position ] );
      return *hostIntersection_[ position ];
    }

    //! \brief relase host intersections
    void release ()
    {
      hostIntersection_[ inner ] = hostIntersection_[ outer ] = nullptr;
    }

    //! \brief relase either inner or outer host intersection
    void release ( const int position )
    {
      hostIntersection_[ position ] = nullptr;
    }

    //! \brief reset pointer to host intersection
    bool reset ( const int position, const HostIntersection &hostIntersection )
    {
      // reset host intersection
      hostIntersection_[ position ] = &hostIntersection;

      // immediately return for inner intersections
      if( position == inner )
        return true;

      // check, if inner and outer host intersection do have common intersection
      // outer host intersection is set if and only if a non-conforming intersection is valid
      assert( hostIntersection_[ inner ] );
      if( !geometryFactory_.initialize( *hostIntersection_[ inner ], *hostIntersection_[ outer ] ) )
        hostIntersection_[ outer ] = nullptr;
      return hostIntersection_[ position ];
    }

  private:
    Connectivity connectivity_;
    int node_;
    const HostIntersection *hostIntersection_[ 2 ];
    GeometryFactory geometryFactory_;
  };



  // Implementation of MultiSPIntersection::GeometryFactory
  // ------------------------------------------------------

  template< class Traits >
  class MultiSPIntersection< Traits >::GeometryFactory
  {
    // this type
    typedef GeometryFactory This;
    // host geometry
    typedef typename HostIntersection::Geometry HostGeometry;
    // host geometry implementation
    typedef typename HostGeometry::Implementation HostGeometryImp;

  public:
    //! \brief geometry cache
    typedef typename HostGeometryImp::GeometryCache GeometryCache;

  private:
    // tolerance for empty intersections
    static double epsilon () { return 1e-10; }

  public:
    //! \brief emtpy constructor
    GeometryFactory ()
    {}

    //! \brief Initialize geometry factory; returns false if given intersections are disjoint
    bool initialize ( const HostIntersection &inside, const HostIntersection &outside )
    {
      // get corners
      GlobalCoordinate a1, a2, b1, b2;
      getBounds( inside.geometry(), a1, b1 );
      getBounds( outside.geometry(), a2, b2 );

      // compute origin and width of intersection
      for( int i = 0; i < dimension; ++i )
      {
        origin_[ i ] = std::max( a1[ i ], a2[ i ] );
        width_[ i ] = std::min( b1[ i ], b2[ i ] ) - origin_[ i ];
        if( width_[ i ] < 0 )
          return false;
      }

      // get direction
      direction_ = inside.geometry().impl().entityInfo().direction();

      // check if intersection is not empty and set direction
      return ( geometryCache().volume() > epsilon() );
    }

    //! \brief return origin
    GlobalCoordinate origin () const
    {
      return origin_;
    }

    //! \brief return geometry cache
    GeometryCache geometryCache () const
    {
      return GeometryCache( width(), direction() );
    }

    //! \brief return origin relativ to given intersection
    GlobalCoordinate origin ( const HostIntersection &hostIntersection ) const
    {
      typedef typename HostIntersection::EntityPointer HostEntityPointer;
      HostEntityPointer inside = hostIntersection.inside();
      return inside->geometry().local( origin() );
    }

    //! \brief return geometry cache relativ to given intersection
    GeometryCache geometryCache ( const HostIntersection &hostIntersection ) const
    {
      typedef typename HostIntersection::EntityPointer HostEntityPointer;
      HostEntityPointer inside = hostIntersection.inside();

      GlobalCoordinate width;
      inside->geometry().impl().geometryCache().jacobianInverseTransposed().mtv( width_, width );
      return GeometryCache( width, direction() );
    }

  private:
    // return width
    const GlobalCoordinate &width() const
    {
      return width_;
    }

    // return direction
    unsigned int direction () const
    {
      return direction_;
    }

    // get bounds of given geometry
    void getBounds ( const HostGeometry &geometry, GlobalCoordinate &a, GlobalCoordinate &b ) const
    {
      assert( geometry.corners() ==  (1 << (dimension-1)) );
      const int first = 0, last = (1<<(dimension-1))-1;
      a = geometry.corner( first );
      b = geometry.corner( last );
    }

    GlobalCoordinate origin_, width_;
    unsigned int direction_;
  };



  // MultiSPLeafIntersectionTraits
  // -----------------------------

  template< class GridImp >
  class MultiSPLeafIntersectionTraits
  {
    // grid traits
    typedef typename std::remove_const< GridImp >::type::Traits Traits;

  public:
    //! \brief grid dimension
    static const int dimension = Traits::dimension;
    //! \brief mydimsion
    static const int mydimension = dimension-1;

    //! \brief grid type
    typedef typename std::remove_const< GridImp >::type Grid;

    //! \brief type of entity pointer
    typedef typename Traits::template Codim< 0 >::EntityPointer EntityPointer;
    //! \brief geometry type
    typedef typename Traits::template Codim< 1 >::Geometry Geometry;
    //! \brief local geometry type
    typedef typename Traits::template Codim< 1 >::LocalGeometry LocalGeometry;

    //! \brief connectivity type
    typedef typename Traits::LeafConnectivity Connectivity;
    //! \brief host intersection type
    typedef typename Connectivity::HostGridView::Intersection HostIntersection;
  };



  // MultiSPLeafIntersection
  // -----------------------

  /** \brief Leaf intersection
   *
   *  \ingroup IntersectionIterator
   */
  template< class Grid >
  class MultiSPLeafIntersection
  : public MultiSPIntersection< MultiSPLeafIntersectionTraits< Grid > >
  {
    // this type
    typedef MultiSPLeafIntersection< Grid > This;
    // traits class
    typedef MultiSPLeafIntersectionTraits< Grid > Traits;
    // base type
    typedef MultiSPIntersection< Traits > Base;

  public:
    //! \brief constructor
    MultiSPLeafIntersection ( const int node, const typename Base::Connectivity &connectivity )
    : Base( node, connectivity )
    {}
  };



  // MultiSPLevelIntersectionTraits
  // ------------------------------

  template< class GridImp >
  class MultiSPLevelIntersectionTraits
  {
    // grid traits
    typedef typename std::remove_const< GridImp >::type::Traits Traits;

  public:
    //! \brief grid dimension
    static const int dimension = Traits::dimension;
    //! \brief mydimsion
    static const int mydimension = dimension-1;

    //! \brief grid type
    typedef typename std::remove_const< GridImp >::type Grid;

    //! \brief type of entity pointer
    typedef typename Traits::template Codim< 0 >::EntityPointer EntityPointer;
    //! \brief geometry type
    typedef typename Traits::template Codim< 1 >::Geometry Geometry;
    //! \brief local geometry type
    typedef typename Traits::template Codim< 1 >::LocalGeometry LocalGeometry;

    //! \brief connectivity type
    typedef typename Traits::LevelConnectivity Connectivity;
    //! \brief host intersection type
    typedef typename Connectivity::HostGridView::Intersection HostIntersection;
  };



  // MultiSPLevelIntersection
  // ------------------------

  /** \brief Level intersection
   *
   *  \ingroup IntersectionIterator
   */
  template< class Grid >
  class MultiSPLevelIntersection
  : public MultiSPIntersection< MultiSPLevelIntersectionTraits< Grid > >
  {
    // this type
    typedef MultiSPLevelIntersection< Grid > This;
    // traits class
    typedef MultiSPLevelIntersectionTraits< Grid > Traits;
    // base type
    typedef MultiSPIntersection< Traits > Base;

  public:
    //! \brief constructor
    MultiSPLevelIntersection ( const int node, const typename Base::Connectivity &connectivity )
    : Base( node, connectivity )
    {}
  };

} // namespace Dune

#endif // #if HAVE_DUNE_SPGRID

#endif // #ifndef DUNE_MULTISPGRID_INTERSECTION_HH
