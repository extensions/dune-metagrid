#ifndef DUNE_MULTISPGRID_UTILITY_HH
#define DUNE_MULTISPGRID_UTILITY_HH

#include <cassert>
#include <limits>
#include <vector>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>

namespace Dune
{

  // MultiSPCoordinateMap
  // --------------------

  template< class T, class ct, int dim >
  class MultiSPCoordinateMap
  {
    // this type
    typedef MultiSPCoordinateMap< T, ct, dim > This;

  public:
    //! \brief type of values stored in this container
    typedef T value_type;

    //! \brief single coordinate type
    typedef ct ctype;
    static const int dimension = dim;
    //! \brief vector type
    typedef FieldVector< ctype, dimension > Coordinate;

    //! \brief iterator type
    typedef typename std::vector< value_type >::iterator iterator;
    //! \brief const iterator type
    typedef typename std::vector< value_type >::const_iterator const_iterator;

    //! \brief constructor
    MultiSPCoordinateMap ( const ctype tolerance )
    : tolerance_( tolerance )
    {}

    //! \brief return begin iterator
    iterator begin () { return values_.begin(); }
    //! \brief return end iterator
    iterator end () { return values_.end(); }
    //! \brief return begin iterator
    const_iterator begin () const { return values_.begin(); }
    //! \brief return end iterator
    const_iterator end () const { return values_.end(); }

    //! \brief return true, if container is empty
    bool empty () const
    {
      return ( size() > 0 );
    }

    //! \return number of coordinates stored
    size_t size () const
    {
      assert( coordinates_.size() == values_.size() );
      return coordinates_.size();
    }

    //! \brief access to data
    value_type &operator[] ( const Coordinate &x )
    {
      size_t index = std::numeric_limits< size_t >::max();

      // try to find node in storage
      const size_t size = This::size();
      for( size_t i = 0; i < size; ++i )
      {
        if( identify( x, coordinates_[ i ] ) )
        {
          if( index < i )
            DUNE_THROW( InvalidStateException, "Could not uniquely identify coordinate" );
          index = i;
        }
      }

      // insert new node to storage
      if( index == std::numeric_limits< size_t >::max() )
      {
        coordinates_.push_back( x );
        values_.push_back( value_type() );
        index = size;
      }

      // return reference to data
      return values_[ index ];
    }

    //! \brief clear storage
    void clear ()
    {
      coordinates_.clear();
      values_.clear();
    }

    //! \brief search for an element with x as key and return an iterator to it if found
    iterator find ( const Coordinate &x )
    {
      const size_t size = This::size();
      for( size_t i = 0; i < size; ++i )
      {
        if( identify( x, coordinates_[ i ] ) )
          return ( values_.begin() + i );
      }
      return values_.end();
    }

    //! \brief search for an element with x as key and return an iterator to it if found
    const_iterator find ( const Coordinate &x ) const
    {
      const size_t size = This::size();
      for( size_t i = 0; i < size; ++i )
      {
        if( identify( x, coordinates_[ i ] ) )
          return ( values_.begin() + i );
      }
      return values_.end();
    }

  private:
    // return distance ||x - y||_2
    ctype dist ( const Coordinate &x, const Coordinate &y ) const
    {
      return (x - y).two_norm();
    }

    // brief return true, if vectors x and y are identical up to fixed tolerance
    bool identify ( const Coordinate &x, const Coordinate &y ) const
    {
      return ( dist( x, y ) < tolerance_ );
    }

    ctype tolerance_;
    std::vector< Coordinate > coordinates_;
    std::vector< value_type > values_;
  };

} // namespace Dune

#endif // #ifndef DUNE_MULTISPGRID_UTILITY_HH
