#ifndef DUNE_MULTISPGRID_INDEXSET_HH
#define DUNE_MULTISPGRID_INDEXSET_HH

// C++ includes
#include <algorithm>
#include <cassert>
#include <vector>

// dune-common includes
#include <dune/common/typetraits.hh>

// dune-grid includes
#include <dune/grid/common/indexidset.hh>


namespace Dune
{

  // MultiSPIndexSetTraits
  // -------------------

  /** \brief Base class for index sets
   *
   *  \ingroup IndexIdSets
   */

  template< class ConnectivityImp, class GridImp >
  class MultiSPIndexSetTraits
  {
    // grid traits
    typedef typename std::remove_const< GridImp >::type::Traits Traits;

  public:
    //! \brief grid dimension
    static const int dimension = Traits::dimension;

    //!\ brief grid type
    typedef typename std::remove_const< GridImp >::type Grid;
    //! \brief grid types
    template< int codim >
    struct Codim
    : public Traits::template Codim< codim >
    {};

    //! \brief grid view type
    typedef ConnectivityImp Connectivity;
    //! \brief host grid view
    typedef typename Connectivity::HostGridView HostGridView;
    //! \brief host index set
    typedef typename HostGridView::IndexSet HostIndexSet;
    //! \brief index type
    typedef typename HostIndexSet::IndexType IndexType;
  };



  // MultiSPIndexSet
  // -------------

  template< class Traits >
  class MultiSPIndexSet
  : public IndexSet< const typename Traits::Grid, MultiSPIndexSet< Traits >, typename Traits::IndexType >
  {
    // this type
    typedef MultiSPIndexSet< Traits > This;
    // base type
    typedef IndexSet< const typename Traits::Grid, MultiSPIndexSet< Traits >, typename Traits::IndexType > Base;

  public:
    //! \brief index type
    typedef typename Traits::IndexType IndexType;

    //! \brief grid dimensiion
    static const int dimension = Traits::dimension;
    //! \brief grid view type
    typedef typename Traits::Connectivity Connectivity;;
    //! \brief host index set type
    typedef typename Traits::HostIndexSet HostIndexSet;

    //! \brief entity type
    template< int codim >
    struct Codim
    : public Traits::template Codim< codim >
    {};

    //! \brief emtpy constructor
    explicit MultiSPIndexSet ( const Connectivity &connectivity )
    : connectivity_( connectivity ),
      offsets_( dimension+1 )
    {
      // this class has yet to be initialized
      invalidate();
      assert( !*this );
    }

    //! \brief copy constructor
    MultiSPIndexSet ( const This &other )
    : connectivity_( other.connectivity_ ),
      offsets_( dimension+1 )
    {
      // this class has yet to be initialized
      invalidate();
      assert( !*this );
    }

    //! \brief copy constructor
    This &operator= ( const This &other )
    {
      connectivity_ = other.connectivity_;
      offsets_.resize( dimension+1 );

      // this class has yet to be initialized
      invalidate();
      assert( !*this );
      return *this;
    }

    //! \brief cast to bool
    operator bool () const
    {
      // dirty hack, assumes we have at least one codim 0 entity
      return !offsets_[ 0 ].empty();
    }

    //! \brief return size per type
    IndexType size ( const GeometryType &type ) const
    {
      return ( type.isCube() ? size( dimension - type.dim() ) : 0 );
    }

    //! \brief return size per codim
    IndexType size ( const int codim ) const
    {
      assert( *this );
      return offsets_[ codim ][ nodes()-1 ];
    }

    //! \brief return index
    template< class Entity >
    IndexType index ( const Entity &entity ) const
    {
      return index< Entity::codimension >( entity );
    }

    //! \brief return index
    template< int codim >
    IndexType index ( const typename Codim< codim >::Entity &entity ) const
    {
      assert( contains( entity ) );
      const int node = entity.impl().node();
      IndexType result = offset( node, codim );
      result += hostIndexSet( node ).index( entity.impl().hostEntity() );
      return result;
    }

    //! \brief return sub index
    template< class Entity >
    IndexType subIndex ( const Entity &entity, int i, unsigned int codim ) const
    {
      return subIndex< Entity::codimension >( entity, i, codim );
    }

    //! \brief return sub index
    template< int cd >
    IndexType subIndex ( const typename Codim< cd >::Entity &entity, int i, unsigned int codim ) const
    {
      assert( contains( entity ) );
      const int node = entity.impl().node();
      IndexType result = offset( node, cd + codim );
      result += hostIndexSet( node ).template subIndex( entity.impl().hostEntity(), i, codim );
      return result;
    }

    //! \brief return geometry types
    const std::vector< GeometryType > &geomTypes ( int codim ) const
    {
      assert( (codim >= 0) && (codim <= dimension) );
      return connectivity().grid().hostGrid( 0 ).leafIndexSet().geomTypes( codim );
    }

    //! \brief return true if entity is contained in this index set
    template< class EntityType >
    bool contains ( const EntityType &entity ) const
    {
      const int node = entity.impl().node();
      return ( contains( node ) && hostIndexSet( node ).contains( entity.impl().hostEntity() ) );
    }

    //! \brief update index set
    void update () const
    {
      // get number of nodes
      const int nodes = This::nodes();
      for( int codim = 0; codim <= dimension; ++codim )
      {
        // resize and initialize offset vector
        offsets_[ codim ].resize( nodes );
        std::fill( offsets_[ codim ].begin(), offsets_[ codim ].end(), IndexType( 0 ) );

        // compute offsets
        int first = connectivity().first();
        IndexType offset = hostIndexSet( first ).size( codim );

        for( int node = first+1; node <= nodes-1; ++node )
        {
          if( !contains( node ) )
            continue;
          offsets_[ codim ][ node-1 ] = offset;
          offset += hostIndexSet( node ).size( codim );
        }
        offsets_[ codim ][ nodes-1 ] = offset;
      }
      assert( *this );
    }

    // invalidate this index set
    void invalidate () const
    {
      assert( offsets_.size() == dimension+1 );
      for( int codim = 0; codim <= dimension; ++codim )
        offsets_[ codim ].clear();
      assert( !*this );
    }

    //! \brief return true if entities of given node are contained in this index set
    bool contains ( const int node ) const
    {
      return connectivity().contains( node );
    }

    //! \brief return host index set for given node
    const HostIndexSet &hostIndexSet ( const int node ) const
    {
      assert( contains( node ) );
      return connectivity().hostGridView( node ).indexSet();
    }

  private:
    // return connectvitiy
    const Connectivity &connectivity() const
    {
      return connectivity_;
    }

    // return number of nodes
    int nodes () const
    {
      return connectivity().nodes();
    }

    // return offset per node and codim
    IndexType offset ( int node, int codim ) const
    {
      assert( *this );
      return ( node > 0 ) ? offsets_[ codim ][ node-1 ] : 0;
    }

    Connectivity connectivity_;
    mutable std::vector< std::vector< IndexType > > offsets_;
  };



  // MultiSPLeafIndexSetTraits
  // -----------------------

  template< class Grid >
  class MultiSPLeafIndexSetTraits
  : public MultiSPIndexSetTraits< typename std::remove_const< Grid >::type::Traits::LeafConnectivity, Grid >
  {};



  // MultiSPLeafIndexSet
  // -----------------

  /** \brief Leaf index sets
   *
   *  \ingroup IndexIdSets
   */

  template< class Grid >
  class MultiSPLeafIndexSet
  : public MultiSPIndexSet< MultiSPLeafIndexSetTraits< Grid > >
  {
    // this type
    typedef MultiSPLeafIndexSet< Grid > This;
    // traits type
    typedef MultiSPLeafIndexSetTraits< Grid > Traits;
    // base type
    typedef MultiSPIndexSet< Traits > Base;

  public:
    //! \brief constructor
    explicit MultiSPLeafIndexSet ( const Grid &grid )
    : Base( grid.leafConnectivity() )
    {}

    //! \brief constructor
    explicit MultiSPLeafIndexSet ( const typename Traits::Connectivity &connectivity )
    : Base( connectivity )
    {}
  };



  // MultiSPLevelIndexSetTraits
  // -----------------------

  template< class Grid >
  class MultiSPLevelIndexSetTraits
  : public MultiSPIndexSetTraits< typename std::remove_const< Grid >::type::Traits::LevelConnectivity, Grid >
  {};



  // MultiSPLevelIndexSet
  // ------------------

  /** \brief Level index sets
   *
   *  \ingroup IndexIdSets
   */

  template< class Grid >
  class MultiSPLevelIndexSet
  : public MultiSPIndexSet< MultiSPLevelIndexSetTraits< Grid > >
  {
    // this type
    typedef MultiSPLevelIndexSet< Grid > This;
    // traits type
    typedef MultiSPLevelIndexSetTraits< Grid > Traits;
    // base type
    typedef MultiSPIndexSet< Traits > Base;

  public:
    //! \brief constructor
    explicit MultiSPLevelIndexSet ( const int level, const Grid &grid )
    : Base( grid.levelConnectivity( level ) )
    {}

    //! \brief constructor
    explicit MultiSPLevelIndexSet ( const typename Traits::Connectivity &connectivity )
    : Base( connectivity )
    {}
  };

} // namespace Dune

#endif // #ifndef DUNE_MULTISPGRID_INDEXSET_HH
