#ifndef DUNE_SPHEREGRID_DATAHANDLE_HH
#define DUNE_SPHEREGRID_DATAHANDLE_HH

#include <cstddef>

#include <type_traits>

#include <dune/grid/common/datahandleif.hh>

namespace Dune
{

  // SphereGridDataHandle
  // --------------------

  template< class WrappedHandle, class Grid >
  class SphereGridDataHandle
  : public CommDataHandleIF< SphereGridDataHandle< WrappedHandle, Grid >, typename WrappedHandle::DataType >
  {
    typedef SphereGridDataHandle< WrappedHandle, Grid > This;

    // type of traits
    typedef typename std::remove_const_t< Grid >::Traits Traits;

    template< int codim >
    struct Codim
    {
      // type of entity
      typedef typename Traits::template Codim< codim >::Entity Entity;
    };

  public:
    // type of data to be communicated
    typedef typename WrappedHandle::DataType DataType;

    typedef typename Traits::MapToSphere MapToSphere;

    typedef CommDataHandleIF< This, DataType > DataHandleIF;

    SphereGridDataHandle ( WrappedHandle &wrappedHandle, const MapToSphere &mapToSphere )
      : wrappedHandle_( wrappedHandle ), mapToSphere_( mapToSphere )
    {}

    SphereGridDataHandle ( const This & ) = delete;
    SphereGridDataHandle ( This && ) = delete;

    bool contains ( int dim, int codim ) const
    {
      return wrappedHandle_.contains( dim, codim );
    }

    bool fixedSize ( int dim, int codim ) const
    {
      return wrappedHandle_.fixedSize( dim, codim );
    }

    template< class HostEntity >
    size_t size ( const HostEntity &hostEntity ) const
    {
      typedef typename Codim< HostEntity::codimension >::Entity Entity;
      const Entity entity( typename Entity::Implementation( hostEntity, mapToSphere_ ) );
      return wrappedHandle_.size( entity );
    }

    template< class MessageBuffer, class HostEntity >
    void gather ( MessageBuffer &buffer, const HostEntity &hostEntity ) const
    {
      typedef typename Codim< HostEntity::codimension >::Entity Entity;
      const Entity entity( typename Entity::Implementation( hostEntity, mapToSphere_ ) );
      wrappedHandle_.gather( buffer, entity );
    }

    template< class MessageBuffer, class HostEntity >
    void scatter ( MessageBuffer &buffer, const HostEntity &hostEntity, size_t size )
    {
      typedef typename Codim< HostEntity::codimension >::Entity Entity;
      const Entity entity( typename Entity::Implementation( hostEntity, mapToSphere_ ) );
      wrappedHandle_.scatter( buffer, entity, size );
    }

  private:
    WrappedHandle &wrappedHandle_;
    MapToSphere mapToSphere_;
  };



  // SphereGridWrappedDofManager
  // ---------------------------

  template< class WrappedDofManager, class Grid >
  class SphereGridWrappedDofManager
  {
    typedef SphereGridWrappedDofManager< WrappedDofManager, Grid > This;

    typedef typename std::remove_const_t< Grid >::Traits Traits;

    // type of element (i.e., entity of codimension 0)
    typedef typename Traits::template Codim< 0 >::Entity Element;

    // type of host element (i.e., host entity of codimension 0)
    typedef typename Traits::HostGrid::template Codim< 0 >::Entity HostElement;

    typedef typename Traits::MapToSphere MapToSphere;

  public:
    SphereGridWrappedDofManager ( WrappedDofManager &wrappedDofManager, const MapToSphere &mapToSphere )
      : wrappedDofManager_( wrappedDofManager ), mapToSphere_( mapToSphere )
    {}

    SphereGridWrappedDofManager ( const This & ) = delete;
    SphereGridWrappedDofManager ( This && ) = delete;

    template< class MessageBuffer >
    void inlineData ( MessageBuffer &buffer, const HostElement &hostElement )
    {
      const Element element( typename Element::Implementation( hostElement, mapToSphere_ ) );
      wrappedDofManager_.inlineData( buffer, element );
    }

    template< class MessageBuffer >
    void xtractData ( MessageBuffer &buffer, const HostElement &hostElement, std::size_t newElements )
    {
      const Element element( typename Element::Implementation( hostElement, mapToSphere_ ) );
      wrappedDofManager_.xtractData( buffer, element, newElements );
    }

    void compress () { wrappedDofManager_.compress(); }

  private:
    WrappedDofManager &wrappedDofManager_;
    MapToSphere mapToSphere_;
  };

} // namespace Dune

#endif // #ifndef DUNE_SPHEREGRID_DATAHANDLE_HH
