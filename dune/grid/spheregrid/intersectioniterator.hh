#ifndef DUNE_SPHEREGRID_INTERSECTIONITERATOR_HH
#define DUNE_SPHEREGRID_INTERSECTIONITERATOR_HH

#include <dune/grid/spheregrid/intersection.hh>

namespace Dune
{

  // SphereGridIntersectionIterator
  // ------------------------------

  template< class Grid, class HostIntersectionIterator >
  class SphereGridIntersectionIterator
  {
    typedef SphereGridIntersectionIterator< Grid, HostIntersectionIterator > This;

    typedef typename std::remove_const_t< Grid >::Traits Traits;

    typedef SphereGridIntersection< Grid, typename HostIntersectionIterator::Intersection > IntersectionImpl;

  public:
    typedef Dune::Intersection< Grid, IntersectionImpl > Intersection;

    typedef typename Traits::MapToSphere MapToSphere;

  private:
    struct Storage
      : public MapToSphere
    {
      Storage () = default;

      Storage ( HostIntersectionIterator hostIterator, MapToSphere mapToSphere )
        : MapToSphere( std::move( mapToSphere ) ), hostIterator( std::move( hostIterator ) )
      {}

      HostIntersectionIterator hostIterator;
    };

  public:
    SphereGridIntersectionIterator () = default;

    SphereGridIntersectionIterator ( HostIntersectionIterator hostIterator, MapToSphere mapToSphere )
      : storage_( std::move( hostIterator ), std::move( mapToSphere ) )
    {}

    bool equals ( const This &other ) const { return (hostIterator() == other.hostIterator()); }

    void increment () { ++hostIterator(); }

    Intersection dereference () const { return Intersection( IntersectionImpl( *hostIterator(), mapToSphere() ) ); }

    const HostIntersectionIterator &hostIterator () const { return storage_.hostIterator; }
    HostIntersectionIterator &hostIterator () { return storage_.hostIterator; }

    const MapToSphere &mapToSphere () const { return storage_; }

  private:
    Storage storage_;
  };

} // namespace Dune

#endif // #ifndef DUNE_SPHEREGRID_INTERSECTIONITERATOR_HH
