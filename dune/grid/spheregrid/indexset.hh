#ifndef DUNE_SPHEREGRID_INDEXSETS_HH
#define DUNE_SPHEREGRID_INDEXSETS_HH

#include <cassert>

#include <array>
#include <type_traits>
#include <vector>

#include <dune/grid/common/gridenums.hh>
#include <dune/grid/common/indexidset.hh>

#include <dune/grid/spheregrid/declaration.hh>

namespace Dune
{

  // SphereGridIndexSet
  // ------------------

  template< class Grid, class HostIndexSet >
  class SphereGridIndexSet
    : public IndexSet< Grid, SphereGridIndexSet< Grid, HostIndexSet >, typename HostIndexSet::IndexType, std::array< GeometryType, 1 > >
  {
    typedef SphereGridIndexSet< Grid, HostIndexSet > This;
    typedef IndexSet< Grid, SphereGridIndexSet< Grid, HostIndexSet >, typename HostIndexSet::IndexType, std::array< GeometryType, 1 > > Base;

    typedef typename std::remove_const_t< Grid >::Traits Traits;

    typedef typename Traits::HostGrid HostGrid;

  public:
    static constexpr int dimension = Traits::dimension;

    using typename Base::IndexType;
    using typename Base::Types;

    SphereGridIndexSet () = default;

    explicit SphereGridIndexSet ( const HostIndexSet &hostIndexSet ) : hostIndexSet_( &hostIndexSet ) {}

    SphereGridIndexSet ( const This &other ) : hostIndexSet_( other.hostIndexSet_ ) {}

    const This &operator= ( const This &other )
    {
      hostIndexSet_ = other.hostIndexSet_;
      return *this;
    }

    template< class Entity >
    IndexType index ( const Entity &entity ) const
    {
      return index< Entity::codimension >( entity );
    }

    template< int cd >
    IndexType index ( const typename Traits::template Codim< cd >::Entity &entity ) const
    {
      return index( Grid::template getHostEntity< cd >( entity ) );
    }

    template< int cd >
    IndexType index ( const typename Traits::HostGrid::template Codim< cd >::Entity &entity ) const
    {
      return hostIndexSet().index( entity );
    }

    template< class Entity >
    IndexType subIndex ( const Entity &entity, int i, unsigned int codim ) const
    {
      return subIndex< Entity::codimension >( entity, i, codim );
    }

    template< int cd >
    IndexType subIndex ( const typename Traits::template Codim< cd >::Entity &entity, int i, unsigned int codim ) const
    {
      return subIndex( Grid::template getHostEntity< cd >( entity ), i, codim );
    }

    template< int cd >
    IndexType subIndex ( const typename Traits::HostGrid::template Codim< cd >::Entity &entity, int i, unsigned int codim ) const
    {
      return hostIndexSet().subIndex( entity, i, codim );
    }

    IndexType size ( GeometryType type ) const
    {
      return hostIndexSet().size( type );
    }

    int size ( int codim ) const
    {
      return hostIndexSet().size( codim );
    }

    template< class Entity >
    bool contains ( const Entity &entity ) const
    {
      static const int cc = Entity::codimension;
      return hostIndexSet().contains( Grid::template getHostEntity< cc >( entity ) );
    }

    Types types ( int codim ) const { return {{ GeometryTypes::simplex( dimension - codim ) }}; }

    explicit operator bool () const { return bool( hostIndexSet_ ); }

  private:
    const HostIndexSet &hostIndexSet () const
    {
      assert( hostIndexSet_ );
      return *hostIndexSet_;
    }

    const HostIndexSet *hostIndexSet_ = nullptr;
  };

} // namespace Dune

#endif // #ifndef DUNE_SPHEREGRID_INDEXSETS_HH
