#ifndef DUNE_SPHEREGRID_MAPTOSPHERE_HH
#define DUNE_SPHEREGRID_MAPTOSPHERE_HH

#include <limits>

#include <dune/common/fvector.hh>

namespace Dune
{

  template< class ctype >
  struct DefaultMapToSphere
  {
    typedef FieldVector< ctype, 3 > DomainVector;
    typedef FieldVector< ctype, 3 > RangeVector;

    DefaultMapToSphere ( ctype radius = 1.0 ) : radius_( radius ) {}

    RangeVector operator() ( const DomainVector &x ) const
    {
      RangeVector y( x );
      y *= radius() / x.two_norm();
      return y;
    }

    ctype radius () const { return radius_; }

  private:
    ctype radius_;
  };

} // namespace Dune

#endif // #ifndef DUNE_SPHEREGRID_MAPTOSPHERE_HH
