#ifndef DUNE_SPHEREGRID_ENTITY_HH
#define DUNE_SPHEREGRID_ENTITY_HH

#include <cassert>

#include <type_traits>
#include <utility>

#include <dune/geometry/type.hh>

#include <dune/grid/common/entity.hh>
#include <dune/grid/geometrygrid/hostcorners.hh>

#include <dune/grid/spheregrid/geometry.hh>


namespace Dune
{

  // External Forward Declarations
  // -----------------------------

  template< class, class > class SphereGridIntersection;
  template< class, class > class SphereGridIntersectionIterator;

  template< class, class > class SphereGridIterator;



  // SphereGridEntityCoordVector
  // ---------------------------

  template< class HostEntity, class MapToSphere >
  class SphereGridEntityCoordVector
  : private GeoGrid::HostCorners< HostEntity >
  {
    typedef GeoGrid::HostCorners< HostEntity > Base;

  public:
    SphereGridEntityCoordVector ( const HostEntity &hostEntity, MapToSphere mapToSphere )
    : Base( hostEntity ),
      mapToSphere_( mapToSphere )
    {}

    FieldVector< typename HostEntity::Geometry::ctype, 3 >
    operator[] ( int i ) const
    {
      return mapToSphere_( static_cast< const Base & >( *this )[ i ] );
    }

  private:
    MapToSphere mapToSphere_;
  };



  // SphereGridEntity
  // ----------------

  /** \copydoc SphereGridEntity
   *
   *  \nosubgrouping
   */
  template< int codim, int dim, class Grid >
  class SphereGridEntity
  {
    typedef typename std::remove_const_t< Grid >::Traits Traits;

  public:
    /** \name Attributes
     *  \{ */

    //! codimensioon of the entity
    static const int codimension = codim;
    //! dimension of the grid
    static const int dimension = Traits::dimension;
    //! dimension of the entity
    static const int mydimension = dimension - codimension;
    //! dimension of the world
    static const int dimensionworld = Traits::dimensionworld;

    /** \} */

    /** \name Types Required by DUNE
     *  \{ */

    //! coordinate type of the grid
    typedef typename Traits::ctype ctype;

    typedef typename Traits::MapToSphere MapToSphere;

    //! type of corresponding entity seed
    typedef typename Grid::template Codim< codimension >::EntitySeed EntitySeed;
    //! type of corresponding geometry
    typedef typename Traits::template Codim< codimension >::Geometry Geometry;

    /** \} */

  private:
    // type of entity implementation
    typedef typename Traits::template Codim< codimension > :: EntityImpl  EntityImpl;

    typedef typename Traits::HostGrid HostGrid;

  public:
    /** \name Host Types
     *  \{ */

    //! type of corresponding host entity
    typedef typename HostGrid::template Codim< codimension >::Entity HostEntity;
    /** \} */

  private:
    typedef SphereGridGeometry< mydimension, dimensionworld, Grid > GeometryImpl;

    struct Storage
      : public MapToSphere
    {
      Storage () = default;

      Storage ( HostEntity hostEntity, MapToSphere mapToSphere )
        : MapToSphere( std::move( mapToSphere ) ), hostEntity( std::move( hostEntity ) )
      {}

      HostEntity hostEntity;
    };

  public:
    /** \name Construction, Initialization and Destruction
     *  \{ */

    /** \brief construct a null entity */
    SphereGridEntity () = default;

    /** \brief construct an initialized entity
     *
     *  \param[in]  hostEntity  corresponding entity in the host grid
     */
    SphereGridEntity ( HostEntity hostEntity, MapToSphere mapToSphere )
      : storage_( std::move( hostEntity ), mapToSphere )
    {}

    /** \} */

    operator bool () const { return bool( storage_.hostEntity ); }

    /** \name Methods Shared by Entities of All Codimensions
     *  \{ */

    /** \brief obtain the name of the corresponding reference element
     *
     *  This type can be used to access the DUNE reference element.
     */
    GeometryType type () const
    {
      // as only triangular host grids are supported, we overwrite the type for
      // performance reasons
      assert( hostEntity().type() == GeometryTypes::simplex( mydimension ) );
      return GeometryTypes::simplex( mydimension );
    }

    /** \brief obtain the level of this entity */
    int level () const
    {
      return hostEntity().level();
    }

    /** \brief obtain the partition type of this entity */
    PartitionType partitionType () const
    {
      return hostEntity().partitionType();
    }

    /** obtain the geometry of this entity */
    Geometry geometry () const
    {
      typedef SphereGridEntityCoordVector< HostEntity, MapToSphere > CoordVector;
      return Geometry( GeometryImpl( CoordVector( hostEntity(), mapToSphere() ), mapToSphere().radius() ) );
    }

    /** \brief return EntitySeed of host grid entity */
    EntitySeed seed () const { return hostEntity().seed(); }

    /** return true if entity equals given entity */
    bool equals( const EntityImpl& other ) const { return hostEntity() ==  other.hostEntity(); }

    unsigned int subEntities ( unsigned int cc ) const { return hostEntity().subEntities( cc ); }

    /** \} */


    /** \name Methods Supporting the Grid Implementation
     *  \{ */

    const HostEntity &hostEntity () const { return storage_.hostEntity; }

    const MapToSphere &mapToSphere () const { return storage_; }

    /** \} */

  private:
    Storage storage_;
  };



  // SphereGridEntity for codimension 0
  // ----------------------------------

  /** \copydoc SphereGridEntity
   *
   *  \nosubgrouping
   */
  template< int dim, class Grid >
  class SphereGridEntity< 0, dim, Grid >
  {
    typedef typename std::remove_const_t< Grid >::Traits Traits;

    typedef typename Traits::HostGrid HostGrid;

  public:
    /** \name Attributes
     *  \{ */

    //! codimensioon of the entity
    static const int codimension = 0;
    //! dimension of the grid
    static const int dimension = Traits::dimension;
    //! dimension of the entity
    static const int mydimension = dimension - codimension;
    //! dimension of the world
    static const int dimensionworld = Traits::dimensionworld;

    /** \} */

  public:
    /** \name Host Types
     *  \{ */

    //! type of corresponding host entity
    typedef typename HostGrid::template Codim< codimension >::Entity HostEntity;

    /** \} */

  public:
    /** \name Types Required by DUNE
     *  \{ */

    //! coordinate type of the grid
    typedef typename Traits::ctype ctype;

    typedef typename Traits::MapToSphere MapToSphere;

    //! type of corresponding entity seed
    typedef typename Grid::template Codim< codimension >::EntitySeed EntitySeed;
    //! type of corresponding geometry
    typedef typename Traits::template Codim< codimension >::Geometry Geometry;
    //! type of corresponding local geometry
    typedef typename Traits::template Codim< codimension >::LocalGeometry LocalGeometry;

    //! type of hierarchic iterator
    typedef typename Traits::HierarchicIterator HierarchicIterator;

    /** \} */

  private:
    // type of entity implementation
    typedef typename Traits::template Codim< codimension > :: EntityImpl  EntityImpl;

    typedef SphereGridGeometry< mydimension, dimensionworld, Grid > GeometryImpl;

    struct Storage
      : public MapToSphere
    {
      Storage () = default;

      Storage ( HostEntity hostEntity, MapToSphere mapToSphere )
        : MapToSphere( std::move( mapToSphere ) ), hostEntity( std::move( hostEntity ) )
      {}

      HostEntity hostEntity;
    };

  public:
    /** \name Construction, Initialization and Destruction
     *  \{ */

    /** \brief construct a null entity */
    SphereGridEntity () = default;

    /** \brief construct an initialized entity
     *
     *  \param[in]  hostEntity  corresponding entity in the host grid
     */
    SphereGridEntity ( HostEntity hostEntity, MapToSphere mapToSphere )
      : storage_( std::move( hostEntity ), mapToSphere )
    {}

    /** \} */

    operator bool () const { return bool( storage_.hostEntity ); }

    /** \name Methods Shared by Entities of All Codimensions
     *  \{ */

    /** \brief obtain the name of the corresponding reference element
     *
     *  This type can be used to access the DUNE reference element.
     */
    GeometryType type () const
    {
      // as only triangular host grids are supported, we overwrite the type for
      // performance reasons
      assert( hostEntity().type() == GeometryTypes::simplex( mydimension ) );
      return GeometryTypes::simplex( mydimension );
    }

    /** \brief obtain the level of this entity */
    int level () const
    {
      return hostEntity().level();
    }

    /** \brief obtain the partition type of this entity */
    PartitionType partitionType () const
    {
      return hostEntity().partitionType();
    }

    /** obtain the geometry of this entity */
    Geometry geometry () const
    {
      typedef SphereGridEntityCoordVector< HostEntity, MapToSphere > CoordVector;
      return Geometry( GeometryImpl( CoordVector( hostEntity(), mapToSphere() ), mapToSphere().radius() ) );
    }

    /** \brief return EntitySeed of host grid entity */
    EntitySeed seed () const { return hostEntity().seed(); }

    /** return true if entity equals given entity */
    bool equals( const EntityImpl& other ) const { return hostEntity() ==  other.hostEntity(); }

    /** \} */

    unsigned int subEntities ( unsigned int cc ) const { return hostEntity().subEntities( cc ); }

    template< int codim >
    SphereGridEntity< codim, dim, Grid > subEntity ( int i ) const
    {
      return SphereGridEntity< codim, dim, Grid >( hostEntity().template subEntity< codim >( i ), mapToSphere() );
    }

    bool hasBoundaryIntersections () const
    {
      return hostEntity().hasBoundaryIntersections();
    }

    bool isLeaf () const { return hostEntity().isLeaf(); }

    SphereGridEntity< 0, dim, Grid > father () const { return SphereGridEntity< 0, dim, Grid >( hostEntity().father(), mapToSphere() ); }

    bool hasFather () const { return hostEntity().hasFather(); }

    LocalGeometry geometryInFather () const { return hostEntity().geometryInFather(); }

    HierarchicIterator hbegin ( int maxLevel ) const
    {
      typedef SphereGridIterator< Grid, typename HostEntity::HierarchicIterator > IteratorImpl;
      return IteratorImpl( hostEntity().hbegin( maxLevel ), mapToSphere() );
    }

    HierarchicIterator hend ( int maxLevel ) const
    {
      typedef SphereGridIterator< Grid, typename HostEntity::HierarchicIterator > IteratorImpl;
      return IteratorImpl( hostEntity().hend( maxLevel ), mapToSphere() );
    }

    bool isRegular () const
    {
      return hostEntity().isRegular();
    }

    bool isNew () const
    {
      return hostEntity().isNew();
    }

    bool mightVanish () const
    {
      return hostEntity().mightVanish();
    }

    const HostEntity &hostEntity () const { return storage_.hostEntity; }

    const MapToSphere &mapToSphere () const { return storage_; }

    /** \} */

  private:
    Storage storage_;
  };

} // namespace Dune

#endif // #ifndef DUNE_SPHEREGRID_ENTITY_HH
