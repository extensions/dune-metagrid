#ifndef DUNE_SPHEREGRID_PERSISTENTCONTAINER_HH
#define DUNE_SPHEREGRID_PERSISTENTCONTAINER_HH

#include <dune/grid/utility/persistentcontainerwrapper.hh>

#include <dune/grid/spheregrid/declaration.hh>
#include <dune/grid/spheregrid/hostgridaccess.hh>

namespace Dune
{

  // PersistentContainer for SphereGrid
  // ----------------------------------

  template< class HostGrid, class MapToSphere, class T >
  class PersistentContainer< SphereGrid< HostGrid, MapToSphere >, T >
  : public PersistentContainerWrapper< SphereGrid< HostGrid, MapToSphere >, T >
  {
    typedef PersistentContainerWrapper< SphereGrid< HostGrid, MapToSphere >, T > Base;

  public:
    typedef typename Base::Grid Grid;
    typedef typename Base::Value Value;

    PersistentContainer ( const Grid &grid, int codim, const Value &value = Value() )
    : Base( grid, codim, value )
    {}
  };

} // namespace Dune

#endif // #ifndef DUNE_SPHEREGRID_PERSISTENTCONTAINER_HH
