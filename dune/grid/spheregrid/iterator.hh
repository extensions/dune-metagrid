#ifndef DUNE_SPHEREGRID_ITERATOR_HH
#define DUNE_SPHEREGRID_ITERATOR_HH

#include <type_traits>
#include <utility>

#include <dune/geometry/referenceelements.hh>

#include <dune/grid/common/entityiterator.hh>

namespace Dune
{

  // SphereGridIterator
  // ------------------

  template< class Grid, class HostIterator >
  class SphereGridIterator
  {
    typedef SphereGridIterator< Grid, HostIterator > This;

    typedef typename std::remove_const_t< Grid >::Traits Traits;

  public:
    static constexpr int codimension = HostIterator::Entity::codimension;
    static constexpr int dimension = HostIterator::Entity::dimension;

    typedef typename Traits::template Codim< codimension >::Entity Entity;

    typedef typename Traits::MapToSphere MapToSphere;

  private:
    typedef SphereGridEntity< codimension, dimension, Grid > EntityImpl;

    struct Storage
      : public MapToSphere
    {
      Storage ( HostIterator hostIterator, MapToSphere mapToSphere )
        : MapToSphere( std::move( mapToSphere ) ), hostIterator( std::move( hostIterator ) )
      {}

      HostIterator hostIterator;
    };

  public:
    SphereGridIterator ( HostIterator hostIterator, MapToSphere mapToSphere )
      : storage_( std::move( hostIterator ), std::move( mapToSphere ) )
    {}

    bool equals ( const This &other ) const { return (hostIterator() == other.hostIterator()); }

    Entity dereference () const { return Entity( EntityImpl( *hostIterator(), mapToSphere() ) ); }

    void increment () { ++hostIterator(); }

    const HostIterator &hostIterator () const { return storage_.hostIterator; }
    HostIterator &hostIterator () { return storage_.hostIterator; }

    const MapToSphere &mapToSphere () const { return storage_; }

  protected:
    Storage storage_;
  };

} // namespace Dune

#endif // #ifndef DUNE_SPHEREGRID_ITERATOR_HH
