#ifndef DUNE_SPHEREGRID_BACKUPRESTORE_HH
#define DUNE_SPHEREGRID_BACKUPRESTORE_HH

#include <dune/grid/common/backuprestore.hh>

#include <dune/grid/spheregrid/capabilities.hh>

namespace Dune
{

  // BackupRestoreFacility for SphereGrid
  // ------------------------------------

  template< class HostGrid, class MapToSphere >
  struct BackupRestoreFacility< SphereGrid< HostGrid, MapToSphere > >
  {
    typedef SphereGrid< HostGrid, MapToSphere > Grid;
    typedef BackupRestoreFacility< HostGrid > HostBackupRestoreFacility;

    static void backup ( const Grid &grid, const std::string &path, const std::string &fileprefix )
    {
      HostBackupRestoreFacility::backup( grid.hostGrid(), path, fileprefix );
    }

    static void backup ( const Grid &grid, std::ostream &stream )
    {
      HostBackupRestoreFacility::backup( grid.hostGrid(), stream );
    }

    static Grid *restore ( const std::string &path, const std::string &fileprefix )
    {
      std::shared_ptr< HostGrid > hostGridPtr( HostBackupRestoreFacility::restore( path, fileprefix ) );
      return new Grid( hostGridPtr );
    }

    static Grid *restore ( std::istream &stream )
    {
      std::shared_ptr< HostGrid > hostGridPtr( HostBackupRestoreFacility::restore( stream ) );
      return new Grid( hostGridPtr );
    }
  };

} // namespace Dune

#endif // #ifndef DUNE_SPHEREGRID_BACKUPRESTORE_HH
