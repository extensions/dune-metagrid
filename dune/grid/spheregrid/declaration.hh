#ifndef DUNE_SPHEREGRID_DECLARATION_HH
#define DUNE_SPHEREGRID_DECLARATION_HH

namespace Dune
{

  template< class HostGrid, class MapToSphere >
  class SphereGrid;

} // namespace Dune

#endif // #ifndef DUNE_SPHEREGRID_DECLARATION_HH
