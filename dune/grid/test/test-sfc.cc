#include <config.h>

// iostream includes
#include <iostream>
#include <fstream>
#include<vector>

#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/io/file/dgfparser/dgfyasp.hh>
#include <dune/grid/yaspgrid.hh>

#include <dune/grid/parallelgrid/sfciterator.hh>

// print space filling curve
template <class HGridType>
void printCurve( HGridType &grid, std::ostream& out )
{
  typedef typename HGridType :: LeafGridView LeafView ;
  typedef typename HGridType :: template Codim< 0 > :: Geometry Geometry;

  int count = 0;
  Dune :: SpaceFillingCurveIterator< LeafView > it( grid.leafGridView() );
  for( it.first(); !it.done(); it.next(), ++count )
  {
    const Geometry& geo = it.item().geometry();
    out << geo.center() << std::endl;
  }
  if( count != grid.size( 0 ) )
    DUNE_THROW( Dune :: InvalidStateException, "SFC iterator worng" );
}

// print space filling curve
template <class HGridType>
void printGrid( HGridType &grid, std::ostream& out )
{
  typedef typename HGridType :: LeafGridView LeafView ;
  typedef typename HGridType :: template Codim< 0 > :: Geometry Geometry;
  Dune :: SpaceFillingCurveIterator< LeafView > it( grid.leafGridView() );
  for( it.first(); !it.done(); it.next() )
  {
    const Geometry& geo = it.item().geometry();

    const int corners = geo.corners();
    for( int i=0; i<corners; ++i )
      out << geo.corner( i ) << std::endl;
  }
}


// main
// ----

int main ( int argc, char **argv )
try
{
  // initialize MPI, if necessary
  Dune::MPIHelper::instance( argc, argv );

  // type of hierarchical grid
  //typedef Dune :: AlbertaGrid< 2 , 2 > GridType;
  typedef Dune :: YaspGrid< 3 > HGridType ;
  //typedef Dune :: GridSelector :: GridType  HGridType ;

  std::string gridfile( "3dcube.dgf" );

  // construct macro using the DGF Parser
  Dune::GridPtr< HGridType > gridPtr( gridfile );
  HGridType& grid = *gridPtr ;

  std::ofstream file( "curve.gnu" );
  printCurve( grid, file );

  std::ofstream gridout( "grid.gnu" );
  printGrid( grid, gridout );

  return 0;
}
catch( const Dune::Exception &exception )
{
  std::cerr << "Error: " << exception << std::endl;
  return 1;
}
