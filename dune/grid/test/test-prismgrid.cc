#define DISABLE_DEPRECATED_METHOD_CHECK 1

#include <config.h>

#include <cstddef>
#include <iostream>

#include <dune/common/exceptions.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/common/unused.hh>

#include <dune/grid/test/gridcheck.hh>
#include <dune/grid/test/checkgeometryinfather.hh>
#include <dune/grid/test/checkintersectionit.hh>
#include <dune/grid/test/checkiterators.hh>
#include <dune/grid/test/checkcommunicate.hh>

#include <dune/grid/prismgrid.hh>
#include <dune/grid/prismgrid/hostgridaccess.hh>


using namespace Dune;


template< class Grid >
void traverseColumns ( const Grid &grid )
{
  // access host grid
  typedef typename HostGridAccess< Grid >::HostGrid HostGrid;
  const HostGrid &hostGrid = HostGridAccess< Grid >::hostGrid( grid );

  // host grid leaf view
  typedef typename HostGrid::LeafGridView HostGridView;
  HostGridView hostGridView = hostGrid.leafView();

  // count elements
  std::size_t count = 0;

  // traverse host grid's leaf elements
  typedef typename HostGridView::template Codim< 0 >::Iterator HostIterator;
  const HostIterator end = hostGridView.template end< 0 >();
  for( HostIterator it = hostGridView.template begin< 0 >(); it != end; ++it )
  {
    // get host grid element
    const typename HostIterator::Entity &hostEntity = *it;

    // traverse column over host grid element
    typedef typename Grid::template Codim< 0 >::ColumnIterator ColumnIterator;
    const ColumnIterator cend = grid.template cend< 0 >( hostEntity );
    for( ColumnIterator cit = grid.template cbegin< 0 >( hostEntity ); cit != cend; ++cit )
    {
      const typename Grid::template Codim< 0 >::Entity &entity DUNE_UNUSED = *cit;
      ++count;
    }
  }

  if( count != std::size_t( grid.size( 0 ) ) )
    DUNE_THROW( InvalidStateException, "Sizes do not match." );
}


template< class Grid >
void test ( Grid &grid )
{
  gridcheck( grid );
  checkIterators( grid.leafView() );
  checkGeometryInFather( grid );
  checkIntersectionIterator( grid );
}


int main ( int argc, char ** argv )
{
  try
  {
    // initialize MPI
    MPIHelper::instance( argc, argv );

    // read parameters from file
    ParameterTree parameter;
    ParameterTreeParser::readINITree( "parameter", parameter );

    // create host grid
    typedef GridSelector::GridType HostGrid;
    std::string filename = parameter[ "grid file" ];
    GridPtr< HostGrid > hgridPtr( filename );
    HostGrid &hostGrid = *hgridPtr;

    // initial refinement
    int startLevel = parameter.get( "start level", 1 );
    hostGrid.globalRefine( startLevel );

    // create interval
    typedef HostGrid::ctype ctype;
    ctype a = parameter.get( "a", 0 );
    ctype b = parameter.get( "b", 1 );

    // create level function
    const int layers = parameter.get( "layers", 10 );

    // create grid
    typedef PrismGrid< HostGrid > Grid;
    Grid grid( hostGrid, a, b, layers );

    // run tests
    test( grid );

    // traverse columns
    traverseColumns( grid );
  }
  catch ( const std::exception &e )
  {
    std::cout << "STL ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch ( const Dune::Exception &e )
  {
    std::cout << "DUNE ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (...)
  {
    std::cout << "Unknown ERROR" << std::endl;
    return 1;
  }
  return 0;
}
