#include <config.h>

#include <iostream>
#include <sstream>
#include <string>

#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/cacheitgrid.hh>
#include <dune/grid/io/file/dgfparser/dgfwriter.hh>

#include <dune/grid/test/gridcheck.hh>

#include <dune/grid/test/checkgeometryinfather.hh>
#include <dune/grid/test/checkintersectionit.hh>
#include <dune/grid/test/checkcommunicate.hh>
#include <dune/grid/test/checkiterators.hh>
#include <dune/grid/test/checkpartition.hh>

using namespace Dune;

template <bool leafconform, class Grid>
void checkCapabilities(const Grid& grid)
{
  static_assert( Dune::Capabilities::isLevelwiseConforming< Grid > :: v == ! leafconform,
                 "isLevelwiseConforming is not set correctly");
  static_assert( Dune::Capabilities::isLeafwiseConforming< Grid > :: v == leafconform,
                 "isLevelwiseConforming is not set correctly");
   static const bool hasEntity = Dune::Capabilities::hasEntity<Grid, 1> :: v == true;
  static_assert( hasEntity,
                 "hasEntity is not set correctly");
  static_assert( Dune::Capabilities::hasBackupRestoreFacilities< Grid > :: v == true,
                 "hasBackupRestoreFacilities is not set correctly");

   static const bool reallyParallel =
#if ALU3DGRID_PARALLEL && ALU2DGRID_PARALLEL
    true ;
#elif ALU3DGRID_PARALLEL
    Grid :: dimension == 3;
#else
    false ;
#endif
   static const bool reallyCanCommunicate =
#if ALU3DGRID_PARALLEL && ALU2DGRID_PARALLEL
    true ;
#elif ALU3DGRID_PARALLEL
    Grid :: dimension == 3;
#else
    false ;
#endif
   static const bool canCommunicate = Dune::Capabilities::canCommunicate< Grid, 1 > :: v
     == reallyCanCommunicate;
  static_assert( canCommunicate,
                 "canCommunicate is not set correctly");
}

template <class GridType>
void makeNonConfGrid(GridType &grid,int level,int adapt) {
  int myrank = grid.comm().rank();
  grid.loadBalance();
  grid.globalRefine(level);
  grid.loadBalance();
  for (int i=0;i<adapt;i++)
  {
    if (myrank==0)
    {
      typedef typename GridType :: template Codim<0> ::
            template Partition<Interior_Partition> :: LeafIterator LeafIterator;

      LeafIterator endit = grid.template leafend<0,Interior_Partition>   ();
      int nr = 0;
      int size = grid.size(0);
      for(LeafIterator it    = grid.template leafbegin<0,Interior_Partition> ();
          it != endit ; ++it,nr++ )
      {
        grid.mark(1, *it );
        if (nr>size*0.8) break;
      }
    }
    grid.adapt();
    grid.postAdapt();
    grid.loadBalance();
  }
}

template <class GridView>
void writeFile( const GridView& gridView )
{
  DGFWriter< GridView > writer( gridView );
  writer.write( "dump.dgf" );
}

template< class Grid >
void doCheck ( Grid &grid, const bool display = false )
{
  std::cerr << ">>> Checking grid..." << std::endl;
  gridcheck( grid );
  checkIterators( grid.leafGridView() );
  checkPartitionType( grid.leafGridView() );
  std::cerr << ">>> Checking intersections..." << std::endl;
  checkIntersectionIterator( grid );
  if( grid.maxLevel() > 0 )
  {
    std::cerr << ">>> Checking geometry in father..." << std::endl;
    checkGeometryInFather( grid );
  }

  std::cerr << ">>> Checking communication..." << std::endl;
  checkCommunication( grid, -1, std::cout );
}

template< class Grid >
void checkSerial ( Grid &grid, const int maxLevel = 2, const bool display = false )
{
  doCheck( grid, display );

  for( int i = 0; i < maxLevel; ++i )
  {
    std::cerr << ">>> Refining grid globally..." << std::endl;
    grid.globalRefine( 1 );
    doCheck( grid, display );
  }

  // check also non-conform grids
  std::cerr << ">>> Trying to make grid non-conform..." << std::endl;
  makeNonConfGrid( grid, 0, 1 );
  doCheck( grid, display );
}

template <class GridType>
void checkParallel(GridType & grid, int gref, int mxl = 3, const bool display = false )
{
#if HAVE_MPI
  makeNonConfGrid(grid,gref,mxl);

  // -1 stands for leaf check
  checkCommunication(grid, -1, std::cout);

  for(int l=0; l<= mxl; ++l)
    checkCommunication(grid, l , Dune::dvverb);
#endif
}


int main (int argc , char **argv)
{

  // this method calls MPI_Init, if MPI is enabled
  MPIHelper & mpihelper = MPIHelper::instance(argc,argv);
  int myrank = mpihelper.rank();
  int mysize = mpihelper.size();

  try {
    /* use grid-file appropriate for dimensions */

    if( argc < 2 )
    {
      std::cerr << "Usage: " << argv[0] << " <dgf file of hostgrid>" << std::endl;
      return 1;
    }

    const bool display = (argc > 2);

    typedef Dune::GridSelector :: GridType HostGridType ;
    std::string filename( argv[1] );
    GridPtr< HostGridType > hostGrid( filename );

    typedef CacheItGrid< HostGridType > CacheItGridType;
    CacheItGridType grid( *hostGrid );

    //grid.loadBalance();

    {
      std::cout << "Check serial grid" << std::endl;
      checkSerial(grid,
                     (mysize == 1) ? 1 : 0,
                     (mysize == 1) ? display: false);
    }

    // perform parallel check only when more then one proc
    if(mysize > 1)
    {
      if (myrank == 0) std::cout << "Check conform grid" << std::endl;
      checkParallel(grid,1,0, display );
      if (myrank == 0) std::cout << "Check non-conform grid" << std::endl;
      checkParallel(grid,0,2, display );
    }

  }
  catch (Dune::Exception &e)
  {
    std::cerr << e << std::endl;
    return 1;
  }
  catch (...)
  {
    std::cerr << "Generic exception!" << std::endl;
    return 2;
  }

  return 0;
}
