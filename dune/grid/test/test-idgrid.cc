#include <config.h>

#include <iostream>
#include <sstream>
#include <string>

#define DISABLE_DEPRECATED_METHOD_CHECK 1

#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/idgrid.hh>
#include <dune/grid/io/file/dgfparser/dgfwriter.hh>

using namespace Dune;

#include "checkcalls.cc"

int main (int argc , char **argv)
{

  // this method calls MPI_Init, if MPI is enabled
  MPIHelper & mpihelper = MPIHelper::instance(argc,argv);
  int myrank = mpihelper.rank();
  int mysize = mpihelper.size();

  try {
    /* use grid-file appropriate for dimensions */

    if( argc < 2 )
    {
      std::cout << "Usage: " << argv[0] << " <optional: dgf file of hostgrid>" << std::endl;
    }

    const bool display = (argc > 2);

    typedef Dune::GridSelector :: GridType HostGridType ;

    std::string filename = std::to_string(HostGridType::dimension) + "dcube.dgf";

    if( argc > 1 )
      filename = argv[1];

    GridPtr< HostGridType > hostGrid( filename );

    typedef IdGrid< HostGridType > IdGridType;
    IdGridType grid( *hostGrid );

    //grid.loadBalance();

    {
      std::cout << "Check serial grid" << std::endl;
      checkSerial(grid,
                     (mysize == 1) ? 1 : 0,
                     (mysize == 1) ? display: false);
    }

    // perform parallel check only when more then one proc
    if(mysize > 1)
    {
      if (myrank == 0) std::cout << "Check conform grid" << std::endl;
      checkParallel(grid,1,0, display );
      if (myrank == 0) std::cout << "Check non-conform grid" << std::endl;
      checkParallel(grid,0,2, display );
    }

  }
  catch (Dune::Exception &e)
  {
    std::cerr << e << std::endl;
    return 1;
  }
  catch (...)
  {
    std::cerr << "Generic exception!" << std::endl;
    return 2;
  }

  return 0;
}
