#include <config.h>

#define DISABLE_DEPRECATED_METHOD_CHECK 1


#include <iostream>
#include <sstream>
#include <string>

#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/spheregrid.hh>

#include <dune/grid/io/file/dgfparser/dgfwriter.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>

#if HAVE_DUNE_ALUGRID
// use overloaded test because intersection test fails for manifold
// intersections, there is some discussion needed.
#include <dune/alugrid/test/checkintersectionit.hh>
#endif

#include "checkcalls.cc"

namespace Dune
{

  template< int dim, int dimworld >
  class AlbertaGrid;

}

template< int dim, int dimworld >
struct EnableLevelIntersectionIteratorCheck< Dune::AlbertaGrid< dim, dimworld > >
{
  static const bool v = false;
};

template< int dim, int dimworld >
struct EnableLevelIntersectionIteratorCheck< Dune::ALUGrid< dim, dimworld, Dune::simplex, Dune::conforming > >
{
  static const bool v = false;
};

template< class HostGrid, class MapToSphere >
struct EnableLevelIntersectionIteratorCheck< Dune::SphereGrid< HostGrid, MapToSphere > >
{
  static const bool v = EnableLevelIntersectionIteratorCheck< HostGrid >::v;
};

template< class HostGrid, class MapToSphere >
struct EnableLevelIntersectionIteratorCheck< const Dune::SphereGrid< HostGrid, MapToSphere > >
{
  static const bool v = EnableLevelIntersectionIteratorCheck< HostGrid >::v;
};


using namespace Dune;

int main (int argc , char **argv)
try {
  MPIHelper &mpihelper = MPIHelper::instance( argc, argv );
  int myrank = mpihelper.rank();
  int mysize = mpihelper.size();

  /* use grid-file appropriate for dimensions */

  if( argc < 2 )
  {
    std::cout << "Usage: " << argv[ 0 ] << " <optional: dgf file of hostgrid>" << std::endl;
  }

  const bool display = (argc > 2);

  typedef Dune::GridSelector::GridType HostGrid;

  std::string filename ("cubedsphere2d.dgf");

  if( argc > 1 )
    filename = argv[1];

  GridPtr< HostGrid > hostPtr( filename );
  HostGrid& hostGrid = *hostPtr;
  hostGrid.globalRefine( 4 );
  {
    Dune :: VTKWriter< HostGrid :: LeafGridView> vtkWriter(hostGrid.leafGridView());
    vtkWriter.write( "orgshpere.vtu" );
  }


  typedef SphereGrid< HostGrid > Grid;
  GridPtr< Grid > gridPtr ( filename );
  Grid& grid = *gridPtr;


  //grid.loadBalance();

  /*
  {
    std::cout << "Check serial grid" << std::endl;
    checkSerial(grid,
                   (mysize == 1) ? 1 : 0,
                   (mysize == 1) ? display: false);
  }

  // perform parallel check only when more then one proc
  if(mysize > 1)
  {
    if (myrank == 0) std::cout << "Check conform grid" << std::endl;
    checkParallel(grid,1,0, display );
    if (myrank == 0) std::cout << "Check non-conform grid" << std::endl;
    checkParallel(grid,0,2, display );
  }
  */

  {
    Dune :: VTKWriter< typename Grid :: LeafGridView> vtkWriter(grid.leafGridView());
    vtkWriter.write( "shpere" );
  }

  return 0;
}
catch( Dune::Exception &e )
{
  std::cerr << e << std::endl;
  return 1;
}
catch (...)
{
  std::cerr << "Generic exception!" << std::endl;
  return 2;
}
