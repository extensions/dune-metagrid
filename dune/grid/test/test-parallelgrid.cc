#include <config.h>

// #define NO_2D
// #define NO_3D
#define DISABLE_DEPRECATED_METHOD_CHECK 1

#include <iostream>
#include <sstream>
#include <string>

#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/parallelgrid.hh>
#include <dune/grid/io/file/dgfparser/dgfwriter.hh>

#include <dune/grid/test/gridcheck.hh>

#include <dune/grid/test/checkgeometryinfather.hh>
#include <dune/grid/test/checkintersectionit.hh>
#include <dune/grid/test/checkcommunicate.hh>

#include <dune/grid/io/file/vtk/vtkwriter.hh>

using namespace Dune;

template <bool leafconform, class Grid>
void checkCapabilities(const Grid& grid)
{
   static_assert( Dune::Capabilities::isLevelwiseConforming<Grid>::v == !leafconform,
                  "isLevelwiseConforming is not set correctly");
   static_assert( Dune::Capabilities::isLeafwiseConforming<Grid>::v == leafconform,
                  "isLevelwiseConforming is not set correctly");
   static const bool hasEntity = Dune::Capabilities::hasEntity<Grid, 1>::v == true;
   static_assert( hasEntity,
                  "hasEntity is not set correctly");
   static_assert( Dune::Capabilities::hasBackupRestoreFacilities<Grid>::v == true,
                  "hasBackupRestoreFacilities is not set correctly");

   static const bool reallyCanCommunicate =
#if ALU3DGRID_PARALLEL && ALU2DGRID_PARALLEL
    true ;
#elif ALU3DGRID_PARALLEL
    Grid :: dimension == 3;
#else
    false ;
#endif
   static const bool canCommunicate
    = Dune::Capabilities::canCommunicate<Grid, 1>::v == reallyCanCommunicate;
   static_assert( canCommunicate, "canCommunicate is not set correctly");
}

template <class GridType>
void makeNonConformingGrid(GridType& grid, int level, int adapt)
{
  int myrank = grid.comm().rank();
  grid.loadBalance();
  grid.globalRefine(level);
  grid.loadBalance();
  for (int i=0; i<adapt; i++)
  {
    if (myrank==0)
    {
      std::size_t numberOfRefinedElements = 0;
      const auto size = grid.leafGridView().size(0);
      for(const auto& element : elements(grid.leafGridView()))
      {
        grid.mark(1, element);
        if (numberOfRefinedElements > size*0.8)
          break;
        numberOfRefinedElements++;
      }
    }
    grid.adapt();
    grid.postAdapt();
    grid.loadBalance();
  }
}

template <class GridView>
void writeFile(const GridView& gridView)
{
  DGFWriter< GridView > writer( gridView );
  writer.write( "dump.dgf" );
}

template <class GridType>
void checkSerial(GridType& grid, int mxl = 2)
{
  // be careful, each global refine create 8 x maxlevel elements
  std::cout << "  CHECKING: Macro" << std::endl;
  gridcheck(grid);
  std::cout << "  CHECKING: Macro-intersections" << std::endl;
  checkIntersectionIterator(grid);

  for(int i=0; i<mxl; i++)
  {
    grid.globalRefine( 1 );//DGFGridInfo<GridType> :: refineStepsForHalf() );
    std::cout << "  CHECKING: Refined" << std::endl;
    gridcheck(grid);
    std::cout << "  CHECKING: intersections" << std::endl;
    checkIntersectionIterator(grid);
  }

  // check also non-conform grids
  makeNonConformingGrid(grid,0,1);

  //std::cout << "  CHECKING: non-conform" << std::endl;
  gridcheck(grid);

  // check the method geometryInFather()
  std::cout << "  CHECKING: geometry in father" << std::endl;
  checkGeometryInFather(grid);
  // check the intersection iterator and the geometries it returns
  std::cout << "  CHECKING: intersections" << std::endl;
  checkIntersectionIterator(grid);

  std::cout << std::endl << std::endl;
}

template <class GridType>
void checkParallel(GridType& grid, int gref, int mxl = 3)
{
  std::cout << "P[ " << grid.comm().rank() << " ] = " << grid.size( 0 ) << std::endl;

#if HAVE_MPI
  //makeNonConformingGrid(grid,gref,mxl);

  // -1 stands for leaf check
  checkCommunication(grid, -1, std::cout);

  const int maxLevel = std::min( mxl, grid.maxLevel() );
  for(int l=0; l<= maxLevel; ++l)
    checkCommunication(grid, l, Dune::dvverb);
#endif
}


int main (int argc , char **argv)
{

  // this method calls MPI_Init, if MPI is enabled
  auto& mpiHelper = MPIHelper::instance(argc,argv);
  const int myrank = mpiHelper.rank();
  const int mysize = mpiHelper.size();

  /* use grid-file appropriate for dimensions */
  if( argc < 2 )
  {
    std::cerr << "Usage: " << argv[0] << " <dgf file of hostgrid>" << std::endl;
    return 1;
  }

  using HostGridType = Dune::GridSelector::GridType;
  std::string filename( argv[1] );
  GridPtr< HostGridType > hostGrid( filename );

  using ParallelGridType = ParallelGrid<HostGridType>;
  ParallelGridType grid( *hostGrid );

  grid.loadBalance();
  std::cout << "P["<<myrank<<"]  size = "<< grid.size( 0 ) << std::endl;

  {
    std::cout << "Check serial grid" << std::endl;
    checkSerial(grid, (mysize == 1) ? 1 : 0);
  }

  // perform parallel check only when more then one proc
  if(mysize > 1)
  {
    if (myrank == 0) std::cout << "Check conform grid" << std::endl;
    checkParallel(grid, 1, 0);
    if (myrank == 0) std::cout << "Check non-conform grid" << std::endl;
    checkParallel(grid, 0, 2);
  }

  Dune::VTKWriter<ParallelGridType::LeafGridView> vtkWriter(grid.leafGridView());
  std::vector<int> ranks(grid.leafGridView().size(0), myrank);
  vtkWriter.addCellData( ranks, "rank" );
  vtkWriter.write( "parallel_grid" );

  return 0;
}
