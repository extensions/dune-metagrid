#include <config.h>

#define DISABLE_DEPRECATED_METHOD_CHECK 1

#include <sstream>

#include <dune/common/exceptions.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/test/gridcheck.hh>
#include <dune/grid/test/checkgeometryinfather.hh>
#include <dune/grid/test/checkintersectionit.hh>
#include <dune/grid/test/checkiterators.hh>
#include <dune/grid/test/checkcommunicate.hh>

#include <dune/grid/filteredgrid.hh>
#include <dune/grid/filteredgrid/predicate/radial.hh>
#include <dune/grid/filteredgrid/predicate/refinement.hh>

// host grid type
typedef Dune::GridSelector::GridType HostGrid;
// predicate type
typedef Dune::RadialPredicate< HostGrid > Predicate;
// grid type
typedef Dune::FilteredGrid< HostGrid > Grid;


// run tests
template< class Grid >
void test ( Grid &grid )
{
  gridcheck( grid );
  checkIterators( grid.leafGridView() );
  checkGeometryInFather( grid );
  checkIntersectionIterator( grid );

  // -1 stands for leaf check
  checkCommunication(grid, -1, std::cout);
}


int main ( int argc, char ** argv )
{
  try
  {
    Dune::MPIHelper::instance( argc, argv );

    if( argc < 4 )
    {
      std::cerr << "Usage: " << argv[ 0 ] << " [host grid dgf file][start level] [refinement steps]" << std::endl;
      return 1;
    }

    // create host grid
    std::string filename( argv[ 1 ] );
    Dune::GridPtr< HostGrid > hostGridPtr( filename );
    HostGrid &hostGrid = *hostGridPtr;

    // read parameters
    int startLevel = std::atoi( argv[ 2 ] );
    int refCount = std::atoi( argv[ 3 ] );

    // initial refinement
    hostGrid.globalRefine( startLevel );

    // create predicate
    Predicate::GlobalCoordinate center( 0.5 );
    Predicate::ctype radius( 0.25 );
    Predicate predicate ( center, radius );

    // initial refinement
    for( int i = 0; i < refCount; ++i )
    {
      // Dune::FilteredGridRefinement::globalRefine( hostGrid, predicate );
      Dune::FilteredGridRefinement::adapt( hostGrid, predicate );
    }

    // create grid
    Grid grid( hostGrid, predicate );

    // run test
    test( grid );
  }
  catch ( const Dune::Exception &e )
  {
    std::cout << "DUNE ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (...)
  {
    std::cout << "Unknown ERROR" << std::endl;
    return 1;
  }
  return 0;
}
