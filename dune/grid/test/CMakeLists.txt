set(GRIDTYPE ALUGRID_CUBE)
#set(GRIDTYPE YASPGRID)
set(GRIDDIM  2)
set(WORLDDIM ${GRIDDIM})

set(METAGRIDS  idgrid)
# set(METAGRIDS  idgrid,idgrid,idgrid)

# set(TESTS  test-cacheitgrid  test-cartesiangrid  test-filteredgrid  test-idgrid  test-metagrids test-parallelgrid test-sfc test-prismgrid)
set(TESTS  test-cartesiangrid  test-idgrid )

foreach(_executable ${TESTS})
  dune_add_test(NAME ${_executable} SOURCES ${_executable}.cc)
  set_property(TARGET ${_executable} APPEND PROPERTY COMPILE_DEFINITIONS
    "GRIDDIM=${GRIDDIM}" "WORLDDIM=${WORLDDIM}" "${GRIDTYPE}")
  set_property(TARGET ${_executable} APPEND PROPERTY COMPILE_DEFINITIONS "METAGRIDS=${METAGRIDS}")
endforeach()

dune_add_test(NAME test-spheregrid SOURCES test-spheregrid.cc CMAKE_GUARD dune-alugrid_FOUND)
set_property(TARGET test-spheregrid APPEND PROPERTY COMPILE_DEFINITIONS "GRIDDIM=2" "WORLDDIM=3" "ALUGRID_SIMPLEX")

dune_add_test(NAME test-parallelgrid
              SOURCES test-parallelgrid.cc
              CMAKE_GUARD dune-alugrid_FOUND
              MPI_RANKS 1 2 3
              TIMEOUT 1200
              COMPILE_DEFINITIONS "GRIDDIM=${GRIDDIM}" "WORLDDIM=${WORLDDIM}" "ALUGRID_CUBE_NOCOMM" "METAGRIDS=${METAGRIDS}"
              CMD_ARGS 2dcube.dgf)

set(DGFS
  1dcube.dgf  2dcube.dgf  3dcube.dgf  cubedsphere2d.dgf  octahedron.dgf
)

foreach(dgf ${DGFS})
  configure_file( ${dgf} ${CMAKE_CURRENT_BINARY_DIR}/ COPYONLY)
endforeach()

set(metagridtestdir ${CMAKE_INSTALL_INCLUDEDIR}/dune/grid/test)
set(metagridtest_HEADERS checkcalls.cc  sfc.cc )

install( FILES ${metagridtest_HEADERS} DESTINATION ${metagridtestdir} )
