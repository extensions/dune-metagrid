#ifndef DUNE_CACHEITGRID_HIERARCHICITERATOR_HH
#define DUNE_CACHEITGRID_HIERARCHICITERATOR_HH

//- dune-geometry includes
#include <dune/geometry/referenceelements.hh>

//- dune-grid includes
#include <dune/grid/common/entityiterator.hh>

//- local includes
#include <dune/grid/cacheitgrid/entitypointer.hh>


namespace Dune
{

  // Forward declarations
  // --------------------

  template< int, class > class CacheItGridEntityPointerTraits;
  template< class > class  CacheItGridEntityPointer;



  // CacheItGridHierarchicIteratorTraits
  // -----------------------------------

  template< class Grid >
  struct CacheItGridHierarchicIteratorTraits
  : public CacheItGridEntityPointerTraits< 0, Grid >
  {
    typedef CacheItGridEntityPointer< CacheItGridHierarchicIteratorTraits< Grid > > BaseEntityPointer;

    typedef typename std::remove_const< Grid >::type::Traits Traits;

    typedef typename Traits::HostGrid::Traits::HierarchicIterator HostIterator;
  };



  // CacheItGridHierarchicIterator
  // -----------------------------

  template< class Traits >
  class CacheItGridHierarchicIterator
  : public Traits::BaseEntityPointer
  {
    typedef typename Traits::BaseEntityPointer Base;
    typedef typename Traits::Grid Grid;

  protected:
    using Base::hostIterator_;
    using Base::releaseEntity;

  public:
    typedef typename Base::HostIterator HostIterator;

    explicit CacheItGridHierarchicIterator ( const HostIterator &hostIterator )
    : Base( hostIterator )
    {}

    void increment ()
    {
      ++hostIterator_;
      releaseEntity();
    }
  };

} // namespace Dune

#endif // #ifndef DUNE_CACHEITGRID_HIERARCHICITERATOR_HH
