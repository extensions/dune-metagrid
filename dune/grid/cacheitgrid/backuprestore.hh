#ifndef DUNE_CACHEITGRID_BACKUPRESTORE_HH
#define DUNE_CACHEITGRID_BACKUPRESTORE_HH

#include <dune/grid/common/backuprestore.hh>

#include <dune/grid/cacheitgrid/capabilities.hh>


#include <fstream>

#include <dune/grid/common/forwardbackuprestore.hh>
#include <dune/grid/cacheitgrid/capabilities.hh>

namespace Dune
{
  /** \copydoc Dune::BackupRestoreFacility */
  template< class HostGrid >
  struct BackupRestoreFacility< CacheItGrid< HostGrid > >
    : public ForwardBackupRestoreFacility< CacheItGrid< HostGrid > >
  {
  };

} // namespace Dune

#endif // #ifndef DUNE_CACHEITGRID_BACKUPRESTORE_HH
