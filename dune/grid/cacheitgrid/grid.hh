#ifndef DUNE_CACHEITGRID_GRID_HH
#define DUNE_CACHEITGRID_GRID_HH

#include <dune/common/applyif.hh>
#include <dune/common/forloop.hh>
#include <dune/common/typetraits.hh>

#include <dune/grid/common/grid.hh>

#include <dune/grid/cacheitgrid/backuprestore.hh>
#include <dune/grid/cacheitgrid/capabilities.hh>
#include <dune/grid/cacheitgrid/declaration.hh>
#include <dune/grid/cacheitgrid/gridview.hh>
#include <dune/grid/cacheitgrid/hierarchiciterator.hh>
#include <dune/grid/cacheitgrid/intersection.hh>
#include <dune/grid/cacheitgrid/intersectioniterator.hh>
#include <dune/grid/cacheitgrid/iteratorcache.hh>
#include <dune/grid/cacheitgrid/iterator.hh>

#include <dune/grid/common/codimtable.hh>

#include <dune/grid/idgrid/datahandle.hh>
#include <dune/grid/idgrid/indexset.hh>
#include <dune/grid/idgrid/idset.hh>

#include <dune/grid/common/hostgridinoutstreams.hh>

namespace Dune
{

  // CacheItGridExportParams
  // -----------------------

  template< class HG >
  struct CacheItGridExportParams
    : public HostGridHasInOutStreams< HG >
  {
    typedef HG HostGrid;
  };



  // CacheItGridFamily
  // -----------------

  template< class HostGrid >
  struct CacheItGridFamily
  {
    struct Traits
    : public CacheItGridExportParams< HostGrid >
    {
      typedef CacheItGrid< HostGrid > Grid;

      typedef typename HostGrid::ctype ctype;

      struct EmptyData{};
      // type of data passed to entities, intersections, and iterators
      // for IdGrid this is just an empty place holder
      typedef EmptyData ExtraData;

      static const int dimension = HostGrid::dimension;
      static const int dimensionworld = HostGrid::dimensionworld;

      typedef CacheItGridIntersection< const Grid, typename HostGrid::Traits::LeafIntersection > LeafIntersectionImpl;
      typedef Dune::Intersection< const Grid, LeafIntersectionImpl > LeafIntersection;

      typedef CacheItGridIntersection< const Grid, typename HostGrid::Traits::LevelIntersection > LevelIntersectionImpl;
      typedef Dune::Intersection< const Grid, LevelIntersectionImpl > LevelIntersection;

      typedef CacheItGridIntersectionIterator< const Grid, typename HostGrid::Traits::LeafIntersectionIterator > LeafIntersectionIteratorImpl;
      typedef Dune::IntersectionIterator< const Grid, LeafIntersectionIteratorImpl, LeafIntersectionImpl > LeafIntersectionIterator;
      typedef CacheItGridIntersectionIterator< const Grid, typename HostGrid::Traits::LevelIntersectionIterator > LevelIntersectionIteratorImpl;
      typedef Dune::IntersectionIterator< const Grid, LeafIntersectionIteratorImpl, LevelIntersectionImpl > LevelIntersectionIterator;

      typedef Dune::EntityIterator< 0, const Grid, CacheItGridHierarchicIterator< CacheItGridHierarchicIteratorTraits< const Grid > > >
        HierarchicIterator;

      template< int codim >
      struct Codim
      {
        typedef typename HostGrid::template Codim< codim >::Geometry Geometry;
        typedef typename HostGrid::template Codim< codim >::LocalGeometry LocalGeometry;

        typedef IdGridEntity< codim, dimension, const Grid > EntityImpl;
        typedef Dune::Entity< codim, dimension, const Grid, IdGridEntity > Entity;

        //typedef CacheItGridEntityPointerTraits< codim, const Grid > EntityPointerTraits;
        //typedef CacheItGridEntityPointer< EntityPointerTraits > EntityPointerImpl;
        //typedef typename EntityPointerTraits::Entity Entity;

        typedef IdGridEntitySeed< codim, const Grid > EntitySeedImpl;
        typedef Dune::EntitySeed< const Grid, IdGridEntitySeed< codim, const Grid > > EntitySeed;

        template< PartitionIteratorType pitype >
        struct Partition
        {
          typedef Dune::EntityIterator< codim, const Grid, CacheItGridIterator< codim, const Grid > >
            LeafIterator;
          typedef Dune::EntityIterator< codim, const Grid, CacheItGridIterator< codim, const Grid > >
            LevelIterator;
        };

        typedef typename Partition< All_Partition >::LeafIterator LeafIterator;
        typedef typename Partition< All_Partition >::LevelIterator LevelIterator;
      };

      typedef IdGridIndexSet< const Grid, typename HostGrid::Traits::LeafIndexSet > LeafIndexSet;
      typedef IdGridIndexSet< const Grid, typename HostGrid::Traits::LevelIndexSet > LevelIndexSet;

      typedef IdGridIdSet< const Grid, typename HostGrid::Traits::GlobalIdSet > GlobalIdSet;
      typedef IdGridIdSet< const Grid, typename HostGrid::Traits::LocalIdSet > LocalIdSet;

      typedef typename HostGrid::Traits::Communication Communication;

      template< PartitionIteratorType pitype >
      struct Partition
      {
        typedef Dune::GridView< CacheItGridLeafGridViewTraits < HostGrid, pitype > > LeafGridView;
        typedef Dune::GridView< CacheItGridLevelGridViewTraits< HostGrid, pitype > > LevelGridView;
      };

      typedef Dune::GridView< CacheItGridLeafGridViewTraits < HostGrid, All_Partition > > LeafGridView;
      typedef Dune::GridView< CacheItGridLevelGridViewTraits< HostGrid, All_Partition > > LevelGridView;
    };
  };



  // CacheItGrid
  // -----------

  /** \class CacheItGrid
   *  \brief identical grid wrapper
   *  \ingroup CacheItGrid
   *
   *  \tparam  HostGrid   DUNE grid to be wrapped (called host grid)
   *
   *  \nosubgrouping
   */
  template< class HostGrid >
  class CacheItGrid
  /** \cond */
  : public GridDefaultImplementation
      < HostGrid::dimension, HostGrid::dimensionworld, typename HostGrid::ctype, CacheItGridFamily< HostGrid > >,
    public CacheItGridExportParams< HostGrid >
  /** \endcond */
  {
    typedef CacheItGrid< HostGrid > Grid;

    typedef GridDefaultImplementation
      < HostGrid::dimension, HostGrid::dimensionworld, typename HostGrid::ctype, CacheItGridFamily< HostGrid > >
      Base;

    template< int, int, class > friend class CacheItGridEntity;
    template< class, PartitionIteratorType > friend class CacheItGridLeafGridView;
    template< class, PartitionIteratorType > friend class CacheItGridLevelGridView;
    template< class, class > friend class CacheItGridIntersection;
    template< class, class > friend class CacheItGridIntersectionIterator;
    template< class, class > friend class CacheItGridIdSet;
    template< class > friend class HostGridAccess;

    template< int, PartitionIteratorType, class > friend class CacheItGridLevelIteratorTraits;
    template< int, PartitionIteratorType, class > friend class CacheItGridLeafIteratorTraits;

  public:
    /** \cond */
    typedef CacheItGridFamily< HostGrid > GridFamily;
    /** \endcond */

    /** \name Traits
     *  \{ */

    //! type of the grid traits
    typedef typename GridFamily::Traits Traits;

    static const int dimension = Base::dimension;
    static const int dimensionworld = Base::dimensionworld;

    /** \brief traits structure containing types for a codimension
     *
     *  \tparam codim  codimension
     *
     *  \nosubgrouping
     */
    template< int codim >
    struct Codim;

    /** \} */

    /** \name Iterator Types
     *  \{ */

    //! iterator over the grid hierarchy
    typedef typename Traits::HierarchicIterator HierarchicIterator;
    //! iterator over intersections with other entities on the leaf level
    typedef typename Traits::LeafIntersectionIterator LeafIntersectionIterator;
    //! iterator over intersections with other entities on the same level
    typedef typename Traits::LevelIntersectionIterator LevelIntersectionIterator;

    /** \} */

    /** \name Grid View Types
     *  \{ */

    /** \brief Types for GridView */
    template< PartitionIteratorType pitype >
    struct Partition
    {
      typedef typename GridFamily::Traits::template Partition< pitype >::LevelGridView
         LevelGridView;
      typedef typename GridFamily::Traits::template Partition< pitype >::LeafGridView
         LeafGridView;
    };

    /** \brief View types for All_Partition */
    typedef typename Partition< All_Partition >::LevelGridView LevelGridView;
    typedef typename Partition< All_Partition >::LeafGridView LeafGridView;

    /** \} */

    /** \name Index and Id Set Types
     *  \{ */

    /** \brief type of leaf index set
     *
     *  The index set assigns consecutive indices to the entities of the
     *  leaf grid. The indices are of integral type and can be used to access
     *  arrays.
     *
     *  The leaf index set is a model of Dune::IndexSet.
     */
    typedef typename Traits::LeafIndexSet LeafIndexSet;

    /** \brief type of level index set
     *
     *  The index set assigns consecutive indices to the entities of a grid
     *  level. The indices are of integral type and can be used to access
     *  arrays.
     *
     *  The level index set is a model of Dune::IndexSet.
     */
    typedef typename Traits::LevelIndexSet LevelIndexSet;

    /** \brief type of global id set
     *
     *  The id set assigns a unique identifier to each entity within the
     *  grid. This identifier is unique over all processes sharing this grid.
     *
     *  \note Id's are neither consecutive nor necessarily of an integral
     *        type.
     *
     *  The global id set is a model of Dune::IdSet.
     */
    typedef typename Traits::GlobalIdSet GlobalIdSet;

    /** \brief type of local id set
     *
     *  The id set assigns a unique identifier to each entity within the
     *  grid. This identifier needs only to be unique over this process.
     *
     *  Though the local id set may be identical to the global id set, it is
     *  often implemented more efficiently.
     *
     *  \note Ids are neither consecutive nor necessarily of an integral
     *        type.
     *  \note Local ids need not be compatible with global ids. Also, no
     *        mapping from local ids to global ones needs to exist.
     *
     *  The global id set is a model of Dune::IdSet.
     */
    typedef typename Traits::LocalIdSet LocalIdSet;

    /** \} */

    /** \name Miscellaneous Types
     * \{ */

    //! type of vector coordinates (e.g., double)
    typedef typename Traits::ctype ctype;

    //! communicator with all other processes having some part of the grid
    typedef typename Traits::Communication Communication;

    /** \} */

    /** \name Construction and Destruction
     *  \{ */

    /** \brief constructor
     *
     *  The references to host grid and coordinate function are stored in the
     *  grid. Therefore, they must remain valid until the grid is destroyed.
     *
     *  \param[in]  hostGrid       reference to the grid to wrap
     */
    explicit CacheItGrid ( HostGrid &hostGrid )
    : hostGrid_( &hostGrid ),
      deleteHostGrid_( false ),
      levelIndexSets_( hostGrid.maxLevel()+1, (LevelIndexSet *)0 ),
      leafIndexSet_( 0 )
    {
      const std::size_t numLevels = hostGrid.maxLevel()+1;
      ForLoop< ApplyIf< ClearIteratorCache, CodimAvailable >::template Operation, 0, dimension >
        ::apply( numLevels, iteratorCaches_ );
    }

    /** \brief constructor
     *
     *  The references to host grid and coordinate function are stored in the
     *  grid. Therefore, they must remain valid until the grid is destroyed.
     *
     *  \param[in]  hostGrid       pointer to the grid to wrap, ownership is taken,
     *                             otherwise pass reference
     */
    explicit CacheItGrid ( HostGrid *hostGrid )
    : hostGrid_( hostGrid ),
      deleteHostGrid_( true ),
      levelIndexSets_( hostGrid->maxLevel()+1, (LevelIndexSet *)0 ),
      leafIndexSet_( 0 )
    {
      const std::size_t numLevels = hostGrid->maxLevel()+1;
      ForLoop< ApplyIf< ClearIteratorCache, CodimAvailable >::template Operation, 0, dimension >
        ::apply( numLevels, iteratorCaches_ );
    }

    /** \brief destructor
     */
    ~CacheItGrid ()
    {
      if( leafIndexSet_ )
        delete leafIndexSet_;

      for( unsigned int i = 0; i < levelIndexSets_.size(); ++i )
      {
        if( levelIndexSets_[ i ] )
          delete( levelIndexSets_[ i ] );
      }

      if( deleteHostGrid_ )
        delete hostGrid_;
    }

    /** \} */

    /** \name Size Methods
     *  \{ */

    /** \brief obtain maximal grid level
     *
     *  Grid levels are numbered 0, ..., L, where L is the value returned by
     *  this method.
     *
     *  \returns maximal grid level
     */
    int maxLevel () const
    {
      return hostGrid().maxLevel();
    }

    /** \brief obtain number of entites on a level
     *
     *  \param[in]  level  level to consider
     *  \param[in]  codim  codimension to consider
     *
     *  \returns number of entities of codimension \em codim on grid level
     *           \em level.
     */
    int size ( int level, int codim ) const
    {
      return hostGrid().size( level, codim );
    }

    /** \brief obtain number of leaf entities
     *
     *  \param[in]  codim  codimension to consider
     *
     *  \returns number of leaf entities of codimension \em codim
     */
    int size ( int codim ) const
    {
      return hostGrid().size( codim );
    }

    /** \brief obtain number of entites on a level
     *
     *  \param[in]  level  level to consider
     *  \param[in]  type   geometry type to consider
     *
     *  \returns number of entities with a geometry of type \em type on grid
     *           level \em level.
     */
    int size ( int level, GeometryType type ) const
    {
      return hostGrid().size( level, type );
    }

    /** \brief returns the number of boundary segments within the macro grid
     *
     *  \returns number of boundary segments within the macro grid
     */
    int size ( GeometryType type ) const
    {
      return hostGrid().size( type );
    }

    /** \brief obtain number of leaf entities
     *
     *  \param[in]  type   geometry type to consider
     *
     *  \returns number of leaf entities with a geometry of type \em type
     */
    size_t numBoundarySegments () const
    {
      return hostGrid().numBoundarySegments( );
    }
    /** \} */

    template< int codim >
    typename Codim< codim >::LevelIterator lbegin ( int level ) const
    {
      return lbegin< codim, All_Partition >( level );
    }

    template< int codim >
    typename Codim< codim >::LevelIterator lend ( int level ) const
    {
      return lend< codim, All_Partition >( level );
    }

    template< int codim, PartitionIteratorType pitype >
    typename Codim< codim >::template Partition< pitype >::LevelIterator
    lbegin ( int level ) const
    {
      std::integral_constant< int, codim > codimVariable;
      const IteratorCache< codim > &iteratorCache = iteratorCaches_[ codimVariable ];

      assert( (level >= 0) && (level <= maxLevel()) );
      assert( iteratorCache.levels.size() == std::size_t( maxLevel()+1 ) );
      if( !iteratorCache.levels[ level ] )
        iteratorCache.levels[ level ].update( hostGrid().levelGridView( level ) );

      typedef CacheItGridIterator< codim, const Grid > Impl;
      return Impl( hostGrid(), iteratorCache.levels[ level ].template begin< pitype >() );
    }

    template< int codim, PartitionIteratorType pitype >
    typename Codim< codim >::template Partition< pitype >::LevelIterator
    lend ( int level ) const
    {
      std::integral_constant< int, codim > codimVariable;
      const IteratorCache< codim > &iteratorCache = iteratorCaches_[ codimVariable ];

      assert( (level >= 0) && (level <= maxLevel()) );
      assert( iteratorCache.levels.size() == std::size_t( maxLevel()+1 ) );
      if( !iteratorCache.levels[ level ] )
        iteratorCache.levels[ level ].update( hostGrid().levelGridView( level ) );

      typedef CacheItGridIterator< codim, const Grid > Impl;
      return Impl( hostGrid(), iteratorCache.levels[ level ].template end< pitype >() );
    }

    template< int codim >
    typename Codim< codim >::LeafIterator leafbegin () const
    {
      return leafbegin< codim, All_Partition >();
    }

    template< int codim >
    typename Codim< codim >::LeafIterator leafend () const
    {
      return leafend< codim, All_Partition >();
    }

    template< int codim, PartitionIteratorType pitype >
    typename Codim< codim >::template Partition< pitype >::LeafIterator
    leafbegin () const
    {
      std::integral_constant< int, codim > codimVariable;
      const IteratorCache< codim > &iteratorCache = iteratorCaches_[ codimVariable ];

      if( !iteratorCache.leaf )
        iteratorCache.leaf.update( hostGrid().leafGridView() );

      typedef CacheItGridIterator< codim, const Grid > Impl;
      return Impl( hostGrid(), iteratorCache.leaf.template begin< pitype >() );
    }

    template< int codim, PartitionIteratorType pitype >
    typename Codim< codim >::template Partition< pitype >::LeafIterator
    leafend () const
    {
      std::integral_constant< int, codim > codimVariable;
      const IteratorCache< codim > &iteratorCache = iteratorCaches_[ codimVariable ];

      if( !iteratorCache.leaf )
        iteratorCache.leaf.update( hostGrid().leafGridView() );

      typedef CacheItGridIterator< codim, const Grid > Impl;
      return Impl( hostGrid(), iteratorCache.leaf.template end< pitype >() );
    }

    const GlobalIdSet &globalIdSet () const
    {
      if( !globalIdSet_ )
        globalIdSet_ = GlobalIdSet( hostGrid().globalIdSet() );
      assert( globalIdSet_ );
      return globalIdSet_;
    }

    const LocalIdSet &localIdSet () const
    {
      if( !localIdSet_ )
        localIdSet_ = LocalIdSet( hostGrid().localIdSet() );
      assert( localIdSet_ );
      return localIdSet_;
    }

    const LevelIndexSet &levelIndexSet ( int level ) const
    {
      assert( levelIndexSets_.size() == (size_t)(maxLevel()+1) );
      if( (level < 0) || (level > maxLevel()) )
      {
        DUNE_THROW( GridError, "LevelIndexSet for nonexisting level " << level
                               << " requested." );
      }

      LevelIndexSet *&levelIndexSet = levelIndexSets_[ level ];
      if( !levelIndexSet )
        levelIndexSet = new LevelIndexSet( hostGrid().levelIndexSet( level ) );
      assert( levelIndexSet );
      return *levelIndexSet;
    }

    const LeafIndexSet &leafIndexSet () const
    {
      if( !leafIndexSet_ )
        leafIndexSet_ = new LeafIndexSet( hostGrid().leafIndexSet() );
      assert( leafIndexSet_ );
      return *leafIndexSet_;
    }

    void globalRefine ( int refCount )
    {
      hostGrid().globalRefine( refCount );
      // update overall status
      update();
    }

    bool mark ( int refCount, const typename Codim< 0 >::Entity &entity )
    {
      return hostGrid().mark( refCount, getHostEntity< 0 >( entity ) );
    }

    int getMark ( const typename Codim< 0 >::Entity &entity ) const
    {
      return hostGrid().getMark( getHostEntity< 0 >( entity ) );
    }

    bool preAdapt ()
    {
      return hostGrid().preAdapt();
    }

    bool adapt ()
    {
      bool ret = hostGrid().adapt();
      update();
      return ret;
    }

    void postAdapt ()
    {
      hostGrid().postAdapt();
    }

    /** \name Parallel Data Distribution and Communication Methods
     *  \{ */

    /** \brief obtain size of overlap region for the leaf grid
     *
     *  \param[in]  codim  codimension for with the information is desired
     */
    int overlapSize ( int codim ) const
    {
      return hostGrid().overlapSize( codim );
    }

    /** \brief obtain size of ghost region for the leaf grid
     *
     *  \param[in]  codim  codimension for with the information is desired
     */
    int ghostSize( int codim ) const
    {
      return hostGrid().ghostSize( codim );
    }

    /** \brief obtain size of overlap region for a grid level
     *
     *  \param[in]  level  grid level (0, ..., maxLevel())
     *  \param[in]  codim  codimension (0, ..., dimension)
     */
    int overlapSize ( int level, int codim ) const
    {
      return hostGrid().overlapSize( level, codim );
    }

    /** \brief obtain size of ghost region for a grid level
     *
     *  \param[in]  level  grid level (0, ..., maxLevel())
     *  \param[in]  codim  codimension (0, ..., dimension)
     */
    int ghostSize ( int level, int codim ) const
    {
      return hostGrid().ghostSize( level, codim );
    }

    /** \brief communicate information on a grid level
     *
     *  \param      dataHandle  communication data handle (user defined)
     *  \param[in]  interface   communication interface (one of
     *                          InteriorBorder_InteriorBorder_Interface,
     *                          InteriorBorder_All_Interface,
     *                          Overlap_OverlapFront_Interface,
     *                          Overlap_All_Interface,
     *                          All_All_Interface)
     *  \param[in]  direction   communication direction (one of
     *                          ForwardCommunication or BackwardCommunication)
     *  \param[in]  level       grid level to communicate
     */
    template< class DataHandle, class Data >
    void communicate ( CommDataHandleIF< DataHandle, Data > &dataHandle,
                       InterfaceType interface,
                       CommunicationDirection direction,
                       int level ) const
    {
      levelGridView( level ).communicate( dataHandle, interface, direction );
    }

    /** \brief communicate information on leaf entities
     *
     *  \param      dataHandle  communication data handle (user defined)
     *  \param[in]  interface   communication interface (one of
     *                          InteriorBorder_InteriorBorder_Interface,
     *                          InteriorBorder_All_Interface,
     *                          Overlap_OverlapFront_Interface,
     *                          Overlap_All_Interface,
     *                          All_All_Interface)
     *  \param[in]  direction   communication direction (one of
     *                          ForwardCommunication, BackwardCommunication)
     */
    template< class DataHandle, class Data >
    void communicate ( CommDataHandleIF< DataHandle, Data > &dataHandle,
                       InterfaceType interface,
                       CommunicationDirection direction ) const
    {
      leafGridView().communicate( dataHandle, interface, direction );
    }

    /** \brief obtain Communication object
     *
     *  The Communication object should be used to globally
     *  communicate information between all processes sharing this grid.
     *
     *  \note The Communication object returned is identical to the
     *        one returned by the host grid.
     */
    const Communication &comm () const
    {
      return hostGrid().comm();
    }

    // data handle interface different between geo and interface

    /** \brief rebalance the load each process has to handle
     *
     *  A parallel grid is redistributed such that each process has about
     *  the same load (e.g., the same number of leaf entites).
     *
     *  \note DUNE does not specify, how the load is measured.
     *
     *  \returns \b true, if the grid has changed.
     */
    bool loadBalance ()
    {
      const bool gridChanged = hostGrid().loadBalance();
      if( gridChanged )
        update();
      return gridChanged;
    }

    /** \brief rebalance the load each process has to handle
     *
     *  A parallel grid is redistributed such that each process has about
     *  the same load (e.g., the same number of leaf entites).
     *
     *  The data handle is used to communicate the data associated with
     *  entities that move from one process to another.
     *
     *  \note DUNE does not specify, how the load is measured.
     *
     *  \param  dataHandle  communication data handle (user defined)
     *
     *  \returns \b true, if the grid has changed.
     */
    template< class DataHandle, class Data >
    bool loadBalance ( CommDataHandleIF< DataHandle, Data > &dataHandle )
    {
      typedef CommDataHandleIF< DataHandle, Data > HostDataHandle;
      IdGridDataHandle< HostDataHandle, GridFamily > dataHandleWrapper( dataHandle );
      return hostGrid().loadBalance( dataHandleWrapper );
    }

    /** \brief rebalance the load each process has to handle
     *
     *  A parallel grid is redistributed such that each process has about
     *  the same load (e.g., the same number of leaf entites).
     *
     *  The data handle is used to communicate the data associated with
     *  entities that move from one process to another.
     *
     *  \note DUNE does not specify, how the load is measured.
     *
     *  \param  dataHandle  data handle following the ALUGrid interface
     *
     *  \returns \b true, if the grid has changed.
     */
    template< class DofManager >
    bool loadBalance ( DofManager &dofManager )
    {
      IdGridWrappedDofManager< DofManager, GridFamily > wrappedDofManager( dofManager );

      const bool gridChanged = hostGrid().loadBalance( wrappedDofManager );
      if( gridChanged )
        update();
      return gridChanged;
    }

    /** \brief View for a grid level */
    template< PartitionIteratorType pitype >
    typename Partition< pitype >::LevelGridView levelGridView ( int level ) const
    {
      typedef typename Partition< pitype >::LevelGridView View;
      typedef typename View::GridViewImp ViewImp;
      return View( ViewImp( *this, level ) );
    }

    /** \brief View for the leaf grid */
    template< PartitionIteratorType pitype >
    typename Partition< pitype >::LeafGridView leafGridView () const
    {
      typedef typename Traits::template Partition< pitype >::LeafGridView View;
      typedef typename View::GridViewImp ViewImp;
      return View( ViewImp( *this ) );
    }

    /** \brief View for a grid level for All_Partition */
    LevelGridView levelGridView ( int level ) const
    {
      typedef typename LevelGridView::GridViewImp ViewImp;
      return LevelGridView( ViewImp( *this, level ) );
    }

    /** \brief View for the leaf grid for All_Partition */
    LeafGridView leafGridView() const
    {
      typedef typename LeafGridView::GridViewImp ViewImp;
      return LeafGridView( ViewImp( *this ) );
    }

    /** \brief obtain EntityPointer from EntitySeed. */
    template< class EntitySeed >
    typename Traits::template Codim< EntitySeed::codimension >::Entity
    entity ( const EntitySeed &seed ) const
    {
      typedef typename Traits::template Codim< EntitySeed::codimension >::EntityImpl EntityImpl;
      return EntityImpl( hostGrid().entity( seed.impl().hostEntitySeed() ) );
    }

    /** \} */

    /** \name Miscellaneous Methods
     *  \{ */

    const HostGrid &hostGrid () const
    {
      return *hostGrid_;
    }

    HostGrid &hostGrid ()
    {
      return *hostGrid_;
    }

    /** \brief update grid caches
     *
     *  This method has to be called whenever the underlying host grid changes.
     *
     *  \note If you adapt the host grid through this geometry grid's
     *        adaptation or load balancing methods, update is automatically
     *        called.
     */
    void update ()
    {
      const std::size_t newNumLevels = maxLevel()+1;
      const std::size_t oldNumLevels = levelIndexSets_.size();

      for( std::size_t i = newNumLevels; i < oldNumLevels; ++i )
      {
        if( levelIndexSets_[ i ] != 0 )
          delete levelIndexSets_[ i ];
      }
      levelIndexSets_.resize( newNumLevels, (LevelIndexSet *)0 );

      ForLoop< ApplyIf< ClearIteratorCache, CodimAvailable >::template Operation, 0, dimension >
        ::apply( newNumLevels, iteratorCaches_ );
    }

    /** \} */

  public:
    using Base::getRealImplementation;

    template< int codim >
    static const typename HostGrid::template Codim< codim >::Entity &
    getHostEntity( const typename Codim< codim >::Entity &entity )
    {
      return getRealImplementation( entity ).hostEntity();
    }

    typedef typename Traits :: ExtraData ExtraData;
    ExtraData extraData () const  { return ExtraData(); }

  protected:
    template< int codim >
    struct CodimAvailable
    {
      static const bool value = Capabilities::hasEntity< HostGrid, codim >::v;
    };

    template< int codim >
    struct IteratorCache
    {
      typedef typename std::conditional< CodimAvailable< codim >::value, CacheItGridIteratorCache< HostGrid, codim >, void * >::type Cache;
      mutable Cache leaf;
      mutable std::vector< Cache > levels;
    };

    typedef GenericGeometry::CodimTable< IteratorCache, dimension > IteratorCacheTable;

    template< int codim >
    struct ClearIteratorCache
    {
      static void apply ( std::size_t numLevels, IteratorCacheTable &caches )
      {
        std::integral_constant< int, codim > codimVariable;
        IteratorCache< codim > &cache = caches[ codimVariable ];

        cache.leaf.clear();
        for( std::size_t level = 0; level < cache.levels.size(); ++level )
          cache.levels[ level ].clear();

        cache.levels.resize( numLevels );
      }
    };

    HostGrid *const hostGrid_;
    const bool deleteHostGrid_;

    IteratorCacheTable iteratorCaches_;
    mutable std::vector< LevelIndexSet * > levelIndexSets_;
    mutable LeafIndexSet *leafIndexSet_;
    mutable GlobalIdSet globalIdSet_;
    mutable LocalIdSet localIdSet_;
  };



  // CacheItGrid::Codim
  // ------------------

  template< class HostGrid >
  template< int codim >
  struct CacheItGrid< HostGrid >::Codim
  : public Base::template Codim< codim >
  {
    /** \name Entity and Entity Pointer Types
     *  \{ */

    /** \brief type of entity
     *
     *  The entity is a model of Dune::Entity.
     */
    typedef typename Traits::template Codim< codim >::Entity Entity;

    /** \brief type of entity pointer
     *
     *  The entity pointer is a model of Dune::EntityPointer.
     */
    typedef typename Traits::template Codim< codim >::EntityPointer EntityPointer;

    /** \} */

    /** \name Geometry Types
     *  \{ */

    /** \brief type of world geometry
     *
     *  Models the geomtry mapping of the entity, i.e., the mapping from the
     *  reference element into world coordinates.
     *
     *  The geometry is a model of Dune::Geometry, implemented through the
     *  generic geometries provided by dune-grid.
     */
    typedef typename Traits::template Codim< codim >::Geometry Geometry;

    /** \brief type of local geometry
     *
     *  Models the geomtry mapping into the reference element of dimension
     *  \em dimension.
     *
     *  The local geometry is a model of Dune::Geometry, implemented through
     *  the generic geometries provided by dune-grid.
     */
    typedef typename Traits::template Codim< codim >::LocalGeometry LocalGeometry;

    /** \} */

    /** \name Iterator Types
     *  \{ */

    template< PartitionIteratorType pitype >
    struct Partition
    {
      typedef typename Traits::template Codim< codim >
        ::template Partition< pitype >::LeafIterator
        LeafIterator;
      typedef typename Traits::template Codim< codim >
        ::template Partition< pitype >::LevelIterator
        LevelIterator;
    };

    /** \brief type of level iterator
     *
     *  This iterator enumerates the entites of codimension \em codim of a
     *  grid level.
     *
     *  The level iterator is a model of Dune::LevelIterator.
     */
    typedef typename Partition< All_Partition >::LeafIterator LeafIterator;

    /** \brief type of leaf iterator
     *
     *  This iterator enumerates the entites of codimension \em codim of the
     *  leaf grid.
     *
     *  The leaf iterator is a model of Dune::LeafIterator.
     */
    typedef typename Partition< All_Partition >::LevelIterator LevelIterator;

    /** \} */
  };

} // namespace Dune

#include <dune/grid/cacheitgrid/persistentcontainer.hh>
#include <dune/grid/cacheitgrid/twistutility.hh>

#endif // #ifndef DUNE_CACHEITGRID_GRID_HH
