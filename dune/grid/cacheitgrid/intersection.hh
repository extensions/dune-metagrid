#ifndef DUNE_CACHEITGRID_INTERSECTION_HH
#define DUNE_CACHEITGRID_INTERSECTION_HH

#include <dune/grid/cacheitgrid/declaration.hh>
#include <dune/grid/cacheitgrid/entitypointer.hh>

namespace Dune
{

  // CacheItGridIntersection
  // -----------------------

  template< class Grid, class HostIntersection >
  class CacheItGridIntersection
  {
    typedef typename std::remove_const< Grid >::type::Traits Traits;

  public:
    typedef typename Traits::ctype ctype;

    static const int dimension = Traits::dimension;
    static const int dimensionworld = Traits::dimensionworld;

    typedef typename Traits::template Codim< 0 >::Entity Entity;
    typedef typename Traits::template Codim< 0 >::EntityPointer EntityPointer;
    typedef typename Traits::template Codim< 1 >::Geometry Geometry;
    typedef typename Traits::template Codim< 1 >::LocalGeometry LocalGeometry;

  private:
    typedef typename Traits::template Codim< 0 >::EntityPointerImpl EntityPointerImpl;

  public:
    CacheItGridIntersection ()
    : hostIntersection_( 0 )
    {}

    explicit CacheItGridIntersection ( const HostIntersection &hostIntersection )
    : hostIntersection_( &hostIntersection )
    {}

    operator bool () const { return bool( hostIntersection_ ); }

    const EntityPointer inside () const
    {
      return EntityPointer( EntityPointerImpl( hostIntersection().inside() ) );
    }

    EntityPointer outside () const
    {
      return EntityPointer( EntityPointerImpl( hostIntersection().outside() ) );
    }

    bool boundary () const
    {
      return hostIntersection().boundary();
    }

    bool conforming () const
    {
      return hostIntersection().conforming();
    }

    bool neighbor () const
    {
      return hostIntersection().neighbor();
    }

    int boundaryId () const
    {
      return hostIntersection().boundaryId();
    }

    size_t boundarySegmentIndex () const
    {
      return hostIntersection().boundarySegmentIndex();
    }

    LocalGeometry geometryInInside () const
    {
      return LocalGeometry( hostIntersection().geometryInInside() );
    }

    LocalGeometry geometryInOutside () const
    {
      return LocalGeometry( hostIntersection().geometryInOutside() );
    }

    Geometry geometry () const
    {
      return Geometry( hostIntersection().geometry() );
    }

    GeometryType type () const
    {
      return hostIntersection().type();
    }

    int indexInInside () const
    {
      return hostIntersection().indexInInside();
    }

    int indexInOutside () const
    {
      return hostIntersection().indexInOutside();
    }

    FieldVector< ctype, dimensionworld >
    integrationOuterNormal ( const FieldVector< ctype, dimension-1 > &local ) const
    {
      return hostIntersection().integrationOuterNormal( local );
    }

    FieldVector< ctype, dimensionworld >
    outerNormal ( const FieldVector< ctype, dimension-1 > &local ) const
    {
      return hostIntersection().outerNormal( local );
    }

    FieldVector< ctype, dimensionworld >
    unitOuterNormal ( const FieldVector< ctype, dimension-1 > &local ) const
    {
      return hostIntersection().unitOuterNormal( local );
    }

    FieldVector< ctype, dimensionworld > centerUnitOuterNormal () const
    {
      return hostIntersection().centerUnitOuterNormal();
    }

    const HostIntersection &hostIntersection () const
    {
      assert( *this );
      return *hostIntersection_;
    }

  private:
    const HostIntersection *hostIntersection_;
  };

} // namespace Dune

#endif // #ifndef DUNE_CACHEITGRID_INTERSECTION_HH
