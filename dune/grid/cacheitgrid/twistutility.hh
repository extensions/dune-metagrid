#ifndef DUNE_CACHEITGRID_TWISTUTILITY_HH
#define DUNE_CACHEITGRID_TWISTUTILITY_HH

#include <cassert>

#include <dune/grid/cacheitgrid/declaration.hh>

#if HAVE_DUNE_FEM
#include <dune/fem/quadrature/caching/twistutility.hh>

namespace Dune
{
  namespace Fem
  {

    // Specialization for CacheItGrid
    // -------------------------

    template< class HostGrid >
    struct TwistUtility< CacheItGrid< HostGrid > >
      : public ForwardTwistUtility< CacheItGrid< HostGrid > >
    {};
  } // end namespace Fem

} // end namespace Dune

#endif // #if HAVE_DUNE_FEM

#endif // #ifndef DUNE_TWISTUTILITY_HH
