#ifndef DUNE_CACHEITGRID_ENTITYPOINTER_HH
#define DUNE_CACHEITGRID_ENTITYPOINTER_HH

#include <dune/grid/common/grid.hh>

#include <dune/grid/cacheitgrid/declaration.hh>

namespace Dune
{

  // External Forward Declarations
  // -----------------------------

  template< class >
  struct CacheItGridExportParams;

  template< int, int, class >
  class CacheItGridEntity;

  template< int, class >
  class CacheItGridIterator;



  // EntityPointerTraits
  // -------------------

  template< int codim, class Grid >
  struct CacheItGridEntityPointerTraits;

  /** \cond */
  template< int codim, class Grid >
  struct CacheItGridEntityPointerTraits< codim, const Grid >
  : public CacheItGridEntityPointerTraits< codim, Grid >
  {};
  /** \endcond */

  template< int codim, class HostGrid >
  struct CacheItGridEntityPointerTraits< codim, CacheItGrid< HostGrid > >
  : public CacheItGridExportParams< HostGrid >
  {
    typedef Dune::CacheItGrid< HostGrid > Grid;

    typedef typename HostGrid::ctype ctype;

    static const int dimension = HostGrid::dimension;
    static const int codimension = codim;

    typedef Dune::Entity< codimension, dimension, const Grid, CacheItGridEntity > Entity;

    typedef typename HostGrid::template Codim< codim >::Entity HostEntity;
    typedef HostEntityPointer HostIterator;
  };



  // CacheItGridEntityPointer
  // ------------------------

  template< class Traits >
  class CacheItGridEntityPointer
  {
    typedef CacheItGridEntityPointer< Traits > This;

    typedef typename Traits::Grid Grid;

    typedef CacheItGridEntityPointerTraits< Traits::codimension, const Grid > BaseTraits;
    friend class CacheItGridEntityPointer< BaseTraits >;

  public:
    static const int dimension = Traits::dimension;
    static const int codimension = Traits::codimension;

    typedef typename Traits::Entity Entity;

    typedef CacheItGridEntityPointer< BaseTraits > EntityPointerImp;

  protected:
    typedef typename Traits::HostIterator HostIterator;

  private:
    typedef CacheItGridEntity< codimension, dimension, const Grid > EntityImpl;

  public:
    explicit CacheItGridEntityPointer ( const HostIterator &hostIterator )
    : entity_( EntityImpl() ),
      hostIterator_( hostIterator )
    {}

    CacheItGridEntityPointer ( const EntityImpl &entity )
    : entity_( EntityImpl() ),
      hostIterator_( entity.hostEntity() )
    {}

    CacheItGridEntityPointer ( const This &other )
    : entity_( EntityImpl() ),
      hostIterator_( other.hostIterator_ )
    {}

    template< class T >
    CacheItGridEntityPointer ( const CacheItGridEntityPointer< T > &other )
    : entity_( EntityImpl() ),
      hostIterator_( other.hostIterator_ )
    {}

    CacheItGridEntityPointer ( const CacheItGridIterator< codimension, const Grid > &other )
    : entity_( EntityImpl() ),
      hostIterator_( other.hostIterator() )
    {}

    const This &operator= ( const This &other )
    {
      releaseEntity();
      hostIterator_ = other.hostIterator_;
      return *this;
    }

    template< class T >
    const This &operator= ( const CacheItGridEntityPointer< T > &other )
    {
      releaseEntity();
      hostIterator_ = other.hostIterator_;
      return *this;
    }

    const This &operator= ( const CacheItGridIterator< codimension, const Grid > &other )
    {
      releaseEntity();
      hostIterator_ = other.hostIterator();
      return *this;
    }

    template< class T >
    bool equals ( const CacheItGridEntityPointer< T > &other ) const
    {
      return (hostIterator() == other.hostIterator());
    }

    bool equals ( const CacheItGridIterator< codimension, const Grid > &other ) const
    {
      return (hostIterator() == other.hostIterator());
    }

    Entity &dereference () const
    {
      if( !entityImpl() )
        entityImpl() = EntityImpl( *hostIterator() );
      return entity_;
    }

    int level () const
    {
      return hostIterator().level();
    }

    const HostIterator &hostIterator() const
    {
      return hostIterator_;
    }

  protected:
    void releaseEntity () { entityImpl() = EntityImpl(); }

  private:
    EntityImpl &entityImpl () const
    {
      return Grid::getRealImplementation( entity_ );
    }

    mutable Entity entity_;

  protected:
    HostIterator hostIterator_;
  };

} // namespace Dune

#endif // #ifndef DUNE_CACHEITGRID_ENTITYPOINTER_HH
