#ifndef DUNE_CACHEITGRID_GRIDVIEW_HH
#define DUNE_CACHEITGRID_GRIDVIEW_HH

#include <dune/common/typetraits.hh>

#include <dune/grid/common/capabilities.hh>
#include <dune/grid/common/gridview.hh>

#include <dune/grid/cacheitgrid/intersection.hh>
#include <dune/grid/cacheitgrid/intersectioniterator.hh>
#include <dune/grid/cacheitgrid/iterator.hh>
#include <dune/grid/idgrid/datahandle.hh>
#include <dune/grid/idgrid/indexset.hh>

namespace Dune
{

  // External Forward Declarations
  // -----------------------------

  template< class HostGrid >
  class CacheItGridFamily;



  // Internal Forward Declarations
  // -----------------------------

  template< class HostGrid, PartitionIteratorType pitype >
  class CacheItGridLevelGridView;

  template< class HostGrid, PartitionIteratorType pitype >
  class CacheItGridLeafGridView;



  // CacheItGridLevelGridViewTraits
  // ------------------------------

  template< class HostGrid, PartitionIteratorType pitype >
  class CacheItGridLevelGridViewTraits
  {
    friend class CacheItGridLevelGridView< HostGrid, pitype >;

    typedef typename HostGrid::template Partition< pitype >::LevelGridView HostGridView;

  public:
    typedef CacheItGridLevelGridView< HostGrid, pitype > GridViewImp;

    typedef CacheItGrid< HostGrid > Grid;

    typedef IdGridIndexSet< const Grid, typename HostGrid::Traits::LevelIndexSet > IndexSet;

    typedef CacheItGridIntersection< const Grid, typename HostGridView::Intersection > IntersectionImpl;
    typedef Dune::Intersection< const Grid, IntersectionImpl > Intersection;

    typedef CacheItGridIntersectionIterator< const Grid, typename HostGridView::IntersectionIterator > IntersectionIteratorImpl;
    typedef Dune::IntersectionIterator< const Grid, IntersectionIteratorImpl, IntersectionImpl > IntersectionIterator;

    typedef typename HostGridView::Communication Communication;

    template< int codim >
    struct Codim
    {
      typedef typename Grid::Traits::template Codim< codim >::Entity Entity;
      typedef typename Grid::Traits::template Codim< codim >::EntityPointer EntityPointer;

      typedef typename Grid::template Codim< codim >::Geometry Geometry;
      typedef typename Grid::template Codim< codim >::LocalGeometry LocalGeometry;

      template< PartitionIteratorType pit >
      struct Partition
      {
        typedef Dune::EntityIterator< codim, const Grid, CacheItGridIterator< codim, const Grid > > Iterator;
      };

      typedef typename Partition< pitype >::Iterator Iterator;
    };

    static const bool conforming = HostGridView::conforming;
  };



  // CacheItGridLevelGridView
  // ------------------------

  template< class HostGrid, PartitionIteratorType pitype >
  class CacheItGridLevelGridView
  {
    typedef CacheItGridLevelGridView< HostGrid, pitype > This;

    typedef CacheItGridFamily< HostGrid > GridFamily;

  public:
    typedef CacheItGridLevelGridViewTraits< HostGrid, pitype > Traits;

    typedef typename Traits::HostGridView HostGridView;

    typedef typename Traits::Grid Grid;

    typedef typename Traits::IndexSet IndexSet;

    typedef typename Traits::Intersection Intersection;

    typedef typename Traits::IntersectionIterator IntersectionIterator;

    typedef typename Traits::Communication Communication;

    template< int codim >
    struct Codim
    : public Traits::template Codim< codim >
    {};

    static const bool conforming = Traits::conforming;

    CacheItGridLevelGridView ( const Grid &grid, int level )
    : grid_( &grid ),
      level_( level ),
      hostGridView_( grid.hostGrid().template levelGridView< pitype >( level ) )
    {}

    const Grid &grid () const
    {
      assert( grid_ );
      return *grid_;
    }

    const IndexSet &indexSet () const
    {
      return grid().levelIndexSet( level_ );
    }

    int size ( int codim ) const
    {
      return hostGridView().size( codim );
    }

    int size ( const GeometryType &type ) const
    {
      return hostGridView().size( type );
    }

    template< int codim >
    typename Codim< codim >::Iterator begin () const
    {
      return grid().template lbegin< codim, pitype >( level_ );
    }

    template< int codim, PartitionIteratorType pit >
    typename Codim< codim >::template Partition< pit >::Iterator begin () const
    {
      return grid().template lbegin< codim, pit >( level_ );
    }

    template< int codim >
    typename Codim< codim >::Iterator end () const
    {
      return grid().template lend< codim, pitype >( level_ );
    }

    template< int codim, PartitionIteratorType pit >
    typename Codim< codim >::template Partition< pit >::Iterator end () const
    {
      return grid().template lend< codim, pit >( level_ );
    }

    IntersectionIterator ibegin ( const typename Codim< 0 >::Entity &entity ) const
    {
      typedef typename Traits::IntersectionIteratorImpl IntersectionIteratorImpl;
      return IntersectionIteratorImpl( hostGridView().ibegin( Grid::getRealImplementation( entity ).hostEntity() ) );
    }

    IntersectionIterator iend ( const typename Codim< 0 >::Entity &entity ) const
    {
      typedef typename Traits::IntersectionIteratorImpl IntersectionIteratorImpl;
      return IntersectionIteratorImpl( hostGridView().iend( Grid::getRealImplementation( entity ).hostEntity() ) );
    }

    bool isConforming () const
    {
      return hostGridView().isConforming();
    }

    const Communication &comm () const
    {
      return hostGridView().comm();
    }

    int overlapSize ( int codim ) const
    {
      return hostGridView().overlapSize( codim );
    }

    int ghostSize ( int codim ) const
    {
      return hostGridView().ghostSize( codim );
    }

    template< class DataHandle, class Data >
    void communicate ( CommDataHandleIF< DataHandle, Data > &dataHandle,
                       InterfaceType interface,
                       CommunicationDirection direction ) const
    {
      typedef CommDataHandleIF< DataHandle, Data > DataHandleIF;
      typedef IdGridDataHandle< DataHandleIF, GridFamily > WrappedDataHandle;

      WrappedDataHandle wrappedDataHandle( dataHandle );
      hostGridView().communicate( wrappedDataHandle, interface, direction );
    }

    const HostGridView &hostGridView () const { return hostGridView_; }

  private:
    const Grid *grid_;
    int level_;
    HostGridView hostGridView_;
  };



  // CacheItGridLeafGridViewTraits
  // -----------------------------

  template< class HostGrid, PartitionIteratorType pitype >
  class CacheItGridLeafGridViewTraits
  {
    friend class CacheItGridLeafGridView< HostGrid, pitype >;

    typedef typename HostGrid::template Partition< pitype >::LeafGridView HostGridView;

  public:
    typedef CacheItGridLeafGridView< HostGrid, pitype > GridViewImp;

    typedef CacheItGrid< HostGrid > Grid;

    typedef IdGridIndexSet< const Grid, typename HostGrid::Traits::LeafIndexSet > IndexSet;

    typedef CacheItGridIntersection< const Grid, typename HostGridView::Intersection > IntersectionImpl;
    typedef Dune::Intersection< const Grid, IntersectionImpl > Intersection;

    typedef CacheItGridIntersectionIterator< const Grid, typename HostGridView::IntersectionIterator > IntersectionIteratorImpl;
    typedef Dune::IntersectionIterator< const Grid, IntersectionIteratorImpl, IntersectionImpl > IntersectionIterator;

    typedef typename HostGridView::Communication Communication;

    template< int codim >
    struct Codim
    {
      typedef typename Grid::Traits::template Codim< codim >::Entity Entity;
      typedef typename Grid::Traits::template Codim< codim >::EntityPointer EntityPointer;

      typedef typename Grid::template Codim< codim >::Geometry Geometry;
      typedef typename Grid::template Codim< codim >::LocalGeometry LocalGeometry;

      template< PartitionIteratorType pit >
      struct Partition
      {
        typedef Dune::EntityIterator< codim, const Grid, CacheItGridIterator< codim, const Grid > > Iterator;
      };

      typedef typename Partition< pitype >::Iterator Iterator;
    };

    static const bool conforming = HostGridView::conforming;
  };



  // CacheItGridLeafGridView
  // -----------------------

  template< class HostGrid, PartitionIteratorType pitype >
  class CacheItGridLeafGridView
  {
    typedef CacheItGridLeafGridView< HostGrid, pitype > This;

    typedef CacheItGridFamily< HostGrid > GridFamily;

  public:
    typedef CacheItGridLeafGridViewTraits< HostGrid, pitype > Traits;

    typedef typename Traits::HostGridView HostGridView;

    typedef typename Traits::Grid Grid;

    typedef typename Traits::IndexSet IndexSet;

    typedef typename Traits::Intersection Intersection;

    typedef typename Traits::IntersectionIterator IntersectionIterator;

    typedef typename Traits::Communication Communication;

    template< int codim >
    struct Codim
    : public Traits::template Codim< codim >
    {};

    static const bool conforming = Traits::conforming;

  public:
    explicit CacheItGridLeafGridView ( const Grid &grid )
    : grid_( &grid ),
      hostGridView_( grid.hostGrid().template leafGridView< pitype >() )
    {}

    const Grid &grid () const
    {
      assert( grid_ );
      return *grid_;
    }

    const IndexSet &indexSet () const
    {
      return grid().leafIndexSet();
    }

    int size ( int codim ) const
    {
      return hostGridView().size( codim );
    }

    int size ( const GeometryType &type ) const
    {
      return hostGridView().size( type );
    }

    template< int codim >
    typename Codim< codim >::Iterator begin () const
    {
      return grid().template leafbegin< codim, pitype >();
    }

    template< int codim, PartitionIteratorType pit >
    typename Codim< codim >::template Partition< pit >::Iterator begin () const
    {
      return grid().template leafbegin< codim, pit >();
    }

    template< int codim >
    typename Codim< codim >::Iterator end () const
    {
      return grid().template leafend< codim, pitype >();
    }

    template< int codim, PartitionIteratorType pit >
    typename Codim< codim >::template Partition< pit >::Iterator end () const
    {
      return grid().template leafend< codim, pit >();
    }

    IntersectionIterator ibegin ( const typename Codim< 0 >::Entity &entity ) const
    {
      typedef typename Traits::IntersectionIteratorImpl IntersectionIteratorImpl;
      return IntersectionIteratorImpl( hostGridView().ibegin( Grid::getRealImplementation( entity ).hostEntity() ) );
    }

    IntersectionIterator iend ( const typename Codim< 0 >::Entity &entity ) const
    {
      typedef typename Traits::IntersectionIteratorImpl IntersectionIteratorImpl;
      return IntersectionIteratorImpl( hostGridView().iend( Grid::getRealImplementation( entity ).hostEntity() ) );
    }

    bool isConforming () const
    {
      return hostGridView().isConforming();
    }

    const Communication &comm () const
    {
      return hostGridView().comm();
    }

    int overlapSize ( int codim ) const
    {
      return hostGridView().overlapSize( codim );
    }

    int ghostSize ( int codim ) const
    {
      return hostGridView().ghostSize( codim );
    }

    template< class DataHandle, class Data >
    void communicate ( CommDataHandleIF< DataHandle, Data > &dataHandle,
                       InterfaceType interface,
                       CommunicationDirection direction ) const
    {

      typedef CommDataHandleIF< DataHandle, Data > DataHandleIF;
      typedef IdGridDataHandle< DataHandleIF, GridFamily > WrappedDataHandle;

      WrappedDataHandle wrappedDataHandle( dataHandle );
      hostGridView().communicate( wrappedDataHandle, interface, direction );
    }

    const HostGridView &hostGridView () const { return hostGridView_; }

  private:
    const Grid *grid_;
    HostGridView hostGridView_;
  };

} // namespace Dune

#endif // #ifndef DUNE_CACHEITGRID_GRIDVIEW_HH
