install(FILES capabilities.hh
              datahandle.hh
              declaration.hh
              dgfparser.hh
              entity.hh
              entityseed.hh
              geometry.hh
              grid.hh
              gridview.hh
              hostgridaccess.hh
              idset.hh
              indexset.hh
              intersection.hh
              intersectioniterator.hh
              iterator.hh
              persistentcontainer.hh
              twistutility.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/grid/idgrid)
