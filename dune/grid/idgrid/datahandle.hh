#ifndef DUNE_IDGRID_DATAHANDLE_HH
#define DUNE_IDGRID_DATAHANDLE_HH

//- dune-common includes
#include <dune/common/typetraits.hh>

//- dune-grid includes
#include <dune/grid/common/datahandleif.hh>

#if HAVE_DUNE_ALUGRID
#include <dune/alugrid/common/ldbhandleif.hh>
#endif

namespace Dune
{

  // IdGridDataHandleBase
  // ---------------------------

#if HAVE_DUNE_ALUGRID
  template< class WrappedHandle,
            bool hasCompressAndReserve = std::is_base_of< LoadBalanceHandleWithReserveAndCompress, WrappedHandle >::value >
  class IdGridDataHandleBase;
#else // #if HAVE_DUNE_ALUGRID
  template< class WrappedHandle, bool hasCompressAndReserve = false >
  class IdGridDataHandleBase;
#endif // #else // #if HAVE_DUNE_ALUGRID

#if HAVE_DUNE_ALUGRID
  template< class WrappedHandle >
  struct IdGridDataHandleBase< WrappedHandle, true >
    : public LoadBalanceHandleWithReserveAndCompress
  {
    explicit IdGridDataHandleBase ( WrappedHandle &handle )
      : wrappedHandle_( handle )
    {}

    void reserveMemory ( std::size_t newElements )
    {
      return wrappedHandle_.reserveMemory( newElements );
    }

    void compress () { return wrappedHandle_.compress(); }

  protected:
    WrappedHandle &wrappedHandle_;
  };
#endif // #if HAVE_DUNE_ALUGRID

  template< class WrappedHandle >
  struct IdGridDataHandleBase< WrappedHandle, false >
  {
    explicit IdGridDataHandleBase ( WrappedHandle &handle )
      : wrappedHandle_( handle )
    {}

  protected:
    WrappedHandle &wrappedHandle_;
  };


  // IdGridDataHandle
  // ----------------

  template< class Grid, class WrappedHandle >
  class IdGridDataHandle
  : public IdGridDataHandleBase< WrappedHandle >,
    public CommDataHandleIF< IdGridDataHandle< Grid, WrappedHandle >, typename WrappedHandle::DataType >
  {
  protected:
    typedef IdGridDataHandle< Grid, WrappedHandle > This;
    typedef IdGridDataHandleBase< WrappedHandle >   Base;

    using Base :: wrappedHandle_ ;

    // type of traits
    typedef typename std::remove_const< Grid >::type::Traits Traits;

    typedef typename Traits :: ExtraData ExtraData ;

    template< int codim >
    struct Codim
    {
      // type of entity
      typedef typename Traits::template Codim< codim >::Entity Entity;
    };

  public:
    // type of data to be communicated
    typedef typename WrappedHandle::DataType DataType;

    typedef CommDataHandleIF< This, DataType > DataHandleIF;

  private:
    // prohibit copying
    IdGridDataHandle ( const This & );

  public:
    IdGridDataHandle ( ExtraData data, WrappedHandle &wrappedHandle )
    : Base( wrappedHandle ),
      data_( data )
    {}

    bool contains ( int dim, int codim ) const
    {
      return wrappedHandle_.contains( dim, codim );
    }

    bool fixedSize ( int dim, int codim ) const
    {
      return wrappedHandle_.fixedSize( dim, codim );
    }

    template< class HostEntity >
    size_t size ( const HostEntity &hostEntity ) const
    {
      typedef typename Codim< HostEntity::codimension >::Entity Entity;
      const Entity entity( typename Entity::Implementation( data(), hostEntity ) );
      return wrappedHandle_.size( entity );
    }

    template< class MessageBuffer, class HostEntity >
    void gather ( MessageBuffer &buffer, const HostEntity &hostEntity ) const
    {
      typedef typename Codim< HostEntity::codimension >::Entity Entity;
      const Entity entity( typename Entity::Implementation( data(), hostEntity ) );
      wrappedHandle_.gather( buffer, entity );
    }

    template< class MessageBuffer, class HostEntity >
    void scatter ( MessageBuffer &buffer, const HostEntity &hostEntity, size_t size )
    {
      typedef typename Codim< HostEntity::codimension >::Entity Entity;
      const Entity entity( typename Entity::Implementation( data(), hostEntity ) );
      wrappedHandle_.scatter( buffer, entity, size );
    }

    ExtraData data() const { return data_; }

  protected:
    ExtraData data_;
  };



  // IdGridWrappedDofManager
  // -----------------------

  template< class WrappedDofManager, class Grid >
  class IdGridWrappedDofManager
  {
    typedef IdGridWrappedDofManager< WrappedDofManager, Grid > This;

    typedef typename std::remove_const< Grid >::type::Traits Traits;

    // type of element (i.e., entity of codimension 0)
    typedef typename Traits::template Codim< 0 >::Entity Element;

    // type of host element (i.e., host entity of codimension 0)
    typedef typename Traits::HostGrid::template Codim< 0 >::Entity HostElement;

  private:
    // prohibit copy constructor
    IdGridWrappedDofManager ( const This & );

  public:
    explicit IdGridWrappedDofManager ( WrappedDofManager &wrappedDofManager )
    : wrappedDofManager_( wrappedDofManager )
    {}

    template< class MessageBuffer >
    void inlineData ( MessageBuffer &buffer, const HostElement &hostElement )
    {
      const Element element( (typename Element::Implementation)( hostElement ) );
      wrappedDofManager_.inlineData( buffer, element );
    }

    template< class MessageBuffer >
    void xtractData ( MessageBuffer &buffer, const HostElement &hostElement, std::size_t newElements )
    {
      const Element element( (typename Element::Implementation)( hostElement ) );
      wrappedDofManager_.xtractData( buffer, element, newElements );
    }

    void compress () { wrappedDofManager_.compress(); }

  private:
    WrappedDofManager &wrappedDofManager_;
  };

} // namespace Dune

#endif // #ifndef DUNE_IDGRID_DATAHANDLE_HH
