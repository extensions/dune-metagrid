#ifndef DUNE_IDGRID_CAPABILITIES_HH
#define DUNE_IDGRID_CAPABILITIES_HH

//- dune-common includes
#include <dune/common/version.hh>

//- dune-grid includes
#include <dune/grid/common/capabilities.hh>

//- dune-metagrid includes
#include <dune/grid/idgrid/declaration.hh>

namespace Dune
{

  namespace Capabilities
  {

    // Capabilities from dune-grid
    // ---------------------------

    template< class HostGrid >
    struct hasSingleGeometryType< IdGrid< HostGrid > >
    {
      static const bool v = hasSingleGeometryType< HostGrid >::v;
      static const unsigned int topologyId = hasSingleGeometryType< HostGrid >::topologyId;
    };


    template< class HostGrid >
    struct isCartesian< IdGrid< HostGrid > >
    {
      static const bool v = isCartesian< HostGrid >::v;
    };


    template< class HostGrid, int codim >
    struct hasEntity< IdGrid< HostGrid >, codim >
    {
      static const bool v = hasEntity< HostGrid, codim >::v;
    };

#if ! DUNE_VERSION_NEWER(DUNE_GRID,2,6)
    template< class HostGrid >
    struct isParallel< IdGrid< HostGrid > >
    {
      static const bool v = isParallel< HostGrid >::v;
    };
#endif


    template< class HostGrid, int codim >
    struct canCommunicate< IdGrid< HostGrid >, codim >
    {
      static const bool v = canCommunicate< HostGrid, codim >::v;
    };


    template< class HostGrid >
    struct hasBackupRestoreFacilities< IdGrid< HostGrid > >
    {
      static const bool v = hasBackupRestoreFacilities< HostGrid >::v;
    };

    template< class HostGrid >
    struct isLevelwiseConforming< IdGrid< HostGrid > >
    {
      static const bool v = isLevelwiseConforming< HostGrid >::v;
    };

    template< class HostGrid >
    struct isLeafwiseConforming< IdGrid< HostGrid > >
    {
      static const bool v = isLeafwiseConforming< HostGrid >::v;
    };

    template< class HostGrid >
    struct threadSafe< IdGrid< HostGrid > >
    {
      static const bool v = false;
    };

    template< class HostGrid >
    struct viewThreadSafe< IdGrid< HostGrid > >
    {
      static const bool v = false;
    };

  } // namespace Capabilities

} // namespace Dune

#endif // #ifndef DUNE_IDGRID_CAPABILITIES_HH
