#ifndef DUNE_IDGRID_TWISTUTILITY_HH
#define DUNE_IDGRID_TWISTUTILITY_HH

//- C++ includes
#include <cassert>

//- dune-metagrid includes
#include <dune/grid/idgrid/declaration.hh>

#if HAVE_DUNE_FEM
//- dune-fem includes
#include <dune/fem/quadrature/caching/twistutility.hh>
#include <dune/grid/common/forwardtwistutility.hh>

namespace Dune
{

  namespace Fem
  {

    // Specialization for IdGrid
    // -------------------------

    template< class HostGrid >
    struct TwistUtility< IdGrid< HostGrid > >
      : public ForwardTwistUtility< IdGrid< HostGrid > >
    {
    };

  } // namespace Fem

} // namespace Dune

#endif // #if HAVE_DUNE_FEM

#endif // #ifndef DUNE_IDGRID_TWISTUTILITY_HH
