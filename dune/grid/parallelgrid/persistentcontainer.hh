#ifndef DUNE_PARALLELGRID_PERSISTENTCONTAINER_HH
#define DUNE_PARALLELGRID_PERSISTENTCONTAINER_HH

#include <dune/grid/utility/persistentcontainerwrapper.hh>

#include <dune/grid/parallelgrid/declaration.hh>

namespace Dune
{

  // PersistentContainer for ParallelGrid
  // ------------------------------------

  template< class HostGrid, class T >
  class PersistentContainer< ParallelGrid< HostGrid >, T >
  : public PersistentContainerWrapper< ParallelGrid< HostGrid >, T >
  {
    typedef PersistentContainerWrapper< ParallelGrid< HostGrid >, T > Base;

  public:
    typedef typename Base::Grid Grid;
    typedef typename Base::Value Value;

    PersistentContainer ( const Grid &grid, int codim, const Value &value = Value() )
    : Base( grid, codim, value )
    {}
  };

} // namespace Dune

#endif // #ifndef DUNE_PARALLELGRID_PERSISTENTCONTAINER_HH
