#ifndef DUNE_PARALLELGRID_ENTITY_HH
#define DUNE_PARALLELGRID_ENTITY_HH

#include <dune/geometry/referenceelements.hh>

#include <dune/grid/idgrid/entity.hh>

namespace Dune
{

  // External Forward Declarations
  // -----------------------------

  template< class, class > class ParallelGridIntersection;
  template< class, class > class ParallelGridIntersectionIterator;

  template< class, PartitionIteratorType, class > class ParallelGridIteratorChecked;

  // ParallelGridEntity
  // ------------------

  /** \copydoc ParallelGridEntity
   *
   *  \nosubgrouping
   */
  template< int codim, int dim, class Grid >
  class ParallelGridEntityBase : public IdGridEntity< codim, dim, Grid >
  {
    using Base = IdGridEntity<codim, dim, Grid>;
  protected:
    using HostGrid = typename Base::HostGrid;
    using ExtraData = typename Base::ExtraData;

  public:
    using Base::data;
    using Base::hostEntity;

    /** \name Host Types
     *  \{ */

    //! type of corresponding host entity
    using HostEntity = typename Base::HostEntity;

    /** \} */

  public:
    /** \name Construction, Initialization and Destruction
     *  \{ */

    /** \brief construct a null entity */
    explicit ParallelGridEntityBase(ExtraData data)
    : Base(data)
    {}

    /** \brief Default constructed null entity */
    ParallelGridEntityBase()
    : Base(nullptr)
    {}

    /** \brief construct an initialized entity
     *
     *  \param[in]  hostEntity  corresponding entity in the host grid
     *
     *  \note The reference to the host entity must remain valid  as long as
     *        this entity is in use.
     */
    ParallelGridEntityBase(ExtraData data,
                           const HostEntity& hostEntity )
    : Base(data, hostEntity)
    {}

    /** \brief copy constructor */
    ParallelGridEntityBase(const ParallelGridEntityBase& other)
    : Base(other)
    {}

    /** \name Methods Shared by Entities of All Codimensions
     *  \{ */

    /** \brief obtain the partition type of this entity */
    PartitionType partitionType () const
    {
      // return partitionType from rank manager
      return data()->rankManager().partitionType( hostEntity() );
    }
    /** \} */
  };

  // ParallelGridEntity
  // ------------------

  /** \copydoc ParallelGridEntity
   *
   *  \nosubgrouping
   */
  template< int codim, int dim, class Grid >
  class ParallelGridEntity
  : public ParallelGridEntityBase<codim, dim, Grid>
  {
    using Base = ParallelGridEntityBase<codim, dim, Grid>;
  public:
    //! inherit constructors
    using Base::Base;
  };

  /** \brief ParallelGridEntity specialization for codim-0 entities
   *
   *  \nosubgrouping
   */
  template<int dim, class Grid >
  class ParallelGridEntity<0, dim, Grid>
  : public ParallelGridEntityBase<0, dim, Grid>
  {
    using Base = ParallelGridEntityBase<0, dim, Grid>;
  public:
    //! inherit constructors
    using Base::Base;

    /** \name Types required by DUNE
     *  \{ */

    //! type of hierarchic iterator
    using HierarchicIterator = typename Base::HierarchicIterator;

    /** \} */

    //! hierarchic iterators are overloaded because of special constructor
    HierarchicIterator hbegin (int maxLevel) const
    {
      using Impl = typename Base::Traits::HierarchicIteratorImpl;
      return Impl(this->data(), this->hostEntity().hbegin(maxLevel), this->hostEntity().hend(maxLevel));
    }

    //! hierarchic iterators are overloaded because of special constructor
    HierarchicIterator hend (int maxLevel) const
    {
      using Impl = typename Base::Traits::HierarchicIteratorImpl;
      return Impl(this->data(), this->hostEntity().hend(maxLevel));
    }

    //! \brief return true if entity has boundary intersections
    bool hasBoundaryIntersections () const
    {
      // ghosts don't have boundary intersections (only internal intersections)
      if ( this->partitionType() == GhostEntity )
        return false;
      else
        return Base::hasBoundaryIntersections();
    }

  };

} // namespace Dune

#endif // #ifndef DUNE_PARALLELGRID_ENTITY_HH
