#ifndef DUNE_PARALLELGRID_GRIDVIEW_HH
#define DUNE_PARALLELGRID_GRIDVIEW_HH

//- dune-common includes
#include <dune/common/typetraits.hh>

//- dune-grid includes
#if HAVE_DUNE_ALUGRID
#include <dune/alugrid/common/defaultindexsets.hh>
#endif

#include <dune/grid/common/capabilities.hh>
#include <dune/grid/common/gridview.hh>

//- dune-metagrid includes
#include <dune/grid/common/metagriddatahandle.hh>
#include <dune/grid/parallelgrid/communication.hh>
#include <dune/grid/parallelgrid/intersection.hh>
#include <dune/grid/parallelgrid/intersectioniterator.hh>
#include <dune/grid/parallelgrid/iterator.hh>

namespace Dune
{

  // Internal Forward Declarations
  // -----------------------------

  template< class HGV, PartitionIteratorType pitype >
  class ParallelGridView;

  template< class HostGrid, PartitionIteratorType pitype >
  class ParallelGridLeafView;

  template< class HostGrid, PartitionIteratorType pitype >
  class ParallelGridLevelView;



  // ParallelGridViewTraits
  // ----------------------

  template< class HGV, PartitionIteratorType pitype >
  class ParallelGridViewTraits
  {
    friend class ParallelGridView< HGV, pitype >;

    typedef HGV HostGridView;

    typedef typename HostGridView::Grid HostGrid;

  public:
    typedef ParallelGrid< HostGrid > Grid;

    typedef ParallelGridIntersection< const Grid, typename HostGridView::Intersection > IntersectionImpl;
    typedef Dune::Intersection< const Grid, IntersectionImpl > Intersection;

    typedef ParallelGridIntersectionIterator< const Grid, typename HostGridView::IntersectionIterator > IntersectionIteratorImpl;
    typedef Dune::IntersectionIterator< const Grid, IntersectionIteratorImpl, IntersectionImpl > IntersectionIterator;

    typedef typename Grid::Communication Communication;

    template< int codim >
    struct Codim
    {
      typedef typename Grid::Traits::template Codim< codim >::Entity Entity;

      typedef typename Grid::template Codim< codim >::Geometry Geometry;
      typedef typename Grid::template Codim< codim >::LocalGeometry LocalGeometry;

      template< PartitionIteratorType pit >
      struct Partition
      {
        typedef ParallelGridIteratorChecked< const Grid, pit, typename HostGridView::template Codim< codim >::Iterator > IteratorImpl;
        typedef Dune::EntityIterator< codim, const Grid, IteratorImpl > Iterator;
      };

      typedef typename Partition< pitype >::Iterator Iterator;
    };

    typedef DefaultIndexSet< Grid, typename Codim< 0 >::Iterator > IndexSet;

    static const bool conforming = HostGridView::conforming;
  };



  // ParallelGridView
  // ----------------

  template< class HGV, PartitionIteratorType pitype >
  class ParallelGridView
  {
    typedef ParallelGridView< HGV, pitype > This;

  public:
    typedef ParallelGridViewTraits< HGV, pitype > Traits;

    typedef typename Traits::HostGridView HostGridView;

    typedef typename Traits::Grid Grid;

    typedef typename Traits::IndexSet IndexSet;

    typedef typename Traits::Intersection Intersection;

    typedef typename Traits::IntersectionIterator IntersectionIterator;

    typedef typename Traits::Communication Communication;

    template< int codim >
    struct Codim
    : public Traits::template Codim< codim >
    {};

    static const bool conforming = Traits::conforming;

    static const int dimension = Grid::dimension;

    typedef typename Grid::RankManager RankManager;

    ParallelGridView ( const Grid &grid, const HostGridView &hostGridView )
    : grid_( &grid ),
      hostGridView_( hostGridView )
    {}

    const Grid &grid () const
    {
      assert( grid_ );
      return *grid_;
    }

    template< int codim >
    typename Codim< codim >::Iterator begin () const
    {
      return begin< codim, pitype >();
    }

    template< int codim, PartitionIteratorType pit >
    typename Codim< codim >::template Partition< pit >::Iterator begin () const
    {
      typedef typename Traits::template Codim< codim >::template Partition< pit >::IteratorImpl Impl;
      return Impl( grid_, hostGridView().template begin< codim >(), hostGridView().template end< codim >() );
    }

    template< int codim >
    typename Codim< codim >::Iterator end () const
    {
      return end< codim, pitype >();
    }

    template< int codim, PartitionIteratorType pit >
    typename Codim< codim >::template Partition< pit >::Iterator end () const
    {
      typedef typename Traits::template Codim< codim >::template Partition< pit >::IteratorImpl Impl;
      return Impl( grid_, hostGridView().template end< codim >() );
    }

    IntersectionIterator ibegin ( const typename Codim< 0 >::Entity &entity ) const
    {
      using IntersectionIteratorImpl = typename Traits::IntersectionIteratorImpl;
      return IntersectionIteratorImpl(
        grid_,
        hostGridView().ibegin( entity.impl().hostEntity() ),
        hostGridView().iend( entity.impl().hostEntity() )
      );
    }

    IntersectionIterator iend ( const typename Codim< 0 >::Entity &entity ) const
    {
      using IntersectionIteratorImpl = typename Traits::IntersectionIteratorImpl;
      return IntersectionIteratorImpl(
        grid_,
        hostGridView().iend( entity.impl().hostEntity() )
      );
    }

    bool isConforming () const
    {
      return hostGridView().isConforming();
    }

    const Communication &comm () const { return grid().comm(); }

    int overlapSize ( int codim ) const { return 0; }
    int ghostSize ( int codim ) const { return (codim == 0 ? 1 : 0); }

    template< class DataHandle, class Data >
    void communicate ( CommDataHandleIF< DataHandle, Data > &dataHandle,
                       InterfaceType interface,
                       CommunicationDirection direction ) const
    {
      assert( (direction == ForwardCommunication) || (direction == BackwardCommunication) );

      // only communicate, if number of processes is larger than 1
      if( comm().size() <= 1 )
        return;

      if( (interface == InteriorBorder_All_Interface) || (interface == All_All_Interface) )
      {
        ParallelGridCommunication< Grid > communication( rankManager() );

        switch( direction )
        {
        case ForwardCommunication:
          communication( begin< 0, Interior_Partition >(), end< 0, Interior_Partition >(),
                         begin< 0, Ghost_Partition >(), end< 0, Ghost_Partition >(),
                         dataHandle, InteriorEntity, GhostEntity,
                         (interface == All_All_Interface) );
          break;

        case BackwardCommunication:
          communication( begin< 0, Ghost_Partition >(), end< 0, Ghost_Partition >(),
                         begin< 0, Interior_Partition >(), end< 0, Interior_Partition >(),
                         dataHandle, GhostEntity, InteriorEntity,
                         (interface == All_All_Interface) );
          break;
        }
      }
      else
        DUNE_THROW( NotImplemented, "Communication on interface type " << interface << " not implemented." );
    }

    const HostGridView &hostGridView () const { return hostGridView_; }
    const RankManager &rankManager () const { return grid().rankManager(); }

  private:
    const Grid *grid_;
    HostGridView hostGridView_;
  };



  // ParallelGridLeafViewTraits
  // --------------------------

  template< class HostGrid, PartitionIteratorType pitype >
  struct ParallelGridLeafViewTraits
  : public ParallelGridViewTraits< typename HostGrid::LeafGridView, pitype >
  {
    typedef ParallelGridLeafView< HostGrid, pitype > GridViewImp;
  };



  // ParallelGridLeafView
  // --------------------

  template< class HostGrid, PartitionIteratorType pitype >
  class ParallelGridLeafView
  : public ParallelGridView< typename HostGrid::LeafGridView, pitype >
  {
    typedef ParallelGridView< HostGrid, pitype > This;
    typedef ParallelGridView< typename HostGrid::LeafGridView, pitype > Base;

  public:
    typedef typename Base::Grid Grid;
    typedef typename Base::IndexSet IndexSet;

    ParallelGridLeafView ( const Grid &grid )
    : Base( grid, grid.hostGrid().leafGridView() )
    {}

    using Base::grid;

    const IndexSet &indexSet () const
    {
      return grid().leafIndexSet();
    }

    int size ( int codim ) const
    {
      return grid().size( codim );
    }

    int size ( const GeometryType &type ) const
    {
      return grid().size( type );
    }
  };



  // ParallelGridLevelViewTraits
  // ---------------------------

  template< class HostGrid, PartitionIteratorType pitype >
  struct ParallelGridLevelViewTraits
  : public ParallelGridViewTraits< typename HostGrid::LevelGridView, pitype >
  {
    typedef ParallelGridLevelView< HostGrid, pitype > GridViewImp;
  };



  // ParallelGridLevelView
  // ---------------------

  template< class HostGrid, PartitionIteratorType pitype >
  class ParallelGridLevelView
  : public ParallelGridView< typename HostGrid::LevelGridView, pitype >
  {
    typedef ParallelGridView< HostGrid, pitype > This;
    typedef ParallelGridView< typename HostGrid::LevelGridView, pitype > Base;

  public:
    typedef typename Base::Grid Grid;
    typedef typename Base::IndexSet IndexSet;

    ParallelGridLevelView ( const Grid &grid, int level )
    : Base( grid, grid.hostGrid().levelGridView( level ) ),
      level_( level )
    {}

    using Base::grid;

    const IndexSet &indexSet () const
    {
      return grid().levelIndexSet( level_ );
    }

    int size ( int codim ) const
    {
      return grid().size( level_, codim );
    }

    int size ( const GeometryType &type ) const
    {
      return grid().size( level_, type );
    }

  private:
    int level_;
  };

} // namespace Dune

#endif // #ifndef DUNE_PARALLELGRID_GRIDVIEW_HH
