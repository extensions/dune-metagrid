#ifndef DUNE_PARALLELGRID_INTERSECTIONITERATOR_HH
#define DUNE_PARALLELGRID_INTERSECTIONITERATOR_HH

#include <dune/grid/idgrid/intersection.hh>

namespace Dune {

template< class Grid, class HostIntersectionIterator >
class ParallelGridIntersectionIterator
{
    using This = ParallelGridIntersectionIterator< Grid, HostIntersectionIterator >;

protected:
  using Traits = typename std::remove_const< Grid >::type::Traits;
  static const bool isLeafIntersection = std::is_same<
    HostIntersectionIterator,
    typename Grid::HostGrid::Traits::LeafIntersectionIterator
  >::value;

public:
  using Intersection = typename std::conditional<isLeafIntersection,
    typename Traits :: LeafIntersection, typename Traits :: LevelIntersection
  >::type;

  using IntersectionImpl = typename Intersection :: Implementation;
  using ExtraData = typename Traits :: ExtraData;

public:
  ParallelGridIntersectionIterator () = default;

  // constructor fo a begin iterator
  ParallelGridIntersectionIterator ( ExtraData data, const HostIntersectionIterator& hostIterator, const HostIntersectionIterator& endIterator )
  : hostIterator_( hostIterator )
  , endIterator_( endIterator )
  , data_( data )
  {
    keepIncrementingIfInvalid_();
  }

  // constructor for an end iterator
  ParallelGridIntersectionIterator ( ExtraData data, const HostIntersectionIterator& endIterator )
  : hostIterator_( endIterator )
  , endIterator_( endIterator )
  , data_( data )
  { }

  bool equals ( const This &other ) const
  {
    return (hostIterator_ == other.hostIterator_);
  }

  void increment ()
  {
    ++hostIterator_;
    keepIncrementingIfInvalid_();
  }

  Intersection dereference () const
  {
    return IntersectionImpl( data(), *hostIterator_ );
  }

  ExtraData data() const { return data_; }

protected:
  HostIntersectionIterator hostIterator_;
  HostIntersectionIterator endIterator_;
  ExtraData data_;

private:
  void keepIncrementingIfInvalid_()
  {
    while ( hostIterator_ != endIterator_ )
    {
      const auto& hostIntersection = *hostIterator_;
      const auto& insideElement = hostIntersection.inside();
      const auto& insideInfo = data_->rankManager().partitionInfo( insideElement );

      // if the inside element is interior all intersections are valid -> early return
      if ( insideInfo.partitionType() == InteriorEntity )
        return;

      // if the inside element is ghost only intersections where the outside
      // element is interior are valid, all others are skipped
      if ( insideInfo.partitionType() == GhostEntity && hostIntersection.neighbor() )
      {
        const auto& outsideElement = hostIntersection.outside();
        if ( data_->rankManager().validEntity( outsideElement )
             && data_->rankManager().partitionInfo( outsideElement ).partitionType() == InteriorEntity )
            return;
      }

      // if we get to here the intersection is invalid and needs to be skipped
      ++hostIterator_;
    }
  }
};

} // end namespace Dune

#endif
