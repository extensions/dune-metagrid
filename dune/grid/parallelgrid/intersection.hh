#ifndef DUNE_PARALLELGRID_INTERSECTION_HH
#define DUNE_PARALLELGRID_INTERSECTION_HH

#include <dune/grid/parallelgrid/declaration.hh>
#include <dune/grid/parallelgrid/rankmanager.hh>
#include <dune/grid/idgrid/intersection.hh>

namespace Dune
{

  // ParallelGridIntersection
  // ------------------------

  template< class Grid, class HostIntersection >
  class ParallelGridIntersection : public IdGridIntersection< Grid, HostIntersection >
  {
    typedef IdGridIntersection< Grid, HostIntersection > Base ;

  public:
    typedef typename Base::ExtraData ExtraData;

    using Base :: data ;
    using Base :: hostIntersection ;

    ParallelGridIntersection ()
    : Base ()
    {}

    explicit ParallelGridIntersection ( ExtraData data )
    : Base( data )
    {}

    ParallelGridIntersection ( ExtraData data, const HostIntersection &hostIntersection )
    : Base( data, hostIntersection )
    {}

    bool neighbor () const
    {
      return (hostIntersection().neighbor() && data()->rankManager().validEntity( hostIntersection().outside() ));
    }

    std::size_t boundarySegmentIndex() const
    {
      return data()->boundarySegmentIndices_.at( hostIntersection().boundarySegmentIndex() );
    }

    std::size_t hostBoundarySegmentIndex() const
    {
      return hostIntersection().boundarySegmentIndex();
    }
  };

} // namespace Dune

#endif // #ifndef DUNE_PARALLELGRID_INTERSECTION_HH
