#ifndef DUNE_PARALLELGRID_ITERATOR_HH
#define DUNE_PARALLELGRID_ITERATOR_HH

#include <dune/grid/parallelgrid/rankmanager.hh>

namespace Dune
{

  // ParallelGridIteratorChecked
  // ---------------------------

  template< class Grid, PartitionIteratorType pitype, class HostIterator >
  class ParallelGridIteratorChecked
  {
    using RankManager = ParallelGridRankManager<std::remove_const_t<Grid>>;
    using Traits = typename std::remove_const_t<Grid>::Traits;

    using ExtraData = typename Traits::ExtraData;
    static constexpr int codimension = HostIterator::Entity::codimension;

  public:
    using Entity = typename Traits::template Codim<codimension>::Entity;
    static constexpr int dimension = HostIterator::Entity::dimension;

  private:
    using EntityImpl = typename Traits::template Codim<codimension>::EntityImpl;

    void keepIncrementingIfInvalid_()
    {
      for( ; hostIterator_ != endIterator_; ++hostIterator_ )
      {
        const auto& hostEntity = *hostIterator_;

        if( !rankManager_().validEntity( hostEntity ) )
          continue;

        // determine interior and ghost
        const auto& info = rankManager_().partitionInfo( hostEntity );
        switch( pitype )
        {
        case All_Partition:
          return;

        case Interior_Partition:
          if( info.partitionType() == InteriorEntity )
            return;
          break;

        case InteriorBorder_Partition:
        case Overlap_Partition:
        case OverlapFront_Partition:
          if( info.partitionType() != GhostEntity )
            return;
          break;

        case Ghost_Partition:
          if( info.partitionType() == GhostEntity )
            return;
          break;

        default:
          DUNE_THROW( InvalidStateException, "wrong PartitionIteratorType: " << pitype << "." );
        }
      }
    }

  public:
    // constructor for begin iterator
    ParallelGridIteratorChecked ( ExtraData data, const HostIterator &hostIterator, const HostIterator &endIterator )
    : data_(data)
    , hostIterator_(hostIterator)
    , endIterator_(endIterator)
    {
      keepIncrementingIfInvalid_();
    }

    // constructor for end iterator
    ParallelGridIteratorChecked ( ExtraData data, const HostIterator &endIterator )
    : data_(data)
    , hostIterator_(endIterator)
    , endIterator_(endIterator)
    {}

    void increment ()
    {
      ++hostIterator_;
      keepIncrementingIfInvalid_();
    }

    /** \brief check for equality */
    bool equals ( const ParallelGridIteratorChecked& other ) const
    {
      return (hostIterator_ == other.hostIterator_);
    }

    /** \brief dereference entity */
    Entity dereference () const
    {
      return Entity( EntityImpl( data_, *hostIterator_ ) );
    }

    /** \brief obtain level */
    int level () const { return hostIterator_->level(); }

  private:
    const RankManager& rankManager_() const
    { return data_->rankManager(); }

    ExtraData data_;
    HostIterator hostIterator_;
    HostIterator endIterator_;
  };

} // namespace Dune

#endif
