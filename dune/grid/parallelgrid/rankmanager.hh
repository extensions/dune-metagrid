#ifndef DUNE_PARALLELGRID_RANKMANAGER_HH
#define DUNE_PARALLELGRID_RANKMANAGER_HH

//- system includes
#include <list>
#include <map>
#include <string>
#include <vector>
#include <memory>

//- dune-common includes
#include <dune/common/version.hh>
#include <dune/common/parallel/mpihelper.hh>

//- dune-geometry includes
#include <dune/geometry/referenceelements.hh>

//- dune-alugrid includes
#if  HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>

//- dune-metagrid includes
#include <dune/grid/parallelgrid/macrodecomposition.hh>
#include <dune/grid/parallelgrid/partitioner.hh>
#include <dune/grid/parallelgrid/sync.hh>

namespace Dune
{

  // PartitionInfo
  // -------------

  class PartitionInfo
  {
    typedef PartitionInfo This;

  public:
    typedef std::set< int > Connectivity;
    typedef Connectivity::const_iterator iterator;

    explicit PartitionInfo ( PartitionType partitionType )
    : partitionType_( partitionType )
    {}

    //! set connectivity to given oitherRank and possibly change PartitionType
    void setConnectivity ( const int otherRank, const PartitionType type )
    {
      connectivity_.insert( otherRank );
      setPartitionType( type );
    }

    //! set PartitionType of entity
    void setPartitionType ( const PartitionType type )
    {
      // don't overwrite Border Entity
      if( partitionType_ != BorderEntity )
        partitionType_ = type;
    }

    //! returns the Dune PpartitionType for this entity
    PartitionType partitionType () const { return partitionType_; }

    //! returns true if the entity has connections to other partitions
    bool hasConnectivity () const { return !connectivity_.empty(); }

    //! returns true if the element is interior and has connections to other partitions
    bool isInteriorAtBorder () const { return (partitionType() == InteriorEntity) && hasConnectivity (); }

    //! return begin iterator for connectivities
    iterator begin () const { return connectivity_.begin(); }
    //! return end iterator for connectivities
    iterator end ()   const { return connectivity_.end();  }

    double memUsage () const
    {
      double sum = sizeof( PartitionInfo );
      sum += sizeof( Connectivity ) + 8*connectivity_.size();
      return sum;
    }

  protected:
    Connectivity connectivity_;
    PartitionType partitionType_;
  };



  // ParallelGridRankManager
  // -----------------------

  template< class Grid >
  class ParallelGridRankManager
  {
    typedef ParallelGridRankManager< Grid > This;

  public:
    static const int dimension = Grid::dimension;

    typedef typename Grid::ctype ctype;
    typedef typename Grid::HostGrid HostGrid;

    typedef typename HostGrid::LocalIdSet LocalIdSetType;
    typedef typename LocalIdSetType::IdType IdType;

    typedef std::map< IdType, int > RankMapType;

  public:
    typedef PersistentContainer< HostGrid, std::shared_ptr< PartitionInfo > > PartitionInfoStorage;

    typedef ALU3DSPACE ObjectStream ObjectStream;

    typedef MessageBufferIF< ObjectStream > MessageBufferObjectStream;

    //type of communicator
    typedef ALU3DSPACE ALUMpAccess MpAccess;

    typedef typename Grid::template Codim< 0 >::Entity Element;

    typedef typename HostGrid::LevelGridView HostMacroGridView;
    typedef typename HostGrid::LeafGridView HostLeafGridView;
    typedef typename HostGrid::template Codim< 0 >::Entity HostElement;

    typedef typename HostGrid::LevelGridView AllMacroGridView;

  protected:
    typedef ParallelGridPartitioner< Grid > Partitioner;
    typedef ParallelGridMacroDecomposition< Grid > MacroDecomposition;
    typedef std::array< PartitionInfoStorage *, dimension+1 > PartitionInfoStorageArray;

    RankMapType rankMap_;

    Grid &grid_;
    const LocalIdSetType &idSet_;

    MpAccess mpAccess_;

    HostMacroGridView hostMacroView_;
    HostLeafGridView hostLeafView_;

    int myRank_;

    Partitioner partitioner_;
    MacroDecomposition macroDecomposition_;

    std::shared_ptr< PartitionInfo > interiorPartitionInfo_;
    PartitionInfoStorageArray partitionInfoStorage_;

    template < int codim >
    struct CreatePartitionInfo
    {
      static void apply ( const HostGrid &grid, PartitionInfoStorageArray &partitionInfoStorage )
      {
        const bool codimAvailable = Capabilities::hasEntity< HostGrid, codim >::v;
        partitionInfoStorage[ codim ] = (codimAvailable ? new PartitionInfoStorage( grid, codim ) : nullptr);
      }
    };

  public:
    explicit ParallelGridRankManager ( Grid &grid );

    const HostGrid &hostGrid () const { return grid_.hostGrid(); }
    HostGrid &hostGrid () { return grid_.hostGrid(); }

    bool codimAvailable ( const int codim ) const
    {
      assert( (codim >= 0) && (codim <= dimension) );
      return bool( partitionInfoStorage_[ codim ] );
    }

    template< int codim >
    bool codimAvailable () const
    {
      static_assert( (codim >= 0) && (codim <= dimension), "Invalid codimension" );
      const bool result = Capabilities::hasEntity< HostGrid, codim >::v;
      assert( result == codimAvailable( codim ) );
      return result;
    }

    PartitionInfoStorage &partitionInfoStorage ( const int codim ) const
    {
      assert( codimAvailable( codim ) );
      return *(partitionInfoStorage_[ codim ]);
    }

    template< class Entity >
    bool validEntity ( const Entity &entity ) const
    {
      return bool( partitionInfoStorage( Entity::codimension )[ entity ] );
    }

    bool validEntity ( const HostElement &element, int subEntity, unsigned int codim ) const
    {
      return bool( partitionInfoStorage( codim )( element, subEntity ) );
    }

    template< class Entity >
    PartitionInfo &partitionInfo ( const Entity &entity ) const
    {
      assert( validEntity( entity ) );
      return *partitionInfoStorage( Entity::codimension )[ entity ];
    }

    PartitionInfo &partitionInfo ( const HostElement &element, int subEntity, unsigned int codim ) const
    {
      return *partitionInfoStorage( codim )( element, subEntity );
    }

    const MpAccess &mpAccess () const { return mpAccess_; }
    MpAccess &mpAccess () { return mpAccess_; }

    //! return partition type of entity
    template< class Entity >
    PartitionType partitionType ( const Entity &entity ) const
    {
      return partitionType< Entity::codimension >( entity );
    }

    //! return partition type of entity
    template< int codim >
    PartitionType partitionType ( const typename Grid::template Codim< codim >::Entity &entity ) const
    {
      return partitionType( Grid::getHostEntity< codim >( entity ) );
    }

    //! return partition type of entity
    template< int codim >
    PartitionType partitionType ( const typename HostGrid::template Codim< codim >::Entity &entity ) const
    {
      return partitionInfo( entity ).partitionType();
    }

    void backup ( const std::string &filename ) const
    {
      macroDecomposition_.backup( filename + ".rm" );
    }

    void backup ( std::ostream& out ) const
    {
      const int rank = myRank_;
      out << rank << " ";
      macroDecomposition_.backup( out );
    }

    void restore ( const std::string &filename )
    {
      macroDecomposition_.restore( filename + ".rm" );
      update();
    }

    void restore ( std::istream& in )
    {
      int rank ;
      in >> rank ;
      myRank_ = rank ;
      macroDecomposition_.restore( in );
      update();
    }

  private:
    void printMsg ( const std::string &msg, std::ostream &out = std::cout ) const
    {
      out << "P["<< myRank_ << "]: " << msg << std::endl;
    }

    void printMemUsage() const
    {
      double sum = 0;
      for( int codim = 0; codim <= dimension ; ++codim )
      {
        if( codimAvailable( codim ) )
        {
          typedef typename PartitionInfoStorage::Iterator Iterator;
          Iterator end = partitionInfoStorage( codim ).end();
          for( Iterator it = partitionInfoStorage( codim ).begin(); it != end; ++it )
          {
            if( *it )
              sum += (*it)->memUsage();
          }
        }
      }

      std::cout << "P[ " << myRank_ << " ] ParallelGridRankManager::memusage: " <<
        double(sum/1024./1024.) << std::endl;
    }

    // print given map to std::cout
    void printMap(const RankMapType& m, std::string mapName ) const
    {
      typedef typename RankMapType :: const_iterator iterator;
      iterator end = m.end();
      std::cout << "P[ " << myRank_ << " ]: print " << mapName << " map (" << m.size() << ") \n";
      for(iterator it = m.begin(); it != end; ++it)
      {
        std::cout << "m[" << (*it).first << "] = "<< (*it).second << "\n";
      }
    }

    void clearMaps ()
    {
      rankMap_.clear();
      for( int codim = 0; codim <= dimension; ++codim )
      {
        if( codimAvailable( codim ) )
        {
          partitionInfoStorage( codim ).resize();
          partitionInfoStorage( codim ).shrinkToFit();
          partitionInfoStorage( codim ).fill( typename PartitionInfoStorage::Value() );
        }
      }
    }

    template< int codim >
    struct Gather
    {
      template< class Stream , class DataHandle>
      static void apply( const HostElement& helement, Stream& stream, DataHandle& dataHandle )
      {
        if(  dataHandle.contains( dimension, codim ) )
         {
            const int numSubEntities = helement.subEntities(codim);
            for( int i = 0 ; i < numSubEntities ; ++i )
            {
              const auto& pEntity = helement.template subEntity< codim >( i );
              dataHandle.gather( stream , pEntity );
            }
         }
      }
    };

    template< int codim >
    struct Scatter
    {
      template< class Stream ,class DataHandle>
      static void apply( const HostElement& helement, Stream& stream, DataHandle& dataHandle,size_t size )
      {
        if(  dataHandle.contains( dimension, codim ) )
         {
            const int numSubEntities = helement.subEntities(codim);
            for( int i = 0 ; i < numSubEntities ; ++i )
            {
              const auto& pEntity = helement.template subEntity< codim >( i );
              dataHandle.scatter( stream , pEntity, size);
            }
         }
      }
    };

  public:
    std::set< int > update ( const MacroDecomposition& macroDecomposition );

    void update ();

    bool loadBalance ()
    {
      const MacroDecomposition *macroDecomposition = partitioner_.repartition( macroDecomposition_ );
      if( !macroDecomposition )
        return false;

      macroDecomposition_ = *macroDecomposition;
      delete macroDecomposition;

      update();

      debugBarrier();
      return true;
    }

    template< class DataHandle >
    bool loadBalance ( DataHandle &dataHandle )
    {
      const MacroDecomposition *macroDecomposition = partitioner_.repartition( macroDecomposition_ );
      if( !macroDecomposition )
        return false;

      migrateUserData( *macroDecomposition, dataHandle );
      macroDecomposition_ = *macroDecomposition;
      delete macroDecomposition;

      debugBarrier();
      return true;
    }

  private:
    void debugBarrier () const
    {
#ifndef NDEBUG
      mpAccess_.barrier();
#endif
    }

    void setPartitionType ( std::shared_ptr< PartitionInfo > &pInfo, PartitionType ptype );
    void setConnectivity ( std::shared_ptr< PartitionInfo > &pInfo, int otherRank, PartitionType ptype );

    void insertAllSubEntities ( const HostElement &hostElement, PartitionType ptype, int rank );
    void insertConnectivity ( const HostElement &hostElement, const PartitionType ptype,
                              const int face, const int otherRank );

    void markInterior ( const HostElement &hostElement );
    void hierarchicMarkInterior ( const HostElement &hostElement );

    void setGhosts ( const HostElement &hostElement, const int rank, std::set< int > &links );

    template< class HostGridView >
    bool checkNeighbors ( const int ghostRank, const HostGridView &hostGridView, const HostElement &hostElement );

    template< class DataHandle >
    void migrateUserData ( const MacroDecomposition &macroDecomposition, DataHandle &dataHandle );

    std::size_t treeSize ( const HostElement &hostElement ) const;
  };



  // Implementation of ParallelGridRankManager
  // -----------------------------------------

  template< class Grid >
  inline ParallelGridRankManager< Grid >::ParallelGridRankManager ( Grid &grid )
  : rankMap_(),
    grid_( grid ),
    idSet_( hostGrid().localIdSet() ),
    mpAccess_( MPIHelper::getCommunicator() ),
    hostMacroView_( hostGrid().levelGridView( 0 ) ),
    hostLeafView_( hostGrid().leafGridView() ),
    myRank_( mpAccess_.myrank() ),
    partitioner_( grid, mpAccess_ ),
    macroDecomposition_( hostGrid() ),
    interiorPartitionInfo_( new PartitionInfo( InteriorEntity ) )
  {
    // create partition infos
    Hybrid::forEach(std::make_index_sequence<dimension+1>{}, [&](auto i){
      CreatePartitionInfo<i>::apply(
        hostGrid(), partitionInfoStorage_
      );
    });

    update();
  }


  template< class Grid >
  inline std::set< int > ParallelGridRankManager< Grid >::update ( const MacroDecomposition& macroDecomposition )
  {
    typedef typename HostMacroGridView::template Codim< 0 >::Iterator HostIterator;

    clearMaps();

    // setup all interior elements
    const HostIterator end = hostMacroView_.template end< 0 >();
    for( HostIterator it = hostMacroView_.template begin< 0 >(); it != end; ++it )
    {
      const HostElement &hostElement = *it;

      const int rank = macroDecomposition.rank( hostElement );
      assert( rank >= 0 );

      if( rank == myRank_ )
        hierarchicMarkInterior( hostElement );
    }

    // setup all ghost elements
    std::set< int > links;
    for( HostIterator it = hostMacroView_.template begin< 0 >(); it != end; ++it )
    {
      const HostElement &hostElement = *it;

      const int rank = macroDecomposition.rank( hostElement );
      assert( rank >= 0 );

      if( rank != myRank_ )
        setGhosts( hostElement, rank, links );
    }
    return links;
  }

  template< class Grid >
  inline void ParallelGridRankManager< Grid >::update ()
  {
    std::set< int > links=update( macroDecomposition_);
    // create new linkage
    mpAccess_.removeLinkage();
    mpAccess_.insertRequestSymetric( links );
  }
  template< class Grid >
  inline void ParallelGridRankManager< Grid >
    ::setPartitionType ( std::shared_ptr< PartitionInfo > &pInfo, PartitionType ptype )
  {
    if( pInfo && (pInfo != interiorPartitionInfo_) )
      pInfo->setPartitionType( ptype );
    else if( ptype == InteriorEntity )
      pInfo = interiorPartitionInfo_;
    else
      pInfo = std::shared_ptr< PartitionInfo >( new PartitionInfo( ptype ) );
  }


  template< class Grid >
  inline void ParallelGridRankManager< Grid >
    ::setConnectivity ( std::shared_ptr< PartitionInfo > &pInfo, int otherRank, PartitionType ptype )
  {
    assert( pInfo );
    if( pInfo == interiorPartitionInfo_ )
      pInfo = std::shared_ptr< PartitionInfo >( new PartitionInfo( InteriorEntity ) );
    pInfo->setConnectivity( otherRank, ptype );
  }


  template< class Grid >
  inline void ParallelGridRankManager< Grid >
    ::insertAllSubEntities ( const HostElement &hostElement, PartitionType ptype, int rank )
  {
    const auto refElement = referenceElement(hostElement);

    assert( rank == myRank_ );

    // insert element
    setPartitionType( partitionInfoStorage( 0 )[ hostElement ], ptype );

    // insert subentities
    for( int codim = 1; codim <= dimension; ++codim )
    {
      if( codimAvailable( codim ) )
      {
        const int numSubEntities = refElement.size( codim );
        for( int subEntity = 0; subEntity < numSubEntities; ++subEntity )
          setPartitionType( partitionInfoStorage( codim )( hostElement, subEntity ), ptype );
      }
    }
  }


  template< class Grid >
  inline void ParallelGridRankManager< Grid >
    ::insertConnectivity ( const HostElement &hostElement, const PartitionType ptype,
                           const int face, const int otherRank )
  {
    // now set connectivity for ghost element
    assert( otherRank != myRank_ );
    setConnectivity( partitionInfoStorage( 0 )[ hostElement ], otherRank, ptype );

    // now set border face and sub entities to border
    if( codimAvailable< 1 >() )
      setConnectivity( partitionInfoStorage( 1 )( hostElement, face ), otherRank, BorderEntity );

    const auto refElement = referenceElement(hostElement);
    for( int codim = 2; codim <= dimension; ++codim )
    {
      if( codimAvailable( codim ) )
      {
        const int size = refElement.size( face, 1, codim );
        for( int k = 0; k < size; ++k )
        {
          const int subEntity = refElement.subEntity( face, 1, k, codim );
          setConnectivity( partitionInfoStorage( codim )( hostElement, subEntity ), otherRank, BorderEntity );
        }
      }
    }
  }


  template< class Grid >
  inline void ParallelGridRankManager< Grid >::markInterior ( const HostElement &hostElement )
  {
    // insert rank into map
    rankMap_[ idSet_.id( hostElement ) ] = myRank_;

    // insert all sub entities to be interior elements
    insertAllSubEntities( hostElement, InteriorEntity, myRank_ );
  }


  template< class Grid >
  inline void ParallelGridRankManager< Grid >::hierarchicMarkInterior ( const HostElement &hostElement )
  {
    typedef typename HostElement::HierarchicIterator HierarchicIterator;

    markInterior( hostElement );

    const int maxLevel = hostGrid().maxLevel();
    const HierarchicIterator hend = hostElement.hend( maxLevel );
    for( HierarchicIterator hit = hostElement.hbegin( maxLevel ); hit != hend; ++hit )
      markInterior( *hit );
  }


  template< class Grid >
  inline void ParallelGridRankManager< Grid >
    ::setGhosts ( const HostElement &hostElement, const int rank, std::set< int > &links )
  {
    typedef typename HostElement::HierarchicIterator HierarchicIterator;

    assert( (rank >= 0) && (rank != myRank_) );

    const int level = hostElement.level();

    // check neighbors for interior elements
    bool hasInteriorNeighbor = checkNeighbors( rank, hostGrid().levelGridView( level ), hostElement );
    if( hostElement.isLeaf() )
      hasInteriorNeighbor = checkNeighbors( rank, hostLeafView_, hostElement ) || hasInteriorNeighbor;

    // if we have connection to interior elements, we have found a ghost element
    if( hasInteriorNeighbor )
    {
      // create linkage map
      links.insert( rank );

      // set new rank
      rankMap_[ idSet_.id( hostElement ) ] = rank;

      if( !hostElement.isLeaf() )
      {
        const HierarchicIterator hend = hostElement.hend( level+1 );
        for( HierarchicIterator hit = hostElement.hbegin( level+1 ); hit != hend; ++hit )
          setGhosts( *hit, rank, links );
      }
    }
  }


  template< class Grid >
  template< class HostGridView >
  inline bool ParallelGridRankManager< Grid >
    ::checkNeighbors ( const int ghostRank, const HostGridView &hostGridView, const HostElement &hostElement )
  {
    bool hasInteriorNeighbor = false;

    // if we find at least on neighbor that
    // is an interior entity, we have a ghost
    for ( const auto& hostIntersection : intersections(hostGridView, hostElement) )
    {
      if( !hostIntersection.neighbor() )
        continue;

      const HostElement &outside = hostIntersection.outside();

      // find rank of neighbor
      typename RankMapType::const_iterator rankIt = rankMap_.find( idSet_.id( outside ) );

      // if element is not found in map its definitely
      // not an interior element
      if( (rankIt == rankMap_.end()) || ((*rankIt).second != myRank_) )
        continue;

      const int indexInInside  = hostIntersection.indexInInside();
      const int indexInOutside = hostIntersection.indexInOutside();

      // then store destination rank for this interior element
      // needed during communication

      /// note: hostElement is the ghost
      insertAllSubEntities( hostElement, GhostEntity, myRank_ );
      insertConnectivity( hostElement, GhostEntity, indexInInside, ghostRank );

      // insert connectivity for interior element
      insertConnectivity( outside, InteriorEntity, indexInOutside, ghostRank );

      hasInteriorNeighbor = true;
    }
    return hasInteriorNeighbor;
  }


  template< class Grid >
  inline std::size_t ParallelGridRankManager< Grid >::treeSize ( const HostElement &hostElement ) const
  {
    typedef typename HostGrid::HierarchicIterator HierarchicIterator;

    // we have at least the element itself
    std::size_t count = 1;

    const int maxLevel = hostGrid().maxLevel();
    HierarchicIterator hend  = hostElement.hend( maxLevel );
    for( HierarchicIterator hit = hostElement.hbegin( maxLevel ); hit != hend; ++hit )
      ++count;
    return count;
  }


  template< class Grid >
  template< class DataHandle >
  inline void ParallelGridRankManager< Grid >
    ::migrateUserData ( const MacroDecomposition &macroDecomposition, DataHandle &dataHandle )
  {
#if ALU3DGRID_PARALLEL
    typedef typename HostMacroGridView::template Codim< 0 >::Iterator HostIterator;
    const int maxLevel=grid_.maxLevel();
    // setup linkage for migration of user data
    std::set< int > links;
    const HostIterator end = hostMacroView_.template end< 0 >();
    for( HostIterator it = hostMacroView_.template begin< 0 >(); it != end; ++it )
    {
      const HostElement &hostElement = *it;

      const int newRank = macroDecomposition.rank( hostElement );
      const int oldRank = macroDecomposition_.rank( hostElement );
      if( (oldRank == myRank_) && (newRank != myRank_) )
        links.insert( newRank );
      if( (oldRank != myRank_) && (newRank == myRank_) )
        links.insert( oldRank );
    }
    mpAccess_.removeLinkage();
    mpAccess_.insertRequestSymetric( links );

    // message buffers
    const int nlinks = mpAccess_.nlinks();
    std::vector< ObjectStream > buffer( nlinks );

    for( int link = 0; link < nlinks; ++link )
      buffer[ link ].clear();

    ObjectStreamSync< ObjectStream > sync;

    // pack data
    for( HostIterator it = hostMacroView_.template begin< 0 >(); it != end; ++it )
    {
      const HostElement &hostElement = *it;

      const int newRank = macroDecomposition.rank( hostElement );
      const int oldRank = macroDecomposition_.rank( hostElement );
      if( (oldRank != myRank_) || (newRank == myRank_) )
        continue;

      const int link = mpAccess_.link( newRank );

      sync.write( buffer[ link ] );
      buffer[ link ].write( macroDecomposition.index( hostElement ) );
      sync.write( buffer[ link ], treeSize( hostElement ) );

      MessageBufferObjectStream mboStream( buffer[link]);

      Hybrid::forEach(std::make_index_sequence<dimension+1>{}, [&](auto i){
        Gather<i>::apply(
          hostElement, mboStream, dataHandle
        );
      });

      typedef typename HostElement::HierarchicIterator HierarchicIterator;

      const HierarchicIterator hend=hostElement.hend(maxLevel);

      for( HierarchicIterator hit = hostElement.hbegin(maxLevel); hit!=hend ; ++hit )
      {
        Hybrid::forEach(std::make_index_sequence<dimension+1>{}, [&](auto i){
          Gather<i>::apply(
            *hit, mboStream, dataHandle
          );
        });
      }
    }
    // write end marker to all streams
    for( int link = 0; link < nlinks; ++link )
    {
      sync.write( buffer[ link ] );
      buffer[ link ].write( std::numeric_limits< std::size_t >::max() );
    }

    // exchange data
    buffer = mpAccess_.exchange( buffer );
    links = update( macroDecomposition );

    // read next macro index for each link
    std::vector< std::size_t > nextMacroIndex( nlinks );
    for( int link = 0; link < nlinks; ++link )
    {
      sync.ensure( buffer[ link ] );
      buffer[ link ].read( nextMacroIndex[ link ] );
    }

    // unpack data
    for( HostIterator it = hostMacroView_.template begin< 0 >(); it != end; ++it )
    {
      const HostElement &hostElement = *it;

      const int newRank = macroDecomposition.rank( hostElement );
      const int oldRank = macroDecomposition_.rank( hostElement );
      if( (oldRank == myRank_) || (newRank != myRank_) )
        continue;

      const int link = mpAccess_.link( oldRank );
      if( nextMacroIndex[ link ] != macroDecomposition.index( hostElement ) )
        continue;

      const std::size_t size = treeSize( hostElement );
      dataHandle.reserveMemory( size );

      sync.ensure( buffer[ link ], size );
      MessageBufferObjectStream mboStream( buffer[link]);

      Hybrid::forEach(std::make_index_sequence<dimension+1>{}, [&](auto i){
        Scatter<i>::apply(
          hostElement, mboStream, dataHandle, size
        );
      });

      typedef typename HostElement::HierarchicIterator HierarchicIterator;

      const HierarchicIterator hend=hostElement.hend(maxLevel);

      for( HierarchicIterator hit = hostElement.hbegin(maxLevel); hit!=hend ; ++hit )
      {
        Hybrid::forEach(std::make_index_sequence<dimension+1>{}, [&](auto i){
          Scatter<i>::apply(
            *hit, mboStream, dataHandle, size
          );
        });
      }

      sync.ensure( buffer[ link ] );
      buffer[ link ].read( nextMacroIndex[ link ] );
    }

    dataHandle.compress( );


  //update linkage
    mpAccess_.removeLinkage();
    mpAccess_.insertRequestSymetric( links );
#endif // #if ALU3DGRID_PARALLEL
  }

} // namespace Dune

#endif // #if  HAVE_DUNE_ALUGRID

#endif // #ifndef DUNE_PARALLELGRID_RANKMANAGER_HH
