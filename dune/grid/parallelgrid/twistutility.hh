#ifndef DUNE_PARALLELGRID_TWISTUTILITY_HH
#define DUNE_PARALLELGRID_TWISTUTILITY_HH

#include <dune/geometry/type.hh>

#include <dune/grid/parallelgrid/declaration.hh>

#if HAVE_DUNE_FEM
#include <dune/fem/quadrature/caching/twistutility.hh>
#include <dune/fem/misc/capabilities.hh>

namespace Dune
{
  namespace Fem
  {

    // Specialization for ParallelGrid
    // -------------------------------

    /** \brief Specialization of TwistUtility for ParallelGrid
    */
    template< class HostGrid >
    struct TwistUtility< ParallelGrid< HostGrid > >
    {
      typedef ParallelGrid< HostGrid > GridType;
      typedef typename GridType::Traits::LeafIntersectionIterator  LeafIntersectionIterator;
      typedef typename LeafIntersectionIterator::Intersection  LeafIntersection;
      typedef typename GridType::Traits::LevelIntersectionIterator LevelIntersectionIterator;
      typedef typename LevelIntersectionIterator::Intersection LevelIntersection;

      static const int dimension = GridType::dimension;

      typedef TwistUtility < HostGrid > HostTwistUtility ;

    public:
      //! \brief return twist for inner face
      template <class Intersection>
      static int twistInSelf ( const GridType &grid, const Intersection &it )
      {
        return HostTwistUtility::twistInSelf( grid.hostGrid(),
            GridType::getRealImplementation( it ).hostIntersection() );
      }

      //! \brief return twist for outer face
      template <class Intersection>
      static int twistInNeighbor ( const GridType &grid, const Intersection &it )
      {
        return HostTwistUtility::twistInNeighbor( grid.hostGrid(),
            GridType::getRealImplementation( it ).hostIntersection() );
      }

      /** \brief return element geometry type of inside or outside entity
      */
      template< class Intersection >
      static GeometryType
      elementGeometry ( const Intersection &intersection, bool inside )
      {
        return HostTwistUtility
          ::elementGeometry( GridType::getRealImplementation( intersection ).hostIntersection(), inside );
      }
    };

  }  // end namespace Fem

} // namespace Dune
#endif

#endif // #ifndef DUNE_PARALLELGRID_TWISTUTILITY_HH
