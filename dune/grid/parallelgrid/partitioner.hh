#ifndef DUNE_PARALLELGRID_PARTITIONER_HH
#define DUNE_PARALLELGRID_PARTITIONER_HH

//- system includes
#include <string>
#include <list>
#include <map>
#include <vector>

//- dune-common includes
#include <dune/common/parallel/mpihelper.hh>

//- dune-grid includes
#include <dune/grid/common/partitionset.hh>

#if HAVE_DUNE_ALUGRID
//- dune-alugrid includes
#include <dune/alugrid/grid.hh>
#include <dune/alugrid/impl/serial/serialize.h>

//- dune-metagrid includes
#include <dune/grid/idgrid/indexset.hh>
#include <dune/grid/parallelgrid/macrodecomposition.hh>
#include <dune/grid/parallelgrid/sfciterator.hh>
#include <dune/grid/parallelgrid/mpaccess.hh>


#if ! HAVE_DUNE_FEM_DG || not defined ITERATORS_WITHOUT_MYALLOC
#define GRAPHVERTEX_WITH_CENTER
#endif


namespace Dune
{

  // ALUGridPartitioner
  // ------------------

#if HAVE_MPI
  template< class GV, PartitionIteratorType pitype, class IS = typename GV::IndexSet >
  class ALUGridPartitioner
  {
    typedef ALUGridPartitioner< GV, pitype, IS > This;

  public:
    typedef GV GridView;
    typedef typename GridView::Grid Grid;

    typedef IS IndexSet;

  protected:
    typedef typename IndexSet::IndexType IndexType;

    static const int dimension = Grid::dimension;

    typedef typename Grid::template Codim< 0 >::Entity Element;

    typedef ALU3DSPACE LoadBalancer LoadBalancerType;
    typedef typename LoadBalancerType::DataBase DataBase;

    // type of communicator
    typedef ALU3DSPACE MpAccessMPI MpAccess;

  public:
    ALUGridPartitioner ( const GridView &gridView, const int pSize, const IndexSet &indexSet )
    : mpAccess_( MPIHelper::getCommunicator() ),
      db_(),
      gridView_( gridView ),
      indexSet_( indexSet ),
      ldbOver_( 1.2 ),
      ldbUnder_( 0.0 ),
      pSize_( pSize ),
      index_( indexSet_.size( 0 ), -1 )
    {
      calculateGraph();
    }

    void calculateGraph ()
    {
      for (const auto& element : elements(gridView(), partitionSet<pitype>()))
        ldbUpdateVertex( element, db_ );
    }

    int weight ( const Element &element ) const
    {
      // set weight to 1 for the element itself
      int weight = 1;

      // for every child, increase weight by 1
      const int maxLevel = gridView().grid().maxLevel();
      for ([[maybe_unused]] const auto& child : descendantElements(element, maxLevel))
        ++weight;

      return weight;
    }

    void ldbUpdateVertex ( const Element &element, DataBase &db )
    {
      const int index = indexSet_.index( element );
      const int wght = weight( element );

      typename LoadBalancerType::GraphVertex graphVx( index, wght );

      assert( graphVx.isValid() );
      db.vertexUpdate( graphVx );

      // set weight for faces (to be revised)
      updateFaces( element, index, wght, db );
    }

    void updateFaces ( const Element &element, const int elementId, const int weight, DataBase &db )
    {
      for(const auto& intersection : intersections(gridView(), element))
      {
        if( !intersection.neighbor() )
          continue;

        const auto& outside = intersection.outside();

        if( outside.partitionType() == InteriorEntity )
        {
          const int outsideId = indexSet_.index( outside );
          db.edgeUpdate( typename LoadBalancerType::GraphEdge( elementId, outsideId, weight, 0, 0 ) );
        }
      }
    }

  public:
    bool repartition ()
    {
      return db_.repartition( mpAccess_, DataBase::ALUGRID_SpaceFillingCurve );
    }

    bool serialPartition()
    {
#if HAVE_DUNE_ALUGRID
      if( pSize_ > 1 )
      {
        partition_ = db_.repartition( mpAccess_, DataBase::ALUGRID_SpaceFillingCurve, pSize_ );
        assert( partition_.size() > 0 );
        return (partition_.size() > 0);
      }
      else
      {
        partition_.resize( indexSet_.size( 0 ) );
        std::fill( partition_.begin(), partition_.end(), 0 );
        return false;
      }
#else
      DUNE_THROW( NotImplemented, "HAVE_DUNE_FEM_DG && defined ITERATORS_WITHOUT_MYALLOC needed." );
#endif
    }

    bool checkPartition () const
    {
      // Kriterium, wann eine Lastneuverteilung vorzunehmen ist:
      //
      // load  - eigene ElementLast
      // mean  - mittlere ElementLast
      // nload - Lastverh"altnis

      const int np = mpAccess_.psize();
      double load = db_.accVertexLoad();
      std::vector< double > v( mpAccess_.gcollect( load ) );
      double mean = std::accumulate( v.begin(), v.end(), 0.0 ) / double( np );

      bool unbalanced = false;
      for( const double load : v )
        unbalanced |= (load > mean ? (load > ldbOver_*mean) : (load < ldbUnder_*mean));
#ifndef NDEBUG
      // make sure all processes have come to the same conclusion
      int local = int( unbalanced );
      int global = mpAccess_.gmax( local );
      assert( local == global );
#endif // #ifndef NDEBUG

      return unbalanced;
    }

    template< class Entity >
    int getDestination ( const Entity &entity ) const
    {
      assert( Entity::codimension == 0 );
      return getDestination( (int)indexSet_.index( entity ) );
    }

    int getDestination ( int index ) const
    {
      return db_.destination( index );
    }

    std::set< int, std::less< int > > scan () const
    {
      return db_.scan();
    }

    int getRank ( const Element &element ) const
    {
      const std::size_t index = indexSet_.index( element );
      assert( index < partition_.size() );
      return partition_[ index ];
    }

    bool validEntity ( const Element &element, const int rank ) const
    {
      return (getRank( element ) == rank);
    }

    const GridView &gridView () const { return gridView_; }

  protected:
    mutable MpAccess mpAccess_;

    mutable DataBase db_;

    GridView gridView_;
    const IndexSet &indexSet_;

    // load balancer bounds
    const double ldbOver_ ;
    const double ldbUnder_;
    const int pSize_ ;

    std::vector< int > index_;

    std::vector< int > partition_;
  };
#endif // #if HAVE_MPI



  // SimplePartitioner
  // -----------------

  template< class GV, PartitionIteratorType pitype, class IS = typename GV::IndexSet >
  class SimplePartitioner
  {
    typedef SimplePartitioner< GV, pitype, IS > This;

  public:
    typedef GV GridView;
    typedef typename GridView::Grid Grid;

    typedef IS IndexSet;

  protected:
    typedef typename IndexSet::IndexType IndexType;

    static const int dimension = Grid::dimension;

    typedef typename Grid::template Codim< 0 >::Entity Element;

    // type of communicator
    typedef Dune :: Communication< typename MPIHelper :: MPICommunicator >
      Communication ;

  public:
    SimplePartitioner ( const GridView &gridView, const int pSize, const IndexSet &indexSet )
    : comm_( MPIHelper :: getCommunicator() ),
      gridView_( gridView ),
      indexSet_( indexSet )
    {
      // per default every entity is on rank 0
      partition_.resize( indexSet_.size( 0 ) );
      std::fill( partition_.begin(), partition_.end(), 0 );
    }

  public:
    bool checkPartition () const
    {
      return true ;
    }

    bool repartition ()
    {
      calculatePartition();
      return true ;
    }

    template< class Entity >
    int getDestination ( const Entity &entity ) const
    {
      assert( Entity::codimension == 0 );
      return getDestination( (int)indexSet_.index( entity ) );
    }

    int getDestination ( int index ) const
    {
      return partition_[ index ];
    }

    const GridView &gridView () const { return gridView_; }

  protected:
    void calculatePartition()
    {
      const size_t partSize = indexSet_.size( 0 );

      // get number of MPI processes
      const int procSize = comm_.size();

      // get minimal number of entities per process
      const size_t minPerProc = (double(partSize) / double( procSize ));
      size_t maxPerProc = minPerProc ;
      if( partSize % procSize != 0 )
        ++ maxPerProc ;

      // calculate percentage of elements with larger number
      // of elements per process
      double percentage = (double(partSize) / double( procSize ));
      percentage -= minPerProc ;
      percentage *= procSize ;

      /*
      if( comm_.rank() == 0 )
      {
        std::cout << "PartSize = " << partSize << std::endl;
        std::cout << "Percentage = " << percentage << std::endl;
        std::cout << "min-max " << minPerProc << " " << maxPerProc << std::endl;
      }
      */

      // per default every entity is on rank 0
      partition_.resize( indexSet_.size( 0 ) );
      std::fill( partition_.begin(), partition_.end(), 0 );

      int rank = 0;
      size_t elementCount  = maxPerProc ;
      size_t elementNumber = 0;
      size_t localElementNumber = 0;
      const int lastRank = procSize - 1;
      // create the space filling curve iterator
      SpaceFillingCurveIterator< GridView > sfc( gridView() );
      for( sfc.first(); ! sfc.done(); sfc.next() )
      {
        const Element &element = sfc.item() ;
        if( localElementNumber >= elementCount )
        {
          if( rank < lastRank )
          {
            // increase rank
            ++ rank;
          }

          // reset local number
          localElementNumber = 0;

          // switch to smaller number if red line is crossed
          if( elementCount == maxPerProc && rank >= percentage )
          {
            elementCount = minPerProc ;
          }
        }

        const size_t index = indexSet_.index( element );
        assert( rank < procSize );
        partition_[ index ] = rank ;

        // increase counters
        ++elementNumber;
        ++localElementNumber;
      }

      std::vector< int > elCount( procSize, 0);
      for( size_t i=0; i<partition_.size(); ++i )
      {
        ++elCount[ partition_[ i ] ];
      }

      int minEl = partSize ;
      int maxEl = 0;
//      int maxRank = 0;
      int sum = 0;
      for( int i=0; i<procSize; ++i )
      {
        sum += elCount[ i ] ;
        minEl = std::min( minEl, elCount[ i ] );
        if( elCount[ i ] >= maxEl )
        {
          maxEl = elCount[ i ];
//          maxRank = i;
        }
        /*
        if( elCount[ i ] < minPerProc )
        {
          std::cout << "min P[ " << i << " ] = " << elCount [ i ] << std::endl;
        }
        if( elCount[ i ] > maxPerProc )
        {
          std::cout << "max P[ " << i << " ] = " << elCount [ i ] << std::endl;
        }
        */
      }

      /*
      if( comm_.rank() == 0 )
      {
        std::cout << "minEl " << minEl << std::endl;
        std::cout << "maxEl " << maxEl << std::endl;
        std::cout << "sumEl " << sum << std::endl;
        std::cout << "maxRank " << maxRank << std::endl;
      }
      */

      /*
      if( comm_.rank() == 0 )
      {
        for( size_t i=0; i<partition_.size(); ++i )
          std::cout << "rank[ " << i << " ] = " << partition_[ i ] << std::endl;
      }
      */
    }

    Communication comm_;

    GridView gridView_;
    const IndexSet &indexSet_;

    // load balancer bounds
    std::vector< int > partition_;
  };



  // RankMapDecomposition
  // --------------------

  template< class IdSet >
  struct RankMapDecomposition
  {
    typedef typename IdSet::IdType IdType;

    typedef std::map< IdType, int > RankMap;

    explicit RankMapDecomposition ( const IdSet &idSet, const RankMap &rankMap )
    : idSet_( idSet ),
      rankMap_( rankMap )
    {}

    template< class HostElement >
    int rank ( const HostElement &hostElement ) const
    {
      typename RankMap::const_iterator pos = rankMap_.find( idSet_.id( hostElement ) );
      return (pos == rankMap_.end() ? -1 : pos->second);
    }

  private:
    const IdSet &idSet_;
    const RankMap &rankMap_;
  };



  // ParallelGridPartitioner
  // -----------------------

  template< class Grid >
  class ParallelGridPartitioner
  {
    typedef ParallelGridPartitioner< Grid > This;

  public:
    typedef ParallelGridMacroDecomposition< Grid > MacroDecomposition;

  private:
    typedef typename Grid::HostGrid HostGrid;
    typedef typename Grid::LevelGridView MacroView;

    typedef typename HostGrid::LevelGridView HostMacroGridView;
    typedef typename HostGrid::LocalIdSet HostIdSet;

    typedef typename HostIdSet::IdType HostId;

    typedef typename Grid::template Codim<0> :: Entity     Element ;
    typedef typename HostGrid::template Codim<0> :: Entity HostElement ;

    typedef IdGridIndexSet< Grid, typename HostMacroGridView::IndexSet > MacroIndexSet;

    static  const bool reallyCommunicate = true ;

    // use MacroView if reallyCommunicate is true
    typedef Dune::ALUGridPartitioner<MacroView, All_Partition, MacroIndexSet > Partitioner;

    //typedef Dune::SimplePartitioner< HostMacroGridView, All_Partition, MacroIndexSet > Partitioner;

    typedef ALU3DSPACE ObjectStream ObjectStream;
    typedef ALU3DSPACE ALUMpAccess MpAccess;

  public:
    ParallelGridPartitioner ( const Grid &grid, const MpAccess &mpAccess )
    : grid_( grid ),
      hostMacroView_( hostGrid().levelGridView( 0 ) ),
      macroView_( grid_.levelGridView( 0 ) ),
      hostIdSet_( hostGrid().localIdSet() ),
      macroIndexSet_( hostMacroView_.indexSet() ),
      mpAccess_( mpAccess )
    {
      if( reallyCommunicate )
        setupAllToAllLinkage( mpAccess_ );
    }

    const MacroDecomposition *repartition ( const MacroDecomposition &currentDecomposition ) const
    {
      Partitioner partitioner( macroView_, mpAccess_.psize(), macroIndexSet_ );

      if( partitioner.checkPartition() && partitioner.repartition() )
      {
        MacroDecomposition *macroDecomposition = new MacroDecomposition( hostGrid() );
        communicatePartitioning( partitioner, currentDecomposition, *macroDecomposition );
        return macroDecomposition;
      }
      else
        return nullptr;
    }

  private:
    // get host entity id for a host grid element
    HostId getEntityId( const HostElement& element ) const
    {
      return hostIdSet_.id( element );
    }

    // get host entity id for a grid element, forwarind to the host element
    HostId getEntityId( const Element& element ) const
    {
      return getEntityId( Grid::template getHostEntity< 0 >( element ) );
    }

    void communicatePartitioning ( const Partitioner &partitioner,
                                   const MacroDecomposition &oldDecomposition,
                                   MacroDecomposition &newDecomposition ) const
    {

      typedef std::map< HostId, int > RankMap;
      typedef typename Partitioner::GridView::template Codim< 0 >::Iterator Iterator;

      const int myrank = mpAccess_.myrank();
      const int nlinks = mpAccess_.nlinks();
      std::size_t size = 0;

      RankMap rankMap;

      // fill rankMap
      const Iterator end = partitioner.gridView().template end< 0 >();
      for( Iterator it = partitioner.gridView().template begin< 0 >(); it != end; ++it )
      {
        const typename Iterator::Entity &element = *it;

        // if reallyCommunicate is false, then fill rank map anyway
        const bool fillRankMap = reallyCommunicate ? ( oldDecomposition.rank( element ) == myrank ) : true ;

        // fill rank map with new destinations
        if( fillRankMap )
        {
          const int newRank = partitioner.getDestination( element );
          assert( (newRank >= 0) && (newRank < mpAccess_.psize()) );

          const HostId hostId = getEntityId( element );
          rankMap[ hostId ] = newRank;

          ++size;
        }
      }

      int checkSize = size ;
      checkSize = mpAccess_.gmax( checkSize );
      if(!reallyCommunicate)
      {
        if( checkSize != int(size) )
        DUNE_THROW(InvalidStateException,"size of partition is not the same on all procs" << size );
      }
#if ALU3DGRID_PARALLEL
      // only fir partitioning should be communicated
      if( reallyCommunicate )
      {
        const size_t chunkSize = sizeof( std::size_t ) + size*(sizeof( HostId ) + sizeof( int ));
        // prepare message buffers
        std::vector< ObjectStream > buffer( nlinks
#ifdef ALUGRID_CONSTRUCTION_WITH_STREAMS
              , ObjectStream( chunkSize ) // set chunkSize to what we need exactly
#endif
            );
        for( int link = 0; link < nlinks; ++link )
        {
          // clear message buffer
          buffer[ link ].clear();

          // reserve size that will be written to stream
          buffer[ link ].reserve( chunkSize );

          // write number of (id,rank) pairs that will be written to the stream
          buffer[ link ].write( size );
        }

        // write (id,rank) pairs
        for( typename RankMap::const_iterator it = rankMap.begin(); it != rankMap.end(); ++it )
        {
          const HostId &id = (*it).first;
          const int &rank = (*it).second;

          // write my rank or rank of others to streams
          for( int link = 0; link < nlinks; ++link )
          {
            buffer[ link ].write( id );
            buffer[ link ].write( rank );
          }
        }

        // exchange data
        buffer = mpAccess_.exchange( buffer );

        // unpack streams
        for( int link = 0; link < nlinks; ++link )
        {
          std::size_t size = 0;
          buffer[ link ].read( size );

          // read (id,rank) pairs
          for( std::size_t i = 0; i < size; ++i )
          {
            HostId id;
            buffer[ link ].read( id );

            int rank;
            buffer[ link ].read( rank );

            // insert into partition map
            rankMap[ id ] = rank;
          }
        }
      }
#endif // #if ALU3DGRID_PARALLEL

      newDecomposition = RankMapDecomposition< HostIdSet >( hostIdSet_, rankMap );
    }

    static void setupAllToAllLinkage ( MpAccess &mpAccess )
    {
      mpAccess.removeLinkage();
      std::set< int > allLinks;
      const int myrank = mpAccess.myrank();
      const int psize = mpAccess.psize();
      for( int rank = 0; rank < psize; ++rank )
      {
        if( rank != myrank )
          allLinks.insert( rank );
      }
      mpAccess.insertRequestSymetric( allLinks );
    }

    const HostGrid &hostGrid () const { return grid_.hostGrid(); }

    const Grid &grid_;
    HostMacroGridView hostMacroView_;
    MacroView macroView_;
    const HostIdSet &hostIdSet_;
    MacroIndexSet macroIndexSet_;
    MpAccess mpAccess_;
  };

} // namespace Dune

#endif // #if HAVE_DUNE_ALUGRID

#endif // #ifndef DUNE_PARALLELGRID_PARTITIONER_HH
