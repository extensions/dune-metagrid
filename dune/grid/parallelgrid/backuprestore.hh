#ifndef DUNE_PARALLELGRID_BACKUPRESTORE_HH
#define DUNE_PARALLELGRID_BACKUPRESTORE_HH

#include <dune/grid/common/backuprestore.hh>

#include <dune/grid/parallelgrid/capabilities.hh>

#include <dune/grid/io/file/vtk/vtkwriter.hh>

namespace Dune
{

  // BackupRestoreFacility for ParallelGrid
  // --------------------------------------

  template< class HostGrid >
  struct BackupRestoreFacility< ParallelGrid< HostGrid > >
  {
    typedef ParallelGrid< HostGrid > Grid;
    typedef BackupRestoreFacility< HostGrid > HostBackupRestoreFacility;

    static void backup ( const Grid &grid, const std::string &filename )
    {
      HostBackupRestoreFacility::backup( grid.hostGrid(), filename );
      std::string newfilename( filename + ".paragrid" );
      std::ofstream out( newfilename.c_str() );
      grid.backup( out );
    }

    static void backup ( const Grid &grid, std::ostream &stream )
    {
      HostBackupRestoreFacility::backup( grid.hostGrid(), stream );
      grid.backup( stream );
    }

    static Grid *restore ( const std::string &filename )
    {
      HostGrid* hostGrid = HostBackupRestoreFacility::restore( filename );
      if( ! hostGrid )
        DUNE_THROW(IOError,"HostGrid was not created!");
      std::string newfilename( filename + ".paragrid" );
      std::ifstream in ( newfilename.c_str() );
      Grid* grid = new Grid( hostGrid );
      grid->restore( in );
      return grid ;
    }

    static Grid *restore ( std::istream &stream )
    {
      HostGrid* hostGrid = HostBackupRestoreFacility::restore( stream );
      if( ! hostGrid )
        DUNE_THROW(IOError,"HostGrid was not created!");
      Grid* grid = new Grid( hostGrid );
      grid->restore( stream );
      return grid ;
    }
  };


} // namespace Dune

#endif // #ifndef DUNE_PARALLELGRID_BACKUPRESTORE_HH
