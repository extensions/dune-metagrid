#ifndef DUNE_PARALLELGRID_MPACCESS_HH
#define DUNE_PARALLELGRID_MPACCESS_HH

//- dune-alugrid includes
#if HAVE_DUNE_ALUGRID
#include <dune/alugrid/impl/serial/serialize.h>
#include <dune/alugrid/3d/grid.hh>

namespace ALUGrid
{

  // but without any communication, this is needed to avoid
  // communication during the call of DataBase :: repartition
  class MpAccessNoMPI
#if HAVE_MPI
  : public MpAccessLocal
#endif // #if HAVE_MPI
  {
  public:
    template <class NoComm>
    MpAccessNoMPI( const NoComm& ) {}

    typedef MpAccessGlobal::minmaxsum_t  minmaxsum_t;
    typedef MpAccessLocal::NonBlockingExchange  NonBlockingExchange;

    int psize() const override { return 1; }
    int nlinks () const { return 0; }
    int myrank () const override { return 0; }

    int barrier () const override { return psize(); }
    bool gmax (bool value) const override { return value; }
    int gmax (int value) const override { return value; }
    int gmin ( int value ) const override { return value; }
    int gsum ( int value ) const override { return value; }
    long gmax ( long value ) const override { return value; }
    long gmin ( long value ) const override { return value; }
    long gsum ( long value ) const override { return value; }
    double gmax ( double value ) const override { return value; }
    double gmin ( double value ) const override { return value; }
    double gsum ( double value ) const override { return value; }
    void gmax (double* in, int length, double* out) const override { std::copy(in, in+length, out ); }
    void gmin (double* in, int length, double* out) const override { std::copy(in, in+length, out ); }
    void gsum (double* in, int length, double* out) const override { std::copy(in, in+length, out ); }
    void gmax (int*, int, int*) const override {}
    void gmin (int*, int, int*) const override {}
    void gsum (int*, int, int*) const override {}
    minmaxsum_t minmaxsum( double value ) const override { return minmaxsum_t( value ); }
    std::pair<double, double> gmax (std::pair<double, double> value ) const override { return value; }
    std::pair<double, double> gmin (std::pair<double, double> value ) const override { return value; }
    std::pair<double, double> gsum (std::pair<double, double> value ) const override { return value; }
    void bcast(int*, int, int) const override {}
    void bcast(char*, int, int) const override {}
    void bcast(double*, int, int) const override {}
    int exscan( int value ) const override { return 0; }
    int scan( int value ) const override { return value; }

    std::vector< int > gcollect ( int value ) const override { return std::vector< int >( psize(), value ); }
    std::vector< double > gcollect ( double value ) const override { return std::vector< double >( psize(), value ); }

    std::vector< std::vector< int > > gcollect ( const std::vector< int > &value ) const override
    { return std::vector< std::vector< int > > (psize(), value); }

    std::vector< std::vector< double > > gcollect ( const std::vector< double > &value ) const override
    { return std::vector< std::vector< double > > ( psize(), value ); }

    std::vector< ObjectStream > gcollect ( const ObjectStream &os ) const override
    { return std::vector< ObjectStream >( psize(), os ); }

#if HAVE_MPI
    // return handle for non-blocking exchange and already do send operation
    NonBlockingExchange* nonBlockingExchange (const int tag, const std::vector<ObjectStream>&) const override
    { return 0; }
    // return handle for non-blocking exchange
    NonBlockingExchange* nonBlockingExchange (const int tag) const override { return 0; }
#else // #if HAVE_MPI
    std::vector< int > dest () const { return std::vector< int >(); }
#endif // #else // #if HAVE_MPI

    void printLinkage ( std::ostream & ) const {}
    void removeLinkage () {}
    int link (int) const { return 0; }
    int insertRequestSymetric (std::set < int, std::less < int > >){ return 0; }

    // exchange data and return new vector of object streams
    std::vector< ObjectStream > exchange (const std::vector<ObjectStream>& v) const override { return v; }
    void exchange ( const std::vector< ObjectStream > &, NonBlockingExchange::DataHandleIF& ) const override {};
    void exchange ( NonBlockingExchange::DataHandleIF& ) const override {};
    void exchangeSymmetric ( NonBlockingExchange::DataHandleIF& dh ) const override {};

  };

#if HAVE_MPI
  // type of communicator
  typedef MpAccessMPI ALUMpAccess;
#else
  // empty communicator
  typedef MpAccessNoMPI ALUMpAccess;
#endif

} // namespace ALUGridSpace

#endif // #if HAVE_DUNE_ALUGRID

#endif // #ifndef DUNE_PARALLELGRID_MPACCESS_HH
