#ifndef DUNE_GRID_PRISMGRID_INDEXSET_HH
#define DUNE_GRID_PRISMGRID_INDEXSET_HH

#include <algorithm>
#include <vector>
#include <set>

#include <dune/common/exceptions.hh>
#include <dune/common/typetraits.hh>

#include <dune/geometry/referenceelements.hh>

#include <dune/grid/common/indexidset.hh>

namespace Dune
{

  // PrismIndexSet
  // -------------

  template< class GridImp, class HostIndexSetImp >
  class PrismIndexSet
  : public Dune::IndexSet< GridImp, PrismIndexSet< GridImp, HostIndexSetImp >, typename HostIndexSetImp::IndexType >
  {
    typedef PrismIndexSet< GridImp, HostIndexSetImp> This;
    typedef Dune::IndexSet< GridImp, PrismIndexSet< GridImp, HostIndexSetImp >, typename HostIndexSetImp::IndexType > Base;

    typedef typename remove_const< GridImp >::type::Traits Traits;

    typedef typename Traits::ctype ctype;
    typedef typename Traits::HostGridView HostGridView;
    typedef typename HostGridView::template Codim< 0 >::Entity HostEntity;

  public:
    typedef typename Traits::Grid Grid;
    static const int dimension = Traits::dimension;

    typedef HostIndexSetImp HostIndexSet;
    typedef typename HostIndexSet::IndexType IndexType;

    template< int codim >
    struct Codim
    {
      typedef typename Traits::template Codim< codim >::Entity Entity;
    };

    PrismIndexSet () : hostIndexSet_( nullptr ) {}

    explicit PrismIndexSet( const HostIndexSet &hostIndexSet, const int layers )
    : hostIndexSet_( &hostIndexSet ),
      layers_( layers )
    {
      getGeometryTypes( hostIndexSet.geomTypes( 0 ), geomTypes_ );
    }

    PrismIndexSet ( const This &other )
    : hostIndexSet_( other.hostIndexSet_ ),
      layers_( other.layers_ ),
      geomTypes_( other.geomTypes_ )
    {}

    This &operator= ( const This &other )
    {
      hostIndexSet_ = other.hostIndexSet_;
      layers_ = other.layers_;
      geomTypes_ = other.geomTypes_;
      return *this;
    }

    template< class EntityType >
    IndexType index ( const EntityType &entity ) const { return index< EntityType::codimension >( entity ); }
    template< int codim >
    IndexType index ( const typename Codim< codim >::Entity &entity ) const;
    IndexType subIndex ( const typename Codim< 0 >::Entity &entity, int i, unsigned int codim ) const;

    const std::vector< GeometryType > &geomTypes ( int codim ) const { return geomTypes_[ codim ]; }

    IndexType size ( const GeometryType &type ) const;
    IndexType size ( const int codim ) const;

    template< class EntityType >
    bool contains ( const EntityType &entity ) const
    {
      return hostIndexSet().contains( entity.impl().hostEntity() );
    }

    const HostIndexSet &hostIndexSet () const
    {
      assert( hostIndexSet_ );
      return *hostIndexSet_;
    }

    operator bool () const { return hostIndexSet_; }

  private:
    static void getGeometryTypes ( const std::vector< GeometryType > &hostTypes, std::array< std::vector< GeometryType >, dimension+1 > &geometryTypes );

    int layers () const { return layers_; }

    const HostIndexSet *hostIndexSet_;
    int layers_;
    std::array< std::vector< GeometryType >, Base::dimension+1 > geomTypes_;
  };



  // Implementation of PrismIndexSet
  // -------------------------------

  template< class GridImp, class HostIndexSetImp >
  template< int codim >
  inline typename PrismIndexSet< GridImp, HostIndexSetImp >::IndexType
  PrismIndexSet< GridImp, HostIndexSetImp >
    ::index ( const typename Codim< codim >::Entity &entity ) const
  {
    typedef typename Codim< codim >::Entity::Implementation EntityImp;
    const EntityImp &entityImp= entity.impl();
    IndexType ret = hostIndexSet().index( entityImp.hostEntity() );
    ret *= ( codim == 0 ) ? layers() : layers() + 1;
    ret += entityImp.subIndex();
    return ret;
  }


  template< class GridImp, class HostIndexSetImp >
  inline typename PrismIndexSet< GridImp, HostIndexSetImp >::IndexType
  PrismIndexSet< GridImp, HostIndexSetImp >
    ::subIndex ( const typename Codim< 0 >::Entity &entity, int i, unsigned int codim ) const
  {
    if( codim == 0 )
      return index( entity );

    // get host entity
    const HostEntity &hostEntity = entity.impl().hostEntity();

    if( codim == (unsigned int) dimension )
    {
      // calc vertex index
      int count = hostEntity.template count< HostGridView::dimension >();
      IndexType ret = hostIndexSet().subIndex( hostEntity, i%count, HostGridView::dimension )*( layers() + 1 );
      ret += entity.impl().subIndex() + i/count;
      return ret;
    }

    // get host reference element
    const ReferenceElement< ctype, HostGridView::dimension > &hostReferenceElement
      = ReferenceElements< ctype, HostGridView::dimension >::general( hostEntity.type() );

    int count = hostReferenceElement.size( codim );
    const bool isRegular = ( i < count );

    IndexType ret;
    if( isRegular )
    {
      ret = hostIndexSet().subIndex( hostEntity, i , codim )*layers();
      ret += entity.impl().subIndex();
    }
    else
    {
      i -= count;
      count = hostReferenceElement.size( codim-1 ) ;
      ret = hostIndexSet().subIndex( hostEntity, i%count, codim-1 )*( layers() + 1 );
      ret += entity.impl().subIndex() + i/count;
    }
    return ret;
  }


  template< class GridImp, class HostIndexSetImp >
  inline void PrismIndexSet< GridImp, HostIndexSetImp >
    ::getGeometryTypes ( const std::vector< GeometryType > &hostTypes,
                         std::array< std::vector< GeometryType >, dimension+1 > &geometryTypes )
  {
    // temporary storage
    std::array< std::set< GeometryType >, dimension+1 > uniqueTypes;

    const std::vector< GeometryType >::const_iterator end = hostTypes.end();
    for( std::vector< GeometryType >::const_iterator it = hostTypes.begin(); it !=  end; ++it )
    {
      // get host geometry type
      const GeometryType hostType = *it;
      if( int( hostType.dim() ) != HostGridView::dimension )
        DUNE_THROW( InvalidStateException, "Geometry type has wrong dimension." );

      // get topology id of prismatic element
      const unsigned int topologyId = hostType.id() | (1 << (dimension - 1));

      // get reference element
      GeometryType type( topologyId, dimension );
      const ReferenceElement< ctype, dimension > &referenceElement
        = ReferenceElements< ctype, dimension >::general( type );

      // get all geometry types for element
      for( int cd = 0; cd <= dimension; ++cd )
      {
        const int nSubEntities = referenceElement.size( cd );
        for( int i = 0; i < nSubEntities; ++i )
          uniqueTypes[ cd ].insert( referenceElement.type( i, cd ) );
      }
    }

    // sort vector and remove duplicates
    for( int cd = 0; cd <= dimension; ++cd )
    {
      const std::set< GeometryType > &source = uniqueTypes[ cd ];
      std::vector< GeometryType > &dest = geometryTypes[ cd ];
      dest.clear();
      dest.resize( source.size() );
      std::copy( source.begin(), source.end(), dest.begin() );
    }
  }


  template< class GridImp, class HostIndexSetImp >
  inline typename PrismIndexSet< GridImp, HostIndexSetImp >::IndexType
  PrismIndexSet< GridImp, HostIndexSetImp >::size ( const GeometryType &type ) const
  {
    const int dim = type.dim();
    IndexType size( 0 );

    if( dim < dimension )
      size += hostIndexSet().size( type )*( layers() + 1 );

    if( dim > 0 )
    {
      const unsigned int topologyId = type.id() & ( ( 1 << (dim-1) ) - 1 );
      size += hostIndexSet().size( GeometryType( topologyId, dim-1 ) )*layers();
    }

    return size;
  }


  template< class GridImp, class HostIndexSetImp >
  inline typename PrismIndexSet< GridImp, HostIndexSetImp >::IndexType
  PrismIndexSet< GridImp, HostIndexSetImp >::size ( int codim ) const
  {
    IndexType size( 0 );
    if( codim < dimension )
      size += hostIndexSet().size( codim )*layers();
    if( codim > 0 )
      size += hostIndexSet().size( codim-1 )*( layers() + 1 );
    return size;
  }

} // namespace Dune

#endif // #ifndef DUNE_GRID_PRISMGRID_INDEXSET_HH
