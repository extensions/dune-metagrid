#ifndef DUNE_GRID_PRISMGRID_DGFPARSER_HH
#define DUNE_GRID_PRISMGRID_DGFPARSER_HH

#include <algorithm>
#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include <dune/grid/common/intersection.hh>
#include <dune/grid/io/file/dgfparser/dgfparser.hh>
#include <dune/grid/io/file/dgfparser/parser.hh>

#include "grid.hh"

namespace Dune
{

  // ReplaceDGFBlock
  // ---------------

  struct ReplaceDGFBlock
  {
    static void replace( const std::string &id, std::istream &input,
                         std::istream &content, std::ostream &modified );

  private:
    // make upper case
    static std::string makeUpperCase ( const std::string &in );
    // return true if line begins with identifier
    static bool found ( std::string line, std::string identifier );
    // reset stream
    static void reset ( std::istream &input );
  };



  // DGFGridInfo
  // -----------

  template < class HostGrid >
  struct DGFGridInfo< PrismGrid< HostGrid > >
  {
    static int refineStepsForHalf () { return DGFGridInfo< HostGrid >::refineStepsForHalf(); }
    static double refineWeight () { return -1.; }
  };



  namespace dgf
  {

    // NormalGridInfo
    // --------------

    struct NormalGridInfo
    {
      double left, right;
      unsigned int layers;
    };



    // PrismGridBlock
    // --------------

    struct PrismGridBlock
    : public BasicBlock
    {
      static const char* ID;
      typedef double ctype;

      explicit PrismGridBlock ( std::istream &in );
      const NormalGridInfo &normalGridInfo () const
      {
        return normalGridInfo_;
      }

    private:
      NormalGridInfo normalGridInfo_;
    };

  } // end namespace dgf



  // DGFGridFactory for PrismGrid
  // ----------------------------

  template< class HostGrid >
  struct DGFGridFactory< PrismGrid< HostGrid > >
  {
    typedef PrismGrid< HostGrid > Grid;

    const static int dimension = Grid::dimension;
    const static int dimensionworld = Grid::dimensionworld;
    typedef typename Grid::ctype ctype;

    typedef MPIHelper::MPICommunicator MPICommunicatorType;

    typedef DGFGridFactory< HostGrid > HostGridFactory;

    explicit DGFGridFactory ( const std::string &filename,
                              MPICommunicatorType comm = MPIHelper::getCommunicator() )
    : grid_( nullptr ),
      hostGridFactory_( nullptr )
    {
      std::ifstream input( filename.c_str() );
      if( !input )
        DUNE_THROW( DGFException, "Macrofile " << filename << " not found" );

      generate( input, comm );
      input.close();
    }

    explicit DGFGridFactory ( std::istream &input,
                              MPICommunicatorType comm = MPIHelper::getCommunicator() )
    : grid_( nullptr ),
      hostGridFactory_( nullptr )
    {
      input.clear();
      input.seekg( 0 );

      if( !input )
        DUNE_THROW( DGFException, "Error resetting input stream" );

      generate( input, comm );
    }

    ~DGFGridFactory ()
    {
      if( hostGridFactory_ )
        delete hostGridFactory_;
      hostGridFactory_ = nullptr;
    }

    Grid *grid ()
    {
      assert( grid_ );
      return grid_;
    }

    template < class Intersection >
    bool wasInserted ( const Intersection &intersection ) const
    {
      return false;
    }

    template < class Intersection >
    int boundaryId ( const Intersection &intersection ) const
    {
      return ( intersection.boundary() ? 1 : 0 );
    }

    template< int codim >
    int numParameters () const { return 0; }

    template< class Entity >
    std::vector< ctype > &parameter ( const Entity &entity ) const
    {
      DUNE_THROW( DGFException, "Method parameter not implemented.");
    }

    bool haveBoundaryParameters () const
    {
      assert( hostGridFactory_ );
      if( hostGridFactory_->haveBoundaryParameters() )
        std::cerr << "Warning: Ignoring boundary parameters for host grid";
      return false;
    }

    template < class GridImp, class IntersectionImp >
    const typename DGFBoundaryParameter::type &
    boundaryParameter ( const Intersection< GridImp, IntersectionImp > &intersection ) const
    {
      assert( hostGridFactory_ );
      return DGFBoundaryParameter::defaultValue();
    }

    static dgf::NormalGridInfo
    modifyIntervalBlock ( const dgf::IntervalBlock &intervalBlock,
                          std::ostream &newBlock );

  private:
    void generate ( std::istream &input, MPICommunicatorType comm );
    void create ()
    {
      hostGrid()->loadBalance();
      grid_ = new Grid( *(hostGrid()), normalGridInfo().left, normalGridInfo().right, normalGridInfo().layers );
    }

    HostGrid *hostGrid ()
    {
      assert( hostGridFactory_ );
      return hostGridFactory_->grid();
    }

    const dgf::NormalGridInfo &normalGridInfo () const
    {
      return normalGridInfo_;
    }

    Grid *grid_;
    HostGridFactory *hostGridFactory_;
    dgf::NormalGridInfo normalGridInfo_;
  };



  // Implementation of DGFGridFactory
  // --------------------------------

  template< class HostGrid >
  inline dgf::NormalGridInfo DGFGridFactory< PrismGrid< HostGrid > >
    ::modifyIntervalBlock ( const dgf::IntervalBlock &intervalBlock,
                            std::ostream &newBlock )
  {
    typedef typename dgf::IntervalBlock::Interval Interval;

    if( intervalBlock.dimw() != dimensionworld )
      DUNE_THROW( DGFException, "Interval block has wrong dimension of world" );

    const int numIntervals = intervalBlock.numIntervals();
    if( numIntervals < 1 )
      DUNE_THROW( DGFException, "Empty interval block found" );

    // extract normal grid information from first interval
    dgf::NormalGridInfo normalGridInfo;
    const Interval &interval = intervalBlock.get( 0 );
    normalGridInfo.left   = interval.p[ 0 ][ dimensionworld-1 ];
    normalGridInfo.right  = interval.p[ 1 ][ dimensionworld-1 ];
    normalGridInfo.layers = interval.n[ dimensionworld-1 ];

    // modify interval block
    const std::string identifier = intervalBlock.id();
    newBlock << identifier << std::endl;
    for( int i = 0; i < numIntervals; ++i )
    {
      for( int i = 0; i <= dimensionworld-2; ++i )
        newBlock << interval.p[ 0 ][ i ] << (i == dimensionworld-2 ? "\n" : " ");
      for( int i = 0; i <= dimensionworld-2; ++i )
        newBlock << interval.p[ 1 ][ i ] << (i == dimensionworld-2 ? "\n" : " ");
      for( int i = 0; i <= dimensionworld-2; ++i )
        newBlock << interval.n[ i ] << (i == dimensionworld-2 ? "\n" : " ");

      if( ( interval.p[ 0 ][ dimensionworld-1 ] != normalGridInfo.left )
            || ( interval.p[ 1 ][ dimensionworld-1 ] != normalGridInfo.right )
            || ( interval.n[ dimensionworld-1 ] != int( normalGridInfo.layers ) )
        ) DUNE_THROW( DGFException, "PrismGrid requires the last component of each interval to be identical" );
    }
    newBlock << "#" << std::endl;

    return normalGridInfo;
  }


  template< class HostGrid >
  inline void DGFGridFactory< PrismGrid< HostGrid > >
    ::generate ( std::istream &input, MPICommunicatorType comm )
  {
    // try to read prism grid block
    dgf::PrismGridBlock prismBlock( input );
    if( prismBlock.isactive() )
    {
      // read normal grid parameters
      normalGridInfo_ = prismBlock.normalGridInfo();
      hostGridFactory_ = new DGFGridFactory< HostGrid >( input, comm );
      return create();
    }

    // now, try to read interval block
    dgf::IntervalBlock intervalBlock( input );
    if( intervalBlock.isactive() )
    {
      std::stringstream newBlock;
      normalGridInfo_ = modifyIntervalBlock( intervalBlock, newBlock );

      // create new stream for host grid factory
      std::stringstream modified;
      const std::string identifier = intervalBlock.id();
      ReplaceDGFBlock::replace( identifier, input, newBlock, modified );

      // create host grid
      hostGridFactory_  = new DGFGridFactory< HostGrid >( modified, comm );
      return create();
    }
    // if we reach this point, no suitable block has been found
    DUNE_THROW( DGFException, "No supported block found" );
  }

} // namespace Dune

#endif // #ifndef DUNE_GRID_PRISMGRID_DGFPARSER_HH
