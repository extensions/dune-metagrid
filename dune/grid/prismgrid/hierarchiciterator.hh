#ifndef DUNE_GRID_PRISMGRID_HIERARCHICITERATOR_HH
#define DUNE_GRID_PRISMGRID_HIERARCHICITERATOR_HH

#include "entitypointer.hh"

namespace Dune
{

  // PrismHierarchicIteratorTraits
  // -----------------------------

  template< class Grid >
  struct PrismHierarchicIteratorTraits
  : public PrismEntityPointerTraits< 0, Grid >
  {
    typedef typename remove_const< Grid >::type::Traits::HostGrid
      ::HierarchicIterator HostIterator;
  };



  // PrismHierarchicIterator
  // -----------------------

  template< class GridImp >
  class PrismHierarchicIterator
  : public PrismEntityPointer< PrismHierarchicIteratorTraits< GridImp > > 
  {
    typedef PrismHierarchicIterator< GridImp > This;
    typedef PrismHierarchicIteratorTraits< GridImp > Traits;
    typedef PrismEntityPointer< Traits > Base; 

  public:
    typedef typename Base::Grid Grid;
    typedef typename Base::EntityImp EntityImp;
    typedef typename Base::HostIterator HostIterator;

  protected:
    PrismHierarchicIterator ( const HostIterator &hostIterator, const Grid &grid )
    : Base( hostIterator, grid )
    {}

  public:
    static PrismHierarchicIterator begin ( int maxLevel, const EntityImp &entity, 
                                           const typename Base::Grid &grid )
    {
      return end( maxLevel, entity, grid );
    }

    static PrismHierarchicIterator end ( int maxLevel, const EntityImp &entity, 
                                         const typename Base::Grid &grid )
    {
      const HostIterator &hostIterator = entity.hostEntity().hend( maxLevel ); 
      return This( hostIterator, grid );
    }

    void increment () {}
  };

} // end namespace Dune

#endif // #ifndef DUNE_GRID_PRISMGRID_HIERARCHICITERATOR_HH
