#ifndef DUNE_GRID_PRISMGRID_GEOMETRY_HH
#define DUNE_GRID_PRISMGRID_GEOMETRY_HH

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/common/shared_ptr.hh>
#include <dune/common/typetraits.hh>

#include <dune/geometry/referenceelements.hh>

#include <dune/grid/common/geometry.hh>

#include "geometryhelper.hh"
#include "normalgrid.hh"
#include "tangentialgeometry.hh"
#include "utility.hh"

namespace Dune
{

  // Forward declarations
  // --------------------

  template< int codim, int dim, class Traits >
  class PrismGeometryBase;
  template< class Traits >
  class PrismLateralGeometry;
  template< class Traits >
  class PrismHorizontalGeometry;
  template< class, int >
  class PrismNormalGeometry;
  template< int mydim, int cdim, class Grid >
  class PrismGeometry;
  template< int mydim, int cdim, class Grid >
  class PrismLocalGeometry;



  // FacadeOptions
  // -------------

  namespace FacadeOptions
  {
    template< int mydim, int cdim, class Grid >
    struct StoreGeometryReference< mydim, cdim, Grid, PrismGeometry >
    {
      static const bool v = false;
    };

    template< int mydim, int cdim, class Grid >
    struct StoreGeometryReference< mydim, cdim, Grid, PrismLocalGeometry >
    {
      static const bool v = false;
    };
  }



  // PrismGeometryInterface
  // ----------------------

  template< class Traits >
  struct PrismGeometryInterface
  {
    static const int mydimension = Traits::mydimension;
    static const int coorddimension = Traits::coorddimension;
    static const int dimension = Traits::dimension;

    typedef typename Traits::ctype ctype;

    typedef typename Traits::LocalCoordinate LocalCoordinate;
    typedef typename Traits::GlobalCoordinate GlobalCoordinate;

    typedef typename Traits::JacobianInverseTransposed JacobianInverseTransposed;
    typedef typename Traits::JacobianTransposed JacobianTransposed;

  public:
    virtual ~PrismGeometryInterface () { }

    virtual GeometryType type () const = 0;
    virtual bool affine () const = 0;
    virtual int corners () const = 0;

    virtual GlobalCoordinate corner ( int i ) const = 0;
    virtual GlobalCoordinate global ( const LocalCoordinate &local ) const = 0;
    virtual LocalCoordinate local ( const GlobalCoordinate &global ) const = 0;

    virtual ctype integrationElement ( const LocalCoordinate &local ) const = 0;
    virtual ctype volume () const = 0;

    virtual GlobalCoordinate center () const = 0;

    virtual const JacobianTransposed &jacobianTransposed ( const LocalCoordinate &local ) const = 0;
    virtual const JacobianInverseTransposed &jacobianInverseTransposed ( const LocalCoordinate &local ) const = 0;

    virtual bool isLateral () const = 0;
  };



  // PrismGeometryTraits
  // -------------------

  template< int mydim, int cdim, class Grid >
  class PrismGeometryTraits
  {
    typedef typename remove_const< Grid >::type::Traits Traits;
    typedef typename Traits::HostGridView HostGridView;

  public:
    static const int mydimension = mydim;
    static const int coorddimension = cdim;
    static const int dimension = Traits::dimension;
    static const int codimension = dimension - mydimension;

    typedef typename Traits::ctype ctype;

    typedef Dune::FieldVector< ctype, mydimension > LocalCoordinate;
    typedef Dune::FieldVector< ctype, coorddimension > GlobalCoordinate;

    typedef Dune::FieldMatrix< ctype, coorddimension, mydimension > JacobianInverseTransposed;
    typedef Dune::FieldMatrix< ctype, mydimension, coorddimension > JacobianTransposed;

    template< bool isLateral >
    class IsLateral
    {
      // host geometry
      typedef typename HostGridView::template Codim<
          isLateral ? codimension : codimension-1
        >::Geometry HostGeometry;

    public:
      typedef HostGeometry TangentialGeometry;
      typedef PrismNormalGeometry< ctype, isLateral ? 1 : 0 > NormalGeometry;
    };
  };



  // PrismGeometry
  // -------------

  template< int mydim, int cdim, class Grid >
  class PrismGeometry
  : public PrismGeometryBase< PrismGeometryTraits< mydim, cdim, Grid >::codimension,
                              PrismGeometryTraits< mydim, cdim, Grid >::dimension,
                              PrismGeometryTraits< mydim, cdim, Grid >
                            >
  {
    typedef PrismGeometry< mydim, cdim, Grid > This;

  public:
    typedef PrismGeometryTraits< mydim, cdim, Grid > Traits;

  private:
    typedef PrismGeometryBase< Traits::codimension, Traits::dimension, Traits > Base;

  public:
    template< class TGeometry, class NGeometry >
    PrismGeometry ( TGeometry tangentialGeometry,
                    NGeometry normalGeometry )
    : Base( tangentialGeometry, normalGeometry )
    {}
  };



  // PrismLocalGeometryTraits
  // ------------------------

  template< int mydim, int cdim, class Grid >
  class PrismLocalGeometryTraits
  {
    typedef typename remove_const< Grid >::type::Traits Traits;
    typedef typename Traits::HostGridView HostGridView;

  public:
    static const int mydimension = mydim;
    static const int coorddimension = cdim;
    static const int dimension = Traits::dimension;
    static const int codimension = dimension - mydimension;

    typedef typename Traits::ctype ctype;

    typedef Dune::FieldVector< ctype, mydimension > LocalCoordinate;
    typedef Dune::FieldVector< ctype, coorddimension > GlobalCoordinate;
    typedef Dune::FieldMatrix< ctype, coorddimension, mydimension > JacobianInverseTransposed;
    typedef Dune::FieldMatrix< ctype, mydimension, coorddimension > JacobianTransposed;

    template< bool isLateral >
    class IsLateral
    {
      typedef typename HostGridView::template Codim<
          isLateral ? codimension : codimension-1
        >::Geometry HostGeometry;

      typedef typename HostGridView::template Codim<
          isLateral ? codimension : codimension-1
        >::LocalGeometry HostLocalGeometry;

    public:
      typedef typename SelectType< isLateral, HostLocalGeometry,
                                   PrismLocalTangentialGeometry< HostGeometry >
                                 >::Type TangentialGeometry;

      typedef PrismNormalGeometry< ctype, isLateral ? 1 : 0 > NormalGeometry;
    };
  };



  // PrismLocalGeometry
  // ------------------

  template< int mydim, int cdim, class Grid >
  class PrismLocalGeometry
  : public PrismGeometryBase< PrismLocalGeometryTraits< mydim, cdim, Grid >::codimension,
                              PrismLocalGeometryTraits< mydim, cdim, Grid >::dimension,
                              PrismLocalGeometryTraits< mydim, cdim, Grid >
                            >
  {
    typedef PrismLocalGeometry< mydim, cdim, Grid > This;
    typedef PrismLocalGeometryTraits< mydim, cdim, Grid > Traits;
    typedef PrismGeometryBase< Traits::codimension, Traits::dimension, Traits > Base;

  public:
    template< class TGeometry, class NGeometry >
    PrismLocalGeometry ( TGeometry tangentialGeometry,
                         NGeometry normalGeometry )
    : Base( tangentialGeometry, normalGeometry )
    {}
  };



  // PrismGeometryBase, codimension = 0
  // ----------------------------------

  template< int dim, class Traits >
  class PrismGeometryBase< 0, dim, Traits >
  : public PrismLateralGeometry< Traits >
  {
    typedef PrismGeometryBase< 0, dim, Traits > This;
    typedef PrismLateralGeometry< Traits > Base;

  public:
    typedef typename Traits::template IsLateral< true >::TangentialGeometry TangentialGeometry;

    typedef typename Traits::template IsLateral< true >::NormalGeometry NormalGeometry;

    PrismGeometryBase ( TangentialGeometry tangentialGeometry,
                        NormalGeometry normalGeometry )
    : Base( tangentialGeometry, normalGeometry )
    {}
  };



  // PrismGeometryBase, 0 < codimension < dimension
  // ----------------------------------------------

  template< int codim, int dim, class Traits >
  class PrismGeometryBase
  {
    typedef PrismGeometryBase< codim, dim, Traits > This;

  public:
    static const int dimension = Traits::dimension;
    static const int mydimension = Traits::mydimension;

  private:
    dune_static_assert( mydimension == dim-codim, "Template parameters do not match" );

  public:
    static const int coorddimension = Traits::coorddimension;

    typedef typename Traits::ctype ctype;

    typedef typename Traits::LocalCoordinate LocalCoordinate;
    typedef typename Traits::GlobalCoordinate GlobalCoordinate;

    typedef typename Traits::JacobianInverseTransposed JacobianInverseTransposed;
    typedef typename Traits::JacobianTransposed JacobianTransposed;

  private:
    template< bool isLateral >
    struct IsLateral
    {
      typedef typename SelectType< isLateral, PrismLateralGeometry< Traits >, PrismHorizontalGeometry< Traits > >
        ::Type Implementation;

      static const int size = sizeof( Implementation );
    };

  public:
    PrismGeometryBase () {}

    template< class TGeometry, class NGeometry >
    PrismGeometryBase ( TGeometry tangentialGeometry,
                        NGeometry normalGeometry )
    {
      const bool lateral = !( mydimension == TGeometry::mydimension );
      implementation_ = Dune::shared_ptr< PrismGeometryInterface< Traits > >( new typename IsLateral< lateral >::Implementation( tangentialGeometry, normalGeometry ) );
    }

    GeometryType type () const { return implementation_->type(); }

    bool affine () const { return implementation_->affine(); }

    int corners () const { return implementation_->corners(); }

    const GlobalCoordinate &operator[] ( int i ) const
    {
      return implementation_->corner( i );
    }

    GlobalCoordinate corner ( const int i ) const
    {
      return implementation_->corner( i );
    }

    GlobalCoordinate global ( const LocalCoordinate &local ) const
    {
      return implementation_->global( local );
    }

    LocalCoordinate local ( const GlobalCoordinate &global ) const
    {
      return implementation_->local( global );
    }

    ctype integrationElement ( const LocalCoordinate &local ) const
    {
      return implementation_->integrationElement( local );
    }

    ctype volume () const { return implementation_->volume(); }

    GlobalCoordinate center () const { return implementation_->center(); }

    const JacobianTransposed &jacobianTransposed ( const LocalCoordinate &local ) const
    {
      return implementation_->jacobianTransposed( local );
    }

    const JacobianInverseTransposed &jacobianInverseTransposed ( const LocalCoordinate &local ) const
    {
      return implementation_->jacobianInverseTransposed( local );
    }

    bool isLateral () const { return implementation_->isLateral(); }

    operator bool () const { return bool( implementation_ ); }

  private:
    Dune::shared_ptr< PrismGeometryInterface< Traits > > implementation_;
  };



  // PrismGeometryBase, codimension = dimension
  // ------------------------------------------

  template< int dim, class Traits >
  class PrismGeometryBase< dim, dim, Traits >
  {
    typedef PrismGeometryBase< dim, dim, Traits > This;

  public:
    static const int dimension = Traits::dimension;
    static const int mydimension = Traits::mydimension;

  private:
    dune_static_assert( mydimension == 0, "Template parameters do not match" );

  public:
    static const int coorddimension = Traits::coorddimension;

    typedef typename Traits::ctype ctype;

    typedef typename Traits::LocalCoordinate LocalCoordinate;
    typedef typename Traits::GlobalCoordinate GlobalCoordinate;

    typedef typename Traits::JacobianInverseTransposed JacobianInverseTransposed;
    typedef typename Traits::JacobianTransposed JacobianTransposed;

    typedef typename Traits::template IsLateral< false >::TangentialGeometry TangentialGeometry;
    typedef typename Traits::template IsLateral< false >::NormalGeometry NormalGeometry;

  private:
    dune_static_assert( TangentialGeometry::mydimension + NormalGeometry::mydimension == mydimension,
                        "Dimensions do not match" );
    dune_static_assert( TangentialGeometry::coorddimension + NormalGeometry::coorddimension == coorddimension,
                        "Dimensions do not match" );

    typedef typename TangentialGeometry::GlobalCoordinate TangentialGlobalCoordinate;

  public:
    PrismGeometryBase ( TangentialGeometry tangentialGeometry,
                        NormalGeometry normalGeometry )
    {
      corner_ = map< TangentialGlobalCoordinate, GlobalCoordinate >( tangentialGeometry.corner( 0 ) );
      corner_[ coorddimension-1 ] = normalGeometry.corner( 0 );
    }

    PrismGeometryBase ( GlobalCoordinate &corner ) : corner_( corner ) {}

    GeometryType type () const { return GeometryType( GeometryType::cube, 0 ); }

    bool affine () const {  return true; }

    int corners () const { return 1; }

    GlobalCoordinate corner ( int ) const { return corner_; }

    GlobalCoordinate global ( const LocalCoordinate & ) const { return corner_; }

    LocalCoordinate local ( const GlobalCoordinate & ) const { return LocalCoordinate( 1 ); }

    ctype integrationElement ( const LocalCoordinate & ) const { return volume(); }

    ctype volume () const { return 1; }

    GlobalCoordinate center () const { return corner_; }

    const JacobianTransposed &jacobianTransposed (const LocalCoordinate &local) const
    {
      return jacobianTransposed_;
    }

    const JacobianInverseTransposed &jacobianInverseTransposed (const LocalCoordinate &local) const
    {
      return jacobianInverseTransposed_;
    }

    bool isLateral () const { return false; }

  private:
    GlobalCoordinate corner_;
    JacobianTransposed jacobianTransposed_;
    JacobianInverseTransposed jacobianInverseTransposed_;
  };



  // PrismLateralGeometry
  // --------------------

  template< class Traits >
  class PrismLateralGeometry
  : public PrismGeometryInterface< Traits >
  {
    typedef PrismLateralGeometry< Traits > This;
    typedef PrismGeometryInterface< Traits > Base;

  public:
    static const int mydimension = Base::mydimension;
    static const int coorddimension = Base::coorddimension;
    static const int dimension = Base::dimension;

    typedef typename Base::ctype ctype;

    typedef typename Base::LocalCoordinate LocalCoordinate;
    typedef typename Base::GlobalCoordinate GlobalCoordinate;

    typedef typename Base::JacobianInverseTransposed JacobianInverseTransposed;
    typedef typename Base::JacobianTransposed JacobianTransposed;

    typedef typename Traits::template IsLateral< true >::TangentialGeometry TangentialGeometry;
    typedef typename Traits::template IsLateral< true >::NormalGeometry NormalGeometry;

  private:
    dune_static_assert( TangentialGeometry::mydimension + NormalGeometry::mydimension == mydimension,
                        "Dimensions do not match" );
    dune_static_assert( TangentialGeometry::coorddimension + NormalGeometry::coorddimension == coorddimension,
                        "Dimensions do not match" );

    typedef typename TangentialGeometry::GlobalCoordinate TangentialGlobalCoordinate;
    typedef typename TangentialGeometry::LocalCoordinate TangentialLocalCoordinate;

  public:
    PrismLateralGeometry ( TangentialGeometry tangentialGeometry,
                           NormalGeometry normalGeometry )
    : tangentialGeometry_( tangentialGeometry ),
      normalGeometry_( normalGeometry ),
      haveJacT_( false ),
      haveJacInvT_( false )
    {}

    GeometryType type () const
    {
      const unsigned int topologyId = tangentialGeometry().type().id() | (1 << (mydimension - 1));
      return GeometryType(topologyId, mydimension);
    }

    bool affine () const { return tangentialGeometry().affine(); }

    int corners () const { return 2*tangentialGeometry().corners(); }

    GlobalCoordinate corner ( int i ) const
    {
      // get corner of host entity
      const int tCorners = tangentialGeometry().corners();
      TangentialGlobalCoordinate tCorner = tangentialGeometry().corner( i%tCorners );

      // lift coordinate
      GlobalCoordinate ret = map< TangentialGlobalCoordinate, GlobalCoordinate >( tCorner );

      // add position in normal direction and return vector
      typename NormalGeometry::GlobalCoordinate nCorner = normalGeometry().corner( i/tCorners );
      ret[ coorddimension-1 ] = nCorner;
      return ret;
    }

    GlobalCoordinate global ( const LocalCoordinate &local ) const
    {
      // evaluate host geometry's global()
      TangentialLocalCoordinate tLocal
        = map< LocalCoordinate, TangentialLocalCoordinate >( local );

      TangentialGlobalCoordinate tGlobal = tangentialGeometry().global( tLocal );
      GlobalCoordinate ret = map< TangentialGlobalCoordinate, GlobalCoordinate >( tGlobal );

      // add position in normal direction and return vector
      ret[ coorddimension-1 ] = normalGeometry().global( local[ mydimension-1 ] );
      return ret;
    }

    LocalCoordinate local ( const GlobalCoordinate &global ) const
    {
      // evalute host geometry's local() and map to local coordinates
      TangentialGlobalCoordinate tGlobal
        = map< GlobalCoordinate, TangentialGlobalCoordinate >( global );

      TangentialLocalCoordinate tLocal = tangentialGeometry().local( tGlobal );
      LocalCoordinate ret = map< TangentialLocalCoordinate, LocalCoordinate >( tLocal );

      // add position in normal direction and return vector
      ret[ mydimension-1 ] = normalGeometry().local( global[ coorddimension-1 ] );
      return ret;
    }

    ctype integrationElement ( const LocalCoordinate &local ) const
    {
      TangentialLocalCoordinate tLocal
        = map< LocalCoordinate, TangentialLocalCoordinate >( local );
      return tangentialGeometry().integrationElement( tLocal )*normalGeometry().volume();
    }

    ctype volume () const { return tangentialGeometry().volume()*normalGeometry().volume(); }

    GlobalCoordinate center () const
    {
      // lift center of host geometry to global coordinates
      TangentialGlobalCoordinate tCenter = tangentialGeometry().center();
      GlobalCoordinate ret = map< TangentialGlobalCoordinate, GlobalCoordinate >( tCenter );

      // add position in normal direction and return global vector
      typename NormalGeometry::GlobalCoordinate nCenter = normalGeometry().center();
      ret[ TangentialGeometry::coorddimension ] = nCenter;
      return ret;
    }

    const JacobianTransposed &jacobianTransposed ( const LocalCoordinate &local ) const
    {
      if ( !haveJacT_ )
      {
        jacobianTransposed_ = 0;

        TangentialLocalCoordinate tLocal = map< LocalCoordinate, TangentialLocalCoordinate >( local );
        const typename TangentialGeometry::JacobianTransposed &tJacT = tangentialGeometry().jacobianTransposed( tLocal );
        PrismGeometryHelper< typename TangentialGeometry::JacobianTransposed > helper( tJacT );
        helper.fill( jacobianTransposed_ );

        // set last entry
        jacobianTransposed_[ mydimension-1 ][ coorddimension-1 ] = normalGeometry().volume();
        haveJacT_ = affine();
      }
      return jacobianTransposed_;
    }

    const JacobianInverseTransposed &jacobianInverseTransposed ( const LocalCoordinate &local ) const
    {
      if( !haveJacInvT_ )
      {
        jacobianInverseTransposed_ = 0;

        TangentialLocalCoordinate tLocal = map< LocalCoordinate, TangentialLocalCoordinate >( local );
        const typename TangentialGeometry::JacobianInverseTransposed &tJacInvT = tangentialGeometry().jacobianInverseTransposed( tLocal );
        PrismGeometryHelper< typename TangentialGeometry::JacobianInverseTransposed > helper( tJacInvT );
        helper.fill( jacobianInverseTransposed_ );

        // set last entry
        jacobianInverseTransposed_[ coorddimension-1 ][ mydimension-1 ] = 1./normalGeometry().volume();
        haveJacInvT_ = affine();
      }
      return jacobianInverseTransposed_;
    }

    bool isLateral () const { return true; }

  private:
    const TangentialGeometry &tangentialGeometry () const { return tangentialGeometry_; }

    const NormalGeometry &normalGeometry () const { return normalGeometry_; }

    TangentialGeometry tangentialGeometry_;
    NormalGeometry normalGeometry_;

    mutable JacobianTransposed jacobianTransposed_;
    mutable JacobianInverseTransposed jacobianInverseTransposed_;
    mutable bool haveJacT_, haveJacInvT_;
  };



  // PrismHorizontalGeometry
  // ----------------------

  template< class Traits >
  class PrismHorizontalGeometry
  : public PrismGeometryInterface< Traits >
  {
    typedef PrismHorizontalGeometry< Traits > This;
    typedef PrismGeometryInterface< Traits > Base;

  public:
    static const int mydimension = Base::mydimension;
    static const int coorddimension = Base::coorddimension;
    static const int dimension = Base::dimension;

    typedef typename Base::ctype ctype;

    typedef typename Base::LocalCoordinate LocalCoordinate;
    typedef typename Base::GlobalCoordinate GlobalCoordinate;

    typedef typename Base::JacobianInverseTransposed JacobianInverseTransposed;
    typedef typename Base::JacobianTransposed JacobianTransposed;

    typedef typename Traits::template IsLateral< false >::TangentialGeometry TangentialGeometry;
    typedef typename Traits::template IsLateral< false >::NormalGeometry NormalGeometry;

  private:
    dune_static_assert( TangentialGeometry::mydimension + NormalGeometry::mydimension == mydimension,
                        "Dimensions do not match" );
    dune_static_assert( TangentialGeometry::coorddimension + NormalGeometry::coorddimension == coorddimension,
                        "Dimensions do not match" );

    typedef typename TangentialGeometry::GlobalCoordinate TangentialGlobalCoordinate;
    typedef typename TangentialGeometry::LocalCoordinate TangentialLocalCoordinate;

  public:
    PrismHorizontalGeometry ( TangentialGeometry tangentialGeometry,
                              NormalGeometry normalGeometry )
    : tangentialGeometry_( tangentialGeometry ),
      normalGeometry_( normalGeometry ),
      haveJacT_( false ),
      haveJacInvT_( false )
    {}

    GeometryType type () const { return tangentialGeometry().type(); }

    bool affine () const { return tangentialGeometry().affine(); }

    int corners () const { return tangentialGeometry().corners(); }

    GlobalCoordinate corner ( int i ) const
    {
      // get corner of host entity
      assert( i < corners() );
      TangentialGlobalCoordinate tCorner = tangentialGeometry().corner( i );

      // lift coordinate
      GlobalCoordinate ret = map< TangentialGlobalCoordinate, GlobalCoordinate >( tCorner );

      // add position in normal direction and return vector
      ret[ coorddimension-1 ] = normalGeometry().center();
      return ret;
    }

    GlobalCoordinate global ( const LocalCoordinate &local ) const
    {
      TangentialGlobalCoordinate tGlobal = tangentialGeometry().global( local );
      GlobalCoordinate ret = map< TangentialGlobalCoordinate, GlobalCoordinate >( tGlobal );
      ret[ coorddimension-1 ] = normalGeometry().center();
      return ret;
    }

    LocalCoordinate local ( const GlobalCoordinate &global ) const
    {
      // evalute host geometry's local() and map to local coordinates
      TangentialGlobalCoordinate tGlobal
        = map< GlobalCoordinate, TangentialGlobalCoordinate >( global );

      return tangentialGeometry().local( tGlobal );
    }

    ctype integrationElement ( const LocalCoordinate &local ) const
    {
      return tangentialGeometry().integrationElement( local );
    }

    ctype volume () const { return tangentialGeometry().volume(); }

    GlobalCoordinate center () const
    {
      TangentialGlobalCoordinate tCenter = tangentialGeometry().center();
      GlobalCoordinate ret = map< TangentialGlobalCoordinate, GlobalCoordinate >( tCenter );
      ret[ coorddimension-1 ] = normalGeometry().center();
      return ret;
    }

    const JacobianTransposed &jacobianTransposed ( const LocalCoordinate &local ) const
    {
      if ( !haveJacT_ )
      {
        jacobianTransposed_ = 0;

        const typename TangentialGeometry::JacobianTransposed &tJacT = tangentialGeometry().jacobianTransposed( local );
        PrismGeometryHelper< typename TangentialGeometry::JacobianTransposed > helper( tJacT );
        helper.fill( jacobianTransposed_ );

        haveJacT_ = affine();
      }
      return jacobianTransposed_;
    }

    const JacobianInverseTransposed &jacobianInverseTransposed ( const LocalCoordinate &local ) const
    {
      if( !haveJacInvT_ )
      {
        jacobianInverseTransposed_ = 0;

        const typename TangentialGeometry::JacobianInverseTransposed &tJacInvT = tangentialGeometry().jacobianInverseTransposed( local );
        PrismGeometryHelper< typename TangentialGeometry::JacobianInverseTransposed > helper( tJacInvT );
        helper.fill( jacobianInverseTransposed_ );

        haveJacInvT_ = affine();
      }
      return jacobianInverseTransposed_;
    }

    bool isLateral () const { return false; }

  private:
    const TangentialGeometry &tangentialGeometry () const { return tangentialGeometry_; }

    const NormalGeometry &normalGeometry () const { return normalGeometry_; }

    TangentialGeometry tangentialGeometry_;
    NormalGeometry normalGeometry_;

    mutable JacobianTransposed jacobianTransposed_;
    mutable JacobianInverseTransposed jacobianInverseTransposed_;
    mutable bool haveJacT_, haveJacInvT_;
  };

} // namespace Dune

#endif // #ifndef DUNE_GRID_PRISMGRID_GEOMETRY_HH
