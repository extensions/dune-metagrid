#ifndef DUNE_GRID_PRISMGRID_INTERSECTION_HH
#define DUNE_GRID_PRISMGRID_INTERSECTION_HH

#include <dune/common/exceptions.hh>
#include <dune/common/typetraits.hh>

#include <dune/grid/common/capabilities.hh>
#include <dune/grid/common/intersection.hh>

#include "geometry.hh"
#include "gridenums.hh"
#include "tangentialgeometry.hh"
#include "utility.hh"

namespace Dune
{

  // Forward declaration
  // -------------------

  template< class > class PrismLocalTangentialGeometry;



  // PrismIntersection
  // -----------------

  template< class GridImp >
  class PrismIntersection
  {
    typedef PrismIntersection< GridImp > This;

    typedef typename remove_const< GridImp >::type::Traits Traits;

  public:
    typedef typename Traits::Grid Grid;

    typedef typename Traits::ctype ctype;

    static const int dimension =  Traits::dimension;
    static const int dimensionworld =  Traits::dimensionworld;

    typedef typename Traits::template Codim< 0 >::Entity Entity;
    typedef typename Traits::template Codim< 0 >::EntityPointer EntityPointer;
    typedef typename Traits::template Codim< 1 >::Geometry Geometry;
    typedef typename Traits::template Codim< 1 >::LocalGeometry LocalGeometry;

    typedef typename Geometry::GlobalCoordinate GlobalCoordinate;
    typedef typename Geometry::LocalCoordinate LocalCoordinate;

    typedef typename Traits::HostGridView HostGridView;
    typedef typename HostGridView::Intersection HostIntersection;

  private:
    typedef typename Entity::Implementation EntityImp;
    typedef typename EntityPointer::Implementation EntityPointerImp;
    typedef typename Geometry::Implementation GeometryImp;
    typedef typename LocalGeometry::Implementation LocalGeometryImp;

    typedef typename HostIntersection::EntityPointer HostEntityPointer;
    typedef typename HostIntersection::LocalCoordinate HostLocalCoordinate;
    typedef typename HostIntersection::GlobalCoordinate HostGlobalCoordinate;

    struct PrismIntersectionPositionIterator;

  public:
    PrismIntersection ( PrismIntersectionPosition face, const EntityImp &inside, const Grid &grid )
    : grid_( grid ),
      inside_( &inside ),
      hostIntersection_( nullptr ),
      face_( face ),
      count_( -1 )
    {}

    PrismIntersection ( const This &other )
    : grid_( other.grid_ ),
      inside_( other.inside_ ),
      hostIntersection_( nullptr ),
      face_( other.face_ ),
      count_( other.count_ )
    {}

    This &operator= ( const This &other )
    {
      if( this == &other )
        return *this;

      assert( &grid_ == &(other.grid_) );

      inside_ = other.inside_;
      hostIntersection_ = nullptr;
      face_ = other.face_;
      count_ = other.count_;
      return *this;
    }

    EntityPointer inside () const { return EntityPointer( EntityPointerImp( insideEntity() ) ); }

    EntityPointer outside () const;

    bool conforming () const
    {
      return ( face_ == lateralFace ) ? hostIntersection().conforming() : true;
    }

    bool boundary () const
    {
      if( face_ == lateralFace )
        return hostIntersection().boundary();
      return ( face_ == lowerFace ) ? ( subIndex() == 0 ) : ( subIndex() == grid().layers() - 1 );
    }

    bool neighbor() const
    {
      if( face_ == lateralFace )
        return hostIntersection().neighbor();
      return ( face_ == lowerFace ) ? ( subIndex() > 0 ) : ( subIndex() < grid().layers() - 1 );
    }

    int boundaryId () const { return boundary() ? indexInInside()+1 : 0; }

    size_t boundarySegmentIndex () const { return grid().boundarySegmentIndexSet().index( *this ); }


    LocalGeometry geometryInInside () const;
    LocalGeometry geometryInOutside () const;
    Geometry geometry () const;
    GeometryType type () const;

    int indexInInside () const
    {
      return ( face_ == lateralFace ? hostIntersection().indexInInside() : hostIntersections() + (face_ == lowerFace ? 0 : 1 ) );
    }

    int indexInOutside () const
    {
      assert( neighbor() );
      return ( face_ == lateralFace ? hostIntersection().indexInOutside() : hostIntersections() + 1 - (face_ == lowerFace ? 0 : 1 ) );
    }

    GlobalCoordinate outerNormal ( const typename Geometry::LocalCoordinate &local ) const
    {
      return unitOuterNormal( local );
    }

    GlobalCoordinate integrationOuterNormal ( const typename Geometry::LocalCoordinate &local ) const
    {
      GlobalCoordinate ret = unitOuterNormal( local );
      ret *= geometry().integrationElement( local );
      return ret;
    }

    GlobalCoordinate unitOuterNormal ( const typename Geometry::LocalCoordinate &local ) const;

    GlobalCoordinate centerUnitOuterNormal () const;

    bool isLateral () const { return ( face_ == lateralFace ); }

    bool isHorizontal () const { return !isLateral(); }

    const Grid &grid () const { return grid_; }

    const EntityImp &insideEntity () const
    {
      assert( inside_ );
      return *inside_;
    }

    int subIndex () const { return insideEntity().subIndex(); }

    PrismIntersectionPositionIterator &face () { return face_; }

    const PrismIntersectionPositionIterator &face () const { return face_; }

    void invalidate () { hostIntersection_ = nullptr; }

    bool hasHostIntersection () const { return hostIntersection_; }

    const HostIntersection &hostIntersection () const
    {
      assert( hostIntersection_ );
      return *hostIntersection_;
    }

    void provideHostIntersection ( const HostIntersection &hostIntersection )
    {
      assert( face_ == lateralFace );
      hostIntersection_ = &hostIntersection;
    }

  private:
    HostGridView hostGridView () const { return grid_.hostGridView(); }

    int hostIntersections () const;

    const Grid &grid_;
    const EntityImp *inside_;
    const HostIntersection *hostIntersection_;
    PrismIntersectionPositionIterator face_;
    mutable int count_;
  };



  // Implementation of PrismIntersection
  // -----------------------------------

  template< class Grid >
  inline typename PrismIntersection< Grid >::EntityPointer
  PrismIntersection< Grid >::outside () const
  {
    assert( neighbor() );

    if( face_ == lateralFace )
    {
      HostEntityPointer hostEp = hostIntersection().outside();
      return EntityPointer( EntityPointerImp( hostEp, subIndex(), grid() ) );
    }

    if( face_ == lowerFace )
      return EntityPointer( EntityPointerImp( insideEntity().hostEntity(), subIndex() - 1, grid() ) );
    else
      return EntityPointer( EntityPointerImp( insideEntity().hostEntity(), subIndex() + 1, grid() ) );
  }


  template< class Grid >
  inline typename PrismIntersection< Grid >::LocalGeometry
  PrismIntersection< Grid >::geometryInInside () const
  {
    if( face_ == lateralFace )
      return LocalGeometry( LocalGeometryImp( hostIntersection().geometryInInside(), grid().normalGrid().template localGeometry< 0 >( 0 ) ) );
    else
    {
      typedef typename HostGridView::template Codim< 0 >::Geometry HostGeometry;
      typedef PrismLocalTangentialGeometry< HostGeometry > TangentialGeometry;
      return LocalGeometry( LocalGeometryImp( TangentialGeometry( insideEntity().hostEntity().geometry() ), grid().normalGrid().template localGeometry< 1 >( face_ == lowerFace ? 0 : 1 ) ) );
    }
  }


  template< class Grid >
  inline typename PrismIntersection< Grid >::LocalGeometry
  PrismIntersection< Grid >::geometryInOutside () const
  {
    assert( neighbor() );
    if( face_ == lateralFace )
      return LocalGeometry( LocalGeometryImp( hostIntersection().geometryInOutside(), grid().normalGrid().template localGeometry< 0 >( 0 ) ) );
    else
    {
      typedef typename HostGridView::template Codim< 0 >::Geometry HostGeometry;
      typedef PrismLocalTangentialGeometry< HostGeometry > TangentialGeometry;
      return LocalGeometry( LocalGeometryImp( TangentialGeometry( insideEntity().hostEntity().geometry() ), grid().normalGrid().template localGeometry< 1 >( face_ == lowerFace ? 1 : 0 ) ) );
    }
  }


  template< class Grid >
  inline typename PrismIntersection< Grid >::Geometry
  PrismIntersection< Grid >::geometry () const
  {
    if( face_ == lateralFace )
      return Geometry( GeometryImp( hostIntersection().geometry(), grid().normalGrid().template geometry< 0 >( subIndex() ) ) );
    else
      return Geometry( GeometryImp( insideEntity().hostEntity().geometry(), grid().normalGrid().template geometry< 1 >( subIndex() + (face_ == lowerFace ? 0 : 1 ) ) ) );
  }


  template< class Grid >
  inline GeometryType PrismIntersection< Grid >::type () const
  {
    if( face_ == lateralFace )
    {
      const unsigned int topologyId
        = hostIntersection().type().id() | ( 1 << (dimension-2) );
      return GeometryType( topologyId, dimension-1 );
    }
    else
      return insideEntity().hostEntity().type();
  }


  template< class Grid >
  inline typename PrismIntersection< Grid >::GlobalCoordinate
  PrismIntersection< Grid >::unitOuterNormal ( const LocalCoordinate &local ) const
  {
    if( face_ == lateralFace )
    {
      HostLocalCoordinate hLocal = map< LocalCoordinate, HostLocalCoordinate >( local );
      HostGlobalCoordinate hGlobal = hostIntersection().unitOuterNormal( hLocal );
      return map< HostGlobalCoordinate, GlobalCoordinate >( hGlobal );
    }
    else
    {
      GlobalCoordinate ret( 0 );
      ret[ Geometry::coorddimension-1 ] = ( face_ == lowerFace ) ? -1 : 1;
      return ret;
    }
  }


  template< class Grid >
  inline typename PrismIntersection< Grid >::GlobalCoordinate
  PrismIntersection< Grid >::centerUnitOuterNormal () const
  {
    if( face_ == lateralFace )
      return map< HostGlobalCoordinate, GlobalCoordinate >( hostIntersection().centerUnitOuterNormal() );
    else
    {
      GlobalCoordinate ret( 0 );
      ret[ Geometry::coorddimension-1 ] = ( face_ == lowerFace ) ? -1 : 1;
      return ret;
    }
  }


  template< class Grid >
  inline int PrismIntersection< Grid >::hostIntersections () const
  {
    if ( count_ < 0 )
    {
      if( Dune::Capabilities::isLeafwiseConforming< Grid >::v )
        count_ = insideEntity().hostEntity().template count< 1 >();
      else
      {
        typedef typename HostGridView::template Codim< 0 >::Entity HostEntity;
        typedef typename HostGridView::IntersectionIterator HostIntersectionIterator;

        const HostEntity &hostEntity = insideEntity().hostEntity();
        const HostIntersectionIterator end = hostGridView().iend( hostEntity );

        count_ = 0;
        for( HostIntersectionIterator it = hostGridView().ibegin( hostEntity ); it != end; ++it )
          ++count_;
      }
    }
    return count_;
  }



  // Implementation of PrismIntersectionPositionIterator
  // ---------------------------------------------------

  template< class Grid >
  struct PrismIntersection< Grid >::PrismIntersectionPositionIterator
  {
    PrismIntersectionPositionIterator( PrismIntersectionPosition face )
    : face_( face )
    {}

    PrismIntersectionPositionIterator &operator++ ()
    {
      if( face_ == lateralFace )
        return *this;
      face_ = ( face_ == upperFace ) ? lateralFace : upperFace;
      return *this;
    }

    operator PrismIntersectionPosition () const { return face_; }

  private:
    PrismIntersectionPosition face_;
  };

} // end namespace Dune

#endif // #ifndef DUNE_GRID_PRISMGRID_INTERSECTION_HH
