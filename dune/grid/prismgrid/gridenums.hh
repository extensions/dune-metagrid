#ifndef DUNE_GRID_PRISMGRID_GRIDENUMS_HH
#define DUNE_GRID_PRISMGRID_GRIDENUMS_HH

namespace Dune
{

#ifndef DOXYGEN
  enum PrismIntersectionPosition
  {
    lowerFace = 0,  // Lower intersection
    upperFace = 1,  // Upper intersection
    lateralFace = 2 // Lateral intersection
  };
#endif // #ifndef DOXYGEN



  /** \ingroup ColumnIterator 
   *
   *  \brief Provide names for direction object used in
   *         PrismColumnIterator.
   */
  enum ColumnIteratorType
  {
    up = 1,         //!< From lower to upper entities
    down = -1       //!< From upper to lower entities
  };

} // namespace Dune

#endif // #ifndef DUNE_GRID_PRISMGRID_GRIDENUMS_HH
