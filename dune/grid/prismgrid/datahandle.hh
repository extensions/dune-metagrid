#ifndef DUNE_GRID_PRISMGRID_DATAHANDLE_HH
#define DUNE_GRID_PRISMGRID_DATAHANDLE_HH

#include <dune/common/exceptions.hh>
#include <dune/common/typetraits.hh>

#include <dune/grid/common/datahandleif.hh>

namespace Dune
{

  // PrismDataHandle
  // ---------------

  template< class WrappedHandle, class GridImp >
  class PrismDataHandle
  : public CommDataHandleIF< PrismDataHandle< WrappedHandle, GridImp >, typename WrappedHandle::DataType >
  {
    typedef typename remove_const< GridImp >::type::Traits Traits;

  public:
    typedef typename Traits::Grid Grid;

  private:
    template< int codim >
    struct Codim
    {
      typedef typename Traits::HostGrid::template Codim< codim >::Entity HostEntity;
      typedef typename Grid::template Codim< codim >::ColumnIterator ColumnIterator;
    };

  public:
    PrismDataHandle ( WrappedHandle &handle, const Grid &grid )
    : wrappedHandle_( handle ),
      grid_( grid )
    {}

    bool contains ( int dim, int codim ) const
    {
      const int dimension = dim+1;
      assert( dimension == Grid::dimension );
      return ( wrappedHandle_.contains( dimension, codim ) || wrappedHandle_.contains( dimension, codim+1 ) );
    }

    bool fixedSize ( int dim, int codim ) const
    {
      const int dimension = dim+1;
      assert( dimension == Grid::dimension );
      return wrappedHandle_.fixedSize( dimension, codim ) && wrappedHandle_.fixedSize( dimension, codim+1 );
    }

    size_t size ( const typename Codim< 0 >::HostEntity &hostEntity ) const
    {
      size_t size = 0;
      typedef typename Codim< 0 >::ColumnIterator ColumnIterator;
      const ColumnIterator end = grid_.template cend< 0 >( hostEntity );
      for( ColumnIterator it = grid_.template cbegin< 0 >( hostEntity ); it != end; ++it )
        size += wrappedHandle_.size( *it );
      return size;
    }

    template< class Entity >
    size_t size ( const Entity &entity ) const
    {
      DUNE_THROW( InvalidStateException,
                  "PrismGrid does not provide parallel support for entities of higher codimensions" );
    }

    template< class MessageBuffer >
    void gather ( MessageBuffer &buffer, const typename Codim< 0 >::HostEntity &hostEntity ) const
    {
      typedef typename Codim< 0 >::ColumnIterator ColumnIterator;
      const ColumnIterator end = grid_.template cend< 0 >( hostEntity );
      for( ColumnIterator it = grid_.template cbegin< 0 >( hostEntity ); it != end; ++it )
        wrappedHandle_.gather( buffer, *it );
    }

    template< class MessageBuffer, class Entity >
    void gather ( MessageBuffer &buffer, const Entity &entity ) const
    {
      DUNE_THROW( InvalidStateException,
                  "PrismGrid does not provide parallel support for entities of higher codimensions" );
    }

    template< class MessageBuffer >
    void scatter ( MessageBuffer &buffer, const typename Codim< 0 >::HostEntity &hostEntity, size_t size )
    {
      typedef typename Codim< 0 >::ColumnIterator ColumnIterator;
      const ColumnIterator end = grid_.template cend< 0 >( hostEntity );
      for( ColumnIterator it = grid_.template cbegin< 0 >( hostEntity ); it != end; ++it )
        wrappedHandle_.scatter( buffer, *it, size );
    }

    template< class MessageBuffer, class Entity >
    void scatter ( MessageBuffer &buffer, const Entity &entity, size_t size )
    {
      DUNE_THROW( InvalidStateException,
                  "PrismGrid does not provide parallel support for entities of higher codimensions" );
    }

  private:
    WrappedHandle &wrappedHandle_;
    const Grid &grid_;
  };

} // namespace Dune

#endif // #ifndef DUNE_GRID_PRISMGRID_DATAHANDLE_HH
