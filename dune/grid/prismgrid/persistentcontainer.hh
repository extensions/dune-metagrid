#ifndef DUNE_PRISMGRID_PERSISTENTCONTAINER_HH
#define DUNE_PRISMGRID_PERSISTENTCONTAINER_HH

#include <vector>

#include <dune/grid/utility/persistentcontainer.hh>
#include <dune/grid/utility/persistentcontainervector.hh>

#include "declaration.hh"

namespace Dune
{

  // PersistentContainerWrapper
  // --------------------------

  template< class HostGrid, class T >
  class PersistentContainer< PrismGrid< HostGrid >, T > 
  : public PersistentContainerVector< PrismGrid< HostGrid >, typename PrismGrid< HostGrid >::LeafIndexSet, std::vector< T > >
  {
    typedef PersistentContainerVector< PrismGrid< HostGrid >, typename PrismGrid< HostGrid >::LeafIndexSet, std::vector< T > > Base;

  public:
    typedef typename Base::Grid Grid;
    typedef typename Base::Value Value;

    PersistentContainer ( const Grid &grid, int codim, const Value &value = Value() )
    : Base( grid.leafIndexSet(), codim, value )
    {}
  };

} // namespace Dune

#endif // #ifndef DUNE_PERSISTENTCONTAINERWRAPPER_HH
