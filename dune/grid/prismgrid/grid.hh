#ifndef DUNE_GRID_PRISMGRID_GRID_HH
#define DUNE_GRID_PRISMGRID_GRID_HH

#include<cassert>

#include <dune/common/exceptions.hh>
#include <dune/common/parallel/communication.hh>

#include <dune/grid/common/grid.hh>

#include "boundarysegmentindexset.hh"
#include "capabilities.hh"
#include "datahandle.hh"
#include "gridenums.hh"
#include "gridfamily.hh"
#include "hostcorners.hh"
#include "hostgridaccess.hh"
#include "normalgrid.hh"
#include "persistentcontainer.hh"

namespace Dune
{

  // PrismGrid
  // ---------

  /** \ingroup GridInterface
   *
   * \brief The grid base class
   *
   * \tparam  HostGridImp  Host grid type
   */
  template< class HostGridImp >
  class PrismGrid
  : public GridDefaultImplementation< PrismGridFamily< HostGridImp >::dimension,
                                      PrismGridFamily< HostGridImp >::dimensionworld,
                                      typename PrismGridFamily< HostGridImp >::ctype,
                                      PrismGridFamily< HostGridImp >
                                    >
  {
    typedef PrismGrid< HostGridImp > This;

  public:
    //! \brief grid family
    typedef PrismGridFamily< HostGridImp > GridFamily;
    //! \brief grid traits
    typedef typename GridFamily::Traits Traits;

    //! \brief coordinate type
    typedef typename GridFamily::ctype ctype;

    //! \brief grid dimension
    static const int dimension = Traits::dimension;
    //! \brief world dimension
    static const int dimensionworld = Traits::dimensionworld;

#ifndef DOXYGEN
    typedef typename Traits::LevelIndexSet LevelIndexSet;
    typedef typename Traits::LeafIndexSet LeafIndexSet;

    typedef typename Traits::GlobalIdSet GlobalIdSet;
    typedef typename Traits::LocalIdSet LocalIdSet;
    typedef typename Traits::Communication Communication;
#endif // #ifndef DOXYGEN

    template< int codim >
    struct Codim
    : public Traits::template Codim< codim >
    {};

    template< PartitionIteratorType pitype >
    struct Partition
    : public Traits::template Partition< pitype >
    {};

#ifndef DOXYGEN
    typedef typename Partition< All_Partition >::LevelGridView LevelGridView;
    typedef typename Partition< All_Partition >::LeafGridView LeafGridView;
#endif // #ifndef DOXYGEN

    //! \brief host grid type
    typedef typename GridFamily::HostGrid HostGrid;
    //! \brief host grid leaf view type
    typedef typename GridFamily::HostGridView HostGridView;

#ifndef DOXYGEN
    typedef PrismNormalGrid< ctype > NormalGrid;
    typedef typename NormalGrid::Interval Interval;
#endif // #ifndef DOXYGEN

    //! \brief type of boundary segement index set
    typedef PrismBoundarySegmentIndexSet< const This > BoundarySegmentIndexSet;

  public:
    //! \brief constructor
    PrismGrid ( HostGrid &hostGrid, ctype a, ctype b, int cells )
    : hostGrid_( hostGrid ),
      normalGrid_( a, b, cells ),
      boundarySegmentIndexSet_( nullptr )
    {}

#ifndef DOXYGEN
    ~PrismGrid ()
    {
      if( boundarySegmentIndexSet_ )
        delete boundarySegmentIndexSet_;
    }

    int maxLevel () const { return 0; }

    int size ( int level, int codim ) const
    {
      assert( level == 0 );
      return size( codim );
    }

    int size ( int codim ) const { return leafIndexSet().size( codim ); }

    int size ( int level, GeometryType type ) const
    {
      assert( level == 0 );
      return size( type );
    }

    int size ( GeometryType type ) const { return leafIndexSet().size( type ); }

    size_t numBoundarySegments () const { return boundarySegmentIndexSet().size(); }

    template< int codim >
    typename Codim< codim >::template Partition< All_Partition >::LevelIterator
    lbegin ( int level ) const
    {
      return lbegin< codim, All_Partition >( level );
    }

    template< int codim, PartitionIteratorType pitype >
    typename Codim< codim >::template Partition< pitype >::LevelIterator
    lbegin ( int level ) const
    {
      assert( level == 0 );
      return leafbegin< codim, pitype >();
    }

    template< int codim >
    typename Codim< codim >::template Partition< All_Partition >::LevelIterator
    lend ( int level ) const
    {
      return lend< codim, All_Partition >( level );
    }

    template< int codim, PartitionIteratorType pitype >
    typename Codim< codim >::template Partition< pitype >::LevelIterator
    lend ( int level ) const
    {
      assert( level == 0 );
      return leafend< codim, pitype >();
    }

    template< int codim >
    typename Codim< codim >::template Partition< All_Partition >::LeafIterator
    leafbegin () const
    {
      return leafbegin< codim, All_Partition >();
    }

    template< int codim, PartitionIteratorType pitype >
    typename Codim< codim >::template Partition< pitype >::LeafIterator
    leafbegin () const
    {
      typedef typename Codim< codim >::template Partition< pitype >::LeafIterator::Implementation IteratorImp;
      return IteratorImp::begin( *this );
    }

    template< int codim >
    typename Codim< codim >::template Partition< All_Partition >::LeafIterator
    leafend () const
    {
      return leafend< codim, All_Partition >();
    }

    template< int codim, PartitionIteratorType pitype >
    typename Codim< codim >::template Partition< pitype >::LeafIterator
    leafend () const
    {
      typedef typename Codim< codim >::template Partition< pitype >::LeafIterator::Implementation IteratorImp;
      return IteratorImp::end( *this );
    }

#endif // #ifndef DOXYGEN

    /** \brief Return begin iterator over a individual column.
     *
     *  \tparam  codim  codimension \f$c \in \{0, dimension\}\f$
     *
     *  \param[in]  hostEntity  A host entity
     *  \param[in]  direction   Iterator direction for accessing entities in colum,
     *              default is from lower to upper entities.
     *
     *  \returns column iterator
     */
    template< int codim >
    typename Codim< codim >::ColumnIterator
    cbegin ( const typename HostGridView::template Codim< 0 >::Entity &hostEntity,
             ColumnIteratorType direction = up ) const
    {
      typedef typename Codim< codim >::ColumnIterator::Implementation IteratorImp;
      return IteratorImp::begin( hostEntity, *this, direction );
    }

    /** \brief Return end iterator over a individual column.
     *
     *  \tparam  codim  codimension \f$c \in \{0, dimension\}\f$
     *
     *  \param[in]  hostEntity  A host entity
     *  \param[in]  direction   Iterator direction for accessing entities in colum,
     *              default is from lower to upper entities.
     *
     *  \returns column iterator
     */
    template< int codim >
    typename Codim< codim >::ColumnIterator
    cend ( const typename HostGridView::template Codim< 0 >::Entity &hostEntity,
           ColumnIteratorType direction = up ) const
    {
      typedef typename Codim< codim >::ColumnIterator::Implementation IteratorImp;
      return IteratorImp::end( hostEntity, *this, direction );
    }

#ifndef DOXYGEN
    const GlobalIdSet &globalIdSet () const
    {
      if( !globalIdSet_ )
        globalIdSet_ = GlobalIdSet( hostGrid().globalIdSet() );
      return globalIdSet_;
    }

    const LocalIdSet &localIdSet () const
    {
      if( !localIdSet_ )
        localIdSet_ = LocalIdSet( hostGrid().localIdSet() );
      return localIdSet_;
    }

    const LeafIndexSet &leafIndexSet () const
    {
      if( !leafIndexSet_ )
        leafIndexSet_ = LeafIndexSet( hostGrid().leafIndexSet(), layers() );
      return leafIndexSet_;
    }

    const LevelIndexSet &levelIndexSet ( int level ) const
    {
      assert( level == 0 );
      return leafIndexSet();
    }

#endif // #ifndef DOXYGEN

    //! \brief return boundary segement index set
    const BoundarySegmentIndexSet &boundarySegmentIndexSet () const
    {
      if( !boundarySegmentIndexSet_ )
        boundarySegmentIndexSet_ = new BoundarySegmentIndexSet( *this );
      return *boundarySegmentIndexSet_;
    }

#ifndef DOXYGEN

    void globalRefine ( int refCount ) {}

    bool mark ( int refCount, const typename Codim< 0 >::Entity &entity ) { return false; }
    int getMark ( const typename Codim< 0 >::Entity &entity ) const { return 0; }

    bool preAdapt () { return false; }

    bool adapt() { return false; }

    void postAdapt () {}

    int overlapSize ( int level, int codim ) const
    {
      assert( level == 0 );
      return overlapSize( codim );
    }

    int overlapSize ( int codim ) const;

    int ghostSize ( int level, int codim ) const
    {
      assert( level == 0 );
      return ghostSize( codim );
    }

    int ghostSize ( int codim ) const;

    template< class DataHandleImp, class DataTypeImp >
    void communicate ( CommDataHandleIF< DataHandleImp, DataTypeImp > &dataHandle,
                       InterfaceType iftype,
                       CommunicationDirection dir,
                       int level ) const
    {
      assert( level == 0 );
      return commununicate( dataHandle, iftype, dir );
    }

    template< class DataHandleImp, class DataTypeImp >
    void  communicate ( CommDataHandleIF< DataHandleImp, DataTypeImp > &dataHandle,
                        InterfaceType iftype,
                        CommunicationDirection dir ) const
    {
      typedef CommDataHandleIF< DataHandleImp, DataTypeImp > HostDataHandle;
      PrismDataHandle< HostDataHandle, const This > dataHandleWrapper( dataHandle, *this );
      hostGrid().communicate( dataHandleWrapper, iftype, dir );
    }

    const Communication &comm () const { return hostGrid().comm(); }

    bool loadBalance() { return hostGrid().loadBalance(); }

    template< class DataHandleImp >
    bool loadBalance ( DataHandleImp &dataHandle )
    {
      typedef CommDataHandleIF< DataHandleImp, typename DataHandleImp::DataType > HostDataHandle;
      PrismDataHandle< HostDataHandle, const This > dataHandleWrapper( dataHandle, *this );
      return hostGrid().loadBalance( dataHandleWrapper );
    }

    template< class EntitySeed >
    typename Codim< EntitySeed::codimension >::EntityPointer
    entityPointer ( const EntitySeed &seed ) const;

    template < GrapeIOFileFormatType ftype >
    bool writeGrid ( const std::string filename, ctype time ) const
    {
      DUNE_THROW( NotImplemented, "Method writeGrid() not implemented yet" );
    }

    template < GrapeIOFileFormatType ftype >
    bool readGrid ( const std::string filename, ctype &time )
    {
      DUNE_THROW( NotImplemented, "Method readGrid() not implemented yet" );
    }

#endif // #ifndef DOXYGEN

    //! \brief return reference to host grid
    HostGrid &hostGrid () { return hostGrid_; }
    //! \brief return const reference to host grid
    const HostGrid &hostGrid () const { return hostGrid_; }

    //! \brief return host grid leaf view
    HostGridView hostGridView () const { return hostGrid().leafGridView(); }

    //! \brief return number of layers
    int layers () const { return normalGrid().macroCells(); }

#ifndef DOXYGEN
    const NormalGrid &normalGrid () const { return normalGrid_; }

    const Interval &interval () const { return normalGrid().interval(); }
#endif // DOXYGEN


  private:
    PrismGrid ( const PrismGrid & );
    PrismGrid &operator = ( const PrismGrid & );

    HostGrid &hostGrid_;
    NormalGrid normalGrid_;

    mutable LeafIndexSet leafIndexSet_;
    mutable GlobalIdSet globalIdSet_;
    mutable LocalIdSet localIdSet_;
    mutable BoundarySegmentIndexSet *boundarySegmentIndexSet_;
  };



#ifndef DOXYGEN

  // Implementation of PrismGrid
  // ---------------------------

  template< class HostGrid >
  inline int PrismGrid< HostGrid >::overlapSize ( int codim ) const
  {
    if( codim == 0 )
      return hostGridView().overlapSize( 0 ) * layers();

    if( codim == dimension )
      return hostGridView().overlapSize( HostGridView::dimension ) * ( layers() + 1 );

    int ret = hostGridView().overlapSize( codim ) * layers();
    ret += hostGridView().overlapSize( codim-1 ) * ( layers() + 1 );
    return ret;
  }


  template< class HostGrid >
  inline int PrismGrid< HostGrid >::ghostSize ( int codim ) const
  {
    if( codim == 0 )
      return hostGridView().ghostSize( 0 ) * layers();

    if( codim == dimension )
      return hostGridView().ghostSize( HostGridView::dimension ) * ( layers() + 1 );

    int ret = hostGridView().ghostSize( codim ) * layers();
    ret += hostGridView().ghostSize( codim-1 ) * ( layers() + 1 );
    return ret;
  }


  template< class HostGrid >
  template< class EntitySeed >
  inline typename PrismGrid< HostGrid >::template Codim< EntitySeed::codimension >::EntityPointer
  PrismGrid< HostGrid >::entityPointer ( const EntitySeed &seed ) const
  {
    typedef typename Codim< EntitySeed::codimension >::EntityPointer EntityPointer;
    typedef typename EntityPointer::Implementation EntityPointerImp;

    const typename EntitySeed::HostEntitySeed &hostSeed = seed.hostSeed();
    const typename EntityPointerImp::HostEntityPointer hostEntityPointer = hostGrid().entityPointer( hostSeed );

    return EntityPointer( EntityPointerImp( hostEntityPointer, seed.subIndex(), *this ) );
  }

#endif // #ifndef DOXYGEN

} // namespace Dune

#endif // #ifndef DUNE_GRID_PRISMGRID_GRID_HH
