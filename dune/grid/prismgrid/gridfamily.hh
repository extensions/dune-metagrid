#ifndef DUNE_GRID_PRISMGRID_GRIDFAMILY_HH
#define DUNE_GRID_PRISMGRID_GRIDFAMILY_HH

#include <dune/grid/common/defaultgridview.hh>
#include <dune/grid/common/grid.hh>

#include "columniterator.hh"
#include "declaration.hh"
#include "entity.hh"
#include "entityiterator.hh"
#include "entitypointer.hh"
#include "entityseed.hh"
#include "geometry.hh"
#include "hierarchiciterator.hh"
#include "idset.hh"
#include "indexset.hh"
#include "intersection.hh"
#include "intersectioniterator.hh"

namespace Dune
{

  // PrismGridFamily
  // ---------------

  /** \ingroup GridInterface
   *
   * \brief The grid family
   *
   * \tparam  HostGridImp  Host grid type
  */
  template< class HGrid >
  struct PrismGridFamily
  {
    //! \brief host grid type
    typedef HGrid HostGrid;
    //! \brief host grid view type used
    typedef typename HostGrid::LeafGridView HostGridView;

    //! \brief grid dimension
    static const int dimension = HostGridView::dimension + 1;
    //! \brief world dimension
    static const int dimensionworld = HostGridView::dimensionworld + 1;
    //! \brief coordinate type
    typedef typename HostGridView::ctype ctype;

    //! \brief this type
    typedef PrismGridFamily< HostGrid > GridFamily;

    //! \brief traits class
    struct Traits
    {
      //! \brief host grid type
      typedef HGrid HostGrid;
      //! \brief host grid view type used
      typedef typename HostGrid::LeafGridView HostGridView;

      //! \brief grid dimension
      static const int dimension = HostGridView::dimension + 1;
      //! \brief world dimension
      static const int dimensionworld = HostGridView::dimensionworld + 1;
      //! \brief coordinate type
      typedef typename HostGridView::ctype ctype;

      //! \brief type of PrismGrid
      typedef Dune::PrismGrid< HostGrid > Grid;

#ifndef DOXYGEN
      typedef typename HostGrid::Communication Communication;

      typedef Dune::Intersection< const Grid, PrismIntersection< const Grid > > LeafIntersection;
      typedef Dune::Intersection< const Grid, PrismIntersection< const Grid > > LevelIntersection;

      typedef Dune::IntersectionIterator
        < const Grid, PrismIntersectionIterator< const Grid >, PrismIntersection< const Grid > >
        LeafIntersectionIterator;
      typedef Dune::IntersectionIterator
        < const Grid, PrismIntersectionIterator< const Grid >, PrismIntersection< const Grid > >
        LevelIntersectionIterator;

      typedef Dune::EntityIterator< 0, const Grid, PrismHierarchicIterator< const Grid > >
        HierarchicIterator;

      typedef PrismIndexSet< const Grid, typename HostGrid::LeafIndexSet > LeafIndexSet;
      typedef LeafIndexSet LevelIndexSet;

      typedef PrismIdSet< const Grid, typename HostGrid::GlobalIdSet > GlobalIdSet;
      typedef PrismIdSet< const Grid, typename HostGrid::LocalIdSet > LocalIdSet;
#endif // #ifndef DOXYGEN

      template< int codim >
      class Codim
      {
        typedef PrismEntityPointerTraits< codim, const Grid > EntityPointerTraits;

      public:
#ifndef DOXYGEN
        typedef Dune::Geometry< dimension - codim, dimensionworld, const Grid, PrismGeometry > Geometry;
        typedef Dune::Geometry< dimension - codim, dimension, const Grid, PrismLocalGeometry > LocalGeometry;

        typedef PrismEntityPointer< EntityPointerTraits > EntityPointerImp;
        typedef Dune::EntityPointer< const Grid, EntityPointerImp > EntityPointer;

        typedef Dune::Entity< codim, dimension, const Grid, PrismEntity > Entity;

        typedef PrismEntitySeed< codim, const Grid> EntitySeed;

        template< PartitionIteratorType pitype >
        struct Partition
        {
          typedef Dune::EntityIterator
            < codim, const Grid, PrismEntityIterator< codim, pitype, const Grid > > LevelIterator;
          typedef Dune::EntityIterator
            < codim, const Grid, PrismEntityIterator< codim, pitype, const Grid > > LeafIterator;
        };

        typedef typename Partition< All_Partition >::LevelIterator LevelIterator;
        typedef typename Partition< All_Partition >::LeafIterator LeafIterator;
#endif // #ifndef DOXYGEN

        //! \brief column iterator type
        typedef Dune::EntityIterator
          < codim, const Grid, PrismColumnIterator< codim, const Grid > > ColumnIterator;

      };

#ifndef DOXYGEN
      template< PartitionIteratorType pitype >
      struct Partition
      {
        typedef Dune::GridView< DefaultLevelGridViewTraits< const Grid, pitype > > LevelGridView;
        typedef Dune::GridView< DefaultLeafGridViewTraits< const Grid, pitype > > LeafGridView;
      };

      typedef typename Partition< All_Partition >::LevelGridView LevelGridView;
      typedef typename Partition< All_Partition >::LeafGridView LeafGridView;

      struct Map
      {
        template< int codim >
        class Codim
        {
          template< int cd, bool lateral >
          struct Value
          {
            static const int v = ( lateral? codim - 1 : codim );;
            dune_static_assert( v >= 0 && v < HostGrid::dimension, "Invalid template argument" );
          };

          template< bool lateral >
          struct Value< 0, lateral >
          {
            dune_static_assert( lateral, "Invalid template argument" );
            static const int v = 0;
          };

          template< bool lateral >
          struct Value< HostGrid::dimension+1, lateral >
          {
            dune_static_assert( !lateral, "Invalid template argument" );
            static const int v = HostGrid::dimension;
          };

        public:
          template< bool isLateral >
          struct IsLateral
          {
            static const int v = Value< codim, isLateral >::v;
            dune_static_assert( v >= 0, "Invalid template argument" );
          };

          static const int v = Value< codim, codim == 0 >::v;
        };
      };
#endif // #ifndef DOXYGEN
    };
  };

} // end namespace Dune

#endif // #ifndef DUNE_GRID_PRISMGRID_GRIDFAMILY_HH
