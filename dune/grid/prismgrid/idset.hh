#ifndef DUNE_GRID_PRISMGRID_IDSET_HH
#define DUNE_GRID_PRISMGRID_IDSET_HH

#include <ostream>

#include <dune/common/typetraits.hh>

#include <dune/grid/common/indexidset.hh>

namespace Dune
{

  // PrismIdType
  // -----------

  template< class HostIdType >
  class PrismIdType
  {
    typedef PrismIdType< HostIdType > This;

  public:
    typedef HostIdType FirstType;
    typedef int SecondType;

    PrismIdType () {}

    PrismIdType ( const HostIdType &hostId, int subIndex )
    : hostId_( hostId ),
      subIndex_( subIndex )
    {}

    bool operator== ( const This &other ) const { return ( second() == other.second() && first() == other.first() ); }
    bool operator!= ( const This &other ) const { return !( *this == other ); }

    bool operator< ( const This &other ) const
    {
      if( second() == other.second() )
        return ( first() < other.first() );
      else
        return ( second() < other.second() );
    }

    FirstType &first () { return hostId_; }
    const FirstType &first () const { return hostId_; }

    SecondType &second () { return subIndex_; }
    const SecondType &second () const { return subIndex_; }

  private:
    HostIdType hostId_;
    int subIndex_;
  };

  template< class HostIdType >
  std::ostream &operator<< ( std::ostream &os, const PrismIdType< HostIdType > &id )
  {
    os << "( " << id.first() << ", " << id.second() <<" )";
    return os;
  }



  // PrismIdSet
  // ----------

  template< class GridImp, class HostIdSetImp >
  class PrismIdSet
  : public Dune::IdSet< GridImp, PrismIdSet< GridImp, HostIdSetImp >, PrismIdType< typename HostIdSetImp::IdType > >
  {
    typedef PrismIdSet< GridImp, HostIdSetImp > This;
    typedef Dune::IdSet< GridImp, PrismIdSet< GridImp, HostIdSetImp >, PrismIdType< typename HostIdSetImp::IdType > > Base;

    typedef typename remove_const< GridImp >::type::Traits Traits;
    typedef typename Traits::ctype ctype;
    typedef typename Traits::HostGrid HostGrid;

  public:
    typedef GridImp Grid;
    typedef HostIdSetImp HostIdSet;

    typedef typename Base::IdType IdType;

    template< int codim >
    struct Codim
    {
      typedef typename Traits::template Codim< codim >::Entity Entity;
    };

    PrismIdSet () : hostIdSet_( nullptr ) {}

    explicit PrismIdSet ( const HostIdSet &hostIdSet ) : hostIdSet_( &hostIdSet ) {}

    PrismIdSet ( const This &other ) : hostIdSet_( other.hostIdSet_ ) {}

    This &operator=( const This &other )
    {
      hostIdSet_ = other.hostIdSet_;
      return *this;
    }

    template< class Entity >
    IdType id ( const Entity &entity ) const { return id< Entity::codimension >( entity ); }

    template< int codim >
    IdType id ( const typename Codim< codim >::Entity &entity ) const
    {
      const typename HostIdSet::IdType hostId = hostIdSet().id( entity.impl().hostEntity() );
      const int subIndex = entity.impl().subIndex();
      return IdType( hostId, subIndex );
    }

    IdType subId ( const typename Codim< 0 >::Entity &entity, int i, unsigned int codim ) const;

    const HostIdSet &hostIdSet () const
    {
      assert( hostIdSet_ );
      return *hostIdSet_;
    }

    operator bool () const { return hostIdSet_; }

  private:
    const HostIdSet *hostIdSet_;
  };



  // Implementation of PrismIdSet
  // ----------------------------

  template< class GridImp, class HostIdSetImp >
  inline typename PrismIdSet< GridImp, HostIdSetImp >::IdType
  PrismIdSet< GridImp, HostIdSetImp >::subId ( const typename Codim< 0 >::Entity &entity, int i, unsigned int codim ) const
  {
    if( codim == 0 )
      return id( entity );

    // get host entity
    typedef typename HostGrid::template Codim< 0 >::Entity HostEntity;
    const HostEntity &hostEntity = entity.impl().hostEntity();

    if( codim == (unsigned int) Grid::dimension )
    {
      // compute vertex index
      int count = hostEntity.template count< HostGrid::dimension >();
      const typename HostIdSet::IdType hostId = hostIdSet().subId( hostEntity, i%count, HostGrid::dimension );
      const int subIndex = entity.impl().subIndex() + i/count;
      return IdType( hostId, subIndex );
    }

    // get host reference element
    const ReferenceElement< ctype, HostGrid::dimension > &hostReferenceElement
      = ReferenceElements< ctype, HostGrid::dimension >::general( hostEntity.type() );

    int count = hostReferenceElement.size( codim );
    const bool isRegular = ( i < count );

    const int subIndex = entity.impl().subIndex();
    if ( isRegular )
      return IdType( hostIdSet().subId( hostEntity, i , codim ), subIndex );
    else
    {
      i -= count;
      count = hostReferenceElement.size( codim-1 );
      return IdType( hostIdSet().subId( hostEntity, i%count, codim-1 ), subIndex + i/count );
    }
  }

} // namespace Dune

#endif // #ifndef DUNE_GRID_PRISMGRID_IDSET_HH
