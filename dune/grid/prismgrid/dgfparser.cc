#include <config.h>

//- local includes
#include <dune/grid/prismgrid/dgfparser.hh>


namespace Dune
{

  namespace dgf
  {

    // Implementation of PrismGridBlock
    // --------------------------------

    PrismGridBlock::PrismGridBlock ( std::istream &in )
    : BasicBlock( in, ID )
    {
      if( !isactive() )
        return;

      // read normal boundaries
      getnextline();
      PrismGridBlock::ctype left, right;
      if( ( getnextentry( left ) && getnextentry( right ) ) )
      {
        normalGridInfo_.left  = left;
        normalGridInfo_.right = right;
      }
      else
        DUNE_THROW( DGFException, "Could not read normal boundaries" );

      // read number of layers
      getnextline();
      int layers;
      if( getnextentry( layers ) )
        normalGridInfo_.layers = layers;
      else
        DUNE_THROW( DGFException, "Could not read number of layers!" );
    }

    const char *PrismGridBlock::ID = "PrismGrid";

  } // end namespace dgf



  // Implementation of ReplaceDGFBlock 
  // ---------------------------------

  void ReplaceDGFBlock::replace( const std::string &id, std::istream &input, 
                                 std::istream &content, std::ostream &modified )
  {
    // initialize 
    const std::string identifier = makeUpperCase( id );
    reset( input );

    // copy until identifier is found
    for( std::string line; input.good(); )
    {
      std::getline( input, line );
      if( found( line, identifier ) )
        break;
      modified << line << std::endl;
    }
    assert( !input.eof() );

    // now, skip block
    for( std::string line; input.good(); )
    {
      getline( input, line );
      if( found( line, "#" ) )
        break;
    }

    // replace with content 
    reset( content );
    for( std::string line; content.good(); )
    {
      std::getline( content, line );
      modified << line << std::endl;
    }

    // copy until end 
    for( std::string line; input.good(); )
    {
      std::getline( input, line );
      modified << line << std::endl;
    }
  }


  std::string ReplaceDGFBlock::makeUpperCase ( const std::string &in )
  {
    std::string out( in );
    for( size_t i = 0; i < out.size(); ++i )
      out[ i ] = toupper( out[ i ] );
    return out;
  }


  bool ReplaceDGFBlock::found ( std::string line, std::string identifier )
  {
    std::istringstream linestream( line );
    std::string id;
    linestream >> id;
    return( makeUpperCase( id ) == identifier );
  }


  void ReplaceDGFBlock::reset ( std::istream &input )
  {
    input.clear();
    input.seekg( 0 );
  }

} // end namespace Dune
