#ifndef DUNE_GRID_PRISMGRID_ENTITYPOINTER_HH
#define DUNE_GRID_PRISMGRID_ENTITYPOINTER_HH

#include <dune/common/typetraits.hh>

#include <dune/grid/common/entity.hh>

#include "entity.hh"

namespace Dune
{

  // Forward declaration
  // -------------------

  template< int, int, class > class PrismEntity;



  // PrismEntityPointerTraits
  // ------------------------

  template< int codim, class GridImp >
  struct PrismEntityPointerTraits
  {
    typedef PrismEntityPointerTraits< codim, GridImp > BaseTraits;

    typedef typename remove_const< GridImp >::type::Traits Traits;

    typedef typename Traits::Grid Grid;
    typedef typename Traits::HostGridView HostGridView;

    static const int dimension = Traits::dimension;
    static const int codimension = codim;
    static const int mydimension = dimension - codimension;

    typedef PrismEntity< codimension, dimension, const Grid > EntityImp;
    typedef Dune::Entity< codimension, dimension, const Grid, PrismEntity > Entity;

    typedef typename HostGridView::template Codim<
        Traits::Map::template Codim< codim >::v
      >::EntityPointer HostEntityPointer;
    typedef HostEntityPointer HostIterator;
  };



  // PrismEntityPointer
  // ------------------

  template< class Traits >
  class PrismEntityPointer
  {
    typedef PrismEntityPointer< Traits > This;

    friend class PrismEntityPointer< typename Traits::BaseTraits >;

  public:
    static const int dimension = Traits::dimension;
    static const int codimension = Traits::codimension;

    typedef typename Traits::Entity Entity;
    typedef typename Traits::EntityImp EntityImp;

    typedef PrismEntityPointer< typename Traits::BaseTraits > EntityPointerImp;

    typedef typename Traits::HostEntityPointer HostEntityPointer;
    typedef typename Traits::HostIterator HostIterator;
    typedef typename Traits::Grid Grid;

  public:
    PrismEntityPointer ( const HostIterator &hostIterator,
                         const Grid &grid )
    : entity_( EntityImp( grid ) ),
      hostIterator_( hostIterator )
    {
      assert( subIndex() == 0 );
      assert( !entity_.impl() );
    }

    PrismEntityPointer ( const HostIterator &hostIterator,
                         int subIndex, const Grid &grid )
    : entity_( EntityImp( *hostIterator, subIndex, grid ) ),
      hostIterator_( hostIterator )
    {}

    PrismEntityPointer ( const EntityImp &entity )
    : entity_( entity ),
      hostIterator_( entity.hostEntity() )
    {
      if( entity_.impl() )
        entity_.impl().reset( *hostIterator_ );
    }

    PrismEntityPointer ( const This &other )
    : entity_( other.entity_.impl() ),
      hostIterator_( other.hostIterator_ )
    {
      if( entity_.impl() )
        entity_.impl().reset( *hostIterator_ );
    }

    template< class T >
    PrismEntityPointer ( const PrismEntityPointer< T > &other )
    : entity_( other.entity_.impl() ),
      hostIterator_( other.hostIterator_ )
    {
      if( entity_.impl() )
        entity_.impl().reset( *hostIterator_ );
    }

    const This &operator= ( const This &other )
    {
      entity_.impl() = other.entity_.impl();
      hostIterator_ = other.hostIterator_;
      if( entity_.impl() )
        entity_.impl().reset( *hostIterator_ );
      return *this;
    }

    template< class T >
    const This &operator= ( const PrismEntityPointer< T > &other )
    {
      entity_.impl() = other.entity_.impl();
      hostIterator_ = other.hostIterator_;
      if( entity_.impl() )
        entity_.impl().reset( *hostIterator_ );
      return *this;
    }

    Entity &dereference () const
    {
      if( !(entity_.impl()) )
        entity_.impl().reset( *hostIterator_, 0 );
      return entity_;
    }

    template< class T >
    bool equals ( const PrismEntityPointer< T > &other ) const
    {
      return ( subIndex() == other.subIndex()
               && hostIterator_ == other.hostIterator_ );
    }

    int level () const { return 0; }

  protected:
    template < class > friend class PrismEntityPointer;

    int subIndex () const { return entity_.impl().subIndex(); }

    int layers () const { return grid().layers(); }

    const Grid &grid () const { return entity_.impl().grid(); }

    mutable Entity entity_;
    HostIterator hostIterator_;
  };

} // namespace Dune

#endif // #ifndef DUNE_GRID_PRISMGRID_ENTITYPOINTER_HH
