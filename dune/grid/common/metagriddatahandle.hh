#ifndef DUNE_METAGRID_DATAHANDLE_HH
#define DUNE_METAGRID_DATAHANDLE_HH

#include <dune/common/typetraits.hh>

#include <dune/grid/common/datahandleif.hh>

#if HAVE_DUNE_ALUGRID
#include <dune/alugrid/common/ldbhandleif.hh>
#endif

namespace Dune
{

  // MetaGridGridEntityProxy
  // -----------------------

  template< class Traits, class HostEntity >
  struct MetaGridEntityProxy
  {
    static const int dimension = HostEntity::dimension;
    static const int codimension = HostEntity::codimension;

    // type of extra data
    typedef typename Traits :: ExtraData ExtraData ;

    // type of entity
    typedef typename Traits :: template Codim< codimension > :: Entity  Entity;

    // type of entity implementation
    typedef typename Entity :: Implementation  EntityImpl;

  public:
    MetaGridEntityProxy( ExtraData data, const HostEntity &hostEntity )
    : entity_( EntityImpl( data, hostEntity ) )
    {}

    Entity &operator* () { return entity_; }
    const Entity &operator* () const { return entity_; }

  private:
    // entity object
    Entity entity_;
  };


  // default filter object not filtering anything
  struct MetaGridNoFilter
  {
    template <class Entity>
    bool contains( const Entity& entity ) const
    {
      return true;
    }
  };


  // MetaGridDataHandleBase
  // ----------------------

#if HAVE_DUNE_ALUGRID
  template< class WrappedHandle, bool = std::is_base_of_v<LoadBalanceHandleWithReserveAndCompress, WrappedHandle> >
  struct MetaGridDataHandleBase;

  template< class WrappedHandle >
  struct MetaGridDataHandleBase< WrappedHandle, false >
    : public LoadBalanceHandleWithReserveAndCompress
  {
    MetaGridDataHandleBase ( WrappedHandle &wrappedHandle ) : wrappedHandle_( wrappedHandle ) {}

    void reserveMemory ( std::size_t newElements ) {}
    void compress () {}

  protected:
    WrappedHandle &wrappedHandle_;
  };

  template< class WrappedHandle >
  struct MetaGridDataHandleBase< WrappedHandle, true >
    : public LoadBalanceHandleWithReserveAndCompress
  {
    MetaGridDataHandleBase ( WrappedHandle &wrappedHandle ) : wrappedHandle_( wrappedHandle ) {}

    void reserveMemory ( std::size_t newElements ) { wrappedHandle_.reserveMemory( newElements ); }
    void compress () { wrappedHandle_.compress(); }

  protected:
    WrappedHandle &wrappedHandle_;
  };
#else // #if HAVE_DUNE_ALUGRID
  template< class WrappedHandle >
  struct MetaGridDataHandleBase< WrappedHandle >
  {
    MetaGridDataHandleBase ( WrappedHandle &wrappedHandle ) : wrappedHandle_( wrappedHandle ) {}

  protected:
    WrappedHandle &wrappedHandle_;
  };
#endif // #else // #if HAVE_DUNE_ALUGRID



  // MetaGridDataHandle
  // ------------------

  template< class WrappedHandle, class GridFamily, class Filter = MetaGridNoFilter >
  class MetaGridDataHandle
    : public MetaGridDataHandleBase< WrappedHandle >,
      public CommDataHandleIF< MetaGridDataHandle< WrappedHandle, GridFamily, Filter >, typename WrappedHandle::DataType >
  {
    typedef MetaGridDataHandleBase< WrappedHandle > Base;

    // type of traits
    typedef typename std::remove_const_t<GridFamily>::Traits Traits;

    // type of extra data
    typedef typename Traits :: ExtraData ExtraData ;

  public:
    // type of data to be communicated
    typedef typename WrappedHandle::DataType DataType;

  private:
    // prohibit copying
    MetaGridDataHandle( const MetaGridDataHandle& );

  public:
    MetaGridDataHandle ( ExtraData data, WrappedHandle &handle )
      : Base( handle ),
        data_( data ),
        filter_( *(new Filter()) ),
        deleteFilter_( true )
    {}

    MetaGridDataHandle ( ExtraData data, WrappedHandle &handle, const Filter& filter )
      : Base( handle ),
        data_( data ),
        filter_( filter ),
        deleteFilter_( false )
    {}

    ~MetaGridDataHandle ()
    {
      if( deleteFilter_ )
        delete &filter_ ;
    }

    bool contains ( int dim, int codim ) const
    {
      return wrappedHandle_.contains( dim, codim );
    }

    bool fixedSize ( int dim, int codim ) const
    {
      return wrappedHandle_.fixedSize( dim, codim );
    }

    template< class HostEntity >
    size_t size ( const HostEntity &hostEntity ) const
    {
      if( filter_.contains( hostEntity ) )
      {
        MetaGridEntityProxy< Traits, HostEntity > proxy( data_, hostEntity );
        return wrappedHandle_.size( *proxy );
      }
      else
        return 0;
    }

    template< class MessageBuffer, class HostEntity >
    void gather ( MessageBuffer &buffer, const HostEntity &hostEntity ) const
    {
      if( filter_.contains( hostEntity ) )
      {
        MetaGridEntityProxy< Traits, HostEntity > proxy( data_, hostEntity );
        wrappedHandle_.gather( buffer, *proxy );
      }
    }

    template< class MessageBuffer, class HostEntity >
    void scatter ( MessageBuffer &buffer, const HostEntity &hostEntity, size_t size )
    {
      if( filter_.contains( hostEntity ) )
      {
        MetaGridEntityProxy< Traits, HostEntity > proxy( data_, hostEntity );
        wrappedHandle_.scatter( buffer, *proxy, size );
      }
      else
      {
        // read whatever has been written to buffer
        DataType tmp;
        for( size_t i = 0; i < size; ++i )
          buffer.read( tmp );
      }
    }

  protected:
    using Base::wrappedHandle_;

    mutable ExtraData data_;
    const Filter& filter_;
    bool deleteFilter_;
  };



  // MetaGridWrappedDofManager
  // -----------------------------

  template< class WrappedDofManager, class GridFamily, class Filter = MetaGridNoFilter >
  class MetaGridWrappedDofManager
  {
    typedef typename std::remove_const_t<GridFamily>::Traits Traits;
    // type of extra data
    typedef typename Traits :: ExtraData ExtraData ;
    // type of host element (i.e. codim = 0)
    typedef typename Traits :: HostGrid :: template Codim< 0 > :: Entity HostElement ;

    // entity proxy
    typedef MetaGridEntityProxy< Traits, HostElement > EntityProxy ;

  private:
    // prohibit copy constructor
    MetaGridWrappedDofManager ( const MetaGridWrappedDofManager& );

  public:
    MetaGridWrappedDofManager ( ExtraData data, WrappedDofManager &handle )
    : data_( data ),
      wrappedHandle_( handle ),
      filter_( *(new Filter()) ),
      deleteFilter_( true )
    {}

    MetaGridWrappedDofManager ( ExtraData data,
                                WrappedDofManager &handle,
                                const Filter& filter )
    : data_( data ),
      wrappedHandle_( handle ),
      filter_( filter ),
      deleteFilter_( false )
    {}

    ~MetaGridWrappedDofManager()
    {
      if( deleteFilter_ )
        delete &filter_;
    }

    template< class MessageBuffer >
    void inlineData ( MessageBuffer &buffer, const HostElement &hostElement )
    {
      // TODO: inline Filter data
      if( filter_.contains( hostElement ) )
      {
        const EntityProxy proxy( data_, hostElement );
        wrappedHandle_.inlineData( buffer, *proxy );
      }
    }

    template< class MessageBuffer >
    void xtractData ( MessageBuffer &buffer, const HostElement &hostElement, size_t newElements )
    {
      // TODO: xtract Filter data
      if( filter_.contains( hostElement ) )
      {
        const EntityProxy  proxy( data_, hostElement );
        wrappedHandle_.xtractData( buffer, *proxy, newElements );
      }
    }

    void compress ()
    {
      wrappedHandle_.compress();
    }

  private:
    mutable ExtraData data_;
    WrappedDofManager &wrappedHandle_;
    const Filter& filter_;
    const bool deleteFilter_;
  };

} // namespace Dune

#endif // #ifndef DUNE_PARALLELGRID_DATAHANDLE_HH
