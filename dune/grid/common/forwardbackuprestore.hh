#ifndef DUNE_FORWARDBACKUPRESTORE_HH
#define DUNE_FORWARDBACKUPRESTORE_HH

#include <fstream>

#include <dune/common/exceptions.hh>
#include <dune/grid/common/backuprestore.hh>

namespace Dune
{

  // ForwardBackupRestoreFacilities
  // ------------------------------

  /** \copydoc Dune::BackupRestoreFacility */
  template< class MetaGrid >
  struct ForwardBackupRestoreFacility
  {
    typedef MetaGrid                   Grid;
    typedef typename Grid :: HostGrid  HostGrid;

    typedef BackupRestoreFacility< HostGrid >  HostBackupRestoreFacility;

    /** \copydoc Dune::BackupRestoreFacility::backup(grid,filename)  */
    static void backup ( const Grid &grid, const std::string &filename )
    {
      HostBackupRestoreFacility::backup( grid.hostGrid(), filename );
    }

    /** \copydoc Dune::BackupRestoreFacility::backup(grid,stream)  */
    static void backup ( const Grid &grid, std::ostream &stream )
    {
      HostBackupRestoreFacility::backup( grid.hostGrid(), stream );
    }

    /** \copydoc Dune::BackupRestoreFacility::restore(filename) */
    static Grid *restore ( const std::string &filename )
    {
      HostGrid* hostGrid = HostBackupRestoreFacility::restore( filename );
      if( !hostGrid )
        DUNE_THROW( InvalidStateException, "HostGrid was not created." );
      return new Grid( hostGrid );
    }

    /** \copydoc Dune::BackupRestoreFacility::restore(stream) */
    static Grid *restore ( std::istream &stream )
    {
      HostGrid* hostGrid = HostBackupRestoreFacility::restore( stream );
      if( !hostGrid )
        DUNE_THROW( InvalidStateException, "HostGrid was not created." );
      return new Grid( hostGrid );
    }
  };

} // namespace Dune

#endif // #ifndef DUNE_FORWARDBACKUPRESTORE_HH
