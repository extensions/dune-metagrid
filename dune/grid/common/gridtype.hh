#ifndef METAGRID_PAPER_GRIDTYPE_HH
#define METAGRID_PAPER_GRIDTYPE_HH

//- C++ includes
#include <string>
#include <sstream>

//- dune-grid includes
#if HAVE_DUNE_ALUGRID
#include <dune/alugrid/common/declaration.hh>
#else // #if HAVE_DUNE_ALUGRID
#include <dune/grid/alugrid/common/declaration.hh>
#endif // #else // #if HAVE_DUNE_ALUGRID
#include <dune/grid/geometrygrid.hh>
#include <dune/grid/geometrygrid/cachedcoordfunction.hh>
#include <dune/grid/utility/gridtype.hh>

#if HAVE_DUNE_GRID_DEV_HOWTO
#include <dune/grid/identitygrid.hh>
#endif // #if HAVE_DUNE_GRID_DEV_HOWTO

#if HAVE_DUNE_SPGRID
#include <dune/grid/spgrid/declaration.hh>
#if DUNE_SPGRID_CARTESIAN
#include <dune/grid/cartesiangrid.hh>
#endif // #if DUNE_SPGRID_CARTESIAN
#endif // #if HAVE_DUNE_SPGRID

#include <dune/grid/cacheitgrid.hh>
#include <dune/grid/filteredgrid.hh>
#include <dune/grid/idgrid.hh>
#include <dune/grid/parallelgrid.hh>

#include <dune/grid/cacheitgrid/dgfparser.hh>
//#include <dune/grid/filteredgrid/dgfparser.hh>
#include <dune/grid/idgrid/dgfparser.hh>
#include <dune/grid/parallelgrid/dgfparser.hh>

#if HAVE_DUNE_PRISMGRID
#include <dune/grid/prismgrid.hh>
#endif // #if HAVE_DUNE_PRISMGRID

#include "metagridtags.hh"



namespace
{

  // GridName
  // --------

  template< class Grid >
  struct GridName;



  // Template specialization for ALUGrid
  // -----------------------------------

  template <int dim, int dimworld, Dune::ALUGridElementType elType, Dune::ALUGridRefinementType refineType, class Comm >
  struct GridName< Dune::ALUGrid< dim, dimworld, elType, refineType, Comm > >
  {
    static std::string str ()
    {
      std::stringstream stream;
      stream << "ALUGrid< " << dim << " >";
      return stream.str();
    }
  };



#if HAVE_DUNE_SPGRID
  // Template specialization for SPGrid
  // ----------------------------------

  template< class ct, int dim, Dune::SPRefinementStrategy strategy, class Comm >
  struct GridName< Dune::SPGrid< ct, dim, strategy, Comm > >
  {
    static std::string str ()
    {
      std::stringstream stream;
      stream << "SPGrid< " << dim << " >";
      return stream.str();
    }
  };
#endif // #if HAVE_DUNE_SPGRID

} // end anonymous namespace



namespace Dune
{

  // MetaGridBuilder
  // ---------------

  template< class BaseGrid, class MetaGridTag >
  struct MetaGridBuilder;



  // Template specialization for GeometryGrid
  // ----------------------------------------

#if HAVE_DUNE_FREIBURG
  template< class BaseGridBuilder >
  class MetaGridBuilder< BaseGridBuilder, geogrid >
  {
    typedef typename BaseGridBuilder::Grid HostGrid;
    typedef Dune::DefaultCoordFunction< HostGrid > CoordFunction;

  public:
    typedef Dune::GeometryGrid< HostGrid, CoordFunction, Dune::SmallObjectAllocator< void > > Grid;

    static std::string gridname ()
    {
      return ( "GeometryGrid< " + BaseGridBuilder::gridname() + " >" );
    }
  };



  template< class BaseGridBuilder >
  struct MetaGridBuilder< BaseGridBuilder, geogrid_cached >
  {
    typedef typename BaseGridBuilder::Grid HostGrid;
    typedef Dune::DefaultCoordFunction< HostGrid > DefaultCoordFunction;
    typedef Dune::CachedCoordFunction< HostGrid, DefaultCoordFunction > CachedCoordFunction;

  public:
    typedef Dune::GeometryGrid< HostGrid, CachedCoordFunction, Dune::SmallObjectAllocator< void > > Grid;

    static std::string gridname ()
    {
      return ( "GeometryGrid< " + BaseGridBuilder::gridname() + " >" );
    }
  };
#endif // #if HAVE_DUNE_FREIBURG



#if HAVE_DUNE_GRID_DEV_HOWTO
  // Template specialization for IdentityGrid
  // ----------------------------------------

  template< class BaseGridBuilder >
  struct MetaGridBuilder< BaseGridBuilder, identitygrid >
  {
    typedef typename BaseGridBuilder::Grid HostGrid;

  public:
    typedef Dune::IdentityGrid< HostGrid > Grid;

    static std::string gridname ()
    {
      return ( "IdentityGrid< " + BaseGridBuilder::gridname() + " >" );
    }
  };
#endif // #if HAVE_DUNE_GRID_DEV_HOWTO



  // Template specialization for CartesianGrid
  // -----------------------------------------

#if HAVE_DUNE_SPGRID
#if DUNE_SPGRID_CARTESIAN
  template< class BaseGridBuilder >
  struct MetaGridBuilder< BaseGridBuilder, cartesiangrid >
  {
    typedef typename BaseGridBuilder::Grid HostGrid;

  public:
    typedef Dune::CartesianGrid< HostGrid > Grid;

    static std::string gridname ()
    {
      return ( "CartesianGrid< " + BaseGridBuilder::gridname() + " >" );
    }
  };
#endif // #if DUNE_SPGRID_CARTESIAN
#endif // #if HAVE_DUNE_SPGRID



  // Template specialization for CacheItGrid
  // ---------------------------------------

  template< class BaseGridBuilder >
  struct MetaGridBuilder< BaseGridBuilder, cacheitgrid >
  {
    typedef typename BaseGridBuilder::Grid HostGrid;

  public:
    typedef Dune::CacheItGrid< HostGrid > Grid;

    static std::string gridname ()
    {
      return ( "CacheItGrid< " + BaseGridBuilder::gridname() + " >" );
    }
  };



  // Template specialization for FilteredGrid
  // ----------------------------------------

  template< class BaseGridBuilder >
  struct MetaGridBuilder< BaseGridBuilder, filteredgrid >
  {
    typedef typename BaseGridBuilder::Grid HostGrid;

  public:
    typedef Dune::FilteredGrid< HostGrid > Grid;

    static std::string gridname ()
    {
      return ( "FilteredGrid< " + BaseGridBuilder::gridname() + " >" );
    }
  };



  // Template specialization for IdGrid
  // ----------------------------------

  template< class BaseGridBuilder >
  struct MetaGridBuilder< BaseGridBuilder, idgrid >
  {
    typedef typename BaseGridBuilder::Grid HostGrid;

  public:
    typedef Dune::IdGrid< HostGrid > Grid;

    static std::string gridname ()
    {
      return ( "IdGrid< " + BaseGridBuilder::gridname() + " >" );
    }
  };


  // Template specialization for ParallelGrid
  // ----------------------------------------

  template< class BaseGridBuilder >
  struct MetaGridBuilder< BaseGridBuilder, parallelgrid >
  {
    typedef typename BaseGridBuilder::Grid HostGrid;

  public:
    typedef Dune::ParallelGrid< HostGrid > Grid;

    static std::string gridname ()
    {
      return ( "ParallelGrid< " + BaseGridBuilder::gridname() + " >" );
    }
  };


#if HAVE_DUNE_PRISMGRID
  // Template specialization for PrismGrid
  // -------------------------------------

  template< class BaseGridBuilder >
  struct MetaGridBuilder< BaseGridBuilder, prismgrid >
  {
    typedef typename BaseGridBuilder::Grid HostGrid;

  public:
    typedef Dune::PrismGrid< HostGrid > Grid;

    static std::string gridname ()
    {
      return ( "PrismGrid< " + BaseGridBuilder::gridname() + " >" );
    }
  };
#endif // #if HAVE_DUNE_PRISMGRID



  // GridBuilder
  // -----------

  template< class BaseGrid, class MetaGridTuple, int pos = Dune::tuple_size< MetaGridTuple >::value >
  struct GridBuilder
  : public MetaGridBuilder< GridBuilder< BaseGrid, MetaGridTuple, pos-1 >, typename Dune::tuple_element< pos-1, MetaGridTuple >::type >
  { };



  template< class BaseGrid, class MetaGridTuple >
  struct GridBuilder< BaseGrid, MetaGridTuple, 0 >
  {
    typedef BaseGrid Grid;

    static std::string gridname ()
    {
      return GridName< BaseGrid >::str();
    }
  };

} // end namespace Dune

#endif // #ifndef METAGRID_PAPER_GRIDTYPE_HH
