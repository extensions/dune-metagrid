#ifndef METAGRID_PAPER_METAGRID_TAGS_HH
#define METAGRID_PAPER_METAGRID_TAGS_HH

#warning "tags"

namespace Dune
{

  // Metagrid Tags
  // -------------

  struct geogrid {};
  struct geogrid_cached {};

#if HAVE_DUNE_GRID_DEV_HOWTO
  struct identitygrid {};
#endif // #if HAVE_DUNE_GRID_DEV_HOWTO

#if HAVE_DUNE_SPGRID
  struct cartesiangrid {};
#endif // #if HAVE_DUNE_SPGRID

  struct cacheitgrid {};
  struct filteredgrid {};
  struct idgrid {};
  struct parallelgrid {};

#if HAVE_DUNE_PRISMGRID
  struct prismgrid {};
#endif // #if HAVE_DUNE_PRISMGRID

} // end namespace Dune

#endif // #ifndef METAGRID_PAPER_METAGRID_TAGS_HH
