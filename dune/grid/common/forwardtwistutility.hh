#ifndef DUNE_FORWARDTWISTUTILITY_HH
#define DUNE_FORWARDTWISTUTILITY_HH

//- C++ includes
#include <cassert>

//- dune-metagrid includes
#include <dune/grid/idgrid/declaration.hh>

#if HAVE_DUNE_FEM
//- dune-fem includes
#include <dune/fem/quadrature/caching/twistutility.hh>

namespace Dune
{

  namespace Fem
  {

    // Specialization for IdGrid
    // -------------------------

    template< class MetaGrid >
    struct ForwardTwistUtility
    {
      typedef MetaGrid Grid;
      typedef Grid     GridType;
      typedef typename Grid :: HostGrid  HostGrid;

      typedef typename GridType::Traits::LeafIntersectionIterator  LeafIntersectionIterator;
      typedef typename LeafIntersectionIterator::Intersection  LeafIntersection;
      typedef typename GridType::Traits::LevelIntersectionIterator LevelIntersectionIterator;
      typedef typename LevelIntersectionIterator::Intersection LevelIntersection;

      static const int dimension = GridType::dimension;

      typedef TwistUtility< HostGrid > HostTwistUtilityType;

    public:
      template< class Intersection >
      static int twistInSelf ( const GridType &grid, const Intersection &it )
      {
        return HostTwistUtilityType::twistInSelf( grid.hostGrid(), it.impl().hostIntersection() );
      }

      template< class Intersection >
      static int twistInNeighbor ( const GridType &grid, const Intersection &it )
      {
        return HostTwistUtilityType::twistInNeighbor( grid.hostGrid(), it.impl().hostIntersection() );
      }

      template< class Intersection >
      static inline GeometryType
      elementGeometry ( const Intersection &intersection, const bool inside)
      {
        return HostTwistUtilityType::elementGeometry( intersection.impl().hostIntersection(), inside );
      }
    };

  } // namespace Fem

} // namespace Dune

#endif // #if HAVE_DUNE_FEM

#endif // #ifndef DUNE_FORWARDTWISTUTILITY_HH
