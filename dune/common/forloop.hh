// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_COMMON_FORLOOP_HH
#define DUNE_COMMON_FORLOOP_HH

#include <utility>

#include <dune/common/hybridutilities.hh>

/** \file
 * \brief A static for loop for template meta-programming
 */

namespace Dune
{

  template< template< int > class Operation, int first, int last >
  struct ForLoop
  {
    static_assert( (first <= last), "ForLoop: first > last" );

    template<typename... Args>
    static void apply(Args&&... args)
    {
      Hybrid::forEach(std::make_index_sequence<last+1-first>{},
        [&](auto i){Operation<i+first>::apply(args...);});
    }
  };

} // end namespace Dune

#endif
