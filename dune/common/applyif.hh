#ifndef DUNE_APPLYIF_HH
#define DUNE_APPLYIF_HH

#include <type_traits>

namespace Dune
{
  // DoNothing
  struct DoNothing
  {
    template< typename ...Args >
    static void apply ( Args&... args )
    {}
  };

  // ApplyIf
  template< template< int > class Op, template< int > class Cond >
  struct ApplyIf
  {
    template< int i >
    struct Operation
    : public std::conditional< Cond< i >::value, Op< i >, DoNothing >::type
    {};
  };

} // namespace Dune

#endif // #ifndef DUNE_APPLYIF_HH
