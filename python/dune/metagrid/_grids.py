from __future__ import absolute_import, division, print_function, unicode_literals

import sys
import logging
logger = logging.getLogger(__name__)

from dune.common.checkconfiguration import assertHave, ConfigurationError

try:
    assertHave("HAVE_DUNE_METAGRID")

    def sphereGrid(constructor, dimgrid=2, dimworld=3, elementType="Dune::simplex", comm=None, serial=False, **parameters):
        from dune.grid.grid_generator import module, getDimgrid

        if not dimgrid:
            dimgrid = getDimgrid(constructor)

        if elementType=="Dune::cube":
            raise KeyError("Parameter cube not supported yet!")

        if dimworld is None:
            dimworld = dimgrid
        if elementType is None:
            elementType = parameters.pop("type")
        refinement = parameters["refinement"]

        if refinement == "conforming":
            refinement="Dune::conforming"
        elif refinement == "nonconforming":
            refinement="Dune::nonconforming"

        #if not (2 == dimgrid and dimworld == 3)
        #    raise KeyError("Parameter error in ALUGrid with dimgrid=" + str(dimgrid) + " should be 2, " + str(diworld) + " should be 3!")
        if refinement=="Dune::conforming" and elementType=="Dune::cube":
            raise KeyError("Parameter error in ALUGrid with refinement=" + refinement + " and type=" + elementType + ": conforming refinement is only available with simplex element type")

        # todo add serial
        hostGrid = "Dune::ALUGrid< " + str(dimgrid) + ", " + str(dimworld) + ", " + elementType + ", " + refinement + ">"
        typeName = "Dune::SphereGrid< " + hostGrid + " >"
        includes = ["dune/alugrid/grid.hh", "dune/alugrid/dgf.hh",
                    "dune/grid/spheregrid.hh"]
        gridModule = module(includes, typeName)

        if comm is not None:
            raise Exception("Passing communicator to grid construction is not yet implemented in Python bindings of dune-grid")
            return gridModule.LeafGrid(gridModule.reader(constructor, comm))
        else:
            return gridModule.LeafGrid(gridModule.reader(constructor))


    def sphereConformGrid(constructor, dimgrid=None, dimworld=None, comm=None, serial=False, **parameters):
        return sphereGrid(constructor, dimgrid, dimworld, elementType="Dune::simplex", refinement="Dune::conforming", comm=comm, serial=serial)


    def sphereCubeGrid(constructor, dimgrid=None, dimworld=None, comm=None, serial=False, **parameters):
        return sphereGrid(constructor, dimgrid, dimworld, elementType="Dune::cube", refinement="Dune::nonconforming", comm=comm, serial=serial)


    def sphereSimplexGrid(constructor, dimgrid=None, dimworld=None, comm=None, serial=False, **parameters):
        return sphereGrid(constructor, dimgrid, dimworld, elementType="Dune::simplex", refinement="Dune::nonconforming", comm=comm, serial=serial)

    grid_registry = {
            "SphereConform" : sphereConformGrid,
            #"ALUCube" :    aluCubeGrid,
            "SphereSimplex" : sphereSimplexGrid,
        }
except ConfigurationError:
    grid_registry = {}
    pass

if __name__ == "__main__":
    import doctest
    doctest.testmod(optionflags=doctest.ELLIPSIS)
